﻿using Accounting.Web.Models.DBModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models
{

    public class CDbcontext : DbContext
    {
        public CDbcontext(DbContextOptions<CDbcontext> options) : base(options)
        {

        }

        public CDbcontext()
        {
            TransactionEntryModel transaction = new TransactionEntryModel();
        }

        protected override void OnModelCreating(ModelBuilder objBuilder)
        {
            objBuilder.ApplyConfiguration(new UserModelConfiguration());
            objBuilder.ApplyConfiguration(new RoleModelConfiguration());
            objBuilder.ApplyConfiguration(new EntityLevelConfigration());
            objBuilder.ApplyConfiguration(new EntityMasterModelConfiguration());
            objBuilder.ApplyConfiguration(new MajorCategoryModelConfiguration()); 
            objBuilder.ApplyConfiguration(new CategoryMasterModelConfiguration());
            objBuilder.ApplyConfiguration(new LedgerGroupMasterModelConfiguration());
            objBuilder.ApplyConfiguration(new GroupTypeModelConfiguration());
            objBuilder.ApplyConfiguration(new ChartofAccountsModelConfiguration());
            objBuilder.ApplyConfiguration(new AccountTypeModelConfiguration());
            objBuilder.ApplyConfiguration(new CostCentreModelConfiguration());
            objBuilder.ApplyConfiguration(new AnalyticsModelConfiguration());
            objBuilder.ApplyConfiguration(new UDcategoryMasterConfiguration());
            objBuilder.ApplyConfiguration(new UDoptionsModelConfiguration());
            objBuilder.ApplyConfiguration(new ProfitCentreModelConfiguration());
            objBuilder.ApplyConfiguration(new LocationModelConfiguration());
            objBuilder.ApplyConfiguration(new UnitModelConfiguration());
            objBuilder.ApplyConfiguration(new ProjectModelConfiguration());
            objBuilder.ApplyConfiguration(new LegalEntityProfitCentreMappingModelConfiguration());
            objBuilder.ApplyConfiguration(new LegalEntityLocationMappingModelConfiguration());
            objBuilder.ApplyConfiguration(new LegalEntityUnitMappingModelConfiguration());
            objBuilder.ApplyConfiguration(new LegalEntityProjectMappingModelConfiguration());
            objBuilder.ApplyConfiguration(new TransactionHeadModelConfiguration());
            objBuilder.ApplyConfiguration(new TransactionEntryModelConfiguration());
            objBuilder.ApplyConfiguration(new FinancialYearModelConfiguration());
            objBuilder.ApplyConfiguration(new OpeningBalanceModelConfiguration());
            objBuilder.ApplyConfiguration(new CaseManagementModelConfigration());
            objBuilder.ApplyConfiguration(new ProductManagementModelConfigration());
            objBuilder.ApplyConfiguration(new ProductManagement_TatDefinationConfigration());
            objBuilder.ApplyConfiguration(new TransactionInputTypeConfiguration());
            objBuilder.ApplyConfiguration(new TransactionInputTypeConfiguration1());
            objBuilder.ApplyConfiguration(new TransactionInputTypeConfiguration2());

        }

        public virtual DbSet<UserModel> AccountingUsers { get; set; }
        public virtual DbSet<RoleModel> AccountingRoles { get; set; }

        public virtual DbSet<EntityMasterModel> AccountingEntityMasterModel { get; set; }
        public virtual DbSet<EntityLevelModel> AccountingEntityLevel { get; set; }
        public virtual DbSet<CategoryMasterModel> AccountingCategoryMasterModel { get; set; }
        public virtual DbSet<UDcategoryMaster> AccountingUDCategoryMasterModel { get; set; }
        public virtual DbSet<UDoptionsModel> AccountingUDOptionsMasterModel { get; set; }
        public virtual DbSet<MajorCategoryModel> AccountingMajorCategoryModel { get; set; }
        public virtual DbSet<LedgerGroupMasterModel> AccountingLedgerGroupMasterModel { get; set; }
        public virtual DbSet<GroupTypeModel> AccountingGroupTypeModel { get; set; }
        public virtual DbSet<ChartofAccountsModel> AccountingChartofAccountsModel { get; set; }
        public virtual DbSet<AccountTypeModel> AccountingAccountTypeModel { get; set; }
        public virtual DbSet<CostCentreModel> AccountingCostCentreModel { get; set; }
        public virtual DbSet<AnalyticsModel> AccountingAnalyticsModel { get; set; }
        public virtual DbSet<ProfitCentreModel> AccountingProfitCentreModel { get; set; }
        public virtual DbSet<LocationModel> AccountingLocationModel { get; set; }
        public virtual DbSet<UnitModel> AccountingUnitModel { get; set; }
        public virtual DbSet<ProjectModel> AccountingProjectModel { get; set; }
        public virtual DbSet<LegalEntityProfitCentreMappingModel> AccountingLegalEntityProfitCentreMappingModel { get; set; }

        public virtual DbSet<LegalEntityLocationMappingModel> AccountingLegalEntityLocationMappingModel { get; set; }
        public virtual DbSet<LegalEntityUnitMappingModel> AccountingLegalEntityUnitMappingModel { get; set; }
        public virtual DbSet<LegalEntityProjectMappingModel> AccountingLegalEntityProjectMappingModel { get; set; }

        public virtual DbSet<TransactionHeadModel> AccountingTransactionHeadModel { get; set; }
        public virtual DbSet<TransactionEntryModel> AccountingTransactionEntryModel { get; set; }
        public virtual DbSet<TransactionEntryModel> AccountingTransactionEntryModel1 { get; set; }
        public virtual DbSet<TransactionEntryModel> AccountingTransactionEntryModel2 { get; set; }
        public virtual DbSet<FinancialYearModel> AccountingFinancialYearModel { get; set; }
        public virtual DbSet<OpeningBalanceModel> AccountingOpeningBalanceModel { get; set; }
        public virtual DbSet<CaseManagementModel> AccountingCaseManagement { get; set; }
        public virtual DbSet<ProductManagementModel> AccountingProductManagement { get; set; }
        public virtual DbSet<ProductManagement_TatDefination> AccountingProductMagtTatDef { get; set; }
       public virtual DbSet<TransactionInputType> AccountingTransactionInputTypeModel { get; set; }
        public virtual DbSet<TransactioninputTypeCash> AccountingTransactionInputTypeModel2 { get; set; }
        public virtual DbSet<CashTransactionInputType> AccountingTransactionInputTypeModel1 { get; set; }

    }

}
