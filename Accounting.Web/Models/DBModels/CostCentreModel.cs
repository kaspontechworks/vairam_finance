﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class CostCentreModel
    {
        public int CostCentreID { get; set; }
      
        [Required(ErrorMessage = "Please enter Cost Centre name!")]
        [MaxLength(200)]
        public string CostCentreName { get; set; }
        [Required(ErrorMessage = "Please select Legal Entity!")]
        public int LegalEntityID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class CostCentreModelConfiguration : IEntityTypeConfiguration<CostCentreModel>
    {
        public void Configure(EntityTypeBuilder<CostCentreModel> builder)
        {
            builder.ToTable("Accounting_CostCentre");
            builder.HasKey(o => o.CostCentreID);
        }
    }
}
