﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class FinancialYearModel
    {
        public int FinancialYearID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class FinancialYearModelConfiguration : IEntityTypeConfiguration<FinancialYearModel>
    {
        public void Configure(EntityTypeBuilder<FinancialYearModel> builder)
        {
            builder.ToTable("Accounting_FinancialYear");
            builder.HasKey(o => o.FinancialYearID);
        }
    }
}
