﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class ProfitCentreModel
    {
        public int ProfitCentreID { get; set; }
      
        [Required(ErrorMessage = "Please enter Profit Centre name!")]
        [MaxLength(200)]
        public string ProfitCentreName { get; set; }
        //[Required(ErrorMessage = "Please select Legal Entity!")]
        // public int LegalEntityID { get; set; }
        public string ProfitCentreslcode { get; set; }
        public string Address { get; set; }
        public string TANNO { get; set; }
        public string CINNO { get; set; }
        public string PANNO { get; set; }
        public string GSTNO { get; set; }
        public string EXTRAFIELD { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
        public int Pastdate_status { get; set; }
    }
    public class ProfitCentreModelConfiguration : IEntityTypeConfiguration<ProfitCentreModel>
    {
        public void Configure(EntityTypeBuilder<ProfitCentreModel> builder)
        {
            builder.ToTable("Accounting_ProfitCentre");
            builder.HasKey(o => o.ProfitCentreID);
        }
    }
}
