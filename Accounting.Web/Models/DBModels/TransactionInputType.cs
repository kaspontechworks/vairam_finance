﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class TransactionInputType
    {
        public int TransactionInputTypeID { get; set; }

        [Required(ErrorMessage = "Please enter InputType name!")]
        public string TransactionInputTypeName { get; set; }
        //[Required(ErrorMessage = "Please enter account code!")]
      
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }

    }
    public class TransactionInputTypeConfiguration : IEntityTypeConfiguration<TransactionInputType>
    {
        public void Configure(EntityTypeBuilder<TransactionInputType> builder)
        {
            builder.ToTable("Accounting_TransactionInputType");
            builder.HasKey(o => o.TransactionInputTypeID);
        }
    }
}
