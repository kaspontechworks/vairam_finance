﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class ProductManagementModel
    {
        [Required(ErrorMessage = "Please enter Product ID!")]
        [MaxLength(4)]
        public int ProductID { get; set; }
        public string ProductUniqueID { get; set; }
        [Required(ErrorMessage = "Please enter product name!")]
        [MaxLength(250)]
        public string ProductName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }
    public class ProductManagement_TatDefination
    {
        
        public int TATDefinationID { get; set; }
        [Required(ErrorMessage = "Please enter Product ID!")]
        [MaxLength(4)]
        public int ProductID { get; set; }
        [Required(ErrorMessage = "Please enter Product ID!")]
        public int CaseManagmentID { get; set; }
        [Required(ErrorMessage = "Please enter Duration!")]
        public int Duration { get; set; }
        public bool IsDaysHours { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }

    public class ProductManagementModelConfigration : IEntityTypeConfiguration<ProductManagementModel>
    {
        public void Configure(EntityTypeBuilder<ProductManagementModel> builder)
        {
            builder.ToTable("Accounting_ProductManagement");
            builder.HasKey(o => o.ProductID);
        }
    }
    public class ProductManagement_TatDefinationConfigration : IEntityTypeConfiguration<ProductManagement_TatDefination>
    {
        public void Configure(EntityTypeBuilder<ProductManagement_TatDefination> builder)
        {
            builder.ToTable("Accounting_ProductManagement_TATDefination");
            builder.HasKey(o => o.TATDefinationID);
        }
    }
}
