﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class UserModel
    {
        public int UserID { get; set; }
        [Required(ErrorMessage = "Please enter last name!")]
        [MaxLength(100)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please enter first name!")]
        [MaxLength(100)]
        public string FirstName { get; set; }
        //[Required(ErrorMessage = "Please enter list name!")]
        //[MaxLength(200)]
        //public string ListName { get; set; }
        //[Required(ErrorMessage = "Please select role!")]
        public int RoleID { get; set; }
        //[Required(ErrorMessage = "Please enter user name!")]
        //[MaxLength(15)]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please enter password!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter email address!")]
        [MaxLength(150)]
        public string EmailAddress { get; set; }
        //[MaxLength(15)]
        //public int? Workforce_ID { get; set; }
        //public string PhoneNo { get; set; }

        public int CorporateGroupID { get; set; }
        public int BusinessGroupID { get; set; }
        public int VerticalGroupID { get; set; }
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
        public int UnitID { get; set; }
        public int ProjectID { get; set; }
        public bool? IsAccess { get; set; }
        public bool? IsActive { get; set; }
        //public bool? IsLoggedInFirst { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public string Address { get; set; }
       // public string profile_pic { get; set; }
    }
    public class UserModelConfiguration : IEntityTypeConfiguration<UserModel>
    {
        public void Configure(EntityTypeBuilder<UserModel> builder)
        {
            builder.ToTable("Accounting_Config_Users");
            builder.HasKey(o => o.UserID);
        }
    }

    public class RoleModel
    {
        public int RoleID { get; set; }
        [Required(ErrorMessage = "Please enter role name!")]
        [MaxLength(200)]
        public string RoleName { get; set; }
        public int? LandingMenu { get; set; }
        public int? IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int LastModifiedBy { get; set; }
    }

    public class RoleModelConfiguration : IEntityTypeConfiguration<RoleModel>
    {
        public void Configure(EntityTypeBuilder<RoleModel> builder)
        {
            builder.ToTable("Accounting_Config_Roles");
            builder.HasKey(o => o.RoleID);
        }
    }
}
