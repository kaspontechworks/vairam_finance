﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class UDcategoryMaster
    {
        [Key]
        public int UDCategoryID { get; set; }

        [Required(ErrorMessage = "Please enter Category name!")]
        [MaxLength(200)]
        public string UDCategoryName { get; set; }
        [Required(ErrorMessage = "Please select Parent Category!")]
        public bool IsEditable { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class UDcategoryMasterConfiguration : IEntityTypeConfiguration<UDcategoryMaster>
    {
        public void Configure(EntityTypeBuilder<UDcategoryMaster> builder)
        {
            builder.ToTable("Accounting_UDCategory");
            builder.HasKey(o => o.UDCategoryID );
        }
    }
    public class UDoptionsModel
    {
        [Key]
        public int UDID { get; set; }
        public int UDCategoryID { get; set; }

        public string UDDescription { get; set; }
        //[Required(ErrorMessage = "Please select Legal Entity!")]
        // public int LegalEntityID { get; set; }
        public string UDCode { get; set; }
        public string ColorCode { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class UDoptionsModelConfiguration : IEntityTypeConfiguration<UDoptionsModel>
    {
        public void Configure(EntityTypeBuilder<UDoptionsModel> builder)
        {
            builder.ToTable("Accounting_UDOptions");
            builder.HasKey(o => o.UDID);
        }
    }
}
