﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class ChartofAccountsModel
    {
        public int AccountID { get; set; }
      
        [Required(ErrorMessage = "Please enter account name!")]
        [MaxLength(200)]
        public string AccountName { get; set; }
        //[Required(ErrorMessage = "Please enter account code!")]
        [MaxLength(200)]
        public string AccountCode { get; set; }
        [Required(ErrorMessage = "Please select account type!")]
        public int AccountTypeID { get; set; }

        [Required(ErrorMessage = "Please select legal entity!")]
        public int LegalEntityID { get; set; }
        [Required(ErrorMessage = "Please select category type!")]
        public int ParentCategoryID { get; set; }
        [Required(ErrorMessage = "Please select category!")]
        public int CategoryID { get; set; }

        [Required(ErrorMessage = "Please select major group!")]
        public int MajorGroupID { get; set; }
        [Required(ErrorMessage = "Please select group!")]
        public int GroupID { get; set; }
        [Required(ErrorMessage = "Please select sub group!")]
        public int SubGroupID { get; set; }
        [Required(ErrorMessage = "Please select cost center!")]
        public bool IsCostcenter { get; set; }
        [Required(ErrorMessage = "Please select budgetary!")]
        public bool IsBudgetary { get; set; }
        [Required(ErrorMessage = "Please select analytics!")]
        public bool IsAnalytics { get; set; }
        [Required(ErrorMessage = "Please select statutory!")]
        public bool IsStatutory { get; set; }
        [Required(ErrorMessage = "Please select party!")]
        public bool IsParty { get; set; }

        //[Required(ErrorMessage = "Please enter Description!")]
        [MaxLength(250)]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }

    }
    public class ChartofAccountsModelConfiguration : IEntityTypeConfiguration<ChartofAccountsModel>
    {
        public void Configure(EntityTypeBuilder<ChartofAccountsModel> builder)
        {
            builder.ToTable("Accounting_ChartofAccounts");
            builder.HasKey(o => o.AccountID);
        }
    }
    public class AccountTypeModel
    {
        public int AccountTypeID { get; set; }

        [Required(ErrorMessage = "Please enter Type name!")]
        [MaxLength(200)]
        public string AccountTypeName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class AccountTypeModelConfiguration : IEntityTypeConfiguration<AccountTypeModel>
    {
        public void Configure(EntityTypeBuilder<AccountTypeModel> builder)
        {
            builder.ToTable("Accounting_AccountType");
            builder.HasKey(o => o.AccountTypeID);
        }
    }
}
