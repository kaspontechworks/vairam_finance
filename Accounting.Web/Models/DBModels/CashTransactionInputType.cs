﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class CashTransactionInputType
    {
        public int TransactionInputTypeID { get; set; }

        [Required(ErrorMessage = "Please enter InputType name!")]
        public string TransactionInputTypeName { get; set; }
        //[Required(ErrorMessage = "Please enter account code!")]

       // [Required(ErrorMessage = "Please select legal entity!")]
        //public int LegalEntityID { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }

    public class TransactionInputTypeConfiguration1 : IEntityTypeConfiguration<CashTransactionInputType>
    {
        public void Configure(EntityTypeBuilder<CashTransactionInputType> builder)
        {
            builder.ToTable("Accounting_TransactionInputTypeCash");
            builder.HasKey(o => o.TransactionInputTypeID);
        }
    }

}
