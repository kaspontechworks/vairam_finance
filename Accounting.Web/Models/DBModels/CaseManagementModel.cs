﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class CaseManagementModel
    {
        public int CaseID { get; set; }
        [Required(ErrorMessage = "Please enter case ID!")]
        [MaxLength(2)]
        public string CaseUniqueID { get; set; }
        [Required(ErrorMessage = "Please enter case name!")]
        [MaxLength(250)]
        public string CaseName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }
    public class CaseManagementModelConfigration : IEntityTypeConfiguration<CaseManagementModel>
    {
        public void Configure(EntityTypeBuilder<CaseManagementModel> builder)
        {
            builder.ToTable("Accounting_CaseManagement");
            builder.HasKey(o => o.CaseID);
        }
    }
}
