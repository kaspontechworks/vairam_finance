﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class OpeningBalanceModel
    {
        [Key]
        public int OpeningBalanceID { get; set; }
        public int FinancialYearID { get; set; }

        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public int AccountID { get; set; }
        public int AccountTypeID { get; set; }
        public int LegalEntityID { get; set; }
        public int ParentCategoryID { get; set; }
        public int CategoryID { get; set; }
        public int MajorGroupID { get; set; }
        public int GroupID { get; set; }
        public int SubGroupID { get; set; }
        public string Description { get; set; }
       
        public bool? IsActive { get; set; }
       
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public int ProfitCentreID { get; set; }

    }
    public class OpeningBalanceModelConfiguration : IEntityTypeConfiguration<OpeningBalanceModel>
    {
        public void Configure(EntityTypeBuilder<OpeningBalanceModel> builder)
        {
            builder.ToTable("Accounting_OpeningBalance");
            builder.HasKey(o => o.OpeningBalanceID);
        }
    }

}
