﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class AnalyticsModel
    {
        public int AnalyticsID { get; set; }
      
        [Required(ErrorMessage = "Please enter Analytics name!")]
        [MaxLength(200)]
        public string AnalyticsName { get; set; }
        [Required(ErrorMessage = "Please enter Analytics Code!")]
        [MaxLength(200)]
        public string AnalyticsCode { get; set; }
        [Required(ErrorMessage = "Please select Legal Entity!")]
        public int LegalEntityID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class AnalyticsModelConfiguration : IEntityTypeConfiguration<AnalyticsModel>
    {
        public void Configure(EntityTypeBuilder<AnalyticsModel> builder)
        {
            builder.ToTable("Accounting_Analytics");
            builder.HasKey(o => o.AnalyticsID);
        }
    }
}
