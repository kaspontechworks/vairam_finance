﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class LedgerGroupMasterModel
    {
        public int LedgerGroupID { get; set; }
      
        [Required(ErrorMessage = "Please enter Group name!")]
        [MaxLength(200)]
        public string LedgerGroupName { get; set; }
        [Required(ErrorMessage = "Please select Parent Group!")]
        public int ParentLedgerGroupID { get; set; }
        public int GroupTypeID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class LedgerGroupMasterModelConfiguration : IEntityTypeConfiguration<LedgerGroupMasterModel>
    {
        public void Configure(EntityTypeBuilder<LedgerGroupMasterModel> builder)
        {
            builder.ToTable("Accounting_LedgerGroupMaster");
            builder.HasKey(o => o.LedgerGroupID);
        }
    }
    public class GroupTypeModel
    {
        public int GroupTypeID { get; set; }

        [Required(ErrorMessage = "Please enter Type name!")]
        [MaxLength(200)]
        public string GroupTypeName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class GroupTypeModelConfiguration : IEntityTypeConfiguration<GroupTypeModel>
    {
        public void Configure(EntityTypeBuilder<GroupTypeModel> builder)
        {
            builder.ToTable("Accounting_GroupType");
            builder.HasKey(o => o.GroupTypeID);
        }
    }
}
