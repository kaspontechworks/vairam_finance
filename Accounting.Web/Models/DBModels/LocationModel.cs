﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class LocationModel
    {
        public int LocationID { get; set; }
      
        [Required(ErrorMessage = "Please enter Location name!")]
        [MaxLength(200)]
        public string LocationName { get; set; }
       // [Required(ErrorMessage = "Please select Legal Entity!")]
        // public int LegalEntityID { get; set; }
        public string Locationslcode { get; set; }
        public string Address { get; set; }
        public string TANNO { get; set; }
        public string CINNO { get; set; }
        public string PANNO { get; set; }
        public string GSTNO { get; set; }
        public string EXTRAFIELD { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class LocationModelConfiguration : IEntityTypeConfiguration<LocationModel>
    {
        public void Configure(EntityTypeBuilder<LocationModel> builder)
        {
            builder.ToTable("Accounting_Location");
            builder.HasKey(o => o.LocationID);
        }
    }
}
