﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class LegalEntityLocationMappingModel
    {
        public int MapID { get; set; }
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class LegalEntityLocationMappingModelConfiguration : IEntityTypeConfiguration<LegalEntityLocationMappingModel>
    {
        public void Configure(EntityTypeBuilder<LegalEntityLocationMappingModel> builder)
        {
            builder.ToTable("Accounting_LegalEntityLocationMapping");
            builder.HasKey(o => new { o.MapID });
            //builder.HasKey(o => new { o.LegalEntityID,o.LocationID});
        }
    }
}
