﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class CategoryMasterModel
    {
        public int CategoryID { get; set; }
      
        [Required(ErrorMessage = "Please enter Category name!")]
        [MaxLength(200)]
        public string CategoryName { get; set; }
        [Required(ErrorMessage = "Please select Parent Category!")]
        public int ParentCategoryID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class CategoryMasterModelConfiguration : IEntityTypeConfiguration<CategoryMasterModel>
    {
        public void Configure(EntityTypeBuilder<CategoryMasterModel> builder)
        {
            builder.ToTable("Accounting_CategoryMaster");
            builder.HasKey(o => o.CategoryID);
        }
    }
}
