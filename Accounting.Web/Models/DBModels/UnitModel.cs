﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class UnitModel
    {
        public int UnitID { get; set; }
      
        [Required(ErrorMessage = "Please enter Unit name!")]
        [MaxLength(200)]
        public string UnitName { get; set; }
        //[Required(ErrorMessage = "Please select Legal Entity!")]
        // public int LegalEntityID { get; set; }
        public string Unitslcode { get; set; }
        public string Address { get; set; }
        public string TANNO { get; set; }
        public string CINNO { get; set; }
        public string PANNO { get; set; }
        public string GSTNO { get; set; }
        public string EXTRAFIELD { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class UnitModelConfiguration : IEntityTypeConfiguration<UnitModel>
    {
        public void Configure(EntityTypeBuilder<UnitModel> builder)
        {
            builder.ToTable("Accounting_Unit");
            builder.HasKey(o => o.UnitID);
        }
    }
}
