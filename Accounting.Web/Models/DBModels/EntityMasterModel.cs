﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class EntityMasterModel
    {
        public int EntityID { get; set; }
        public int EntityLevel { get; set; }
        public int ParentEntityID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string StatutoryInfo { get; set; }
        public string TANNO {get; set;}
        public string CINNO {get; set;}
        public string PANNO {get; set;}
        public string GSTNO { get; set; }
        public string EXTRAFIELD { get; set; }
        public string LegalEntityslcode { get; set; }
        public Boolean IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int? LastModifiedBy { get; set; }

    }

    public class EntityMasterModelConfiguration : IEntityTypeConfiguration<EntityMasterModel>
    {
        public void Configure(EntityTypeBuilder<EntityMasterModel> builder)
        {
            builder.ToTable("Accounting_EntityMaster");
            builder.HasKey(o => o.EntityID);
        }
    }
}
