﻿using Accounting.Web.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class TransactionEntryModel
    {

        public int TransactionEntryID { get; set; }
        public int Receiptdetailid { get; set; }
        //[Required(ErrorMessage = "Please enter Transaction Date!")]
        public DateTime TransactionEntryDate { get; set; }
        public int FinancialYearID { get; set; }
        public int TransactionHeadID { get; set; }
        public string TransactionRefNo { get; set; }
        public string TransactionShortRefNo { get; set; }
        public int TransactionInputTypeID { get; set; }
        //public string TotalAmount { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal OpenDebit { get; set; }
        public decimal OpenCredit { get; set; }
        public decimal CloseDebit { get; set; }
        public decimal CloseCredit { get; set; }
        //[Required(ErrorMessage = "Please enter Description !")]
        public string Description { get; set; }
        //[Required(ErrorMessage = "Please select account !")]
        public int AccountID { get; set; }
        public int AccountTypeID { get; set; }
        public int ParentCategoryID { get; set; }
        public int CategoryID { get; set; }
        public int MajorGroupID { get; set; }
        public int GroupID { get; set; }
        public int SubGroupID { get; set; }

        public int CorporateGroupID { get; set; }
        public int BusinessGroupID { get; set; }
        public int VerticalGroupID { get; set; }
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
        public int UnitID { get; set; }
        public int ProjectID { get; set; }
        public int RowNo { get; set; }
        public int RowType { get; set; }
        public int CashorBankAccountNo { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
        public int? IsStatus { get; set; }
    }
    public class TransactionEntryModelConfiguration : IEntityTypeConfiguration<TransactionEntryModel>
    {
        public void Configure(EntityTypeBuilder<TransactionEntryModel> builder)
        {
            builder.ToTable("Accounting_TransactionEntry");
            builder.HasKey(o => o.TransactionEntryID);
        }
    }
}
