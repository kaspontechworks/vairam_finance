﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class MajorCategoryModel
    {
        [Key]
        public int MajorCategoryID { get; set; }

        [Required(ErrorMessage = "Please enter Category Name!")]
        [MaxLength(100)]
        public string MajorCategoryName { get; set; }
       
        public bool? IsActive { get; set; }
       
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int LastModifiedBy { get; set; }
       
    }
    public class MajorCategoryModelConfiguration : IEntityTypeConfiguration<MajorCategoryModel>
    {
        public void Configure(EntityTypeBuilder<MajorCategoryModel> builder)
        {
            builder.ToTable("Accounting_MajorCategory");
            builder.HasKey(o => o.MajorCategoryID);
        }
    }

}
