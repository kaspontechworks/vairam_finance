﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class EntityLevelModel
    {
        [Required(ErrorMessage = "Please enter ID!")]
        [MaxLength(2)]
        public int EntityLevelID { get; set; }
        [Required(ErrorMessage = "Please enter case name!")]
        [MaxLength(250)]
        public string EntityLevelName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }
    public class EntityLevelConfigration : IEntityTypeConfiguration<EntityLevelModel>
    {
        public void Configure(EntityTypeBuilder<EntityLevelModel> builder)
        {
            builder.ToTable("Accounting_EntityLevel");
            builder.HasKey(o => o.EntityLevelID);
        }
    }
}
