﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{

    public class TransactionHeadModel
    {

        public int TransactionHeadID { get; set; }
        public int ReceiptDetailid { get; set; }
        //[Required(ErrorMessage = "Please enter Transaction Date!")]
        public DateTime TransactionDate { get; set; }
        public string TransactionRefNo { get; set; }
        public string TransactionShortRefNo { get; set; }
        public string LegalEntityslcode { get; set; }
        public string ProfitCentreslcode { get; set; }
        public string Locationslcode { get; set; }
        public string Unitslcode { get; set; }
        public string Projectslcode { get; set; }
        public string TransactionInputTypeslcode { get; set; }
        public int SlNo { get; set; }
        public int FinancialYearID { get; set; }
        public int TransactionInputTypeID { get; set; }
        public int CorporateGroupID { get; set; }
        public int BusinessGroupID { get; set; }
        public int VerticalGroupID { get; set; }
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
        public int UnitID { get; set; }
        public int ProjectID { get; set; }
        public decimal TransactionTotal { get; set; }
        public int TransactionStatus { get; set; }
        public string DescriptionHead { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class TransactionHeadModelConfiguration : IEntityTypeConfiguration<TransactionHeadModel>
    {
        public void Configure(EntityTypeBuilder<TransactionHeadModel> builder)
        {
            builder.ToTable("Accounting_TransactionHead");
            builder.HasKey(o => o.TransactionHeadID);
        }
    }
}
