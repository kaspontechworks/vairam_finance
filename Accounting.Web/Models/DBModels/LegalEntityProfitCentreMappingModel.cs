﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.DBModels
{
    public class LegalEntityProfitCentreMappingModel
    {
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }
    }
    public class LegalEntityProfitCentreMappingModelConfiguration : IEntityTypeConfiguration<LegalEntityProfitCentreMappingModel>
    {
        public void Configure(EntityTypeBuilder<LegalEntityProfitCentreMappingModel> builder)
        {
            builder.ToTable("Accounting_LegalEntityProfitCentreMapping");
            builder.HasKey(o => new { o.LegalEntityID,o.ProfitCentreID});
        }
    }
}
