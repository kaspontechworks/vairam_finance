﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter user name!")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter password!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please select Legal Entity!")]
        //[Range(1, int.MaxValue, ErrorMessage = "Please select Legal Entity!")]
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
    }
}
