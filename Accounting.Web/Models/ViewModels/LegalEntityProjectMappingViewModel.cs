﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class LegalEntityProjectMappingViewModel
    {
        public List<LegalEntityProjectMappingModel> GetAllLegalEntityProjectMapping { get; set; }
        public List<LegalEntityProjectMappingViewModelList> GetAllLegalEntityProjectMappingViewModelList { get; set; }
        public LegalEntityProjectMappingModel GetLegalEntityProjectMappingByID { get; set; }
    }
    public class LegalEntityProjectDefinations
    {
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
        public int UnitID { get; set; }
        public List<ProjectDefinations> ProjectList { get; set; }
    }
    public class ProjectDefinations
    {
        public int ProjectID { get; set; }
    }
    public class LegalEntityProjectMappingViewModelList
    {
        public int LegalEntityID { get; set; }
        public string LegalEntityName { get; set; }
        public int ProfitCentreID { get; set; }
        public string ProfitCentreName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
    }
}
