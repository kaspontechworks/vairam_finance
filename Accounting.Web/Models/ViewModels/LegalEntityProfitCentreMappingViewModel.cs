﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class LegalEntityProfitCentreMappingViewModel
    {
        public List<LegalEntityProfitCentreMappingModel> GetAllLegalEntityProfitCentreMapping { get; set; }
        public List<LegalEntityProfitCentreMappingViewModelList> GetAllLegalEntityProfitCentreMappingViewModelList { get; set; }
        public LegalEntityProfitCentreMappingModel GetLegalEntityProfitCentreMappingByID { get; set; }
    }
    public class LegalEntityProfitCentreDefinations
    {
        public int LegalEntityID { get; set; }
        public List<ProfitCentreDefinations> ProfitCentreList { get; set; }
    }
    public class ProfitCentreDefinations
    {
        public int ProfitCentreID { get; set; }
    }
    public class LegalEntityProfitCentreMappingViewModelList
    {
        public int LegalEntityID { get; set; }
        public string LegalEntityName { get; set; }
        public int ProfitCentreID { get; set; }
        public string ProfitCentreName { get; set; }
    }
}
