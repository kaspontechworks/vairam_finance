﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class TransactionHeadViewModel
    {
        public List<TransactionHeadModel> GetAllTransactionHead { get; set; }
        public TransactionHeadModel GetTransactionHeadByID { get; set; }
        public List<TransactionHeadModel> GetAllTransactionHead_TransactionID { get; set; }

    }
    public class TransactionHeadViewModelList
    {
        public int TransactionHeadID { get; set; }
        public int ReceiptDetailid { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionRefNo { get; set; }
        public string TransactionShortRefNo { get; set; }
        public int FinancialYearID { get; set; }
        public int TransactionInputTypeID { get; set; }
        public int CorporateGroupID { get; set; }
        public int BusinessGroupID { get; set; }
        public int VerticalGroupID { get; set; }
        public int LegalEntityID { get; set; }
        public string LegalEntityName { get; set; }
        public int ProfitCentreID { get; set; }
        public string ProfitCentreName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public decimal TransactionTotal { get; set; }
        public string DescriptionHead { get; set; }
        public bool IsActive { get; set; }
    }

    public class TransactionHeadViewModelCode
    {
        public string TransactionRefNo { get; set; }
        public string TransactionShortRefNo { get; set; }
        public string LegalEntityslcode { get; set; }
        public string ProfitCentreslcode { get; set; }
        public string Locationslcode { get; set; }
        public string Unitslcode { get; set; }
        public string Projectslcode { get; set; }
        public string TransactionInputTypeslcode { get; set; }
        public int SlNo { get; set; }
        public int ReceiptDetailid { get; set; }
        public int FinancialYearID { get; set; }
    }
    //Accounting =>LMS
    public class TransactionHeadLMSViewModelCode
    {
        public string TransactionRefNo { get; set; }
        public string TransactionShortRefNo { get; set; }
        public string LegalEntityslcode { get; set; }
        public string ProfitCentreslcode { get; set; }
        public string Locationslcode { get; set; }
        public string Unitslcode { get; set; }
        public string Projectslcode { get; set; }
        public string TransactionInputTypeslcode { get; set; }
        public int SlNo { get; set; }
       // public int ReceiptDetailid { get; set; }
        public int FinancialYearID { get; set; }


    }
    public class Printable
    {
        public int printID { get; set; }
        public int Account_id { get; set; }
    }
}
