﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class LedgerGroupMasterViewModel
    {
        public List<LedgerGroupMasterModel> GetAllLedgerGroupMaster { get; set; }
        public LedgerGroupMasterModel GetLedgerGroupMasterByID { get; set; }

        public List<GroupTypeModel> GetAllGroupTypeModel { get; set; }
        public List<LedgerGroupMasterViewModelList> GetAllLedgerGroupMasterViewModelList { get; set; }
    }
    public class LedgerGroupMasterViewModelList
    {
        public int LedgerGroupID { get; set; }
        public string LedgerGroupName { get; set; }
        public int ParentLedgerGroupID { get; set; }
        public string ParentLedgerGroupName { get; set; }
        public int GroupTypeID { get; set; }
        public string GroupTypeName { get; set; }
    }
    //public class LedgerGroup
    //{
    //    public int LedgerGroupID { get; set; }
    //    public string LedgerGroupName { get; set; }
    //    public int ParentLedgerGroupID { get; set; }
    //    public int GroupTypeID { get; set; }
    //    public bool IsActive { get; set; }
    //    public DateTime CreatedOn { get; set; }
    //    public int CreatedBy { get; set; }
    //    public DateTime? LastModifiedon { get; set; }
    //    public int? LastModifiedBy { get; set; }
    //}
}
