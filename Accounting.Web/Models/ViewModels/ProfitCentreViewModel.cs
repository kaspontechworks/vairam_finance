﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class ProfitCentreViewModel
    {
        public List<ProfitCentreModel> GetAllProfitCentre { get; set; }
        public ProfitCentreModel GetProfitCentreByID { get; set; }
    }
}
