﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class LegalEntityLocationMappingViewModel
    {
        public List<LegalEntityLocationMappingModel> GetAllLegalEntityLocationMapping { get; set; }
        public List<LegalEntityLocationMappingViewModelList> GetAllLegalEntityLocationMappingViewModelList { get; set; }
        public LegalEntityLocationMappingModel GetLegalEntityLocationMappingByID { get; set; }
    }
    public class LegalEntityLocationDefinations
    {
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public List<LocationDefinations> LocationList { get; set; }
    }
    public class LocationDefinations
    {
        public int LocationID { get; set; }
    }
    public class LegalEntityLocationMappingViewModelList
    {
        public int LegalEntityID { get; set; }
        public string LegalEntityName { get; set; }
        public int ProfitCentreID { get; set; }
        public string ProfitCentreName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
    }
}
