﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class LegalEntityUnitMappingViewModel
    {
        public List<LegalEntityUnitMappingModel> GetAllLegalEntityUnitMapping { get; set; }
        public List<LegalEntityUnitMappingViewModelList> GetAllLegalEntityUnitMappingViewModelList { get; set; }
        public LegalEntityUnitMappingModel GetLegalEntityUnitMappingByID { get; set; }
    }
    public class LegalEntityUnitDefinations
    {
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
        public List<UnitDefinations> UnitList { get; set; }
    }
    public class UnitDefinations
    {
        public int UnitID { get; set; }
    }
    public class LegalEntityUnitMappingViewModelList
    {
        public int LegalEntityID { get; set; }
        public string LegalEntityName { get; set; }
        public int ProfitCentreID { get; set; }
        public string ProfitCentreName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
    }
}
