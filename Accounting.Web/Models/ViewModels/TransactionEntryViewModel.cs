﻿using Accounting.Web.Models.DBModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class TransactionEntryViewModel
    {
        public int TransactionEntryID { get; set; }
        //[Required(ErrorMessage = "Please enter Transaction Date!")]
        public DateTime TransactionEntryDate { get; set; }
        public int FinancialYearID { get; set; }
        public int TransactionHeadID { get; set; }
        public string TransactionRefNo { get; set; }
        public string TransactionShortRefNo { get; set; }
        public int TransactionInputTypeID { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal OpenDebit { get; set; }
        public decimal OpenCredit { get; set; }
        public decimal CloseDebit { get; set; }
        public decimal CloseCredit { get; set; }
        //[Required(ErrorMessage = "Please enter Description !")]
        public string Description { get; set; }
        //[Required(ErrorMessage = "Please select account !")]
        public int AccountID { get; set; }
        public int AccountTypeID { get; set; }
        public int ParentCategoryID { get; set; }
        public int CategoryID { get; set; }
        public int MajorGroupID { get; set; }
        public int GroupID { get; set; }
        public int SubGroupID { get; set; }
        public string MajorGroupName { get; set; }
        public string GroupName { get; set; }
        public string AccountName { get; set; }

        public int CorporateGroupID { get; set; }
        public int BusinessGroupID { get; set; }
        public int VerticalGroupID { get; set; }
        public int LegalEntityID { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
        public int UnitID { get; set; }
        public int ProjectID { get; set; }
        public int RowNo { get; set; }
        public int RowType { get; set; }
        public int CashorBankAccountNo { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedon { get; set; }
        public int? LastModifiedBy { get; set; }

        public List<TransactionEntryModel> GetAllTransactionEntry { get; set; }
        public TransactionEntryModel GetTransactionEntryByID { get; set; }
        public List<TransactionEntryModel> DeleteTransactionHead { get; set; }
        public List<TransactionEntryModel> GetAlldebitcredit { get; set; }
        public List<TransactionEntryViewModelList> GetAllTransactionHead_Transactiondetails { get; set; }
        public List<TransactionEntryViewModelList> GetAllTransactionHead_TransactionID { get; set; }
        public List<TransactionEntryViewModelList> GetReferenceNo { get; set; }

    }
    public class TransactionEntryDefinations
    {
        public int TransactionEntryID { get; set; }
        public DateTime TransactionEntryDate { get; set; }
        public int FinancialYearID { get; set; }
        public int TransactionHeadID { get; set; }
        public string TransactionRefNo { get; set; }
        public string TransactionShortRefNo { get; set; }
        public int TransactionInputTypeID { get; set; }
        public int CorporateGroupID { get; set; }
        public int BusinessGroupID { get; set; }
        public int VerticalGroupID { get; set; }
        public int LegalEntityID { get; set; }
        public int ReceiptDetailid { get; set; }
        public int ProfitCentreID { get; set; }
        public int LocationID { get; set; }
        public int UnitID { get; set; }
        public int ProjectID { get; set; }
        public decimal TransactionTotal { get; set; }
        public string DescriptionHead { get; set; }
       
        //public int TransactionStatus { get; set; }
        public List<TransactionEntryDetailDefinations> TransactionEntryDetailDefinationsList { get; set; }

    }
    public class TransactionEntryDetailDefinations
    {
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public string Description { get; set; }
        public int AccountID { get; set; }
        public int AccountTypeID { get; set; }
        public int ParentCategoryID { get; set; }
        public int CategoryID { get; set; }
        public int MajorGroupID { get; set; }
        public int GroupID { get; set; }
        public int SubGroupID { get; set; }
        public int RowNo { get; set; }
        public int RowType { get; set; }
        public int CashorBankAccountNo { get; set; }


    }

    public class TransactionEntryTrialList
    {
        public decimal OpenDebit { get; set; }
        public decimal OpenCredit { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit1 { get; set; }
        public decimal Credit1 { get; set; }
        public decimal Debit2 { get; set; }
        public decimal Credit2 { get; set; }
        public decimal Debit3 { get; set; }
        public decimal Credit3 { get; set; }
        public decimal Debit4{ get; set; }
        public decimal Credit4 { get; set; }
        public decimal Debit5{ get; set; }
        public decimal Credit5 { get; set; }
        public decimal CloseDebit { get; set; }
        public decimal CloseCredit { get; set; }
        public int AccountID { get; set; }
        public string AccountName { get; set; }
        public int MajorGroupID { get; set; }
        public string MajorGroupName { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public int SubGroupID { get; set; }
        public string SubGroupName { get; set; }
        public int[] Majorgroup { get; set; }
        public IEnumerable<SelectListItem> Values { get; set; }
    }
    public class ProfitandlossAmount
    {
        public decimal Column1 { get; set; }
        public decimal Column2 { get; set; }
        public decimal Column3 { get; set; }
        public decimal Column4 { get; set; }


    }
    public class TransactionEntryViewModelList
    {
        public int TransactionEntryID { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime TransactionEntryDate { get; set; }
        public int FinancialYearID { get; set; }
        public int TransactionHeadID { get; set; }
        public string TransactionRefNo { get; set; }
        public string TransactionShortRefNo { get; set; }
        public int TransactionInputTypeID { get; set; }
        public string TransactionInputTypeName { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal OpenDebit { get; set; }
        public decimal OpenCredit { get; set; }
        public decimal CloseDebit { get; set; }
        public decimal CloseCredit { get; set; }
        public string Description { get; set; }
        public int AccountID { get; set; }
        public string AccountName { get; set; }
        public int AccountTypeID { get; set; }
        public string AccountTypeName { get; set; }
        public int ParentCategoryID { get; set; }
        public string ParentCategoryName { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int MajorGroupID { get; set; }
        public string MajorGroupName { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public int SubGroupID { get; set; }
        public string SubGroupName { get; set; }
        public int RowNo { get; set; }
        public int RowType { get; set; }
        public int CashorBankAccountNo { get; set; }
        public int CorporateGroupID { get; set; }
        public int BusinessGroupID { get; set; }
        public int VerticalGroupID { get; set; }
        public int LegalEntityID { get; set; }
        public string LegalEntityName { get; set; }
        public int ProfitCentreID { get; set; }
        public string ProfitCentreName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string DescriptionHead { get; set; }
        public int Pastdate_status { get; set; }
       
    }
    public class ValidatingAccount
    {
        public string AccountName { get; set; }
        public int LegalEntityID { get; set; }
    }

    public class JsonAccounts
    {
        public int Accountid { get; set; }
        public int GroupID { get; set; }
        public string Accountname { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
    }

}
