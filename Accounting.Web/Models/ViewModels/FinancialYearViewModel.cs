﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class FinancialYearViewModel
    {
        public List<FinancialYearModel> GetAllFinancialYear { get; set; }
        public FinancialYearModel GetFinancialYearByID { get; set; }
    }
}
