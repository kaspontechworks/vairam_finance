﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class ProductManagementViewModel
    {
        public List<ProductManagementModel> GetAllProduct { get; set; }
        public ProductManagementModel GetProductID { get; set; }
        public List<ProductManagement_TatDefination> GetAllProTatDegByProID { get; set; }
    }
    public class ProductManagemntWithTatDefination
    {
        public string ProductID { get; set; }
        public string ProductUniqueID { get; set; }
        public string ProductName { get; set; }
        public List<TatDefinations> objtatDefinations { get; set; }
    }
    public class TatDefinations
    {
        public string CaseID { get; set; }
        public string Duration { get; set; }
        public string isDaysorhrs { get; set; }
    }
}
