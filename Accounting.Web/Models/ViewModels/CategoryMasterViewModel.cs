﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class CategoryMasterViewModel
    {
        public List<CategoryMasterModel> GetAllCategoryMaster { get; set; }
        public List<CategoryMasterViewModelList> GetAllCategoryMasterViewModelList { get; set; }
        public CategoryMasterModel GetCategoryMasterByID { get; set; }
    }
    public class CategoryMasterViewModelList
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int ParentCategoryID { get; set; }
        public string ParentCategoryName { get; set; }
    }
}
