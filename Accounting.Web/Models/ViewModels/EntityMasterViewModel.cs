﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class EntityMasterViewModel
    {
        public List<EntityMasterModel> GetAllEntityMaster { get; set; }
        public List<EntityMasterViewModelList> GetAllEntityMasterViewModelList { get; set; }
        public EntityMasterModel GetEntityMasterByID { get; set; }
    }
    public class EntityMasterViewModelList
    {
        public int EntityID { get; set; }
        public string Name { get; set; }
        public int EntityLevel { get; set; }
        public string EntityLevelName { get; set; }
        public int ParentEntityID { get; set; }
        public string ParentEntityName { get; set; }
        public string Address { get; set; }
        public string StatutoryInfo { get; set; }
        public string TANNO { get; set; }
        public string CINNO { get; set; }
        public string PANNO { get; set; }
        public string GSTNO { get; set; }
        public string EXTRAFIELD { get; set; }
        public string LegalEntityslcode { get; set; }
    }
}
