﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class ChartofAccountsViewModel
    {
        public List<ChartofAccountsModel> GetAllChartofAccounts { get; set; }
        public ChartofAccountsModel GetChartofAccountsByID { get; set; }
        public List<AccountTypeModel> GetAllAccountType { get; set; }
        public List<ChartofAccountsViewModelList> GetAllChartofAccountsViewModelList { get; set; }
    }
    public class ChartofAccountsViewModelList
    {
        public int AccountID { get; set; }
        public string AccountName { get; set; }
        public string AccountCode { get; set; }
        public int AccountTypeID { get; set; }
        public string AccountTypeName { get; set; }
        public int LegalEntityID { get; set; }
        public string LegalEntityName { get; set; }
        public int ParentCategoryID { get; set; }
        public string ParentCategoryName { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int MajorGroupID { get; set; }
        public string MajorGroupName { get; set; }
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public int SubGroupID { get; set; }
        public string SubGroupName { get; set; }
        public bool IsCostcenter { get; set; }
        public bool IsBudgetary { get; set; }
        public bool IsAnalytics { get; set; }
        public bool IsStatutory { get; set; }
        public bool IsParty { get; set; }
        public string Description { get; set; }
        public List<ChartofAccounts> MajorGroupList { get; set; }

    }
    public class ChartofAccounts
    {
        public int MajorGroupList { get; set; }

    }
    public class SumChartofAccountsViewModelList
    {
        public int GroupID { get; set; }
        public int AccountID { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
    }
    public class MajorGroupViewModelList
    {
        public int MajorGroupID { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
    }
    public class GroupViewModelList
    {
        public int GroupID { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
    }
    public class SubGroupViewModelList
    {
        public int SubGroupID { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
    }
    //public class LedgerGroup
    //{
    //    public int LedgerGroupID { get; set; }
    //    public string LedgerGroupName { get; set; }
    //    public int ParentLedgerGroupID { get; set; }
    //    public int GroupTypeID { get; set; }
    //    public bool IsActive { get; set; }
    //    public DateTime CreatedOn { get; set; }
    //    public int CreatedBy { get; set; }
    //    public DateTime? LastModifiedon { get; set; }
    //    public int? LastModifiedBy { get; set; }
    //}
}
