﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class CaseManagementViewModel
    {
        public List<CaseManagementModel> GetAllCases { get; set; }
        public CaseManagementModel GetCaseByID { get; set; }
    }
}
