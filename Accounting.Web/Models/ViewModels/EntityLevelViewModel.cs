﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class EntityLevelViewModel
    {
        public List<EntityLevelModel> GetAllCases { get; set; }
        public EntityLevelModel GetCaseByID { get; set; }
    }
}
