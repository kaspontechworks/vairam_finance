﻿using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Models.ViewModels
{
    public class UnitViewModel
    {
        public List<UnitModel> GetAllUnit { get; set; }
        public UnitModel GetUnitByID { get; set; }
    }
}
