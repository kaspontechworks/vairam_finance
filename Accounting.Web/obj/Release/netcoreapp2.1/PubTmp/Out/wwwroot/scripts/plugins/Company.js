﻿/* OrgCompany Controller's script Starts */

function fnLoadOrgCompanyView(Id) {
    $("#divOrgCompanyContent").empty();
    $("#divOrgCompanyContent").load("/OrgCompany/EditCompany/" + Id);
}
function fnLoadOrgCompanyAdd() {
    $("#divOrgCompanyContent").empty();
    $("#divOrgCompanyContent").load("/OrgCompany/AddCompany");
}


function fnCompanydelete(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/OrgCompany/DeleteCompany/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/OrgCompany/OrgCompanyModule";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}

function fnDeletpopup(Id) {
   
    $("#DivModelDeleteContent").empty();
    $("#DivModelDeleteContent").load("/OrgCompany/DeleteCompanymodel/" + Id);
}


/* OrgCompany Controller's script ends */