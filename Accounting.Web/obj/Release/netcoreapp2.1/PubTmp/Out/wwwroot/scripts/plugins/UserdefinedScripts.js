﻿/* Configuration Controller's script starts */
function fnLoadUserView() {
    $("#divUserContent").empty();
    $("#divUserContent").load("/Configuration/AddUser");
}
function fnLoadRoleView() {
    $("#divRoleContent").empty();
    $("#divRoleContent").load("/Configuration/AddRole");
}
function fnLoadUserEditView(Id) {
    $("#divUserContent").empty();
    $("#divUserContent").load("/Configuration/EditUser/" + Id);
}
function fnLoadRoleEditView(Id) {
    debugger;
    $("#divRoleContent").empty();
    $("#divRoleContent").load("/Configuration/EditRole/" + Id);
}
function fnLoadRoleDeleteView(Id) {
    $("#divRoleContent").empty();
    $("#divRoleContent").load("/Configuration/DeleteRole/" + Id);
}
function fnLoadRoleAccessView(Id) {
    $("#divRoleContent").empty();
    $("#divRoleContent").load("/Configuration/RoleAccessView/" + Id);
}
function fnUserAccess(UserId, k) {

    $.ajax({
        url: "/Configuration/UserAccess?UserID=" + UserId + "&key=" + k,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {
            if (msg == "Ok") {
                if (k == 2) {
                    $("#btnRevoke_" + UserId).show();
                    $("#btnGrant_" + UserId).hide();
                } else {
                    $("#btnRevoke_" + UserId).hide();
                    $("#btnGrant_" + UserId).show();
                }
            }
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}
/* Configuration Controller's script ends */
/* lead Controller's script end */

function fnAddLeadView() {
    $("#divLeadContent").empty();
    $("#divLeadContent").load("/Lead/AddLead");
}
function fnLoadLeadView(Id) {
    window.location = "/Lead/ViewLeadDetails?Id=" + Id;
}

/* lead Controller's script end */

/* Case Management script starts */

function fnLoadCaseAdd() {
    $("#divCaseManagementModel").empty();
    $("#divCaseManagementModel").load("/CaseManagement/AddCase");
}
function fnLoadCaseView(Id) {
    $("#divCaseManagementModel").empty();
    $("#divCaseManagementModel").load("/CaseManagement/EditCase/" + Id);
}


function fnDeletecase(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/CaseManagement/DeleteCase/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/CaseManagement/Index";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}

function fnDeletpopupcase(Id) {

    $("#DivModelDeleteContent").empty();
    $("#DivModelDeleteContent").load("/CaseManagement/DeleteCasemodel/" + Id);
}


/* Case Management script ends */


/* Case Management_Case script starts */

function fnLoadCasemng_CaseAdd() {
    $("#divCaseManagement_Case_Model").empty();
    $("#divCaseManagement_Case_Model").load("/CaseMng_Case/AddCase");
}
/* Case Management_Case script ends */





/* Orgclient Controller's script Starts */

function fnLoadOrgclientView(Id) {

    $("#divOrgclientContent").empty();
    $("#divOrgclientContent").load("/Client/Editclient/" + Id);
}
function fnLoadOrgclientAdd() {

    $("#divOrgclientContent").empty();
    $("#divOrgclientContent").load("/Client/Addclient");
}

function fnDeleteclient(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/Client/DeleteClient/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/Client/OrgClientModule";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}

function fnDeletpopupClient(Id) {

    $("#DivModelDeleteContent").empty();
    $("#DivModelDeleteContent").load("/Client/DeleteClientmodel/" + Id);
}



/* Orgclient Controller's script ends */






