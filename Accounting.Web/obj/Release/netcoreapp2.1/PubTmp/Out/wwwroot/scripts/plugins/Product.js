﻿
/* Product Management script Start */
function fnLoadProductAdd() {
    $("#divProductManagementModel").empty();
    $("#divProductManagementModel").load("/ProductManagement/AddProduct");
}
function fnAddRow(obj) {

    var tds = '<tr>';
    jQuery.each($('tr:last td', $("#tbl_ProductCase")), function () {
        tds += '<td>' + $(this).val(0).html() + '</td>';
    });
    tds += '</tr>';
    if ($('tbody', this).length > 0) {
        $('tbody', this).append(tds);
    } else {
        $("#tbl_ProductCase").append(tds);
    }
}
function fnRemoveRow(obj) {
    $(this).closest('tr').remove();
}
function fnValidate() {
    var isnotDulpicate = true;
    var selects = $("select[id='ddl_CaseID']");
    var values = [];
    for (i = 0; i < selects.length; i++) {
        var select = selects[i];
        if (values.indexOf(select.value) > -1) {
            alert('duplicate exists' + select.value); return false;
            break;//if duplicates found we are returning. without save and no need to continue.
        }
    }
    return isnotDulpicate;

    //var isnotDulpicate = true;
    //var selects = document.getElementsByTagName('select');
    //var values = [];
    //for (i = 0; i < selects.length; i++) {
    //    var select = selects[i];
    //    if (values.indexOf(select.value) > -1) {
    //        alert('duplicate exists');
    //        return false;
    //        break;
    //    }
    //    else
    //        values.push(select.value);
    //}
    //return isnotDulpicate;
}
function fnSave() {
    if (fnValidate()) {
        var $rows = $("#tbl_ProductCase").find('tr:not(:hidden)');
        var $tds = $("#tbl_ProductCase").find('td:not(:hidden)');
        var headers = [];
        var data = {};
        var fields = [];
        data["ProductUniqueID"] = $("#ProductUniqueID").val();
        data["ProductName"] = $("#ProductName").val();
        data["ProductID"] = $("#ProductID").val();
        headers.push("CaseID");
        headers.push("Duration");
        headers.push("isDaysorhrs");

        $rows.each(function () {
            var $td = $(this).find('td');
            var $td2 = $td.find('input');
            var $td3 = $td.find('select,input');
            var h2 = {};

            headers.forEach(function (header, i) {
                h2[header] = $td3.eq(i).val();
                if ($td3.eq(i).val() === undefined) {
                    h2[header] = $td.eq(i).find('input,select').val() || $td.eq(i).text();
                }
            });
            fields.push(h2);
        });
        data["objtatDefinations"] = fields;
        $.ajax({
            url: "/ProductManagement/SaveProducts",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg == "Ok") {
                    window.location.href = "/ProductManagement/Index";
                }
            },
            failure: function (response) {
                // fnErrormsg(response.responseText);
            },
            error: function (response) {
                //fnErrormsg(response.responseText);
            }
        });
    }

}
function fnLoadProductView(Id) {
    $("#divProductManagementModel").empty();
    $("#divProductManagementModel").load("/ProductManagement/EditProduct/" + Id);
}
function fnUpdate() {
    if (fnValidate()) {
        var $rows = $("#tbl_ProductCase").find('tr:not(:hidden)');
        var $tds = $("#tbl_ProductCase").find('td:not(:hidden)');
        var headers = [];
        var data = {};
        var fields = [];
        data["ProductUniqueID"] = $("#GetProductID_ProductUniqueID").val();
        data["ProductName"] = $("#GetProductID_ProductName").val();
        data["ProductID"] = $("#GetProductID_ProductID").val();
        headers.push("CaseID");
        headers.push("Duration");
        headers.push("isDaysorhrs");

        $rows.each(function () {
            var $td = $(this).find('td');
            var $td2 = $td.find('input');
            var $td3 = $td.find('select,input');
            var h2 = {};

            headers.forEach(function (header, i) {
                h2[header] = $td3.eq(i).val();
                if ($td3.eq(i).val() === undefined) {
                    h2[header] = $td.eq(i).find('input,select').val() || $td.eq(i).text();
                }
            });
            fields.push(h2);
        });
        data["objtatDefinations"] = fields;

        $.ajax({
            url: "/ProductManagement/SaveProducts",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg == "Ok") {
                    window.location.href = "/ProductManagement/Index";
                }
            },
            failure: function (response) {
                // fnErrormsg(response.responseText);
            },
            error: function (response) {
                //fnErrormsg(response.responseText);
            }
        });
    }
}



function fnDeleteProduct(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/ProductManagement/DeleteProduct/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/ProductManagement/Index";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}

function fnDeletpopupProduct(Id) {

    $("#DivModelDeleteContent").empty();
    $("#DivModelDeleteContent").load("/ProductManagement/DeleteProductmodel/" + Id);
}


/* Product Management script ends */