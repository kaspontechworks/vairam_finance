USE [LMS_Live1]
GO
/****** Object:  StoredProcedure [dbo].[usp_CollectionRegister]    Script Date: 11-11-2020 11:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_CollectionRegister]
@companyid int,
@Locationid int,
@Fromdate date,
@Todate date,
@businessarea int,
@vehicletypeid int,
@DealerID int,
@collectionArea int,
@paymode int
AS
BEGIN



SELECT distinct  Receipt.ReceiptDetailId, Receipt.DueAmount, CONVERT(VARCHAR(10), Receipt.ReceiptDate, 103) ReceiptDate,Isnull(ArrearsPrincipal,0)AS ArrearsPrincipal,isnull(ArrearsInterest,0)as ArrearInterest,
ISNULL(Sum(Receipt.LateAmount),0)OD,isnull(AdvancePrincipal ,0)As AdvancePrincipal,isnull(AdvanceInterest,0)As AdvanceInterest,
   Receipt.HLAmount, Receipt.OtherAmount,  Contracts.LoanAccountNo, Contracts.Borrower_Name,
   Receipt.TotalAmount  As TotalAmount,
CASE WHEN UDReceiptType.UDDescription = 'Pre-Closure' OR UDReceiptType.UDDescription = 'Reloan' THEN 
	(SELECT ISNULL(InterestAmount, 0) InterestAmount FROM HPMS_Transaction_ClosureRequest CR WHERE CR.ReceiptId = Receipt.ReceiptDetailId) ELSE 0 END As InterestAmount,
	Receipt.ContractNo,UDAccountStatus.UDDescription  
FROM  HPMS_Transaction_ReceiptDetails Receipt 
INNER JOIN HPMS_Transaction_Contracts Contracts ON Contracts.ContractID = Receipt.ContractNo 
inner join HPMS_Transaction_ContractRepaymentSchedule RP on RP.ContractId=Receipt.ContractNo 
--inner join HPMS_Transaction_ClosureRequest  FC on FC.ContractId=Receipt.ContractNo 
Left JOIN HPMS_Master_Vehicles V ON V.VehicleID = Contracts.VehicleID
Left JOIN HPMS_Master_VehicleType VT ON V.VehicleTypeID = VT.VehicleTypeID
Left join HPMS_Config_CompanyLocations CL on CL.CompanyID =Contracts.CompanyID 
Left join HPMS_Config_BusinessArea BusinessArea on BusinessArea.BusinessAreaID=CL.BusinessAreaID 
LEFT JOIN HPMS_Config_UDOptions UDReceiptType ON UDReceiptType.UDID = Receipt.ReceiptType 
LEFT JOIN HPMS_Config_UDOptions UDRecordStatus ON UDRecordStatus.UDID = Receipt.RecordStatus 
LEFT JOIN HPMS_Config_UDOptions UDAccountStatus ON UDAccountStatus .UDID = Contracts.AccountStatus

---For Arrears amount 
LEFT join 
(select Sum(PrincipalAmount) as ArrearsPrincipal,sum(InterestAmount) as ArrearsInterest,CRS.ContractID from HPMS_Transaction_ContractRepaymentSchedule as CRS
 inner join HPMS_Transaction_Contracts C on CRS.ContractID =C.ContractID 
   where PaidPrincipal is null  and PaidInterest is null and InstallmentDate < Getdate() and 
   C.CompanyID =@companyid and C.LocationID =@Locationid   group by CRS.ContractID )A on A.ContractID =Receipt.ContractNo
   ---For Advance due amount 
 LEFT join (select Sum(PaidPrincipal)AS AdvancePrincipal,sum(PaidInterest)As AdvanceInterest,CRS.ContractID from HPMS_Transaction_ContractRepaymentSchedule as CRS
 inner join HPMS_Transaction_Contracts C on CRS.ContractID =C.ContractID 
  where PaidPrincipal is not null and PaidInterest is not null and InstallmentDate > GETDATE() and 
   C.CompanyID =@companyid and C.LocationID =@Locationid     group by CRS.ContractID  ) B on B.ContractID =Receipt.ContractNo  
   ---For TotalCollection amount 
    LEFT join (select (sum(CRS.EMI))As TotalAmount,CRS.ContractID,C.DealerID,C.AgentID,C.CollectionArea from HPMS_Transaction_ContractRepaymentSchedule as CRS
 inner join HPMS_Transaction_Contracts C on CRS.ContractID =C.ContractID 
  where PaidPrincipal is not null and PaidInterest is not null and
   C.CompanyID =@companyid and C.LocationID =@Locationid     group by CRS.ContractID,C.DealerID,C.AgentID,C.CollectionArea  ) C on C.ContractID =Receipt.ContractNo  
  where 
  ---check case condition for all or specific field
 Receipt.CompanyID =@companyid  and Receipt.LocationID =@Locationid and VT.VehicleTypeID =@vehicletypeid 
and BusinessArea.BusinessAreaID =@businessarea  and Receipt.ReceiptDate >=@Fromdate and Receipt.ReceiptDate <=@Todate and CASE WHEN @DealerID=0 Then 1
Else C.DealerID End = CASE WHEN @DealerID = 0 THEN 1 ELSE @DealerID  END And CASE WHEN @collectionArea  =0 Then 1
Else C.CollectionArea End = CASE WHEN @collectionArea = 0 THEN 1 ELSE @collectionArea   END And CASE WHEN @paymode  =0 Then 1
Else Receipt.PayMode End = CASE WHEN @paymode = 0 THEN 1 ELSE @paymode   END
group by ReceiptDetailId,Receipt.DueAmount,ReceiptDate,PrincipalAmount,RP.InterestAmount,Receipt.ContractNo,Receipt.HLAmount ,
Receipt.OtherAmount,Contracts.ContractID,Contracts.LoanAccountNo ,Contracts.Borrower_Name,Receipt.LateAmount,Receipt.ForeClosureAmount,
Receipt.PartPayment,    Receipt.TotalAmount,A.ArrearsPrincipal 
,A.ArrearsInterest ,B.AdvancePrincipal,B.AdvanceInterest  ,C.TotalAmount ,UDReceiptType.UDDescription,UDRecordStatus.UDDescription,C.DealerID,
C.AgentID ,Receipt.PayMode,C.CollectionArea,UDAccountStatus.UDDescription
 END

