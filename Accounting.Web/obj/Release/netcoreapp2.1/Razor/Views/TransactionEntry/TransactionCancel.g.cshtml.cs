#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\TransactionCancel.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5fdb8573d9e164580e101e9aee2b1e8c0bac0b0f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TransactionEntry_TransactionCancel), @"mvc.1.0.view", @"/Views/TransactionEntry/TransactionCancel.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/TransactionEntry/TransactionCancel.cshtml", typeof(AspNetCore.Views_TransactionEntry_TransactionCancel))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5fdb8573d9e164580e101e9aee2b1e8c0bac0b0f", @"/Views/TransactionEntry/TransactionCancel.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_TransactionEntry_TransactionCancel : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\TransactionCancel.cshtml"
  
   // ViewData["Title"] = "Transaction Entry";
    Layout = "~/Views/Shared/_Layout3.cshtml";

#line default
#line hidden
            BeginContext(103, 4, true);
            WriteLiteral("<h2>");
            EndContext();
            BeginContext(108, 17, false);
#line 5 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\TransactionCancel.cshtml"
Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(125, 265, true);
            WriteLiteral(@" Submitted Successfully.</h2>

<div id=""main-content"" data-plugin=""ProductScript"">
    <div class=""container"">
        <div class=""block-header"">
            <div class=""row"">
                <div class=""col-lg-6 col-md-8 col-sm-12"">
                    <h2>");
            EndContext();
            BeginContext(391, 17, false);
#line 12 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\TransactionCancel.cshtml"
                   Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(408, 7, true);
            WriteLiteral("</h2>\r\n");
            EndContext();
            BeginContext(782, 239, true);
            WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row clearfix\">\r\n            <div class=\"col-lg-12\">\r\n                <div class=\"card\">\r\n                    <div class=\"header\">\r\n                        <h2>");
            EndContext();
            BeginContext(1022, 17, false);
#line 25 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\TransactionCancel.cshtml"
                       Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(1039, 126, true);
            WriteLiteral(" Cancelled.</h2>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
