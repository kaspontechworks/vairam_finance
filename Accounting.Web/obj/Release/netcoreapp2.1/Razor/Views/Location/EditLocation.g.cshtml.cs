#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "561d1b9f396827be6c1efc93df6696791fffc280"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Location_EditLocation), @"mvc.1.0.view", @"/Views/Location/EditLocation.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Location/EditLocation.cshtml", typeof(AspNetCore.Views_Location_EditLocation))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"561d1b9f396827be6c1efc93df6696791fffc280", @"/Views/Location/EditLocation.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_Location_EditLocation : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Accounting.Web.Models.DBModels.LocationModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(53, 146, true);
            WriteLiteral("\r\n\r\n<div id=\"divResult\">\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"title\">Edit Location</h4>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n");
            EndContext();
            BeginContext(1034, 134, true);
            WriteLiteral("        <div class=\"row clearfix\">\r\n            <div class=\"col-lg-4\">\r\n                <div class=\"form-group\">\r\n                    ");
            EndContext();
            BeginContext(1169, 33, false);
#line 30 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.HiddenFor(m => m.LocationID));

#line default
#line hidden
            EndContext();
            BeginContext(1202, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(1267, 20, true);
            WriteLiteral("                    ");
            EndContext();
            BeginContext(1288, 32, false);
#line 32 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.HiddenFor(m => m.CreatedBy));

#line default
#line hidden
            EndContext();
            BeginContext(1320, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(1343, 32, false);
#line 33 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.HiddenFor(m => m.CreatedOn));

#line default
#line hidden
            EndContext();
            BeginContext(1375, 22, true);
            WriteLiteral("\r\n                    ");
            EndContext();
            BeginContext(1398, 31, false);
#line 34 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.HiddenFor(m => m.IsActive));

#line default
#line hidden
            EndContext();
            BeginContext(1429, 70, true);
            WriteLiteral("\r\n                    <span>Location name</span>\r\n                    ");
            EndContext();
            BeginContext(1500, 114, false);
#line 36 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.TextBoxFor(m => m.LocationName, new { @class = "form-control", tabindex = 1, placeholder = "Location name" }));

#line default
#line hidden
            EndContext();
            BeginContext(1614, 117, true);
            WriteLiteral("\r\n                    <span style=\"color:red;display:none;\" id=\"spn_LocationName\">Please enter Location name</span>\r\n");
            EndContext();
            BeginContext(1829, 270, true);
            WriteLiteral(@"                </div>
            </div>
                <div class=""col-lg-4"">
                </div>
                <div class=""col-lg-4"">
                    <div class=""form-group"">
                        <span>Location Code</span>
                        ");
            EndContext();
            BeginContext(2100, 131, false);
#line 46 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
                   Write(Html.TextBoxFor(m => m.Locationslcode, new { @class = "form-control", tabindex = 8, placeholder = "Location Code", maxlength = 4 }));

#line default
#line hidden
            EndContext();
            BeginContext(2231, 332, true);
            WriteLiteral(@"
                        <span style=""color:red;display:none;"" id=""spn_Locationslcode"">Please enter Location Code</span>
                    </div>
                </div>
                <div class=""col-lg-12"">
                    <div class=""form-group"">
                        <span>Address</span>
                        ");
            EndContext();
            BeginContext(2564, 121, false);
#line 53 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
                   Write(Html.TextAreaFor(m => m.Address, new { @class = "form-control", tabindex = 4, placeholder = "Address", maxlength = 200 }));

#line default
#line hidden
            EndContext();
            BeginContext(2685, 527, true);
            WriteLiteral(@"
                        <span style=""color:red;display:none;"" id=""spn_EntityAddress"">Please enter Address</span>
                    </div>
                </div>
           
        </div>
        <div class=""row clearfix "">
            <div class=""col-lg-12 "">
                <h6>Statutory Info :</h6>
            </div>
        </div>
        <div class=""row clearfix "">
            <div class=""col-lg-6"">
                <div class=""form-group"">
                    <span>GST IN</span>
                    ");
            EndContext();
            BeginContext(3213, 116, false);
#line 68 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.TextBoxFor(m => m.GSTNO, new { @class = "form-control", tabindex = 5, placeholder = "GST IN", maxlength = 25 }));

#line default
#line hidden
            EndContext();
            BeginContext(3329, 147, true);
            WriteLiteral("\r\n                    <span style=\"color:red;display:none;\" id=\"spn_GSTIN\">Please enter GST IN</span>\r\n                </div>\r\n            </div>\r\n");
            EndContext();
            BeginContext(3904, 139, true);
            WriteLiteral("            <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                    <span>CIN NO</span>\r\n                    ");
            EndContext();
            BeginContext(4044, 116, false);
#line 82 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.TextBoxFor(m => m.CINNO, new { @class = "form-control", tabindex = 6, placeholder = "CIN NO", maxlength = 25 }));

#line default
#line hidden
            EndContext();
            BeginContext(4160, 339, true);
            WriteLiteral(@"
                    <span style=""color:red;display:none;"" id=""spn_CINNO"">Please enter CIN NO</span>
                </div>
            </div>
        </div>
        <div class=""row clearfix "">
            <div class=""col-lg-6"">
                <div class=""form-group"">
                    <span>PAN NO</span>
                    ");
            EndContext();
            BeginContext(4500, 116, false);
#line 91 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.TextBoxFor(m => m.PANNO, new { @class = "form-control", tabindex = 7, placeholder = "PAN NO", maxlength = 25 }));

#line default
#line hidden
            EndContext();
            BeginContext(4616, 286, true);
            WriteLiteral(@"
                    <span style=""color:red;display:none;"" id=""spn_PANNO"">Please enter PAN NO</span>
                </div>
            </div>
            <div class=""col-lg-6"">
                <div class=""form-group"">
                    <span>TAN NO</span>
                    ");
            EndContext();
            BeginContext(4903, 116, false);
#line 98 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Location\EditLocation.cshtml"
               Write(Html.TextBoxFor(m => m.TANNO, new { @class = "form-control", tabindex = 8, placeholder = "TAN NO", maxlength = 25 }));

#line default
#line hidden
            EndContext();
            BeginContext(5019, 5940, true);
            WriteLiteral(@"
                    <span style=""color:red;display:none;"" id=""spn_TANNO"">Please enter TAN NO</span>
                </div>
            </div>


        </div>
    </div>
            <div class=""modal-footer"">
                <button type=""submit"" id=""btn_Submit"" title=""Save!"" class=""btn btn-primary"">Submit</button>
                <button type=""button"" class=""btn btn-danger"" title=""Close!"" data-dismiss=""modal"">Cancel</button>
            </div>
        </div>

        <script type=""text/javascript"">

            function getdate(str) {
                //alert(str.split(' ')[0]);
                var dat = str.split(' ')[0].split('-');
                return (dat[1] + ""/"" + dat[0] + ""/"" + dat[2] + "" "" + str.split(' ')[1]);
            }

            $(""#btn_Submit"").click(function () {
                if (fnValidate()) {

                    //var hidLegalEntity = $(""#hidLegalEntity""),
                    var LocationID = $(""#LocationID""),
                        LocationName = $(""#L");
            WriteLiteral(@"ocationName""),
                         Address = $(""#Address""),
                            GSTNO = $(""#GSTNO""),
                            CINNO = $(""#CINNO""),
                            PANNO = $(""#PANNO""),
                            TANNO = $(""#TANNO""),
                            Locationslcode = $(""#Locationslcode""),
                        CreatedBy = $(""#CreatedBy""),
                        CreatedOn = $(""#CreatedOn""),
                        IsActive = $(""#IsActive""),
                        data = {};
                    //  alert(getdate(CreatedOn.val()));
                    //alert(CreatedOn.val());
                    //var d = new Date(CreatedOn.val());
                    //alert(d);
                    // alert(formatDate(CreatedOn.val()));
                    data[""LocationID""] = parseInt(LocationID.val());
                    data[""LocationName""] = LocationName.val();
                    data[""Address""] = Address.val();
                            data[""GSTNO""] = GSTNO");
            WriteLiteral(@".val();
                            data[""CINNO""] = CINNO.val();
                            data[""PANNO""] = PANNO.val();
                            data[""TANNO""] = TANNO.val();
                        data[""Locationslcode""] = Locationslcode.val();
                    // data[""LegalEntityID""] = parseInt(hidLegalEntity.val());
                    data[""CreatedBy""] = parseInt(CreatedBy.val());
                    data[""CreatedOn""] = getdate(CreatedOn.val());
                    data[""IsActive""] = IsActive.val();
                    $.ajax({
                        url: ""/Location/InsertLocation"",
                        type: ""POST"",
                        data: JSON.stringify(data),
                        contentType: ""application/json; charset=utf-8"",
                        dataType: ""json"",
                        success: function (msg) {
                            window.location.href = ""/Location/Index"";
                        },
                        error: function (msg) {

 ");
            WriteLiteral(@"                       }
                    });
                }
                else {
                    return false;
                }
            });
            //$(""#btn_Submit"").click(function () {
            //    if (fnValidate()) {

            //        var LocationID = $(""#LocationID""),
            //            LocationName = $(""#LocationName""),
            //            LegalEntityID = $(""#LegalEntityID""),
            //            CreatedBy = $(""#CreatedBy""),
            //            CreatedOn = $(""#CreatedOn""),
            //            IsActive = $(""#IsActive""),
            //            data = {};
            //        alert(LocationID.val());
            //        data[""LocationID""] = parseInt(LocationID.val());
            //        data[""LocationName""] = LocationName.val();
            //        data[""LegalEntityID""] = parseInt(LegalEntityID.val());
            //        data[""CreatedBy""] = parseInt(CreatedBy.val());
            //        data[""CreatedOn""] = Cr");
            WriteLiteral(@"eatedOn.val();
            //        data[""IsActive""] = IsActive.val();

            //        $.ajax({
            //            url: ""/Location/InsertLocation"",
            //            type: ""POST"",
            //            data: JSON.stringify(data),
            //            contentType: ""application/json; charset=utf-8"",
            //            dataType: ""json"",
            //            success: function (msg) {
            //                //$('#DistrictModule').modal('hide');
            //                //$(""#div_tableView"").empty();
            //                //$(""#div_tableView"").load(""/District/ShowDistrictTableView/"" + StateID.val());
            //                window.location.href = ""/Location/Index"";
            //            },
            //            error: function (msg) {

            //            }
            //        });
            //    }
            //    else {
            //        return false;
            //    }
            //});
         ");
            WriteLiteral(@"   function fnValidate() {

                event.preventDefault();
                var LocationName = $(""#LocationName""),
                    // hidLegalEntity = $(""#hidLegalEntity""),
                    isValid = true,
                    spn_LocationName = $(""#spn_LocationName"");
                //  spn_LegalEntity = $(""#spn_LegalEntity"");
                // spn_LegalEntity.hide();
                spn_LocationName.hide();
                //if (hidLegalEntity.val() == 0) {
                //    spn_LegalEntity.show();
                //    isValid = false;
                //}
                if (LocationName.val() == """") {
                    spn_LocationName.show();
                    isValid = false;
                }

                return isValid;
            }
        </script>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Accounting.Web.Models.DBModels.LocationModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
