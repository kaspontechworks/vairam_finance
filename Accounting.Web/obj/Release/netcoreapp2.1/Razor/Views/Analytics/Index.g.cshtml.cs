#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Analytics\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "57cfa1adf2ba41b29bccb5ade252a723b7515a89"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Analytics_Index), @"mvc.1.0.view", @"/Views/Analytics/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Analytics/Index.cshtml", typeof(AspNetCore.Views_Analytics_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"57cfa1adf2ba41b29bccb5ade252a723b7515a89", @"/Views/Analytics/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_Analytics_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Accounting.Web.Models.ViewModels.AnalyticsViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(69, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Analytics\Index.cshtml"
  
    ViewData["Title"] = "Analytics";
    Layout = "~/Views/Shared/_Layout3.cshtml";

#line default
#line hidden
            BeginContext(224, 250, true);
            WriteLiteral("\r\n<div id=\"main-content\" data-plugin=\"ProductScript\">\r\n    <div class=\"container\">\r\n        <div class=\"block-header\">\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-6 col-md-8 col-sm-12\">\r\n                    <h2>Analytics</h2>\r\n");
            EndContext();
            BeginContext(825, 314, true);
            WriteLiteral(@"                </div>
            </div>
        </div>
        <div class=""row clearfix"">
            <div class=""col-lg-12"">
                <div class=""card"">
                    <div class=""header"">
                        <h2>Analytics List</h2>
                        <ul class=""header-dropdown"">
");
            EndContext();
            BeginContext(1474, 34, true);
            WriteLiteral("                            <li>\r\n");
            EndContext();
            BeginContext(1744, 698, true);
            WriteLiteral(@"                                <button class=""btn btn-primary"" id=""btn_addnew"" style=""float:right;"" onclick=""fnAddAnalytics()"" data-toggle=""modal"" data-target=""#AnalyticsModule""><i class=""fa fa-plus-square""></i> <span>New Analytics</span></button>
                            </li>
                            <li class=""dropdown"" style=""display:none;"">
                                <a href=""javascript:void(0);"" class=""dropdown-toggle"" data-toggle=""dropdown"" role=""button"" aria-haspopup=""true"" aria-expanded=""false""></a>
                                <ul class=""dropdown-menu dropdown-menu-right"">
                                    <li><a href=""javascript:void(0);"">Add New</a></li>
");
            EndContext();
            BeginContext(2640, 785, true);
            WriteLiteral(@"                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class=""body"">
                        <div class=""table-responsive"">
                            <table class=""table table-hover js-basic-example dataTable table-custom table-striped"" data-plugin=""dataTable"">
                                <thead class=""thead-dark"">
                                    <tr>
                                        <th>Analytics name</th>
                                        <th>Analytics code</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
");
            EndContext();
#line 56 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Analytics\Index.cshtml"
                                     foreach (var item in Model.GetAllAnalytics)
                                    {

#line default
#line hidden
            BeginContext(3546, 86, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>");
            EndContext();
            BeginContext(3633, 48, false);
#line 59 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Analytics\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.AnalyticsName));

#line default
#line hidden
            EndContext();
            BeginContext(3681, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(3733, 48, false);
#line 60 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Analytics\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.AnalyticsCode));

#line default
#line hidden
            EndContext();
            BeginContext(3781, 210, true);
            WriteLiteral("</td>\r\n                                        <td>\r\n                                            <div class=\"btn-group-sm\">\r\n                                                <button class=\"btn btn-primary right\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3991, "\"", 4044, 3);
            WriteAttributeValue("", 4001, "fnLoadAnalyticsEditModel(", 4001, 25, true);
#line 63 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Analytics\Index.cshtml"
WriteAttributeValue("", 4026, item.AnalyticsID, 4026, 17, false);

#line default
#line hidden
            WriteAttributeValue("", 4043, ")", 4043, 1, true);
            EndWriteAttribute();
            BeginContext(4045, 221, true);
            WriteLiteral(" data-toggle=\"modal\" data-target=\"#AnalyticsModule\"><i class=\"fa fa-edit\"></i></button>\r\n                                                <button class=\"btn danger m-t\" data-toggle=\"modal\" data-target=\"#AnalyticsdelModule\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 4266, "\"", 4316, 3);
            WriteAttributeValue("", 4276, "fnDeletAnalyticsPopUp(", 4276, 22, true);
#line 64 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Analytics\Index.cshtml"
WriteAttributeValue("", 4298, item.AnalyticsID, 4298, 17, false);

#line default
#line hidden
            WriteAttributeValue("", 4315, ")", 4315, 1, true);
            EndWriteAttribute();
            BeginContext(4317, 182, true);
            WriteLiteral("><i class=\"fa fa-trash\"></i> </button>\r\n                                            </div>\r\n                                        </td>\r\n                                    </tr>\r\n");
            EndContext();
#line 68 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Analytics\Index.cshtml"
                                    }

#line default
#line hidden
            BeginContext(4538, 222, true);
            WriteLiteral("                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
            BeginContext(5321, 1814, true);
            WriteLiteral(@"<div id=""AnalyticsModule"" class=""modal"" data-backdrop=""true"">
    <div class=""modal-dialog modal-lg"">
        <div class=""modal-content"">
            <div class=""modal-content"" id=""divAnalyticsContent"">

            </div>
        </div><!-- /.modal-content -->
    </div>
</div>
<div id=""AnalyticsdelModule"" class=""modal fade animate black-overlay"" data-backdrop=""false"">
    <div class=""modal-dialog modal-sm"">
        <!-- /.modal-content -->
        <div id=""DivModelDeleteContent"">

        </div>
    </div>
</div>


<script type=""text/javascript"">
    function fnAddAnalytics() {
        //var ddl_MajorCategory = $(""#ddl_MajorCategory"");
        //var Id = parseInt(ddl_MajorCategory.val());
        $(""#divAnalyticsContent"").empty();
        $(""#divAnalyticsContent"").load(""/Analytics/AddAnalytics/"");
    }
    function fnLoadAnalyticsEditModel(Id) {
        $(""#divAnalyticsContent"").empty();
        $(""#divAnalyticsContent"").load(""/Analytics/EditAnalytics/"" + Id);
    }
    functi");
            WriteLiteral(@"on fnDeletAnalyticsPopUp(Id) {
        //  alert($(""#DivModelDeleteContent""));
        $(""#DivModelDeleteContent"").empty();
        $(""#DivModelDeleteContent"").load(""/Analytics/DeleteAnalyticsmodel/"" + Id);
    }
    function fnDeleteAnalytics(d) {
        $.ajax({
            url: ""/Analytics/DeleteAnalytics/"" + d,
            type: ""POST"",
            contentType: ""application/json; charset=utf-8"",
            dataType: ""json"",
            success: function (msg) {
                $('#DivModelDeleteContent').modal('hide');
                //$(""#div_tableView"").empty();
                //$(""#div_tableView"").load(""/Analytics/ShowAnalyticsTableView/"" + msg);
                window.location.href = ""/Analytics/Index"";
            }
        });
    }
   
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Accounting.Web.Models.ViewModels.AnalyticsViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
