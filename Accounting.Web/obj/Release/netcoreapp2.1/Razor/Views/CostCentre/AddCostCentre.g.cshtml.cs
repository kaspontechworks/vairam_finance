#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\AddCostCentre.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e066d4cf41ec1abc6a39176215bec9525ffb6e43"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_CostCentre_AddCostCentre), @"mvc.1.0.view", @"/Views/CostCentre/AddCostCentre.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/CostCentre/AddCostCentre.cshtml", typeof(AspNetCore.Views_CostCentre_AddCostCentre))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e066d4cf41ec1abc6a39176215bec9525ffb6e43", @"/Views/CostCentre/AddCostCentre.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_CostCentre_AddCostCentre : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Accounting.Web.Models.DBModels.CostCentreModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(55, 166, true);
            WriteLiteral("\r\n    <div id=\"divResult\">\r\n        <div class=\"modal-header\">\r\n            <h4 class=\"title\">Add Cost Centre</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n");
            EndContext();
            BeginContext(787, 42, true);
            WriteLiteral("                <div class=\"form-group\">\r\n");
            EndContext();
            BeginContext(918, 20, true);
            WriteLiteral("                    ");
            EndContext();
            BeginContext(939, 120, false);
#line 21 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\AddCostCentre.cshtml"
               Write(Html.TextBoxFor(m => m.CostCentreName, new { @class = "form-control", tabindex = 2,  placeholder = "Cost Centre name" }));

#line default
#line hidden
            EndContext();
            BeginContext(1059, 182, true);
            WriteLiteral("\r\n                    <span style=\"color:red;display:none;\" id=\"spn_CostCentreName\">Please enter Cost Centre name</span>\r\n                    <input type=\"hidden\" id=\"hidLegalEntity\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1241, "\"", 1269, 1);
#line 23 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\AddCostCentre.cshtml"
WriteAttributeValue("", 1249, ViewBag.LegalEntity, 1249, 20, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1270, 2030, true);
            WriteLiteral(@" />
                </div>
            </div>
            <div class=""modal-footer"">
                <button type=""submit"" id=""btn_Submit"" title=""Save!"" class=""btn btn-primary"">Submit</button>
                <button type=""button"" class=""btn btn-danger"" title=""Close!"" data-dismiss=""modal"">Cancel</button>
            </div>
        </div>
    <script type=""text/javascript"">
    $(""#btn_Submit"").click(function () {
        if (fnValidate()) {
              var hidLegalEntity = $(""#hidLegalEntity""),
                CostCentreName = $(""#CostCentreName""),
                data = {};
            data[""LegalEntityID""] = parseInt(hidLegalEntity.val());
            data[""CostCentreName""] = CostCentreName.val();
            $.ajax({
                url: ""/CostCentre/InsertCostCentre"",
                type: ""POST"",
                data: JSON.stringify(data),
                contentType: ""application/json; charset=utf-8"",
                dataType: ""json"",
                success: function (msg) {
  ");
            WriteLiteral(@"                 window.location.href = ""/CostCentre/Index"";
                },
                error: function (msg) {

                }
            });
        }
        else {
            return false;
        }
    });
      function fnValidate() {
            
        event.preventDefault();
          var CostCentreName = $(""#CostCentreName""),
              // ddl_LegalEntity = $(""#ddl_LegalEntity""),
              isValid = true,
              spn_CostCentreName = $(""#spn_CostCentreName"");
              //  spn_LegalEntity = $(""#spn_LegalEntity"");
            //spn_LegalEntity.hide();
            spn_CostCentreName.hide();
            //if (ddl_LegalEntity.val() == 0) {
            //    spn_LegalEntity.show();
            //    isvalid = false;
            //}
            if (CostCentreName.val() == """") {
                spn_CostCentreName.show();
                isValid = false;
            }
        
        return isValid;
    }
    </script>



");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Accounting.Web.Models.DBModels.CostCentreModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
