#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1a4f3c712a90eb3aa2bc59bac89c1d8430e6b054"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_CostCentre_Index), @"mvc.1.0.view", @"/Views/CostCentre/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/CostCentre/Index.cshtml", typeof(AspNetCore.Views_CostCentre_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1a4f3c712a90eb3aa2bc59bac89c1d8430e6b054", @"/Views/CostCentre/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_CostCentre_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Accounting.Web.Models.ViewModels.CostCentreViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(70, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\Index.cshtml"
  
    ViewData["Title"] = "Cost Centre";
    Layout = "~/Views/Shared/_Layout3.cshtml";

#line default
#line hidden
            BeginContext(228, 252, true);
            WriteLiteral("\r\n<div id=\"main-content\" data-plugin=\"ProductScript\">\r\n    <div class=\"container\">\r\n        <div class=\"block-header\">\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-6 col-md-8 col-sm-12\">\r\n                    <h2>Cost Centre</h2>\r\n");
            EndContext();
            BeginContext(831, 316, true);
            WriteLiteral(@"                </div>
            </div>
        </div>
        <div class=""row clearfix"">
            <div class=""col-lg-12"">
                <div class=""card"">
                    <div class=""header"">
                        <h2>Cost Centre List</h2>
                        <ul class=""header-dropdown"">
");
            EndContext();
            BeginContext(1482, 34, true);
            WriteLiteral("                            <li>\r\n");
            EndContext();
            BeginContext(1752, 702, true);
            WriteLiteral(@"                                <button class=""btn btn-primary"" id=""btn_addnew"" style=""float:right;"" onclick=""fnAddCostCentre()"" data-toggle=""modal"" data-target=""#CostCentreModule""><i class=""fa fa-plus-square""></i> <span>New Cost Centre</span></button>
                            </li>
                            <li class=""dropdown"" style=""display:none;"">
                                <a href=""javascript:void(0);"" class=""dropdown-toggle"" data-toggle=""dropdown"" role=""button"" aria-haspopup=""true"" aria-expanded=""false""></a>
                                <ul class=""dropdown-menu dropdown-menu-right"">
                                    <li><a href=""javascript:void(0);"">Add New</a></li>
");
            EndContext();
            BeginContext(2652, 722, true);
            WriteLiteral(@"                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class=""body"">
                        <div class=""table-responsive"">
                            <table class=""table table-hover js-basic-example dataTable table-custom table-striped"" data-plugin=""dataTable"">
                                <thead class=""thead-dark"">
                                    <tr>
                                        <th>Cost Centre name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
");
            EndContext();
#line 55 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\Index.cshtml"
                                     foreach (var item in Model.GetAllCostCentre)
                                    {

#line default
#line hidden
            BeginContext(3496, 86, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>");
            EndContext();
            BeginContext(3583, 49, false);
#line 58 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.CostCentreName));

#line default
#line hidden
            EndContext();
            BeginContext(3632, 210, true);
            WriteLiteral("</td>\r\n                                        <td>\r\n                                            <div class=\"btn-group-sm\">\r\n                                                <button class=\"btn btn-primary right\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3842, "\"", 3897, 3);
            WriteAttributeValue("", 3852, "fnLoadCostCentreEditModel(", 3852, 26, true);
#line 61 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\Index.cshtml"
WriteAttributeValue("", 3878, item.CostCentreID, 3878, 18, false);

#line default
#line hidden
            WriteAttributeValue("", 3896, ")", 3896, 1, true);
            EndWriteAttribute();
            BeginContext(3898, 223, true);
            WriteLiteral(" data-toggle=\"modal\" data-target=\"#CostCentreModule\"><i class=\"fa fa-edit\"></i></button>\r\n                                                <button class=\"btn danger m-t\" data-toggle=\"modal\" data-target=\"#CostCentredelModule\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 4121, "\"", 4173, 3);
            WriteAttributeValue("", 4131, "fnDeletCostCentrePopUp(", 4131, 23, true);
#line 62 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\Index.cshtml"
WriteAttributeValue("", 4154, item.CostCentreID, 4154, 18, false);

#line default
#line hidden
            WriteAttributeValue("", 4172, ")", 4172, 1, true);
            EndWriteAttribute();
            BeginContext(4174, 182, true);
            WriteLiteral("><i class=\"fa fa-trash\"></i> </button>\r\n                                            </div>\r\n                                        </td>\r\n                                    </tr>\r\n");
            EndContext();
#line 66 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CostCentre\Index.cshtml"
                                    }

#line default
#line hidden
            BeginContext(4395, 222, true);
            WriteLiteral("                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
            BeginContext(5178, 1834, true);
            WriteLiteral(@"<div id=""CostCentreModule"" class=""modal"" data-backdrop=""true"">
    <div class=""modal-dialog modal-lg"">
        <div class=""modal-content"">
            <div class=""modal-content"" id=""divCostCentreContent"">

            </div>
        </div><!-- /.modal-content -->
    </div>
</div>
<div id=""CostCentredelModule"" class=""modal fade animate black-overlay"" data-backdrop=""false"">
    <div class=""modal-dialog modal-sm"">
        <!-- /.modal-content -->
        <div id=""DivModelDeleteContent"">

        </div>
    </div>
</div>


<script type=""text/javascript"">
    function fnAddCostCentre() {
        //var ddl_MajorCategory = $(""#ddl_MajorCategory"");
        //var Id = parseInt(ddl_MajorCategory.val());
        $(""#divCostCentreContent"").empty();
        $(""#divCostCentreContent"").load(""/CostCentre/AddCostCentre/"");
    }
    function fnLoadCostCentreEditModel(Id) {
        $(""#divCostCentreContent"").empty();
        $(""#divCostCentreContent"").load(""/CostCentre/EditCostCentre/"" + Id);
    ");
            WriteLiteral(@"}
    function fnDeletCostCentrePopUp(Id) {
        //  alert($(""#DivModelDeleteContent""));
        $(""#DivModelDeleteContent"").empty();
        $(""#DivModelDeleteContent"").load(""/CostCentre/DeleteCostCentremodel/"" + Id);
    }
    function fnDeleteCostCentre(d) {
        $.ajax({
            url: ""/CostCentre/DeleteCostCentre/"" + d,
            type: ""POST"",
            contentType: ""application/json; charset=utf-8"",
            dataType: ""json"",
            success: function (msg) {
                $('#DivModelDeleteContent').modal('hide');
                //$(""#div_tableView"").empty();
                //$(""#div_tableView"").load(""/CostCentre/ShowCostCentreTableView/"" + msg);
                window.location.href = ""/CostCentre/Index"";
            }
        });
    }
 
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Accounting.Web.Models.ViewModels.CostCentreViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
