#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Unit\AddUnit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "39aa3fa1b8a0ef6d1d232005393a84a6a09f4784"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Unit_AddUnit), @"mvc.1.0.view", @"/Views/Unit/AddUnit.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Unit/AddUnit.cshtml", typeof(AspNetCore.Views_Unit_AddUnit))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"39aa3fa1b8a0ef6d1d232005393a84a6a09f4784", @"/Views/Unit/AddUnit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_Unit_AddUnit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Accounting.Web.Models.DBModels.UnitModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(49, 139, true);
            WriteLiteral("\r\n<div id=\"divResult\">\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"title\">Add Unit</h4>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n");
            EndContext();
            BeginContext(682, 178, true);
            WriteLiteral("        <div class=\"row clearfix\">\r\n            <div class=\"col-lg-4\">\r\n                <div class=\"form-group\">\r\n                    <span>Unit name</span>\r\n                    ");
            EndContext();
            BeginContext(861, 106, false);
#line 23 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Unit\AddUnit.cshtml"
               Write(Html.TextBoxFor(m => m.UnitName, new { @class = "form-control", tabindex = 2, placeholder = "Unit name" }));

#line default
#line hidden
            EndContext();
            BeginContext(967, 109, true);
            WriteLiteral("\r\n                    <span style=\"color:red;display:none;\" id=\"spn_UnitName\">Please enter Unit name</span>\r\n");
            EndContext();
            BeginContext(1174, 266, true);
            WriteLiteral(@"                </div>
            </div>
                <div class=""col-lg-4"">
                </div>
                <div class=""col-lg-4"">
                    <div class=""form-group"">
                        <span>Unit Code</span>
                        ");
            EndContext();
            BeginContext(1441, 123, false);
#line 33 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Unit\AddUnit.cshtml"
                   Write(Html.TextBoxFor(m => m.Unitslcode, new { @class = "form-control", tabindex = 3, placeholder = "Unit Code", maxlength = 4 }));

#line default
#line hidden
            EndContext();
            BeginContext(1564, 328, true);
            WriteLiteral(@"
                        <span style=""color:red;display:none;"" id=""spn_Locationslcode"">Please enter Unit Code</span>
                    </div>
                </div>
                <div class=""col-lg-12"">
                    <div class=""form-group"">
                        <span>Address</span>
                        ");
            EndContext();
            BeginContext(1893, 121, false);
#line 40 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Unit\AddUnit.cshtml"
                   Write(Html.TextAreaFor(m => m.Address, new { @class = "form-control", tabindex = 4, placeholder = "Address", maxlength = 200 }));

#line default
#line hidden
            EndContext();
            BeginContext(2014, 528, true);
            WriteLiteral(@"
                        <span style=""color:red;display:none;"" id=""spn_EntityAddress"">Please enter Address</span>
                    </div>
                
            </div>
        </div>
        <div class=""row clearfix "">
            <div class=""col-lg-12 "">
                <h6>Statutory Info :</h6>
            </div>
        </div>
        <div class=""row clearfix "">
            <div class=""col-lg-6"">
                <div class=""form-group"">
                    <span>GST IN</span>
                    ");
            EndContext();
            BeginContext(2543, 116, false);
#line 55 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Unit\AddUnit.cshtml"
               Write(Html.TextBoxFor(m => m.GSTNO, new { @class = "form-control", tabindex = 5, placeholder = "GST IN", maxlength = 25 }));

#line default
#line hidden
            EndContext();
            BeginContext(2659, 147, true);
            WriteLiteral("\r\n                    <span style=\"color:red;display:none;\" id=\"spn_GSTIN\">Please enter GST IN</span>\r\n                </div>\r\n            </div>\r\n");
            EndContext();
            BeginContext(3234, 139, true);
            WriteLiteral("            <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                    <span>CIN NO</span>\r\n                    ");
            EndContext();
            BeginContext(3374, 116, false);
#line 69 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Unit\AddUnit.cshtml"
               Write(Html.TextBoxFor(m => m.CINNO, new { @class = "form-control", tabindex = 6, placeholder = "CIN NO", maxlength = 25 }));

#line default
#line hidden
            EndContext();
            BeginContext(3490, 339, true);
            WriteLiteral(@"
                    <span style=""color:red;display:none;"" id=""spn_CINNO"">Please enter CIN NO</span>
                </div>
            </div>
        </div>
        <div class=""row clearfix "">
            <div class=""col-lg-6"">
                <div class=""form-group"">
                    <span>PAN NO</span>
                    ");
            EndContext();
            BeginContext(3830, 116, false);
#line 78 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Unit\AddUnit.cshtml"
               Write(Html.TextBoxFor(m => m.PANNO, new { @class = "form-control", tabindex = 7, placeholder = "PAN NO", maxlength = 25 }));

#line default
#line hidden
            EndContext();
            BeginContext(3946, 286, true);
            WriteLiteral(@"
                    <span style=""color:red;display:none;"" id=""spn_PANNO"">Please enter PAN NO</span>
                </div>
            </div>
            <div class=""col-lg-6"">
                <div class=""form-group"">
                    <span>TAN NO</span>
                    ");
            EndContext();
            BeginContext(4233, 116, false);
#line 85 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\Unit\AddUnit.cshtml"
               Write(Html.TextBoxFor(m => m.TANNO, new { @class = "form-control", tabindex = 8, placeholder = "TAN NO", maxlength = 25 }));

#line default
#line hidden
            EndContext();
            BeginContext(4349, 3019, true);
            WriteLiteral(@"
                    <span style=""color:red;display:none;"" id=""spn_TANNO"">Please enter TAN NO</span>
                </div>
            </div>


        </div>
    </div>
            <div class=""modal-footer"">
                <button type=""submit"" id=""btn_Submit"" title=""Save!"" class=""btn btn-primary"">Submit</button>
                <button type=""button"" class=""btn btn-danger"" title=""Close!"" data-dismiss=""modal"">Cancel</button>
            </div>
        </div>
        <script type=""text/javascript"">
            $(""#btn_Submit"").click(function () {
                if (fnValidate()) {
                    // var hidLegalEntity = $(""#hidLegalEntity""),
                    var UnitName = $(""#UnitName""),
                         Address = $(""#Address""),
                            GSTNO = $(""#GSTNO""),
                            CINNO = $(""#CINNO""),
                            PANNO = $(""#PANNO""),
                            TANNO = $(""#TANNO""),
                            Unitslcode = $(""#Uni");
            WriteLiteral(@"tslcode""),
                        data = {};
                    // data[""LegalEntityID""] = parseInt(hidLegalEntity.val());
                    data[""UnitName""] = UnitName.val();
                     data[""Address""] = Address.val();
                            data[""GSTNO""] = GSTNO.val();
                            data[""CINNO""] = CINNO.val();
                            data[""PANNO""] = PANNO.val();
                            data[""TANNO""] = TANNO.val();
                        data[""Unitslcode""] = Unitslcode.val();
                    $.ajax({
                        url: ""/Unit/InsertUnit"",
                        type: ""POST"",
                        data: JSON.stringify(data),
                        contentType: ""application/json; charset=utf-8"",
                        dataType: ""json"",
                        success: function (msg) {
                            window.location.href = ""/Unit/Index"";
                        },
                        error: function (msg) {

    ");
            WriteLiteral(@"                    }
                    });
                }
                else {
                    return false;
                }
            });
            function fnValidate() {

                event.preventDefault();
                var UnitName = $(""#UnitName""),
                    // ddl_LegalEntity = $(""#ddl_LegalEntity""),
                    isValid = true,
                    spn_UnitName = $(""#spn_UnitName"");
                //  spn_LegalEntity = $(""#spn_LegalEntity"");
                //spn_LegalEntity.hide();
                spn_UnitName.hide();
                //if (ddl_LegalEntity.val() == 0) {
                //    spn_LegalEntity.show();
                //    isValid = false;
                //}
                if (UnitName.val() == """") {
                    spn_UnitName.show();
                    isValid = false;
                }

                return isValid;
            }
        </script>



");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Accounting.Web.Models.DBModels.UnitModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
