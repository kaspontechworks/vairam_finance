#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CategoryMaster\AddCategoryMaster.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "68c37d5e2c14707e2f77c2fe7f611b27072f2d0e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_CategoryMaster_AddCategoryMaster), @"mvc.1.0.view", @"/Views/CategoryMaster/AddCategoryMaster.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/CategoryMaster/AddCategoryMaster.cshtml", typeof(AspNetCore.Views_CategoryMaster_AddCategoryMaster))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"68c37d5e2c14707e2f77c2fe7f611b27072f2d0e", @"/Views/CategoryMaster/AddCategoryMaster.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_CategoryMaster_AddCategoryMaster : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Accounting.Web.Models.DBModels.CategoryMasterModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "0", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(59, 279, true);
            WriteLiteral(@"
    <div id=""divResult"">
        <div class=""modal-header"">
            <h4 class=""title"">Add Account Category</h4>
        </div>
        <div class=""modal-body"">
            <div class=""form-group"">
                <select id=""ddl_MajorCategory"" class=""form-control"">
");
            EndContext();
            BeginContext(397, 20, true);
            WriteLiteral("                    ");
            EndContext();
            BeginContext(417, 40, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "aa5bb001a1ce4007b5999f4ee7bbb6a1", async() => {
                BeginContext(435, 13, true);
                WriteLiteral("Category type");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(457, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 12 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CategoryMaster\AddCategoryMaster.cshtml"
                     foreach (var item in @ViewBag.AllMajorCategory)
                    {

#line default
#line hidden
            BeginContext(552, 24, true);
            WriteLiteral("                        ");
            EndContext();
            BeginContext(576, 70, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b5d91d3b3c70455c8fa1a60bf2b86170", async() => {
                BeginContext(615, 22, false);
#line 14 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CategoryMaster\AddCategoryMaster.cshtml"
                                                         Write(item.MajorCategoryName);

#line default
#line hidden
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
#line 14 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CategoryMaster\AddCategoryMaster.cshtml"
                           WriteLiteral(item.MajorCategoryID);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(646, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 15 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CategoryMaster\AddCategoryMaster.cshtml"
                    }

#line default
#line hidden
            BeginContext(671, 200, true);
            WriteLiteral("                </select>\r\n                <span style=\"color:red;display:none;\" id=\"spn_Categorytype\">Please enter category type</span>\r\n            </div>\r\n                <div class=\"form-group\">\r\n");
            EndContext();
            BeginContext(960, 20, true);
            WriteLiteral("                    ");
            EndContext();
            BeginContext(981, 127, false);
#line 21 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\CategoryMaster\AddCategoryMaster.cshtml"
               Write(Html.TextBoxFor(m => m.CategoryName, new { @class = "form-control", tabindex = 2, @rows = "6", placeholder = "Category name" }));

#line default
#line hidden
            EndContext();
            BeginContext(1108, 2095, true);
            WriteLiteral(@"
                    <span style=""color:red;display:none;"" id=""spn_name"">Please enter category name</span>
                </div>
            </div>
            <div class=""modal-footer"">
                <button type=""submit"" id=""btn_Submit"" title=""Save!"" class=""btn btn-primary"">Submit</button>
                <button type=""button"" class=""btn btn-danger"" title=""Close!"" data-dismiss=""modal"">Cancel</button>
            </div>
        </div>
    <script type=""text/javascript"">
    $(""#btn_Submit"").click(function () {
        if (fnValidate()) {
              var ddl_MajorCategory = $(""#ddl_MajorCategory""),
                CategoryName = $(""#CategoryName""),
                data = {};
            data[""ParentCategoryID""] = parseInt(ddl_MajorCategory.val());
            data[""CategoryName""] = CategoryName.val();
            $.ajax({
                url: ""/CategoryMaster/InsertCategoryMaster"",
                type: ""POST"",
                data: JSON.stringify(data),
                contentType: ");
            WriteLiteral(@"""application/json; charset=utf-8"",
                dataType: ""json"",
                success: function (msg) {
                   window.location.href = ""/CategoryMaster/Index"";
                },
                error: function (msg) {

                }
            });
        }
        else {
            return false;
        }
    });
      function fnValidate() {
            
        event.preventDefault();
            var CategoryName = $(""#CategoryName""),
            ddl_MajorCategory = $(""#ddl_MajorCategory""),
            isValid = true,
            spn_name = $(""#spn_name""),
                spn_Categorytype = $(""#spn_Categorytype"");
            spn_Categorytype.hide();
            spn_name.hide();
            if (ddl_MajorCategory.val() == 0) {
                spn_Categorytype.show();
                isValid = false;
            }
            if (CategoryName.val() == """") {
                spn_name.show();
                isValid = false;
            }
        
      ");
            WriteLiteral("  return isValid;\r\n    }\r\n    </script>\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Accounting.Web.Models.DBModels.CategoryMasterModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
