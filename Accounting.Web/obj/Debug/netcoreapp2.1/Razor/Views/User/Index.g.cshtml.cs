#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7120f107baff8622cbf5624b852a2b7df693a766"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_User_Index), @"mvc.1.0.view", @"/Views/User/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/User/Index.cshtml", typeof(AspNetCore.Views_User_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7120f107baff8622cbf5624b852a2b7df693a766", @"/Views/User/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_User_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Accounting.Web.Models.ViewModels.UserManagementViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(64, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml"
  
    ViewData["Title"] = "User";
    Layout = "~/Views/Shared/_Layout3.cshtml";

#line default
#line hidden
            BeginContext(219, 245, true);
            WriteLiteral("\r\n<div id=\"main-content\" data-plugin=\"ProductScript\">\r\n    <div class=\"container\">\r\n        <div class=\"block-header\">\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-6 col-md-8 col-sm-12\">\r\n                    <h2>User</h2>\r\n");
            EndContext();
            BeginContext(831, 309, true);
            WriteLiteral(@"                </div>
            </div>
        </div>
        <div class=""row clearfix"">
            <div class=""col-lg-12"">
                <div class=""card"">
                    <div class=""header"">
                        <h2>User List</h2>
                        <ul class=""header-dropdown"">
");
            EndContext();
            BeginContext(1475, 34, true);
            WriteLiteral("                            <li>\r\n");
            EndContext();
            BeginContext(1745, 683, true);
            WriteLiteral(@"                                <button class=""btn btn-primary"" id=""btn_addnew"" style=""float:right;"" onclick=""fnAddUser()"" data-toggle=""modal"" data-target=""#UserModule""><i class=""fa fa-plus-square""></i> <span>New User</span></button>
                            </li>
                            <li class=""dropdown"" style=""display:none;"">
                                <a href=""javascript:void(0);"" class=""dropdown-toggle"" data-toggle=""dropdown"" role=""button"" aria-haspopup=""true"" aria-expanded=""false""></a>
                                <ul class=""dropdown-menu dropdown-menu-right"">
                                    <li><a href=""javascript:void(0);"">Add New</a></li>
");
            EndContext();
            BeginContext(2626, 833, true);
            WriteLiteral(@"                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class=""body"">
                        <div class=""table-responsive"">
                            <table class=""table table-hover js-basic-example dataTable table-custom table-striped"" data-plugin=""dataTable"">
                                <thead class=""thead-dark"">
                                    <tr>
                                        <th>FirstName</th>
                                        <th>LastName</th>
                                        <th>UserName</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
");
            EndContext();
#line 57 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml"
                                     foreach (var item in Model.AllUsers)
                                    {

#line default
#line hidden
            BeginContext(3573, 86, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>");
            EndContext();
            BeginContext(3660, 44, false);
#line 60 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(3704, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(3756, 43, false);
#line 61 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(3799, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(3851, 43, false);
#line 62 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.UserName));

#line default
#line hidden
            EndContext();
            BeginContext(3894, 210, true);
            WriteLiteral("</td>\r\n                                        <td>\r\n                                            <div class=\"btn-group-sm\">\r\n                                                <button class=\"btn btn-primary right\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 4104, "\"", 4147, 3);
            WriteAttributeValue("", 4114, "fnLoadUserEditModel(", 4114, 20, true);
#line 65 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml"
WriteAttributeValue("", 4134, item.UserID, 4134, 12, false);

#line default
#line hidden
            WriteAttributeValue("", 4146, ")", 4146, 1, true);
            EndWriteAttribute();
            BeginContext(4148, 211, true);
            WriteLiteral(" data-toggle=\"modal\" data-target=\"#UserModule\"><i class=\"fa fa-edit\"></i></button>\r\n                                                <button class=\"btn danger m-t\" data-toggle=\"modal\" data-target=\"#UserdelModule\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 4359, "\"", 4399, 3);
            WriteAttributeValue("", 4369, "fnDeletUserPopUp(", 4369, 17, true);
#line 66 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml"
WriteAttributeValue("", 4386, item.UserID, 4386, 12, false);

#line default
#line hidden
            WriteAttributeValue("", 4398, ")", 4398, 1, true);
            EndWriteAttribute();
            BeginContext(4400, 182, true);
            WriteLiteral("><i class=\"fa fa-trash\"></i> </button>\r\n                                            </div>\r\n                                        </td>\r\n                                    </tr>\r\n");
            EndContext();
#line 70 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\User\Index.cshtml"
                                    }

#line default
#line hidden
            BeginContext(4621, 222, true);
            WriteLiteral("                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
            BeginContext(5404, 631, true);
            WriteLiteral(@"<div id=""UserModule"" class=""modal"" data-backdrop=""true"">
    <div class=""modal-dialog modal-lg"">
        <div class=""modal-content"">
            <div class=""modal-content"" id=""divUserContent"">

            </div>
        </div><!-- /.modal-content -->
    </div>
</div>
<div id=""UserdelModule"" class=""modal fade animate black-overlay"" data-backdrop=""false"">
    <div class=""modal-dialog modal-sm"">
        <!-- /.modal-content -->
        <div id=""DivModelDeleteContent"">

        </div>
    </div>
</div>


<script type=""text/javascript"">
    function validateEmail(email) {
        var emailReg = /^([\w-\.]+");
            EndContext();
            BeginContext(6036, 1259, true);
            WriteLiteral(@"@([\w-]+\.)+[\w-]{2,4})?$/gi;
        //alert(emailReg.test(email));
        return emailReg.test(email);
    }

    function fnAddUser() {
        //var ddl_MajorCategory = $(""#ddl_MajorCategory"");
        //var Id = parseInt(ddl_MajorCategory.val());
        $(""#divUserContent"").empty();
        $(""#divUserContent"").load(""/User/AddUser/"");
    }
    function fnLoadUserEditModel(Id) {
        $(""#divUserContent"").empty();
        $(""#divUserContent"").load(""/User/EditUser/"" + Id);
    }
    function fnDeletUserPopUp(Id) {
        //  alert($(""#DivModelDeleteContent""));
        $(""#DivModelDeleteContent"").empty();
        $(""#DivModelDeleteContent"").load(""/User/DeleteUsermodel/"" + Id);
    }
    function fnDeleteUser(d) {
        $.ajax({
            url: ""/User/DeleteUser/"" + d,
            type: ""POST"",
            contentType: ""application/json; charset=utf-8"",
            dataType: ""json"",
            success: function (msg) {
                $('#DivModelDeleteContent').modal('hi");
            WriteLiteral("de\');\r\n                //$(\"#div_tableView\").empty();\r\n                //$(\"#div_tableView\").load(\"/User/ShowUserTableView/\" + msg);\r\n                window.location.href = \"/User/Index\";\r\n            }\r\n        });\r\n    }\r\n\r\n</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Accounting.Web.Models.ViewModels.UserManagementViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
