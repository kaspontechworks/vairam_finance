#pragma checksum "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "496086629b764f28472f1dd691f4f977cbd51185"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TransactionEntry_AccountTrialBalance), @"mvc.1.0.view", @"/Views/TransactionEntry/AccountTrialBalance.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/TransactionEntry/AccountTrialBalance.cshtml", typeof(AspNetCore.Views_TransactionEntry_AccountTrialBalance))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web;

#line default
#line hidden
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\_ViewImports.cshtml"
using Accounting.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"496086629b764f28472f1dd691f4f977cbd51185", @"/Views/TransactionEntry/AccountTrialBalance.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"352413526b5c2f023f8b226b2d871c8220dbe97b", @"/Views/_ViewImports.cshtml")]
    public class Views_TransactionEntry_AccountTrialBalance : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Accounting.Web.Models.ViewModels.TransactionEntryViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "0", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "1", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "2", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "3", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "4", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/images/loading (1).gif"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("160"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("80"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
  
    ViewData["Title"] = "Trial Balance";
    Layout = "~/Views/Shared/_Layout3.cshtml";

#line default
#line hidden
            BeginContext(164, 813, true);
            WriteLiteral(@"
<style type=""text/css"">
    .modal {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }

    .loading {
        font-family: Arial;
        font-size: 10pt;
        border: 5px solid #67CFF5;
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }
</style>
<div id=""main-content"" data-plugin=""ProductScript"">
    <div class=""container"">
        <div class=""block-header"">
            <div class=""row"">
                <div class=""col-lg-6 col-md-8 col-sm-12"">
                    <h2>");
            EndContext();
            BeginContext(978, 17, false);
#line 38 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
                   Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(995, 7, true);
            WriteLiteral("</h2>\r\n");
            EndContext();
            BeginContext(1369, 169, true);
            WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row clearfix\">\r\n            <div class=\"col-lg-12\">\r\n                <div class=\"card\">\r\n");
            EndContext();
            BeginContext(1680, 3522, true);
            WriteLiteral(@"                    <div class=""body"">
                        <div class=""row clearfix"">
                            <div class=""col-lg-4"">
                                <div class=""form-group clearfix"">
                                    <label class=""fancy-checkbox element-left"">
                                        <input id=""IsDateRange"" type=""checkbox"" checked=""checked"" onclick=""ondatetypechangeIsDateRange()"">
                                        <span>Date Range</span>
                                    </label>
                                </div>
                            </div>
                            <div class=""col-lg-4"">
                                <div class=""form-group clearfix"">
                                    <label class=""fancy-checkbox element-left"">
                                        <input id=""IsAsonDate"" type=""checkbox"" onclick=""ondatetypechangeIsAsonDate()"">
                                        <span>As on Date(From Financial Year)</span>");
            WriteLiteral(@"
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class=""row clearfix"">
                            <div class=""col-lg-4"">
                                <div class=""row"">
                                    <div class=""col-lg-6"">
                                        <span>Start Date</span>
                                        <div class=""input-group date"" data-date-autoclose=""true"" data-provide=""datepicker"" data-date-format=""dd-mm-yyyy"">
                                            <input type=""text"" id=""StartDate"" class=""form-control"" readonly=""readonly"" onblur=""callfromdatefun()"" onchange=""callfromdatefun()"">
                                            <div class=""input-group-append"">
                                                <button class=""btn btn-outline-secondary"" type=""button""><i class=""fa fa-calendar""></i></button>
                                  ");
            WriteLiteral(@"          </div>
                                        </div>
                                        <span style=""color:red;display:none;"" id=""spn_StartDate"">Please enter StartDate</span>
                                    </div>
                                    <div class=""col-lg-6"">
                                        <span>End Date</span>
                                        <div class=""input-group date"" data-date-autoclose=""true"" data-provide=""datepicker"" data-date-format=""dd-mm-yyyy"">
                                            <input type=""text"" id=""EndDate"" class=""form-control"" readonly=""readonly"" onblur=""calltodatefun()"" onchange=""calltodatefun()"">
                                            <div class=""input-group-append"">
                                                <button class=""btn btn-outline-secondary"" type=""button""><i class=""fa fa-calendar""></i></button>
                                            </div>
                                        </div>
              ");
            WriteLiteral(@"                          <span style=""color:red;display:none;"" id=""spn_EndDate"">Please enter EndDate</span>
                                    </div>
                                </div>
                            </div>
                            <div class=""col-lg-4"">
                                <span>Level</span>
                                <select id=""ddl_Account"" class=""form-control"">
                                    ");
            EndContext();
            BeginContext(5202, 32, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "86bc527fbac349be8e14bd70150e78f9", async() => {
                BeginContext(5220, 5, true);
                WriteLiteral("Level");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5234, 38, true);
            WriteLiteral("\r\n                                    ");
            EndContext();
            BeginContext(5272, 44, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "379ed1bf965a458eb821460943ebc5e6", async() => {
                BeginContext(5290, 17, true);
                WriteLiteral("Major Group Level");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5316, 38, true);
            WriteLiteral("\r\n                                    ");
            EndContext();
            BeginContext(5354, 38, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1bc49c6d68ec474aa4267424c1fac8e4", async() => {
                BeginContext(5372, 11, true);
                WriteLiteral("Group Level");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5392, 38, true);
            WriteLiteral("\r\n                                    ");
            EndContext();
            BeginContext(5430, 42, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "951a75b0c463446bab0029dc31265897", async() => {
                BeginContext(5448, 15, true);
                WriteLiteral("Sub Group Level");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5472, 38, true);
            WriteLiteral("\r\n                                    ");
            EndContext();
            BeginContext(5510, 40, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c218231b522411ab1a2aeae1d7d01d5", async() => {
                BeginContext(5528, 13, true);
                WriteLiteral("Account Level");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5550, 434, true);
            WriteLiteral(@"
                                </select>
                            </div>
                            <div class=""col-lg-4"">
                                <span>&nbsp;</span><br />
                                <button type=""submit"" id=""btn_View"" title=""Save!"" class=""btn btn-primary"">View</button>
                                <div id=""divLoader"" class=""loading"" align=""center"">
                                    ");
            EndContext();
            BeginContext(5984, 76, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "7624cb4e3ff144d6858bb52b18597b4a", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6060, 42, true);
            WriteLiteral("\r\n                                </div>\r\n");
            EndContext();
            BeginContext(6188, 74, true);
            WriteLiteral("                                <input type=\"hidden\" id=\"hidLegalEntityID\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 6262, "\"", 6292, 1);
#line 115 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
WriteAttributeValue("", 6270, ViewBag.LegalEntityID, 6270, 22, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(6293, 75, true);
            WriteLiteral(" />\r\n                                <input type=\"hidden\" id=\"hidStartDate\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 6368, "\"", 6394, 1);
#line 116 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
WriteAttributeValue("", 6376, ViewBag.StartDate, 6376, 18, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(6395, 73, true);
            WriteLiteral(" />\r\n                                <input type=\"hidden\" id=\"hidEndDate\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 6468, "\"", 6492, 1);
#line 117 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
WriteAttributeValue("", 6476, ViewBag.EndDate, 6476, 16, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(6493, 77, true);
            WriteLiteral(" />\r\n                                <input type=\"hidden\" id=\"hidCurrentDate\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 6570, "\"", 6598, 1);
#line 118 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
WriteAttributeValue("", 6578, ViewBag.CurrentDate, 6578, 20, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(6599, 5, true);
            WriteLiteral(" />\r\n");
            EndContext();
#line 119 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
                                  
                                    string CompanyName = "";
                                    

#line default
#line hidden
#line 121 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
                                     foreach (var item in @ViewBag.GetCompany)
                                    {
                                        CompanyName = (@item.Name);
                                    }

#line default
#line hidden
            BeginContext(6929, 72, true);
            WriteLiteral("                                    <input type=\"hidden\" id=\"hidcompany\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 7001, "\"", 7042, 1);
#line 125 "D:\Dev-Env\lms-finance-05-01-2022\lms-finance\Accounting.Web\Views\TransactionEntry\AccountTrialBalance.cshtml"
WriteAttributeValue("", 7009, Html.DisplayFor(m =>CompanyName), 7009, 33, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(7043, 5, true);
            WriteLiteral(" />\r\n");
            EndContext();
            BeginContext(7085, 7541, true);
            WriteLiteral(@"
                            </div>
                        </div>
                        <div id=""ViewReport"" class=""row clearfix"">

                            <div id=""divViewReport"" class=""col-lg-12"">
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function ondatetypechangeIsDateRange() {
        var StartDate = $(""#StartDate"");
        var EndDate = $(""#EndDate"");
        var IsDateRange = $('#IsDateRange').is(':checked');
        var IsAsonDate = $('#IsAsonDate').is(':checked');
        //alert(IsDateRange);
        //alert(IsAsonDate);
        //$('#IsDateRange').removeAttr('checked');
        //$('#IsAsonDate').removeAttr('checked');
        $('#IsDateRange').prop('checked', false);
        $('#IsAsonDate').prop('checked', false);
        StartDate.val("""");
        EndDate.val("""");
        if (IsDateRange == true) {
            //$('#IsD");
            WriteLiteral(@"ateRange').attr('checked', 'checked');
            $('#IsAsonDate').prop('checked', false);
            $('#IsDateRange').prop('checked',true);
        }
        else  {
             //$('#IsDateRange').attr('checked', 'checked');
             $('#IsDateRange').prop('checked', false);
            $('#IsAsonDate').prop('checked',true);
        }
    }
    function ondatetypechangeIsAsonDate() {
        //alert(""sdasd"");
        var StartDate = $(""#StartDate"");
        var EndDate = $(""#EndDate"");
         var hidStartDate = $(""#hidStartDate"");
        var hidEndDate = $(""#hidEndDate"");
        var hidCurrentDate = $(""#hidCurrentDate"");
        var IsDateRange = $('#IsDateRange').is(':checked');
        var IsAsonDate = $('#IsAsonDate').is(':checked');
        //alert(IsDateRange);
        //alert(IsAsonDate);
        //$('#IsDateRange').removeAttr('checked');
        //$('#IsAsonDate').removeAttr('checked');
        $('#IsDateRange').prop('checked', false);
        $('#IsAsonDate').prop");
            WriteLiteral(@"('checked', false);
        if (IsAsonDate == true) {
            StartDate.val(hidStartDate.val());
            EndDate.val(hidCurrentDate.val());
            //$('#IsDateRange').attr('checked', 'checked');
            $('#IsAsonDate').prop('checked', true);
            $('#IsDateRange').prop('checked',false);
        }
        else  {
             //$('#IsDateRange').attr('checked', 'checked');
             $('#IsAsonDate').prop('checked', false);
            $('#IsDateRange').prop('checked',true);
        }
    }
      function getdate(str) {
            //alert(str.split(' ')[0]);
            var dat = str.split('-')
            return (dat[1] + ""-"" + dat[0] + ""-"" + dat[2]);
        }
      function getdate_PDF(str) {
            //alert(str.split(' ')[0]);
            var dat = str.split('-')
            return (dat[0] + ""-"" + dat[1] + ""-"" + dat[2]);
        }
      $(""#btn_View"").click(function () {
                if (fnValidate()) {
                     //var hidLegalEntity =");
            WriteLiteral(@" $(""#hidLegalEntity""),
                    //var StartDate = $(""#StartDate""),
                    //     EndDate = $(""#EndDate""),
                    //    data = {};
                    //// data[""LegalEntityID""] = parseInt(hidLegalEntity.val());
                    //data[""StartDate""] = getdate(StartDate.val());
                    //data[""EndDate""] = getdate(EndDate.val());
                    //$.ajax({
                    //    url: ""/TransactionEntry/ViewReport"",
                    //    type: ""POST"",
                    //    data: JSON.stringify(data),
                    //    contentType: ""application/json; charset=utf-8"",
                    //    dataType: ""json"",
                    //    success: function (msg) {
                    //       // window.location.href = ""/FinancialYear/Index"";
                    //    },
                    //    error: function (msg) {

                    //    }
                    //});
                    var hidLegalEntity = $(""#hidLegal");
            WriteLiteral(@"EntityID""),
                        ddl_Account = $(""#ddl_Account""),
                        StartDate = $(""#StartDate""),
                        EndDate = $(""#EndDate""),
                        hidcompany = $(""#hidcompany"");
                    $(document).prop('title', 'TRIAL BALANCE  OF ' + hidcompany.val() + ' - PERIOD:' + getdate_PDF(StartDate.val()) + ' To ' + getdate_PDF(EndDate.val()));
                    $(""#divViewReport"").empty();
                    //alert(hidLegalEntity.val());
                    $(""#divLoader"").show();
                    var modal, loading;
                    modal = document.createElement(""DIV"");
                    modal.className = ""modal"";
                    document.body.appendChild(modal);
                    loading = document.getElementsByClassName(""loading"")[0];
                    loading.style.display = ""block"";
                    var top = Math.max(window.innerHeight / 2 - loading.offsetHeight / 2, 0);
                    var left = Math.max(wi");
            WriteLiteral(@"ndow.innerWidth / 2 - loading.offsetWidth / 2, 0);
                    loading.style.top = top + ""px"";
                    loading.style.left = left + ""px"";
                    //alert(hidLegalEntity.val());
                    //$(""#divViewReport"").load(""/TransactionEntry/AccountTrialBalanceDateRange?LegalEntityID="" + hidLegalEntity.val() + ""&AccountLevel="" + ddl_Account.val() + ""&StartDate="" + getdate(StartDate.val()) + ""&EndDate="" + getdate(EndDate.val()));
                    $(""#divViewReport"").load(""/TransactionEntry/AccountTrialBalanceDateRange?LegalEntityID="" + hidLegalEntity.val() + ""&AccountLevel="" + ddl_Account.val() + ""&StartDate="" + getdate(StartDate.val()) + ""&EndDate="" + getdate(EndDate.val()), function (responseTxt, statusTxt, jqXHR) {
                        if (statusTxt == ""success"") {
                            $(""#divLoader"").hide();
                        }
                        if (statusTxt == ""error"") {
                            $(""#divLoader"").show();
               ");
            WriteLiteral(@"         }
                    });

                }
                else {
                    return false;
                }
            });
            function fnValidate() {

                event.preventDefault();
                var StartDate = $(""#StartDate""),
                    EndDate = $(""#EndDate""),
                    // ddl_LegalEntity = $(""#ddl_LegalEntity""),
                    isValid = true,
                    spn_StartDate = $(""#spn_StartDate""),
                    spn_EndDate = $(""#spn_EndDate"");
                //  spn_LegalEntity = $(""#spn_LegalEntity"");
                //spn_LegalEntity.hide();
                spn_StartDate.hide();
                spn_EndDate.hide();
                //if (ddl_LegalEntity.val() == 0) {
                //    spn_LegalEntity.show();
                //    isValid = false;
                //}
                if (StartDate.val() == """") {
                    spn_StartDate.show();
                    isValid = false;
            ");
            WriteLiteral(@"    }
                //  if (EndDate.val() == """") {
                //    spn_EndDate.show();
                //    isValid = false;
                //}

                return isValid;
            }
    // function ViewReport() {

    //    $(""#divViewReport"").empty();
    //    $(""#divViewReport"").load(""/TransactionEntry/ViewReport/"");
    //}
</script>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Accounting.Web.Models.ViewModels.TransactionEntryViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
