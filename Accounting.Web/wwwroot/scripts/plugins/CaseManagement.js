﻿function fnAddCase() {
    var divCaseTable = $("#divCaseTable"),
        divcreatecase = $("#divcreatecase"),
        ddl_Customer = $("#ddl_Customer"),
        lbl_caseAccountno = $("#lbl_caseAccountno"),
        ddl_Product = $("#ddl_Product"),
        txt_startdate = $("#txt_startdate"),
        ddl_Case = $("#ddl_Case"),
        lbl_duration = $("#lbl_duration"),
        lbl_ishrsDays = $("#lbl_ishrsDays"),
        txt_completiondate = $("#txt_completiondate");

    divCaseTable.hide();
    ddl_Customer.val(0);
    ddl_Product.val(0);
    ddl_Case.html("");
    ddl_Case.append($('<option></option>').val(0).html("Select case name"));
    lbl_caseAccountno.text("N/A");
    txt_startdate.val("");
    lbl_duration.text("N/A");
    lbl_ishrsDays.text("");
    txt_completiondate.val("");
    divcreatecase.show();

}
function fnBindCase() {
    var ddl_Product = $("#ddl_Product");
    if (ddl_Product.val() != 0) {
        $.ajax({
            url: "/Casemng_Case/GetCaseListByProductID/" + ddl_Product.val(),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                $("#ddl_Case").html("");
                $("#lbl_duration").val("N/A");
                $("#lbl_ishrsDays").val("");
                $("#txt_completiondate").val("");
                $("#lbl_caseAccountno").text("N/A");
                $("#txt_startdate").val("");
                $("#ddl_Case").append($('<option></option>').val(0).html("Select Case Name"));
                $.each(msg, function (x, v) {
                    $("#ddl_Case").append($('<option></option>').val(v.caseManagementid).html(v.caseManagementName));
                })
            },

            failure: function (response) {

            },
            error: function (response) {

            }
        });
    }
}
function fnBindCaseDetails() {
    var ddl_Product = $("#ddl_Product"),
        ddl_Case = $("#ddl_Case"),
        lbl_caseAccountno = $("#lbl_caseAccountno"),
        lbl_duration = $("#lbl_duration"),
        lbl_ishrsDays = $("#lbl_ishrsDays"),
        txt_startdate = $("#txt_startdate"),
        txt_completiondate = $("#txt_completiondate"),
        currentdate = "",
        CaseCount = "",
        Caseno = "";

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    currentdate = (day < 10 ? '0' : '') + day + (month < 10 ? '0' : '') + month + d.getFullYear();
    $.ajax({
        url: "/Casemng_Case/GetCaseDetailsBycaseID?ID=" + ddl_Case.val() + "&ProductID=" + ddl_Product.val(),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            Caseno = "SKD/" + msg.productManagementUniqID + "/" + msg.caseManagementUniqID + "/" + currentdate + "/" + msg.count;
            lbl_caseAccountno.text(Caseno);
            lbl_duration.text(msg.duration);
            lbl_ishrsDays.text(msg.isDaysHours ? "Hrs" : "Days");
            if (txt_startdate.val() != "") {
                var today_date = new Date(txt_startdate.val());
                var month = today_date.getMonth() + 1;
                var time = "";
                if (lbl_ishrsDays.text() == "Days") {
                    day = today_date.getDate() + parseInt(lbl_duration.text());
                    time = today_date.getHours() + ":" + today_date.getMinutes();
                }
                else {
                    day = today_date.getDate();
                    time = today_date.getHours() + parseInt(lbl_duration.text()) + ":" + today_date.getMinutes();
                }

                var output = (month < 10 ? '0' : '') + month + '/' +
                    (day < 10 ? '0' : '') + day + '/' +
                    today_date.getFullYear() + ':' + time;
                txt_completiondate.val(output);
            }
        },

        failure: function (response) {

        },
        error: function (response) {

        }
    });
}
function fnSetCompletiondate() {
    var txt_startdate = $("#txt_startdate"),
        lbl_duration = $("#lbl_duration"),
        lbl_ishrsDays = $("#lbl_ishrsDays"),
        txt_completiondate = $("#txt_completiondate"),
        day = "",
        time = "";
    var today_date = new Date(txt_startdate.val());
    var month = today_date.getMonth() + 1;
    if (lbl_ishrsDays.text() == "Days") {
        day = today_date.getDate() + parseInt(lbl_duration.text());
        time = today_date.getHours() + ":" + today_date.getMinutes();
    }
    else {
        day = today_date.getDate();
        time = today_date.getHours() + parseInt(lbl_duration.text()) + ":" + today_date.getMinutes();
    }

    var output = (month < 10 ? '0' : '') + month + '/' +
        (day < 10 ? '0' : '') + day + '/' +
        today_date.getFullYear() + ' ' + time;
    txt_completiondate.val(output);
}
function fnSave() {
    if (fnValidate()) {
        var ddl_Customer = $("#ddl_Customer"),
            ddl_Product = $("#ddl_Product"),
            ddl_Case = $("#ddl_Case"),
            lbl_caseAccountno = $("#lbl_caseAccountno"),
            txt_startdate = $("#txt_startdate"),
            txt_completiondate = $("#txt_completiondate"),
            data = {};
        data["caseMngAccountNo"] = lbl_caseAccountno.text();
        data["customerID"] = ddl_Customer.val();
        data["productID"] = ddl_Product.val();
        data["caseID"] = ddl_Case.val();
        data["startDate"] = txt_startdate.val();
        data["completionDate"] = txt_completiondate.val();

        $.ajax({
            url: "/Casemng_Case/SaveCase",
            type: "POST",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                $("#ddl_Case").html("");
                $("#lbl_duration").val("N/A");
                $("#lbl_ishrsDays").val("");
                $("#txt_completiondate").val("");
                $("#lbl_caseAccountno").text("N/A");
                $("#txt_startdate").val("");
                $("#ddl_Case").append($('<option></option>').val(0).html("Select Case Name"));
                $.each(msg, function (x, v) {
                    $("#ddl_Case").append($('<option></option>').val(v.caseManagementid).html(v.caseManagementName));
                })
            },

            failure: function (response) {

            },
            error: function (response) {

            }
        });
    }
    else {
        alert("Fill all the fields");
    }
}
function fnValidate() {
    var ddl_Customer = $("#ddl_Customer"),
        ddl_Product = $("#ddl_Product"),
        ddl_Case = $("#ddl_Case"),
        lbl_caseAccountno = $("#lbl_caseAccountno"),
        txt_startdate = $("#txt_startdate"),
        txt_completiondate = $("#txt_completiondate"),
        isvalid = true;
    if (ddl_Customer.val() == "0" || ddl_Product.val() == "0" || ddl_Case.val() == "0" || txt_startdate.val() == "0") {
        isvalid = false;
    }
    return isvalid;
}
function fnLoadCasemngEditView(ID) {
    var divCaseTable = $("#divCaseTable"),
        divcreatecase = $("#divcreatecase"),
        ddl_Customer = $("#ddl_Customer"),
        lbl_caseAccountno = $("#lbl_caseAccountno"),
        ddl_Product = $("#ddl_Product"),
        txt_startdate = $("#txt_startdate"),
        ddl_Case = $("#ddl_Case"),
        lbl_duration = $("#lbl_duration"),
        lbl_ishrsDays = $("#lbl_ishrsDays"),
        txt_completiondate = $("#txt_completiondate");

    divCaseTable.hide();
    ddl_Customer.val(0);
    ddl_Product.val(0);
    ddl_Case.html("");
    ddl_Case.append($('<option></option>').val(0).html("Select case name"));
    lbl_caseAccountno.text("N/A");
    txt_startdate.val("");
    lbl_duration.text("N/A");
    lbl_ishrsDays.text("");
    txt_completiondate.val("");
    //divcreatecase.show();
    $.ajax({
        url: "/Casemng_Case/EditCaseManagementID/" + ID,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#ddl_Case").html("");
            $("#lbl_duration").val("N/A");
            $("#lbl_ishrsDays").val("");
            $("#txt_completiondate").val("");
            $("#lbl_caseAccountno").text("N/A");
            $("#txt_startdate").val("");
            $("#ddl_Case").append($('<option></option>').val(0).html("Select Case Name"));
            $.each(msg, function (x, v) {
                $("#ddl_Case").append($('<option></option>').val(v.caseManagementid).html(v.caseManagementName));
            })
        },

        failure: function (response) {

        },
        error: function (response) {

        }
    });
}