﻿/* workforce Controller's script Starts */
function fnAddEmployee() {
    $("#divempContent").empty();
    $("#divempContent").load("/WorkForceEmployee/AddEmployee");
}
function fnLoadLeadView(Id) {
    window.location = "/Lead/ViewLeadDetails?Id=" + Id;
}
function fnCalculateAge() {
    var result = "label[for='objWorkForceEmployeeModel_Age']";
    var WorkForceEmployeeModel_BirthDate = $('#objWorkForceEmployeeModel_BirthDate');
    var obj = WorkForceEmployeeModel_BirthDate.val();
    var tmpAge = obj.split('/');
    if (tmpAge.length != 3) { return false; }

    var currentDate = new Date();
    var birthDate = new Date(tmpAge[2] + "-" + tmpAge[1] + "-" + tmpAge[0]);

    var Months = currentDate.getMonth() - birthDate.getMonth();
    var Years = currentDate.getFullYear() - birthDate.getFullYear();
    var Days = currentDate.getDate() - birthDate.getDate();
    Months = (currentDate.getMonth() + 12 * currentDate.getFullYear()) -
        (birthDate.getMonth() + 12 * birthDate.getFullYear());
    var MonthOverflow = 0;
    if (Months - (Years * 12) < 0)
        MonthOverFlow = -1;
    else
        MonthOverFlow = 1;
    if (MonthOverFlow < 0)
        Years = Years - 1; Months = Months - (Years * 12);
    var LastDayOfMonth = new Date(currentDate.getFullYear(),
        currentDate.getMonth() + 1, 0, 23, 59, 59);
    LastDayOfMonth = LastDayOfMonth.getDate();
    if (MonthOverFlow < 0 && (birthDate.getDate() > currentDate.getDate())) {
        Days = LastDayOfMonth + (currentDate.getDate() - birthDate.getDate()) - 1;
    }
    else
        Days = currentDate.getDate() - birthDate.getDate();
    if (Days < 0)
        Months = Months - 1;
    var l = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0);
    var l1 = new Date(birthDate.getFullYear(), birthDate.getMonth() + 1, 0);
    if (Days < 0) {
        if (l1 > l)
            Days = l1.getDate() + Days;
        else
            Days = l.getDate() + Days;
    }
    //$("#" + result).text(Years + "Year(s), " + Months + " Month(s), " + Days + "Day(s)");
    $('' + result + '').text(Years + "Year(s), " + Months + " Month(s)");
}

function fnDeleteEmployee(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/WorkForceEmployee/DeleteEmployee/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/WorkForceEmployee/WorkForceEmployeeModule";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}

function fnDeletpopupEmployee(Id) {

    $.ajax({
        url: "/WorkForceEmployee/DeleteEmployee/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/WorkForceEmployee/WorkForceEmployeeModule";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
    $("#DivModelDeleteContent").load("/WorkForceEmployee/DeleteEmployee/" + Id);
}

function fngotoEmp() {
    if (fnEmployeeValidationPersonal()) {
        $(".nav-item").removeClass("active");
        $("#liPerson").removeClass("active");
        $("#liEmployee").addClass("active");
        $("#tab2").addClass("active");
        $("#tab1").removeClass("active");
    }
}

function fnEmployeeValidationPersonal() {
    var objWorkForceEmployeeModel_FirstName = $("#objWorkForceEmployeeModel_FirstName"),
        objWorkForceEmployeeModel_LastName = $("#objWorkForceEmployeeModel_LastName"),
        objWorkForceEmployeeModel_Gender = $("#objWorkForceEmployeeModel_Gender"),
        objWorkForceEmployeeModel_BirthDate = $("#objWorkForceEmployeeModel_BirthDate"),
        objWorkForceEmployeeModel_MobileNo = $("#objWorkForceEmployeeModel_MobileNo"),
        objWorkForceEmployeeModel_MaritalStatus = $("#objWorkForceEmployeeModel_MaritalStatus"),
        objWorkForceEmployeeModel_PANNo = $("#objWorkForceEmployeeModel_PANNo"),
        objWorkForceEmployeeModel_AadharNo = $("#objWorkForceEmployeeModel_AadharNo"),
        objWorkForceEmployeeModel_Address = $("#objWorkForceEmployeeModel_Address"),
        isvalid = true;
    if (objWorkForceEmployeeModel_FirstName.val() == "") {
        objWorkForceEmployeeModel_FirstName.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_LastName.val() == "") {
        objWorkForceEmployeeModel_LastName.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_Gender.val() == "") {
        objWorkForceEmployeeModel_Gender.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_BirthDate.val() == "") {
        objWorkForceEmployeeModel_BirthDate.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_MobileNo.val() == "") {
        objWorkForceEmployeeModel_MobileNo.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_MaritalStatus.val() == "") {
        objWorkForceEmployeeModel_MaritalStatus.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_MaritalStatus.val() == "") {
        objWorkForceEmployeeModel_MaritalStatus.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_PANNo.val() == "") {
        objWorkForceEmployeeModel_PANNo.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_AadharNo.val() == "") {
        objWorkForceEmployeeModel_AadharNo.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_Address.val() == "") {
        objWorkForceEmployeeModel_Address.valid();
        isvalid = false;
    }
    return isvalid;
}



function fngotoBank() {
    if (fnEmployeeValidationEmp()) {
        $(".nav-item").removeClass("active");
        $("#liEmployee").removeClass("active");
        $("#liBank").addClass("active");
        $("#tab3").addClass("active");
        $("#tab2").removeClass("active");
    }
}

function fnEmployeeValidationEmp() {
    var objWorkForceEmployeeModel_JoiningDate = $("#objWorkForceEmployeeModel_JoiningDate"),
        objWorkForceEmployeeModel_EmployeeCode = $("#objWorkForceEmployeeModel_EmployeeCode"),
        objWorkForceEmployeeModel_EmergencyContactNumber = $("#objWorkForceEmployeeModel_EmergencyContactNumber"),
        objWorkForceEmployeeModel_Designation = $("#objWorkForceEmployeeModel_Designation"),
        objWorkForceEmployeeModel_Level = $("#objWorkForceEmployeeModel_Level"),
        objWorkForceEmployeeModel_ReportingManager = $("#objWorkForceEmployeeModel_ReportingManager"),
        objWorkForceEmployeeModel_service_area = $("#objWorkForceEmployeeModel_service_area"),
        isvalid = true;
    if (objWorkForceEmployeeModel_JoiningDate.val() == "") {
        objWorkForceEmployeeModel_JoiningDate.valid();
        isvalid = false;
    }

    if (objWorkForceEmployeeModel_EmergencyContactNumber.val() == "") {
        objWorkForceEmployeeModel_EmergencyContactNumber.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_Designation.val() == "") {
        objWorkForceEmployeeModel_Designation.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_Level.val() == "") {
        objWorkForceEmployeeModel_Level.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_ReportingManager.val() == "") {
        objWorkForceEmployeeModel_ReportingManager.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_service_area.val() == "") {
        objWorkForceEmployeeModel_service_area.valid();
        isvalid = false;
    }
    return isvalid;
}

function fngobackEmp() {
    $(".nav-item").removeClass("active");
    $("#liEmployee").removeClass("active");
    $("#liPerson").addClass("active");
    $("#tab1").addClass("active");
    $("#tab2").removeClass("active");
}

function fngobackemp() {
    $(".nav-item").removeClass("active");
    $("#liBank").removeClass("active");
    $("#liEmployee").addClass("active");
    $("#tab2").addClass("active");
    $("#tab3").removeClass("active");


}

function fnSave() {
    debugger;
    if (fnValidateBank()) {
        var data = {},
            objWorkForceEmployeeModel_FirstName = $("#objWorkForceEmployeeModel_FirstName"),
            objWorkForceEmployeeModel_LastName = $("#objWorkForceEmployeeModel_LastName"),
            objWorkForceEmployeeModel_Gender = $("#objWorkForceEmployeeModel_Gender"),
            objWorkForceEmployeeModel_BirthDate = $("#objWorkForceEmployeeModel_BirthDate"),
            objWorkForceEmployeeModel_MobileNo = $("#objWorkForceEmployeeModel_MobileNo"),
            objWorkForceEmployeeModel_MaritalStatus = $("#objWorkForceEmployeeModel_MaritalStatus"),
            objWorkForceEmployeeModel_PANNo = $("#objWorkForceEmployeeModel_PANNo"),
            objWorkForceEmployeeModel_AadharNo = $("#objWorkForceEmployeeModel_AadharNo"),
            objWorkForceEmployeeModel_Address = $("#objWorkForceEmployeeModel_Address"),
            objWorkForceEmployeeModel_JoiningDate = $("#objWorkForceEmployeeModel_JoiningDate"),
            objWorkForceEmployeeModel_EmployeeCode = $("#objWorkForceEmployeeModel_EmployeeCode"),
            objWorkForceEmployeeModel_EmergencyContactNumber = $("#objWorkForceEmployeeModel_EmergencyContactNumber"),
            objWorkForceEmployeeModel_Designation = $("#objWorkForceEmployeeModel_Designation"),
            objWorkForceEmployeeModel_Level = $("#objWorkForceEmployeeModel_Level"),
            objWorkForceEmployeeModel_ReportingManager = $("#objWorkForceEmployeeModel_ReportingManager"),
            objWorkForceEmployeeModel_service_area = $("#objWorkForceEmployeeModel_service_area"),
            objWorkForceEmployeeModel_ACName = $("#objWorkForceEmployeeModel_ACName "),
            objWorkForceEmployeeModel_ACno = $("#objWorkForceEmployeeModel_ACno"),
            objWorkForceEmployeeModel_BankName = $("#objWorkForceEmployeeModel_BankName"),
            objWorkForceEmployeeModel_Branch = $("#objWorkForceEmployeeModel_Branch"),
            objWorkForceEmployeeModel_IFSCCode = $("#objWorkForceEmployeeModel_IFSCCode"),
            objWorkForceEmployeeModel_FatherName = $("#objWorkForceEmployeeModel_FatherName"),
            objWorkForceEmployeeModel_Email = $("#objWorkForceEmployeeModel_Email"),
            objWorkForceEmployeeModel_BloodGroup = $("#objWorkForceEmployeeModel_BloodGroup"),
            objWorkForceEmployeeModel_OfficalPhoneno = $("#objWorkForceEmployeeModel_OfficalPhoneno"),
            objWorkForceEmployeeModel_IFSCCode = $("#objWorkForceEmployeeModel_IFSCCode");

        data["FirstName"] = objWorkForceEmployeeModel_FirstName.val();
        data["LastName"] = objWorkForceEmployeeModel_LastName.val();
        data["Gender"] = parseInt(objWorkForceEmployeeModel_Gender.val());
        data["BirthDate"] = objWorkForceEmployeeModel_BirthDate.val();
        data["MaritalStatus"] = parseInt(objWorkForceEmployeeModel_MaritalStatus.val());

        data["FatherName"] = objWorkForceEmployeeModel_FatherName.val();
        data["MobileNo"] = objWorkForceEmployeeModel_MobileNo.val();
        data["Email"] = objWorkForceEmployeeModel_Email.val();

        data["AadharNo"] = objWorkForceEmployeeModel_AadharNo.val();
        data["PANNo"] = objWorkForceEmployeeModel_PANNo.val();
        data["Address"] = objWorkForceEmployeeModel_Address.val();

        data["BloodGroup"] = objWorkForceEmployeeModel_BloodGroup.val();

        data["EmployeeCode"] = objWorkForceEmployeeModel_EmployeeCode.val();
        data["JoiningDate"] = objWorkForceEmployeeModel_JoiningDate.val();
        data["Designation"] = parseInt(objWorkForceEmployeeModel_Designation.val());
        data["Department"] = parseInt(objWorkForceEmployeeModel_Designation.val());
        data["Level"] = parseInt(objWorkForceEmployeeModel_Designation.val());
        data["ReportingManager"] = objWorkForceEmployeeModel_ReportingManager.val();

        data["OfficalPhoneno"] = objWorkForceEmployeeModel_OfficalPhoneno.val();
        data["EmergencyContactNumber"] = objWorkForceEmployeeModel_EmergencyContactNumber.val();

        data["service_area"] = parseInt(objWorkForceEmployeeModel_service_area.val());
        data["ACName"] = objWorkForceEmployeeModel_ACName.val();
        data["BankName"] = objWorkForceEmployeeModel_BankName.val();
        data["Branch"] = objWorkForceEmployeeModel_Branch.val();
        data["ACno"] = objWorkForceEmployeeModel_ACno.val();
        data["IFSCCode"] = objWorkForceEmployeeModel_IFSCCode.val();

        $.ajax({
            url: "/WorkForceEmployee/SaveEmp",
            type: "POST",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg = "OK") {
                    $(".nav-item").removeClass("active");
                }

            },

            failure: function (response) {

            },
            error: function (response) {

            }
        });
    }
    else {
        // alert("Fill all the fields");
    }
}


function fnValidateBank() {
    var objWorkForceEmployeeModel_ACName = $("#objWorkForceEmployeeModel_ACName "),
        objWorkForceEmployeeModel_ACno = $("#objWorkForceEmployeeModel_ACno"),
        objWorkForceEmployeeModel_BankName = $("#objWorkForceEmployeeModel_BankName"),
        objWorkForceEmployeeModel_Branch = $("#objWorkForceEmployeeModel_Branch"),
        objWorkForceEmployeeModel_IFSCCode = $("#objWorkForceEmployeeModel_IFSCCode"),
        isvalid = true;
    if (objWorkForceEmployeeModel_ACName.val() == "") {
        objWorkForceEmployeeModel_ACName.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_ACno.val() == "") {
        objWorkForceEmployeeModel_ACno.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_BankName.val() == "") {
        objWorkForceEmployeeModel_BankName.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_Branch.val() == "") {
        objWorkForceEmployeeModel_Branch.valid();
        isvalid = false;
    }
    if (objWorkForceEmployeeModel_IFSCCode.val() == "") {
        objWorkForceEmployeeModel_IFSCCode.valid();
        isvalid = false;
    }
    return isvalid;
}