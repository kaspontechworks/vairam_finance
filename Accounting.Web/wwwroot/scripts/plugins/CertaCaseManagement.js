﻿
var UserOptions = "";
fnGetUserList();

/* -- Add Case Button Model Start -- */

function fnAddCase() {
    $("#divCaseTable").hide();
    $("#divCaseContent").show();
    $("#divCaseView").empty();
    $("#divCaseView").load("/CertaCaseManagement/GetCaseManagement");
}
/* -- End -- */



/* -- Bind Case Dropdown Based On Selectd Product Starts -- */
function fnBindCase() {
    var ddl_Product = $("#ddl_Product");
    if (ddl_Product.val() != 0) {
        $.ajax({
            url: "/CertaCaseManagement/BindCaseByProductID/" + ddl_Product.val(),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                $("#ddl_Case").html("");
                $("#lbl_duration").val("N/A");
                $("#lbl_ishrsDays").val("");
                $("#txt_completiondate").val("");
                $("#lbl_caseAccountno").text("N/A");
                $("#txt_startdate").val("");
                $("#ddl_Case").append($('<option></option>').val(0).html("Select Case Name"));
                $.each(msg, function (x, v) {
                    $("#ddl_Case").append($('<option></option>').val(v.caseID).html(v.caseName));
                })
            },

            failure: function (response) {

            },
            error: function (response) {

            }
        });
    }
}
/* -- Ends -- */



/* -- Bind Case Details based On Selectd Case Starts -- */
function fnBindCaseDetails() {
    var ddl_Product = $("#ddl_Product"),
        ddl_Case = $("#ddl_Case"),
        lbl_caseAccountno = $("#lbl_caseAccountno"),
        lbl_duration = $("#lbl_duration"),
        lbl_ishrsDays = $("#lbl_ishrsDays"),
        txt_startdate = $("#txt_startdate"),
        txt_completiondate = $("#txt_completiondate"),
        currentdate = "",
        CaseCount = "",
        Caseno = "";

    var d = new Date();
    var month = d.getMonth() + 1;
    var year = "";
    var day = d.getDate();
    currentdate = (day < 10 ? '0' : '') + day + (month < 10 ? '0' : '') + month + d.getFullYear();
    $.ajax({
        url: "/CertaCaseManagement/BindCaseDetailsByCaseIDAndProductID?CaseID=" + ddl_Case.val() + "&ProductID=" + ddl_Product.val(),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            Caseno = "SKD/" + msg.productManagementUniqID + "/" + msg.caseManagementUniqID + "/" + currentdate + "/" + msg.count;
            lbl_caseAccountno.text(Caseno);
            lbl_duration.text(msg.duration);
            lbl_ishrsDays.text(msg.isDaysHours ? "Hrs" : "Days");
            if (txt_startdate.val() != "") {
                var today_date = new Date(txt_startdate.val());
                var month = today_date.getMonth() + 1;
                var newdate = new Date(Date.parse(today_date));
                //var time = "";
                if (lbl_ishrsDays.text() == "Days") {

                    newdate.setDate(newdate.getDate() + parseInt(lbl_duration.text()));
                    day = newdate.getDate();
                    month = newdate.getMonth() + 1;
                    year = newdate.getFullYear();
                    //day = today_date.getDate() + parseInt(lbl_duration.text());
                    //time = today_date.getHours() + ":" + today_date.getMinutes();
                }
                else {

                    day = newdate.getDate();
                    month = newdate.getMonth() + 1;
                    year = newdate.getFullYear();
                    //time = today_date.getHours() + parseInt(lbl_duration.text()) + ":" + today_date.getMinutes();
                }

                var output = (month < 10 ? '0' : '') + month + '/' +
                    (day < 10 ? '0' : '') + day + '/' +
                    year;
                //var output = (month < 10 ? '0' : '') + month + '/' +
                //    (day < 10 ? '0' : '') + day + '/' +
                //    today_date.getFullYear() + ':' + time;
                txt_completiondate.val(output);
            }
        },

        failure: function (response) {

        },
        error: function (response) {

        }
    });
}
/* -- Ends -- */



/* -- Calculate Case Complete Date Starts -- */
function fnCalculateCompletiondate() {
    var txt_startdate = $("#txt_startdate"),
        lbl_duration = $("#lbl_duration"),
        lbl_ishrsDays = $("#lbl_ishrsDays"),
        txt_completiondate = $("#txt_completiondate"),
        day = "";
    if (lbl_duration.text() != "N/A") {
        var today_date = new Date(txt_startdate.val());
        var month = today_date.getMonth() + 1;
        var newdate = new Date(Date.parse(today_date));
        if (lbl_ishrsDays.text() == "Days") {

            newdate.setDate(newdate.getDate() + parseInt(lbl_duration.text()));
            day = newdate.getDate();
            month = newdate.getMonth() + 1;
            year = newdate.getFullYear();
        }
        else {

            day = newdate.getDate();
            month = newdate.getMonth() + 1;
            year = newdate.getFullYear();
        }

        var output = (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + day + '/' +
            year;
        txt_completiondate.val(output);
    }
}
/* -- Ends -- */



/* -- Case Next Button Function Starts -- */
function fngotoEvidentiaryFromCase() {
    if (ValidateCaseDetails()) {
        $(".nav-item").removeClass("active");
        $("#Case").removeClass("active");
        $("#liEvidentiary").addClass("active");
        $("#Evidentiary").addClass("active");
    }
    else {
        alert("Fill all the fields");
    }

}
/* -- Case Nsext Button Validation --*/
function ValidateCaseDetails() {
    var ddl_Customer = $("#ddl_Customer"),
        ddl_Product = $("#ddl_Product"),
        txt_startdate = $("#txt_startdate"),
        ddl_Case = $("#ddl_Case"),
        txt_completiondate = $("#txt_completiondate"),
        isvalid = true;
    if (ddl_Customer.val() == 0) {
        isvalid = false;
    }
    if (ddl_Product.val() == 0) {
        isvalid = false;
    }
    if (txt_startdate.val() == "") {
        isvalid = false;
    }
    if (ddl_Case.val() == 0) {
        isvalid = false;
    }
    if (txt_completiondate.val() == "") {
        isvalid = false;
    }
    return isvalid;

}
/* -- Ends -- */



/* -- Evidentiary Previous Button Function Starts -- */
function fngotoCase() {
    $(".nav-item").removeClass("active");
    $("#Evidentiary").removeClass("active");
    $("#liCase").addClass("active");
    $("#Case").addClass("active");
}
/* -- Ends -- */



/* -- Evidentiary Next Button Function Starts -- */
function fngotoAssignee() {
    if (ValidateEvidentiaryDetails()) {
        var $rows1 = $("#tbl_Evidentiary").find('tr:not(:hidden)');
        $(".nav-item").removeClass("active");
        $("#Evidentiary").removeClass("active");
        $("#liAssignee").addClass("active");
        $("#Assignee").addClass("active");
        $("#div_Assignee").empty();

        var $tds = $("#tbl_Evidentiary tr");
        var headers = [];
        var data = {};
        var fields = [];
        headers.push("Evidentiary");
        headers.push("Template");
        headers.push("Assignee");
        headers.push("ServiceArea");
        var strHTML = '<table id="tbl_Assignee" class="table v-middle p-0 m-0 box">' +
            '<thead ><tr>' +
            '<th>Evidentiary</th>' +
            '<th>Template</th>' +
            '<th>Assignee</th>' +
            '<th>Service Area</th>' +
            '</tr > </thead>'
        '<tbody><tr>';
        $rows1.each(function () {
            var $td = $(this).find('td');
            var $td3 = $td.find('select');
            var h2 = {};

            headers.forEach(function (header, i) {
                if (header == "Evidentiary") {
                    strHTML += '<td><span>' + $td.eq(i).find('select option:selected').html() + '</span>' +
                        '</td > ';
                }
                else if (header == "Template") {
                    strHTML += '<td><span>' + $td.eq(i).find('select option:selected').html() + '</span>' +
                        '</td > ';
                }
                else if (header == "Assignee") {
                    strHTML += '<td>' +
                        '<select id="ddl_User" class="form-control User" onchange="fnGetservice(this)">' +
                        '<option value = "0" > - Select -</option > ';
                    $.each(UserOptions, function (index, item) {
                        strHTML += '<option value=' + item.employeeID + ' >' + item.firstName + ' ' + item.lastName + '</option>';
                    });
                    strHTML += '</select > ' +
                        '</td > ';
                }
                else if (header == "ServiceArea") {
                    strHTML += '<td>' +
                        '<select id="ddl_ServiceArea" class="form-control Area"><option value="0">- Select -</option></select>' +
                        '</td > ';
                }
            });
            strHTML += '</tr>'
        });
        strHTML += '</tbody></table>';
        $("#div_Assignee").append(strHTML);
    }
    else {
        alert("Fill all the fileds");
    }

}
function ValidateEvidentiaryDetails() {
    var IsSelectedEvi = true;
    var IsSelectedTem = true;
    var isvalid = true;
    $(".evidentiary").each(function () {
        $(this).val() == 0 ? IsSelectedEvi = false : '';
    });
    $(".template").each(function () {
        $(this).val() == 0 ? IsSelectedTem = false : '';
    });
    if (IsSelectedEvi == false || IsSelectedTem == false) {
        isvalid = false;
    }
    return isvalid;
}
function fnGetUserList() {
    var strOption = "";
    $.ajax({
        url: "/CertaCaseManagement/BindUserList",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            UserOptions = msg;
        }
    });

}
function fnGetservice(d) {
    var selectedValue = $(d).val();
    var row = $(d).closest('tr');
    var stateSelect = row.find('.Area');
    stateSelect.append($('<option></option>').val(1).text("Service Area 1"));
    $.ajax({
        //url: "/CertaCaseManagement/BindServiceArea/" + selectedValue,
        url: "/CertaCaseManagement/BindServiceArea",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            stateSelect.empty();
            stateSelect.append($('<option></option>').val(0).text("- Select -"));
            $.each(data, function (index, item) {
                stateSelect.append($('<option></option>').val(item.areaID).text(item.areaName));
            });
        }
    });
}
/* -- Ends -- */


/* -- Assignee Previous Button Function Starts -- */
function fngotoEvidentiaryFromAssignee() {
    $(".nav-item").removeClass("active");
    $("#Assignee").removeClass("active");
    $("#liEvidentiary").addClass("active");
    $("#Evidentiary").addClass("active");
}
/* -- Ends -- */



/* -- Add New Row In The Evidentiary Tab Starts -- */
function fnAddRow(obj) {

    var tds = '<tr>';
    jQuery.each($('tr:last td', $("#tbl_Evidentiary")), function () {
        tds += '<td>' + $(this).val(0).html() + '</td>';
    });
    tds += '</tr>';
    if ($('tbody', this).length > 0) {
        $('tbody', this).append(tds);
    } else {
        $("#tbl_Evidentiary").append(tds);
    }
}
/* -- Ends -- */



function fnSaveData() {
    var ddl_Customer = $("#ddl_Customer"),
        ddl_Product = $("#ddl_Product"),
        ddl_Case = $("#ddl_Case"),
        lbl_caseAccountno = $("#lbl_caseAccountno"),
        txt_startdate = $("#txt_startdate"),
        txt_completiondate = $("#txt_completiondate"),
        objCase_CaseManagementID = $("#objCase_CaseManagementID"),
        Evidentiaryheaders = [],
        Assigneeheaders = [],
        data = {},
        objCase = {},
        objEvidentiary = [],
        objAssignee = [];
    Evidentiaryheaders.push("Evidentiary");
    Evidentiaryheaders.push("Template");
    Evidentiaryheaders.push("Notes");

    Assigneeheaders.push("Evidentiary");
    Assigneeheaders.push("Template");
    Assigneeheaders.push("AssigneeID");
    Assigneeheaders.push("ServiceAreaID");

    objCase["caseMngAccountNo"] = lbl_caseAccountno.text();
    objCase["customerID"] = ddl_Customer.val();
    objCase["productID"] = ddl_Product.val();
    objCase["caseID"] = ddl_Case.val();
    objCase["startDate"] = txt_startdate.val();
    objCase["completionDate"] = txt_completiondate.val();
    objCase["CaseManagementID"] = objCase_CaseManagementID.val();
    data["objCase"] = objCase;

    var $rows2 = $("#tbl_Evidentiary > tbody > tr");
    $rows2.each(function () {
        var $td = $(this).find('td');
        var $td2 = $td.find('textarea');
        var $td3 = $td.find('select,input');
        var fields = {};

        Evidentiaryheaders.forEach(function (header, i) {
            if (header != "Notes") {
                fields[header] = $td3.eq(i).val();
                if ($td3.eq(i).val() === undefined) {
                    debugger;
                    fields[header] = $td.eq(i).find('input,select').val() || $td.eq(i).find('textarea').text() || $td.eq(i).text();
                }
            }
            else {
                fields[header] = $td2.eq(i).prevObject["0"].value;
            }


        });
        objEvidentiary.push(fields);
    });
    data["objEvidentiary"] = objEvidentiary;
    var $rows1 = $("#tbl_Assignee > tbody > tr");
    $rows1.each(function () {
        var $td = $(this).find('td');
        var $td2 = $td.find('span');
        var $td3 = $td.find('select,input');
        var fields = {};

        Assigneeheaders.forEach(function (header, i) {
            if ((header != "Evidentiary" && header != "Template")) {
                fields[header] = $td3.eq(i).val();
                if ($td3.eq(i).val() === undefined) {
                    fields[header] = $td.eq(i).find('input,select').val() || $td.eq(i).find('textarea').text() || $td.eq(i).text();
                }
            }
            else {
                fields[header] = $td2.eq(i).text();
            }


        });
        objAssignee.push(fields);
    });
    data["objAssignee"] = objAssignee;
    $.ajax({
        url: "/CertaCaseManagement/SaveData",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = '/CertaCaseManagement/Index';
            }
        },

        failure: function (response) {

        },
        error: function (response) {

        }
    });
}






/* -- Edit CaseManagement Starts--*/

function fnLoadCasemngEditView(Id) {
    $("#divCaseTable").hide();
    $("#divCaseContent").show();
    $("#divCaseView").empty();
    $("#divCaseView").load("/CertaCaseManagement/EditCaseManagement/" + Id);
}

/* -- Edit CaseManagement Ends--*/

/* -- View CaseManagement Starts--*/

function fnLoadCasemngView(Id) {
    $("#divCaseTable").hide();
    $("#divCaseContent").show();
    $("#divCaseView").empty();
    $("#divCaseView").load("/CertaCaseManagement/ViewCaseManagement/" + Id);
}


function fnLoadEvidentiaryAssigneeImage(Evidentiary, AssigneeID) {
    var objCase_CaseManagementID = $("#objCase_CaseManagementID"),
        EvidentiarytModel = $("#EvidentiarytModel"),
        data = {};
    data["Param1"] = objCase_CaseManagementID.val();
    data["Param2"] = AssigneeID;
    data["Param3"] = Evidentiary;

    $.ajax({
        url: "/CertaCaseManagement/GetImage",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            var divLoadImage = $("#divLoadImage"),
                strHTML = "";
            divLoadImage.empty();
            $.each(msg, function (index, item) {
                strHTML += '<div class="col-6 col-sm-3 col-md-3">' +
                    '<div class="box p-1">' +
                    '<a href="#"><img src="..' + item + '" alt="" class="w-100"></a>' +
                    '</div>' +
                    '</div>';
            });
            divLoadImage.append(strHTML);
            if (msg.length > 0) {
                EvidentiarytModel.modal('show');
            }
            else {

            }
        }
    });
}

function fngotoAssigneeView() {
    $(".nav-item").removeClass("active");
    $("#Evidentiary").removeClass("active");
    $("#liAssignee").addClass("active");
    $("#Assignee").addClass("active");
}
/* -- View CaseManagement Ends--*/