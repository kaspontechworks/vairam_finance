﻿function fnGetEntityByLevelID() {
    alert("sdfsdf");
    var EntityLevel = $("#EntityLevel");
    var EntityLevelID = Number(EntityLevel.val()) - 1;
   // alert(ddl_EntityLevelID.val());
    $.ajax({
        url: "/EntityMaster/BindEntityByLevelID/" + EntityLevelID,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#ParentEntityID").html('');
            $("#ParentEntityID").append($('<option></option>').val(0).html("Select"));
            //alert(msg);
            $.each(msg, function (x, v) {
                $("#ParentEntityID").append($('<option></option>').val(v.entityID).html(v.name));
            })
        },

        failure: function (response) {
            //alert(response);
        },
        error: function (response) {
          //  alert(response);
        }
    });
}

function fnLoadAddEntity() {
    $("#divUserContent").empty();
    $("#divUserContent").load("/EntityMaster/AddEntity");
}
function fnLoadEditEntity(id) {
  
    $("#divUserContent").empty();
    $("#divUserContent").load("/EntityMaster/EditEntity/" + id);
}
function fnDeletpopup(Id) {

    $("#DivModelDeleteContent").empty();
    $("#DivModelDeleteContent").load("/EntityMaster/DeleteEntity/" + Id);
}


function fnDeleteEntity(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/EntityMaster/DeleteEntityModel/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/ServiceArea/ServiceareaModule";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}
