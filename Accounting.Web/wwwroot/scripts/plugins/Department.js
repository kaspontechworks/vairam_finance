﻿
/* Orgdep Controller's script Starts */
function fnLoadOrgDepartmenView(Id) {
    $("#divOrgDepartmentContent").empty();
    $("#divOrgDepartmentContent").load("/OrgDepartment/EditDepartment/" + Id);
}
function fnLoadOrgDepartmentAdd() {
    $("#divOrgDepartmentContent").empty();
    $("#divOrgDepartmentContent").load("/OrgDepartment/AddDepartment");
}


function fnDeleteDepartment(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/OrgDepartment/DeleteDepartment/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/OrgDepartment/OrgDepartmentModule";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}

function fnDeletpopupDepartment(Id) {

    $("#DivModelDeleteContent").empty();
    $("#DivModelDeleteContent").load("/OrgDepartment/DeleteDepartmentmodel/" + Id);
}




/* Orgdep Controller's script ends */