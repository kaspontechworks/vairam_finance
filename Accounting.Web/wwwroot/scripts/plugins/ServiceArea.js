﻿/* ServiceArea Controller's script Starts */

function fnLoadServiceareaView(Id) {

    $("#divServiceareaContent").empty();
    $("#divServiceareaContent").load("/ServiceArea/Editservicearea/" + Id);
}
function fnLoadServiceareayAdd() {
    $("#divServiceareaContent").empty();
    $("#divServiceareaContent").load("/ServiceArea/AddServicearea");
}
function fnDeleteServicearea(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/ServiceArea/DeleteServiceArea/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/ServiceArea/ServiceareaModule";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}

function fnDeletpopup(Id) {

    $("#DivModelDeleteContent").empty();
    $("#DivModelDeleteContent").load("/ServiceArea/DeleteServicemodel/" + Id);
}

/* ServiceArea Controller's script ends */
