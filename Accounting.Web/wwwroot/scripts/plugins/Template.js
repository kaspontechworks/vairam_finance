﻿


/* Template Controller's script Starts */
function fnLoadTeamplateView(Id) {

    $("#divTemplateContent").empty();
    $("#divTemplateContent").load("/Templates/EditTemplates/" + Id);
}
function fnLoadtemplatesAdd() {
    $("#divTemplateContent").empty();
    $("#divTemplateContent").load("/Templates/AddTemplates");
}


function fnTemplatedelete(Id) {
    var data = {};
    data["ID"] = Id;
    $.ajax({
        url: "/Templates/DeleteTTemplates/" + Id,
        type: "POST",
        // data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg == "OK") {
                window.location.href = "/Templates/TemplatesModel";
            }
        },
        failure: function (response) {
            // fnErrormsg(response.responseText);
        },
        error: function (response) {
            //fnErrormsg(response.responseText);
        }
    });
}

function fnDeletpopupTeamplate(Id) {

    $("#DivModelDeleteContent").empty();
    $("#DivModelDeleteContent").load("/Templates/DeleteTemplatesModelsss/" + Id);
}

/* Template Controller's script end */