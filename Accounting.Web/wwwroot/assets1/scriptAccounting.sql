Create DataBase Accounting
USE [Accounting]
GO
/****** Object:  Table [dbo].[Accounting_CaseManagement]    Script Date: 14-05-2019 18:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounting_CaseManagement](
	[CaseID] [int] IDENTITY(1,1) NOT NULL,
	[CaseUniqueID] [varchar](3) NULL,
	[CaseName] [varchar](250) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
	[LastModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CaseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[accounting_Config_Roles]    Script Date: 14-05-2019 18:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accounting_Config_Roles](
	[RoleID] [int] NOT NULL,
	[RoleName] [varchar](50) NULL,
	[LandingMenu] [int] NULL,
	[IsActive] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[LastModifiedOn] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
 CONSTRAINT [PK_CERTA_Config_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[accounting_Config_Users]    Script Date: 14-05-2019 18:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accounting_Config_Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[DesignationID] [int] NULL,
	[RoleID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Password] [varchar](max) NULL,
	[EmailAddress] [varchar](100) NULL,
	[PhoneNo] [varchar](20) NULL,
	[Address] [varchar](2000) NULL,
	[IsAccess] [bit] NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[LastModifiedOn] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Accounting_ProductManagement]    Script Date: 14-05-2019 18:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounting_ProductManagement](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductUniqueID] [varchar](5) NULL,
	[ProductName] [varchar](250) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
	[LastModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Accounting_ProductManagement_TATDefination]    Script Date: 14-05-2019 18:52:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounting_ProductManagement_TATDefination](
	[TATDefinationID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[CaseManagmentID] [int] NULL,
	[Duration] [int] NULL,
	[IsDaysHours] [bit] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[LastModifiedBy] [int] NULL,
	[LastModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[TATDefinationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Accounting_CaseManagement] ON 
GO
INSERT [dbo].[Accounting_CaseManagement] ([CaseID], [CaseUniqueID], [CaseName], [IsActive], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn]) VALUES (1, N'1', N'Box', 1, 1, CAST(N'2019-05-14T00:00:00.000' AS DateTime), 1, CAST(N'2019-05-14T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Accounting_CaseManagement] OFF
GO
INSERT [dbo].[accounting_Config_Roles] ([RoleID], [RoleName], [LandingMenu], [IsActive], [CreatedOn], [LastModifiedOn], [LastModifiedBy]) VALUES (1, N'Admin', NULL, 1, CAST(N'2019-01-22T18:06:45.000' AS DateTime), CAST(N'2019-04-25T13:16:04.323' AS DateTime), 4007)
GO
SET IDENTITY_INSERT [dbo].[accounting_Config_Users] ON 
GO
INSERT [dbo].[accounting_Config_Users] ([UserID], [LastName], [FirstName], [DesignationID], [RoleID], [UserName], [Password], [EmailAddress], [PhoneNo], [Address], [IsAccess], [IsActive], [CreatedOn], [LastModifiedOn], [LastModifiedBy]) VALUES (1, N'k', N'Admin', NULL, 1, N'admin', N'4GgBaKJ4c++xrm2j7O515Tw9CihujcNNKpfX/3GnBPI=', N'admin@gmail.com', N'9840020061', NULL, 1, 1, CAST(N'2019-01-22T18:44:56.000' AS DateTime), CAST(N'2019-04-26T16:44:22.613' AS DateTime), 1)
GO
SET IDENTITY_INSERT [dbo].[accounting_Config_Users] OFF
GO
SET IDENTITY_INSERT [dbo].[Accounting_ProductManagement] ON 
GO
INSERT [dbo].[Accounting_ProductManagement] ([ProductID], [ProductUniqueID], [ProductName], [IsActive], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn]) VALUES (1, N'1', N'test2', 0, 1, CAST(N'2019-05-14T16:33:17.337' AS DateTime), 1, CAST(N'2019-05-14T17:20:39.987' AS DateTime))
GO
INSERT [dbo].[Accounting_ProductManagement] ([ProductID], [ProductUniqueID], [ProductName], [IsActive], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn]) VALUES (2, N'1', N'test1', 0, 1, CAST(N'2019-05-14T16:33:40.993' AS DateTime), 0, NULL)
GO
INSERT [dbo].[Accounting_ProductManagement] ([ProductID], [ProductUniqueID], [ProductName], [IsActive], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn]) VALUES (3, N'2', N'testa', 1, 1, CAST(N'2019-05-14T16:41:01.083' AS DateTime), 0, NULL)
GO
INSERT [dbo].[Accounting_ProductManagement] ([ProductID], [ProductUniqueID], [ProductName], [IsActive], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn]) VALUES (4, N'13', N'Prodcut', 1, 1, CAST(N'2019-05-14T17:33:24.693' AS DateTime), 1, CAST(N'2019-05-14T18:35:52.227' AS DateTime))
GO
INSERT [dbo].[Accounting_ProductManagement] ([ProductID], [ProductUniqueID], [ProductName], [IsActive], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn]) VALUES (5, N'1', N'prd2', 1, 1, CAST(N'2019-05-14T17:35:27.727' AS DateTime), 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[Accounting_ProductManagement] OFF
GO
SET IDENTITY_INSERT [dbo].[Accounting_ProductManagement_TATDefination] ON 
GO
INSERT [dbo].[Accounting_ProductManagement_TATDefination] ([TATDefinationID], [ProductID], [CaseManagmentID], [Duration], [IsDaysHours], [IsActive], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn]) VALUES (3, 5, 1, 4545, 1, 1, 1, CAST(N'2019-05-14T17:35:28.293' AS DateTime), 0, NULL)
GO
INSERT [dbo].[Accounting_ProductManagement_TATDefination] ([TATDefinationID], [ProductID], [CaseManagmentID], [Duration], [IsDaysHours], [IsActive], [CreatedBy], [CreatedOn], [LastModifiedBy], [LastModifiedOn]) VALUES (4, 4, 1, 2, 0, 1, 1, CAST(N'2019-05-14T18:35:54.297' AS DateTime), 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[Accounting_ProductManagement_TATDefination] OFF
GO
ALTER TABLE [dbo].[Accounting_CaseManagement] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Accounting_ProductManagement] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Accounting_ProductManagement_TATDefination] ADD  DEFAULT ((1)) FOR [IsActive]
GO
