﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Accounting.Web.Common
{
    public static class Cryptography
    {
        public static string Encrypt(string strClearText, string strPassword)
        {
            byte[] _objClearBytes = System.Text.Encoding.Unicode.GetBytes(strClearText);
            PasswordDeriveBytes _objPasswordDeriveBytes = new PasswordDeriveBytes(strPassword,
                new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            byte[] _objEncryptedData = ByteEncrypt(_objClearBytes, _objPasswordDeriveBytes.GetBytes(32), _objPasswordDeriveBytes.GetBytes(16));
            return Convert.ToBase64String(_objEncryptedData);
        }

        public static string Decrypt(string strCipherText, string strPassword)
        {
            byte[] _objCipherBytes = Convert.FromBase64String(strCipherText);
            PasswordDeriveBytes _objPasswordDeriveBytes = new PasswordDeriveBytes(strPassword,
                new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            byte[] _objDecryptedData = ByteDecrypt(_objCipherBytes, _objPasswordDeriveBytes.GetBytes(32), _objPasswordDeriveBytes.GetBytes(16));
            return System.Text.Encoding.Unicode.GetString(_objDecryptedData);
        }

        public static byte[] ByteDecrypt(byte[] objCipherData, byte[] objKey, byte[] objIV)
        {
            byte[] _objDecryptedData;
            using (MemoryStream _objMemoryStream = new MemoryStream())
            {
                Rijndael _objRijndael = Rijndael.Create();
                _objRijndael.Key = objKey;
                _objRijndael.IV = objIV;
                CryptoStream _objCryptoStream = new CryptoStream(_objMemoryStream, _objRijndael.CreateDecryptor(), CryptoStreamMode.Write);
                _objCryptoStream.Write(objCipherData, 0, objCipherData.Length);
                _objCryptoStream.Close();
                _objDecryptedData = _objMemoryStream.ToArray();
            }
            return _objDecryptedData;
        }

        public static byte[] ByteEncrypt(byte[] objClearData, byte[] objKey, byte[] objIV)
        {
            byte[] _objEncryptedData;
            using (MemoryStream _objMemoryStream = new MemoryStream())
            {
                Rijndael _objRijndael = Rijndael.Create();
                _objRijndael.Key = objKey;
                _objRijndael.IV = objIV;
                CryptoStream _objCryptoStream = new CryptoStream(_objMemoryStream, _objRijndael.CreateEncryptor(), CryptoStreamMode.Write);
                _objCryptoStream.Write(objClearData, 0, objClearData.Length);
                _objCryptoStream.Close();
                _objEncryptedData = _objMemoryStream.ToArray();
            }
            return _objEncryptedData;
        }
        /// <summary>
        /// External document path
        /// </summary>
        /// <param name="basePath">current running directory</param>
        /// <param name="companyName">Company Name</param>
        /// <returns>Actual path</returns>
        public static string GetExternalDocumentPath(string basePath, string FolderName)
        {
            string rtnExternalPath = string.Empty;
            if (Directory.Exists(basePath))
            {
                rtnExternalPath = basePath + "\\wwwroot\\ExternalDocs";
                if (!Directory.Exists(rtnExternalPath)) Directory.CreateDirectory(rtnExternalPath);
                if (!string.IsNullOrEmpty(FolderName))
                {
                    rtnExternalPath = rtnExternalPath + "\\" + FolderName;
                    if (!Directory.Exists(rtnExternalPath)) Directory.CreateDirectory(rtnExternalPath);
                }
            }
            return rtnExternalPath;
        }
        public static void SaveImage(string base64Image,string directory_path)
        {
            var bytes = Convert.FromBase64String(base64Image);
            if(File.Exists(directory_path))
            {
                File.Delete(directory_path);
            }
            using (var objFile = new FileStream(directory_path, FileMode.Create))
            {
                objFile.Write(bytes, 0, bytes.Length);
                objFile.Flush();
            }
        }
        public static DateTime checkDateNullorEmpty(string ipDate)
        {
            if (String.IsNullOrEmpty(ipDate))
                return new DateTime(1990, 1, 1);
            else
            {
                DateTime d = new DateTime();
                if (ipDate.IndexOf("/") > 0)
                {
                    int intDayInd = ipDate.IndexOf("/");
                    string strDay = ipDate.Substring(0, intDayInd);
                    int intMonInd = ipDate.IndexOf("/", intDayInd + 1);
                    string strMonth = ipDate.Substring(intDayInd + 1, intMonInd - (intDayInd + 1));
                    int len = ipDate.Length;
                    if (ipDate.IndexOf(" ") > 0) len = ipDate.Split(' ')[0].Length;
                    string strYear = ipDate.Substring(intMonInd + 1, (len - (intMonInd + 1)));
                    d = new DateTime(Convert.ToInt32(strYear), Convert.ToInt32(strMonth), Convert.ToInt32(strDay));
                }
                else if (ipDate.IndexOf("-") > 0)
                {
                    int intDayInd = ipDate.IndexOf("-");
                    string strDay = ipDate.Substring(0, intDayInd);
                    int intMonInd = ipDate.IndexOf("-", intDayInd + 1);
                    string strMonth = ipDate.Substring(intDayInd + 1, intMonInd - (intDayInd + 1));
                    int len = ipDate.Length;
                    if (ipDate.IndexOf(" ") > 0) len = ipDate.Split(' ')[0].Length;
                    string strYear = ipDate.Substring(intMonInd + 1, (len - (intMonInd + 1)));
                    d = new DateTime(Convert.ToInt32(strYear), Convert.ToInt32(strMonth), Convert.ToInt32(strDay));
                }
                return d;
            }
        }
    }
}
