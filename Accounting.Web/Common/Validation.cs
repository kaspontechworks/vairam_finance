﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace Accounting.Web.Common
{
    public class Validation
    {
        public static int checkNullorEmpty(string _txt)
        {
            if (String.IsNullOrEmpty(_txt))
                return 0;
            else
                return Convert.ToInt32(_txt);
        }

        public static int CheckInt(string integervalue)
        {
            if (string.IsNullOrEmpty(integervalue))
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(integervalue);
            }
        }

        public static DateTime checkDateNullorEmpty(string ipDate)
        {
            if (String.IsNullOrEmpty(ipDate))
                return new DateTime(1990, 1, 1);
            else
            {
                DateTime d = new DateTime();
                if (ipDate.IndexOf("/") > 0)
                {
                    int intDayInd = ipDate.IndexOf("/");
                    string strDay = ipDate.Substring(0, intDayInd);
                    int intMonInd = ipDate.IndexOf("/", intDayInd + 1);
                    string strMonth = ipDate.Substring(intDayInd + 1, intMonInd - (intDayInd + 1));
                    int len = ipDate.Length;
                    if (ipDate.IndexOf(" ") > 0) len = ipDate.Split(' ')[0].Length;
                    string strYear = ipDate.Substring(intMonInd + 1, (len - (intMonInd + 1)));
                    d = new DateTime(Convert.ToInt32(strYear), Convert.ToInt32(strMonth), Convert.ToInt32(strDay));
                }
                else if (ipDate.IndexOf("-") > 0)
                {
                    int intDayInd = ipDate.IndexOf("-");
                    string strDay = ipDate.Substring(0, intDayInd);
                    int intMonInd = ipDate.IndexOf("-", intDayInd + 1);
                    string strMonth = ipDate.Substring(intDayInd + 1, intMonInd - (intDayInd + 1));
                    int len = ipDate.Length;
                    if (ipDate.IndexOf(" ") > 0) len = ipDate.Split(' ')[0].Length;
                    string strYear = ipDate.Substring(intMonInd + 1, (len - (intMonInd + 1)));
                    d = new DateTime(Convert.ToInt32(strYear), Convert.ToInt32(strMonth), Convert.ToInt32(strDay));
                }
                return d;
            }
        }

        public static decimal checkDecimalNullorEmpty(string _txt)
        {
            if (String.IsNullOrEmpty(_txt))
                return 0;
            else
                return Convert.ToDecimal(_txt);
        }

        public static DateTime checkTimeNullorEmpty(string ipDate)
        {
            if (String.IsNullOrEmpty(ipDate))
                return new DateTime(1990, 1, 1);
            else
            {
                if (ipDate.ToLower().Contains("am") || ipDate.ToLower().Contains("pm"))
                {
                    return Convert.ToDateTime(ipDate);
                }
                else
                {
                    string[] s = ipDate.Split(':');
                    DateTime d = new DateTime(1990, 1, 1, Convert.ToInt16(s[0]), Convert.ToInt16(s[1]), 0);
                    return d;
                }
            }
        }

        public static string calculateAge(string ipDate)
        {
            string ageCalculation = string.Empty;
            DateTime d = checkDateNullorEmpty(ipDate);
            DateTime currentDate = DateTime.Today;
            //int months = currentDate.Month - d.Month;
            int years = currentDate.Year - d.Year;
            int days = currentDate.Day - d.Day;
            int months = (currentDate.Month + 12 * currentDate.Year) - (d.Month + 12 * d.Year);
            int monthOverflow = 0;
            if (months - (years * 12) < 0)
                monthOverflow = -1;
            else
                monthOverflow = 1;
            if (monthOverflow < 0)
                years = years - 1;
            months = months - (years * 12);
            int lastDayofMonth = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
            if (monthOverflow < 0 && (d.Day > currentDate.Day))
                days = lastDayofMonth + (currentDate.Day - d.Day) - 1;
            else
                days = currentDate.Day - d.Day;
            if (days < 0)
                months = months - 1;
            DateTime l = new DateTime(currentDate.Year, currentDate.Month - 1, DateTime.DaysInMonth(currentDate.Year, currentDate.Month - 1));
            DateTime l1 = new DateTime(d.Year, d.Month, DateTime.DaysInMonth(d.Year, d.Month));
            if (days < 0)
            {
                if (l1 > l)
                    days = l1.Day + days;
                else
                    days = l.Day + days;
            }

            ageCalculation = years + "Year(s), " + months + " Month(s), " + days + "Day(s)";
            return ageCalculation;
        }
        public static string ReadPrintContents(string keyName, string fileName)
        {
            string xmlContents = string.Empty;
            XmlDocument doc = ReadXML(fileName);
            if (doc != null)
            {
                if (doc.InnerXml != null)
                {
                    XmlNode oNode = doc.SelectSingleNode("//PrintContents/content[@id='" + keyName + "']");
                    if (oNode != null)
                    {
                        xmlContents = oNode.InnerText;
                    }
                }
            }
            return xmlContents;
        }
        private static XmlDocument ReadXML(string fileName)
        {
            XmlDocument oDoc = new XmlDocument();
            if (File.Exists(fileName))
                oDoc.Load(fileName);
            return oDoc;
        }
        public static string ConvertNumbertoWords(long number)
        {
            if (number == 0) return "ZERO";
            if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";
            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 100000) + " LAKES ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
                number %= 100;
            }
            //if ((number / 10) > 0)  
            //{  
            // words += ConvertNumbertoWords(number / 10) + " RUPEES ";  
            // number %= 10;  
            //}  
            if (number > 0)
            {
                if (words != "") words += "AND ";
                var unitsMap = new[] { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN",
                    "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"  };
                var tensMap = new[] { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };
                if (number < 20) words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0) words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }
    }

    public static class Common
    {
        //  private static readonly ILog logger = LogManager.GetLogger(typeof(Common));
        /// <summary>
        /// External document path
        /// </summary>
        /// <param name="basePath">current running directory</param>
        /// <param name="companyName">Company Name</param>
        /// <returns>Actual path</returns>
        public static string GetExternalDocumentPath(string basePath, string companyName)
        {
            string rtnExternalPath = string.Empty;
            if (Directory.Exists(basePath))
            {
                rtnExternalPath = basePath + "//ExternalDocs";
                if (!Directory.Exists(rtnExternalPath)) Directory.CreateDirectory(rtnExternalPath);
                if (!string.IsNullOrEmpty(companyName))
                {
                    rtnExternalPath = rtnExternalPath + "//" + companyName;
                    if (!Directory.Exists(rtnExternalPath)) Directory.CreateDirectory(rtnExternalPath);
                }
            }
            return rtnExternalPath;
        }

        /// <summary>
        /// External document path
        /// </summary>
        /// <param name="companyName">Company Name</param>
        /// <returns>path as string</returns>
        public static string GetExternalDocumentPath(string companyName)
        {
            return "../ExternalDocs/" + companyName;
        }

        /// <summary>
        /// To delete file from external path 
        /// </summary>
        /// <param name="KeyName">specific file name</param>
        /// <param name="dirPath">directory path</param>
        /// <param name="ltFiles">list of files which are in the directory</param>
        public static void DeleteExternalFiles(string KeyName, string dirPath, List<string> ltFiles)
        {
            if (Directory.Exists(dirPath))
            {
                DirectoryInfo diInfo = new DirectoryInfo(dirPath);
                FileInfo[] fiInfos = diInfo.GetFiles(KeyName + "*", SearchOption.TopDirectoryOnly);
                foreach (FileInfo fi in fiInfos)
                {
                    if (!ltFiles.Contains(fi.Name))
                    {
                        File.Delete(fi.FullName);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="folder"></param>
        /// <returns></returns>
        public static string createDirectory(string path, string folder)
        {
            string rtnPath = string.Empty;
            if (Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            rtnPath = path + "//" + folder;
            if (!Directory.Exists(rtnPath)) Directory.CreateDirectory(rtnPath);
            return rtnPath;
        }

        public static string leadingZero(int num, int length)
        {
            string leadZero = string.Empty;
            int totalZeros = length - num.ToString().Length;
            for (int i = 0; i < totalZeros; i++)
            {
                leadZero += "0";
            }
            leadZero += num;
            return leadZero;
        }

        public static string GetRandomString(int length)
        {
            string passwordString = string.Empty;
            string allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5,6,7,8,9,0,!,@,#,$,&,*,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,TU,V,W,X,Y,Z";
            string[] arr = allowedChars.Split(',');
            Random r = new Random();
            for (int i = 0; i < length; i++)
            {
                passwordString += arr[r.Next(0, arr.Length)];
            }
            return passwordString;
        }

        public static string GetSMSCode(int length)
        {
            string strSMSCode = string.Empty;
            string allowedChars = "1,2,3,4,5,6,7,8,9,0";
            string[] arr = allowedChars.Split(',');
            Random r = new Random();
            for (int i = 0; i < length; i++)
            {
                strSMSCode += arr[r.Next(0, arr.Length)];
            }
            return strSMSCode;
        }

        //public static void SendMail(string subject, string body, string toAddress, string fromAddress, string fileName)
        //{
        //    MailMessage objMail = new MailMessage();
        //    MailAddress objFromAddr = new MailAddress(fromAddress, fromAddress);
        //    MailAddress objToAddr;

        //    SmtpClient smtp = new SmtpClient();
        //    objMail.From = objFromAddr;
        //    string[] strArrTp = toAddress.Split(',');
        //    foreach (string ed in strArrTp)
        //    {
        //        if (ed != "")
        //        {
        //            objToAddr = new MailAddress(ed);
        //            objMail.To.Add(objToAddr);
        //        }
        //    }
        //    objMail.Subject = subject;
        //    objMail.IsBodyHtml = true;
        //    objMail.Body = body;
        //    smtp.Send(objMail);
        //}

        //public static void SendEmail(string subject, string body, string toAddress, string fileName)
        //{
        //    // Gmail Address from where you send the mail
        //    //var fromAddress = "info@interfazia.com";
        //    // any address where the email will be sending
        //    //var toAddress = "vincent@interfazia.com,franklinbharathsj@gmail.com,windavidraj@gmail.com";
        //    //Password of your gmail address
        //    string fromPassword = ConfigurationManager.AppSettings["EmailPassword"];
        //    string fromAddress = ConfigurationManager.AppSettings["EmailAddress"]; //"faziainter*999";
        //    // Passing the values and make a email formate to display
        //    //string subject = "SalonFazia is installing in " + txtSalonName.Text;
        //    //string body = strSubject;
        //    // smtp settings
        //    var smtp = new System.Net.Mail.SmtpClient();
        //    {
        //        smtp.Host = "smtp.gmail.com";
        //        smtp.Port = 587;
        //        smtp.EnableSsl = true;
        //        smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //        smtp.Credentials = new System.Net.NetworkCredential(fromAddress, fromPassword);
        //        smtp.Timeout = 20000;
        //    }
        //    // Passing values to smtp object
        //    smtp.Send(fromAddress, toAddress, subject, body);
        //}

        public static string ConsturctAutoGenCode(string prefix, string suffix, string format, int lastIndex, int maxIndex)
        {
            string finalCode = string.Empty;
            if (!string.IsNullOrEmpty(prefix))
                finalCode += prefix;
            if (!string.IsNullOrEmpty(format))
            {
                string[] ltFormats = format.Split('|');
                foreach (string ch in ltFormats)
                {
                    if (ch == "C")
                    {
                        finalCode += "A";
                    }
                    else if (ch == "MM")
                    {
                        int numChars = ch.Length;
                        finalCode += leadingZero(DateTime.Now.Month, numChars);
                    }
                    else if (ch == "YYYY" || ch == "YY")
                    {
                        int numChars = ch.Length;
                        finalCode += leadingZero(DateTime.Now.Year, numChars);
                    }
                    else if (ch.Contains("N"))
                    {
                        int numChars = ch.Length;
                        finalCode += leadingZero((lastIndex + 1), numChars);
                    }
                }
            }
            return finalCode;
        }

        /*public static TimesheetDay GetDateOfWeek(int dWeek, string weekName, DateTime currentDate)
        {
            TimesheetDay _timesheetDay = new TimesheetDay();
            if ((int)currentDate.DayOfWeek == dWeek)
            {
                _timesheetDay.num = currentDate.Day;
                _timesheetDay.shortDate = currentDate.ToString("dd/MM/yyyy");
                _timesheetDay.data = 0;
                _timesheetDay.headerName = weekName;
                _timesheetDay.CurrentDate = currentDate;
                _timesheetDay.IsEnable = true;
            }
            else
            {
                DateTime dt = currentDate.AddDays(-1);
                _timesheetDay = GetDateOfWeek(dWeek, weekName, dt);
            }
            return _timesheetDay;
        }*/

        //public static string encryptedURLParameters(string url)
        //{
        //    if (url.IndexOf('?') > 0)
        //    {
        //        string param = url.Split('?')[1];
        //        //string param = string.Empty;
        //        //param = parameters.Replace("=", "$").Replace("&", "|").Replace("mode$grid|", "");
        //        return url.Split('?')[0] + "?_link=" + System.Web.HttpContext.Current.Server.UrlEncode(Cryptography.Encrypt(param, "TANDEMURL"));
        //    }
        //    else
        //        return url;
        //}

        public static Dictionary<string, string> decryptedURLParameters(string encKey)
        {
            Dictionary<string, string> docResult = new Dictionary<string, string>();
            string param = Cryptography.Decrypt(encKey, "TANDEMURL");
            string[] strArr = param.Split('&');
            foreach (string s in strArr)
            {
                docResult.Add(s.Split('=')[0], s.Split('=')[1]);
            }
            return docResult;
        }

        public static List<Tuple<string, string>> GetAllTimezones()
        {
            List<Tuple<string, string>> ltTimeZones = new List<Tuple<string, string>>();
            var cole = TimeZoneInfo.GetSystemTimeZones();
            foreach (TimeZoneInfo info in cole)
            {
                ltTimeZones.Add(new Tuple<string, string>(info.Id, info.DisplayName));
            }
            return ltTimeZones;
        }



        public static string ReadXMLContents(string keyName, string fileName)
        {
            string xmlContents = string.Empty;
            XmlDocument doc = ReadXML(fileName);
            if (doc != null)
            {
                if (doc.InnerXml != null)
                {
                    XmlNode oNode = doc.SelectSingleNode("//SMSContents/content[@id='" + keyName + "']");
                    if (oNode != null)
                    {
                        xmlContents = oNode.InnerText;
                    }
                }
            }
            return xmlContents;
        }

        public static Tuple<string, string> ReadEmailContents(string keyName, string fileName)
        {
            string xmlContents = string.Empty;
            string strsubject = string.Empty;
            XmlDocument doc = ReadXML(fileName);
            if (doc != null)
            {
                if (doc.InnerXml != null)
                {
                    XmlNode oNode = doc.SelectSingleNode("//EmailContents/content[@id='" + keyName + "']");
                    if (oNode != null)
                    {
                        xmlContents = oNode.InnerText;
                        strsubject = oNode.Attributes["subject"].Value;
                    }
                }
            }
            return new Tuple<string, string>(strsubject, xmlContents);
        }

        private static XmlDocument ReadXML(string fileName)
        {
            XmlDocument oDoc = new XmlDocument();
            if (File.Exists(fileName))
                oDoc.Load(fileName);
            return oDoc;
        }

        //public static void sendsingleSMS(string mobileNo, string contents)
        //{
        //    try
        //    {
        //        string constructPath = string.Empty;
        //        constructPath = string.Format("http://boancomm.net/boansms/boansmsinterface.aspx?mobileno={0}&smsmsg={1}&uname={2}&pwd={3}&pid={4}", mobileNo, contents,
        //            ConfigurationManager.AppSettings["SMSUserName"], ConfigurationManager.AppSettings["SMSPassword"], ConfigurationManager.AppSettings["SMSPID"]);
        //        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(constructPath);
        //        request.Method = "GET";
        //        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //        if (response.StatusDescription == "Ok")
        //        {
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.Message, ex);
        //    }
        //}

        public static string ConvertNumbertoWords(long number)
        {
            if (number == 0) return "ZERO";
            if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";
            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 100000) + " LAKES ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
                number %= 100;
            }
            //if ((number / 10) > 0)  
            //{  
            // words += ConvertNumbertoWords(number / 10) + " RUPEES ";  
            // number %= 10;  
            //}  
            if (number > 0)
            {
                if (words != "") words += "AND ";
                var unitsMap = new[] { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN",
                    "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"  };
                var tensMap = new[] { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };
                if (number < 20) words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0) words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }
    }

}

