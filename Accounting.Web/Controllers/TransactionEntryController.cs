﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Accounting.Web.Common;
using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Accounting.Web.Controllers
{
    public class TransactionEntryController : Controller
    {
        private readonly CDbcontext _AccountingDBcontext;
        private readonly ITransactionEntryService _objTransactionEntryService;
        private readonly ITransactionHeadService _objITransactionHeadService;
        private readonly IChartofAccountsService _objChartofAccountsService;
        private readonly IFinancialYearService _objIFinancialYearService;
        private readonly IEntityMasterService _objIEntityMasterService;
        private readonly IMajorCategoryService _objMajorCategoryService;
        private readonly ICategoryMasterService _objCategoryMasterService;
        private readonly ILedgerGroupMasterService _objLedgergroupmaster;
        private readonly IProfitCentreService _objprofitcentre;
        //private readonly IHostingEnvironment _env;
        //private readonly IConfiguration _objIConfiguration;

        //List<SelectListItem> ddlMonths = new List<SelectListItem>();
        //List<SelectListItem> ddlYears = new List<SelectListItem>();

        public TransactionEntryController(CDbcontext AccountingDBcontext,ITransactionEntryService objTransactionEntryService, IChartofAccountsService objChartofAccountsService,ICategoryMasterService objCategoryMasterService,
            ITransactionHeadService objITransactionHeadService, IFinancialYearService objIFinancialYearService, IEntityMasterService objIEntityMasterService, IMajorCategoryService objMajorCategoryService,ILedgerGroupMasterService objledgerGroupMaster,IProfitCentreService objprofitcentre)
        {
            _AccountingDBcontext = AccountingDBcontext;
            _objChartofAccountsService = objChartofAccountsService;
            _objTransactionEntryService = objTransactionEntryService;
            _objITransactionHeadService = objITransactionHeadService;
            _objIFinancialYearService = objIFinancialYearService;
            _objIEntityMasterService = objIEntityMasterService;
            _objMajorCategoryService = objMajorCategoryService;
            _objCategoryMasterService = objCategoryMasterService;
            _objLedgergroupmaster = objledgerGroupMaster;
            _objprofitcentre = objprofitcentre;

        }
        public IActionResult Index(int ID)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
            int PageID = ID;
            ViewBag.encPageID = PageID;
            ViewBag.TransactionInputTypeID = PageID;
            InitpageID(PageID);

            ViewBag.GetAllTransactionHeadViewModelList = _objITransactionHeadService.GetAllTransactionHeadViewModelList(PageID, LegalEntityID, ProfitCentreID, LocationID, UnitID, ProjectID);

            return View();//View(_objTransactionEntryViewModel);
        }
        public IActionResult TrialBalance()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            ViewBag.LegalEntityID = LegalEntityID;
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
            }
            //ViewBag.AllAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
            //ViewBag.GetAllTransactionHeadViewModelList = _objITransactionHeadService.GetAllTransactionHeadViewModelList(PageID, LegalEntityID, ProfitCentreID, LocationID, UnitID, ProjectID);

            return View();//View(_objTransactionEntryViewModel);
        }
        public IActionResult TrialBalanceDateRange(int LegalEntityID, int AccountLevel, DateTime StartDate, DateTime EndDate)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            //ViewBag.encPageID = PageID;
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            ViewBag.AccountLevel = AccountLevel;
            ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetAllTransactionEntryViewModelListByLegalEntityIDAccountLevelStartDateEndDate(LegalEntityID, AccountLevel, StartDate, EndDate);
            return PartialView("TrialBalanceDateRange");
        }
        public IActionResult AccountTrialBalance()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
         
            ViewBag.LegalEntityID = LegalEntityID;
            ViewBag.GetCompany = _objIEntityMasterService.GetCompany(LegalEntityID);
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
            }
            //ViewBag.AllAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
            //ViewBag.GetAllTransactionHeadViewModelList = _objITransactionHeadService.GetAllTransactionHeadViewModelList(PageID, LegalEntityID, ProfitCentreID, LocationID, UnitID, ProjectID);

            return View();//View(_objTransactionEntryViewModel);
        }
        public IActionResult AccountTrialBalanceDateRange(int FinancialYearID,int AccountID,int LegalEntityID, int AccountLevel, DateTime StartDate, DateTime EndDate)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
         
            //ViewBag.encPageID = PageID;
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            ViewBag.AccountLevel = AccountLevel;
            if (AccountLevel == 1)
            {
                ViewBag.GetAllTransactionEntryTrialListByLegalEntityID = _objTransactionEntryService.GetAllTransactionEntryTrialListByLegalEntityIDMajorGroups(LegalEntityID, StartDate, EndDate);
            }
            else if (AccountLevel == 2)
            {
                ViewBag.GetAllTransactionEntryTrialListByLegalEntityID = _objTransactionEntryService.GetAllTransactionEntryTrialListByLegalEntityIDGroups(LegalEntityID, StartDate, EndDate);
            }
            else if (AccountLevel == 3)
            {
                ViewBag.GetAllTransactionEntryTrialListByLegalEntityID = _objTransactionEntryService.GetAllTransactionEntryTrialListByLegalEntityIDSubGroups(LegalEntityID, StartDate, EndDate);
            }
            else
            {
                ViewBag.GetAllTransactionEntryTrialListByLegalEntityID = _objTransactionEntryService.GetAllTransactionEntryTrialListByLegalEntityID( LegalEntityID,StartDate, EndDate);
            }
            return PartialView("AccountTrialBalanceDateRange");
        }
        public IActionResult AccountProfitandLoss()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);


         
            ViewBag.LegalEntityID = LegalEntityID;
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
            }
            ViewBag.GetAllMajorCategory = _objMajorCategoryService.GetAllMajorCategory();
            ViewBag.GetCompany = _objIEntityMasterService.GetCompany(LegalEntityID);

            return View();
        }

        public IActionResult AccountProfitandLossDateRange( int LegalEntityID, int MajorGroupID, DateTime StartDate, DateTime EndDate)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            ViewBag.StartDate = StartDate;
            ViewBag.EndDate = EndDate;
            ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetAllTransactionEntryTrialListByLegalEntityIDProfitandLoss(LegalEntityID, MajorGroupID, StartDate, EndDate);
            //ViewBag.Netprofitorloss=
            //TempData["Netprofitorloss"] = "Netprofitorloss";
            return PartialView("AccountProfitandLossDateRange", _objTransactionEntryService.GetAllTransactionEntryTrialListByLegalEntityIDProfitandLoss(LegalEntityID, MajorGroupID, StartDate, EndDate));
        }

        public IActionResult MonthlyProfitandLoss()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
          
            ViewBag.LegalEntityID = LegalEntityID;
            ViewBag.GetProfitCentre = _objprofitcentre.GetProfitCentreName(ProfitCentreID);
            ViewBag.GetCompany = _objIEntityMasterService.GetCompany(LegalEntityID);  
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
               
            }
          //ViewBag.linktoMonthId = GetMonths(Year);
           // ViewBag.linktoYearId = GetYears(Year);
            ViewBag.GetAllMajorCategory = _objMajorCategoryService.GetAllMajorCategory();
            return View("MonthlyProfitandLoss", _objTransactionEntryService.GetCompanydetails_ID(LegalEntityID,ProfitCentreID));
        }
        public IActionResult MonthlyProftandLossDateRange(int LegalEntityID, int MajorGroupID, int  GroupID, string StartDate, string EndDate)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            ViewBag.startdate = StartDate;
            ViewBag.Enddate = EndDate;
            ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetAllTransactionEntryTrialListMonthlyProfitandLoss(LegalEntityID,MajorGroupID,GroupID,StartDate, EndDate);
            return PartialView("MonthlyProftandLossDateRange", _objTransactionEntryService.GetAllTransactionEntryTrialListMonthlyProfitandLoss(LegalEntityID, MajorGroupID, GroupID, StartDate , EndDate));
        }
        public IActionResult Accountdetailsformonth(int LegalEntityID, int MajorGroupID, int GroupID, string StartDate, string EndDate,int Data)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
         
            var html = _objTransactionEntryService.Getaccountdetails(LegalEntityID,GroupID,StartDate,EndDate);

            return Json(html);

        }
        public IActionResult Accountdetails_showhide(int LegalEntityID, int MajorGroupID, int GroupID, DateTime StartDate, DateTime EndDate,int Data)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
           
            var html = _objTransactionEntryService.GetAccountProfitandLoss(LegalEntityID,GroupID,StartDate,EndDate);

            return Json(html);

        }
        public IActionResult BalanceSheetReport()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);


        
            ViewBag.LegalEntityID = LegalEntityID;
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
            }
            ViewBag.GetAllMajorCategory = _objMajorCategoryService.GetAllMajorCategory();
            ViewBag.GetCompany = _objIEntityMasterService.GetCompany(LegalEntityID);
            return View();
        }



        public IActionResult TransactionSuccessCashReceipt()
        {
            ViewBag.TransactionInputType = _objTransactionEntryService.GetAccounting_TransactionInputTypecashreceipt();
            return View("TransactionSuccessCashReceipt");

        }

        [HttpGet]
        [IgnoreAntiforgeryToken]
        public IActionResult TransactionSuccessCashReceiptDateRange(int Transaction_TypeId, DateTime StartDate, DateTime EndDate)
        {
            List<TransactionEntryModel> Model = new List<TransactionEntryModel>();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            //ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetGroupComparisonReportRangeforProfitandLoss(BusinessAreaID, StartDate, EndDate);
            //ViewBag.ListOfTrnEntry = _objTransactionEntryService.GetAllTransaction_CancelledVouchers(Transaction_TypeId, StartDate, EndDate);
            ViewBag.GetTrnModel = _objTransactionEntryService.GetAllTransaction_TransactionSuccessCash(Transaction_TypeId, StartDate, EndDate, LegalEntityID);
            return PartialView("TransactionSuccessCashReceiptDateRange", _objTransactionEntryService.GetAllTransaction_TransactionSuccessCash(Transaction_TypeId, StartDate, EndDate, LegalEntityID));
        }


        public IActionResult BalanceSheetReportRange(int LegalEntityID, int MajorGroupID, int AccountID, DateTime StartDate, DateTime EndDate)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();



            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetBalanceSheetAmount(LegalEntityID, MajorGroupID, StartDate, EndDate);

            return PartialView("BalanceSheetReportRange", _objTransactionEntryService.GetBalanceSheetAmount(LegalEntityID, MajorGroupID, StartDate, EndDate));
        }
        public IActionResult BalanceSheet_GetAccount(int LegalEntityID, int MajorGroupID, int GroupID, DateTime StartDate, DateTime EndDate, int Data)
        {
            int[] Majorgroupid = { 63, 64, 65, 66, 67, 68, 69, 70, 72, 74, 73, 75 };
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();



            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            var html = _objTransactionEntryService.GetAccount_balancesheet(LegalEntityID, GroupID, StartDate, EndDate);

            return Json(html);

        }

        public IActionResult GLAccountReport()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            ViewBag.LegalEntityID = LegalEntityID;
            //ViewBag.TransactionInputTypeID = PageID; 

            //InitpageID(PageID);
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
            }
            ViewBag.AllAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
            //ViewBag.GetAllTransactionHeadViewModelList = _objITransactionHeadService.GetAllTransactionHeadViewModelList(PageID, LegalEntityID, ProfitCentreID, LocationID, UnitID, ProjectID);
            ViewBag.GetCompany = _objIEntityMasterService.GetCompany(LegalEntityID);

            return View();//View(_objTransactionEntryViewModel);

        }


        public IActionResult TransactionCashReceipt()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            ViewBag.LegalEntityID = LegalEntityID;
            //ViewBag.TransactionInputTypeID = PageID; 

            //InitpageID(PageID);
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
            }
            ViewBag.AllAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
            //ViewBag.GetAllTransactionHeadViewModelList = _objITransactionHeadService.GetAllTransactionHeadViewModelList(PageID, LegalEntityID, ProfitCentreID, LocationID, UnitID, ProjectID);
            ViewBag.GetCompany = _objIEntityMasterService.GetCompany(LegalEntityID);

            return View();//View(_objTransactionEntryViewModel);
        }


        public IActionResult GLAccountReportDateRange(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
         
            //ViewBag.encPageID = PageID;
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = _objTransactionEntryService.GetClosingBalanceChartofAccountsViewModelByStartDate(LegalEntityID, AccountID, StartDate);
            decimal ClosingBalanceAmount = 0;
           decimal OpeningBalanceAmount = 0;
            if (objSumChartofAccountsViewModelList != null)
            {
                ClosingBalanceAmount = objSumChartofAccountsViewModelList.Debit - objSumChartofAccountsViewModelList.Credit;
                OpeningBalanceAmount = objSumChartofAccountsViewModelList.Credit;
            }
            ViewBag.ClosingBalanceAmount = ClosingBalanceAmount;
            
            ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate(LegalEntityID, AccountID, StartDate, EndDate);
            return PartialView("GLAccountReportDateRange");
        }
        public IActionResult CashRegister()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
          
            ViewBag.LegalEntityID = LegalEntityID;
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
            }
            ViewBag.AllAccounts = _objChartofAccountsService.GetAllCashChartofAccountsByLegalEntity(LegalEntityID);
            //ViewBag.GetAllTransactionHeadViewModelList = _objITransactionHeadService.GetAllTransactionHeadViewModelList(PageID, LegalEntityID, ProfitCentreID, LocationID, UnitID, ProjectID);

            return View();//View(_objTransactionEntryViewModel);
        }

       
        public IActionResult CashRegisterDateRange(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
      
            //ViewBag.encPageID = PageID;
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = _objTransactionEntryService.GetClosingBalanceChartofAccountsViewModelByStartDate(LegalEntityID, AccountID, StartDate);

            decimal ClosingBalanceAmount = 0;
            if (objSumChartofAccountsViewModelList != null)
            {
                ClosingBalanceAmount = objSumChartofAccountsViewModelList.Debit - objSumChartofAccountsViewModelList.Credit;
            }
            ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate(LegalEntityID, AccountID, StartDate, EndDate);
            ViewBag.GetAllTransactionEntryViewModelListRegisterByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetAllTransactionEntryViewModelListRegisterByLegalEntityIDAccountIDStartDateEndDate(LegalEntityID, AccountID, StartDate, EndDate);

            ViewBag.ClosingBalanceAmount = ClosingBalanceAmount;
            return PartialView("CashRegisterDateRange");
        }
        public IActionResult BankRegister()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
        
            ViewBag.LegalEntityID = LegalEntityID;
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");
            }
            ViewBag.AllAccounts = _objChartofAccountsService.GetAllBankChartofAccountsByLegalEntity(LegalEntityID);
            //ViewBag.GetAllTransactionHeadViewModelList = _objITransactionHeadService.GetAllTransactionHeadViewModelList(PageID, LegalEntityID, ProfitCentreID, LocationID, UnitID, ProjectID);

            return View();//View(_objTransactionEntryViewModel);
        }
        public IActionResult BankRegisterDateRange(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);
         
            //ViewBag.encPageID = PageID;
            //ViewBag.TransactionInputTypeID = PageID;
            //InitpageID(PageID);
            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = _objTransactionEntryService.GetClosingBalanceChartofAccountsViewModelByStartDate(LegalEntityID, AccountID, StartDate);
            decimal ClosingBalanceAmount = 0;
            if (objSumChartofAccountsViewModelList != null)
            {
                ClosingBalanceAmount = objSumChartofAccountsViewModelList.Debit - objSumChartofAccountsViewModelList.Credit;
            }

            ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate(LegalEntityID, AccountID, StartDate, EndDate);
            ViewBag.GetAllTransactionEntryViewModelListRegisterByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetAllTransactionEntryViewModelListRegisterByLegalEntityIDAccountIDStartDateEndDate(LegalEntityID, AccountID, StartDate, EndDate);
            ViewBag.ClosingBalanceAmount = ClosingBalanceAmount;
            return PartialView("BankRegisterDateRange");
        }
        public void InitpageID(int PageID)
        {

            if (PageID == 1)
            {
                ViewData["Title"] = "Cash receipts";
            }
            else if (PageID == 2)
            {
                ViewData["Title"] = "Cash payments";
            }
            else if (PageID == 3)
            {
                ViewData["Title"] = "Bank receipts";
            }
            else if (PageID == 4)
            {
                ViewData["Title"] = "Bank Payments";
            }
            else if (PageID == 5)
            {
                ViewData["Title"] = "Journal entries";
            }
            else if (PageID == 10)
            {
                ViewData["Title"] = "Closing entries";
            }
            else
            {
                PageID = 1;
                ViewData["Title"] = "Cash receipts";
            }
        }
        public void Initpageinfo(int TransactionInputTypeID)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();

            int CorporateGroupID = int.Parse(strUserData.Split("-")[2]);
            int BusinessGroupID = int.Parse(strUserData.Split("-")[3]);
            int VerticalGroupID = int.Parse(strUserData.Split("-")[4]);
            int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            int LocationID = int.Parse(strUserData.Split("-")[7]);
            int UnitID = int.Parse(strUserData.Split("-")[8]);
            int ProjectID = int.Parse(strUserData.Split("-")[9]);

            ViewBag.CorporateGroupID = CorporateGroupID;
            ViewBag.BusinessGroupID = BusinessGroupID;
            ViewBag.VerticalGroupID = VerticalGroupID;
            ViewBag.LegalEntityID = LegalEntityID;
            ViewBag.ProfitCentreID = ProfitCentreID;
            ViewBag.LocationID = LocationID;
            ViewBag.UnitID = UnitID;
            ViewBag.ProjectID = ProjectID;
            if (TransactionInputTypeID == 1)
            {
                ViewBag.DebitAccounts = _objChartofAccountsService.GetAllCashChartofAccountsByLegalEntity(LegalEntityID);
                ViewBag.CreditAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
            }
            else if (TransactionInputTypeID == 2)
            {
                ViewBag.DebitAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
                ViewBag.CreditAccounts = _objChartofAccountsService.GetAllCashChartofAccountsByLegalEntity(LegalEntityID);
            }
            else if (TransactionInputTypeID == 3)
            {
                ViewBag.DebitAccounts = _objChartofAccountsService.GetAllBankChartofAccountsByLegalEntity(LegalEntityID);
                ViewBag.CreditAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
            }
            else if (TransactionInputTypeID == 4)
            {
                ViewBag.DebitAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
                ViewBag.CreditAccounts = _objChartofAccountsService.GetAllBankChartofAccountsByLegalEntity(LegalEntityID);
            }
            else if (TransactionInputTypeID == 5)
            {
                ViewBag.DebitAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
                ViewBag.CreditAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
            }
            else if (TransactionInputTypeID == 10)
            {
                ViewBag.DebitAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
                ViewBag.CreditAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(LegalEntityID);
            }

            string strUserData1 = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData1.Split("-")[0]);
            TransactionHeadModel objTransactionHeadModel = new TransactionHeadModel();
            objTransactionHeadModel.FinancialYearID = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString())).FinancialYearID; //TransactionEntryDefinations.FinancialYearID;
            objTransactionHeadModel.TransactionDate = DateTime.Now;
            objTransactionHeadModel.TransactionInputTypeID = TransactionInputTypeID;
            objTransactionHeadModel.CorporateGroupID = CorporateGroupID;
            objTransactionHeadModel.BusinessGroupID = BusinessGroupID;
            objTransactionHeadModel.VerticalGroupID = VerticalGroupID;
            objTransactionHeadModel.LegalEntityID = LegalEntityID;
            objTransactionHeadModel.ProfitCentreID = ProfitCentreID;
            objTransactionHeadModel.LocationID = LocationID;
            objTransactionHeadModel.UnitID = UnitID;
            objTransactionHeadModel.ProjectID = ProjectID;
            _objITransactionHeadService.SaveTransactionHead(objTransactionHeadModel, UserID);
            ViewBag.TransactionHeadID = objTransactionHeadModel.TransactionHeadID;
            ViewBag.TransactionRefNo = objTransactionHeadModel.TransactionRefNo;
            ViewBag.TransactionShortRefNo = objTransactionHeadModel.TransactionShortRefNo;
            ViewBag.TransactionInputTypeID = TransactionInputTypeID;
            ViewBag.TransactionDate = objTransactionHeadModel.TransactionDate.ToString("dd-MM-yyyy");
            //if (objTransactionHeadModel != null)
            //{
            //    string TransactionInputTypeslcode = "";
            //    if (TransactionInputTypeID == 1)
            //    {
            //        TransactionInputTypeslcode = "CRCT";//"CSHR"; //"Cash receipts";
            //    }
            //    else if (TransactionInputTypeID == 2)
            //    {
            //        TransactionInputTypeslcode = "CPAY";//"CSHP";//"Cash payments";
            //    }
            //    else if (TransactionInputTypeID == 3)
            //    {
            //        TransactionInputTypeslcode = "BRCT";//"BNKR";// "Bank receipts";
            //    }
            //    else if (TransactionInputTypeID == 4)
            //    {
            //        TransactionInputTypeslcode = "BPAY";//"BNKP";//"Bank Payments";
            //    }
            //    else if (TransactionInputTypeID == 5)
            //    {
            //        TransactionInputTypeslcode = "JV";//"JORN"; //"Journal entries";
            //    }
            //    int count = objTransactionHeadModel.TransactionRefNo.Length;
            //    TransactionInputTypeslcode = TransactionInputTypeslcode + "/" + objTransactionHeadModel.LegalEntityslcode + "/../" + objTransactionHeadModel.SlNo.ToString() + "/" + objTransactionHeadModel.TransactionRefNo.Substring(count - 5, 5);
            //    ViewBag.TransactionShortRefNo = TransactionInputTypeslcode;
            //}

        }
        //[HttpPost]
        //[IgnoreAntiforgeryToken]
        //[AllowAnonymous]
        //public JsonResult GetAccountprofitandLoss([FromBody] TransactionEntryTri
        //{

        //}

        [HttpPost]
        [IgnoreAntiforgeryToken]
        [AllowAnonymous]
        public JsonResult SaveTransactionHeadFromAPI([FromBody] TransactionEntryDefinations objTransactionEntryDefination)
        {
            if (objTransactionEntryDefination.TransactionEntryDetailDefinationsList.Where(x => x.AccountID == 0).Count() == 0)
            {
                TransactionHeadModel objTransactionHeadModel = new TransactionHeadModel
                {
                    LegalEntityID = objTransactionEntryDefination.LegalEntityID,
                    ProfitCentreID = objTransactionEntryDefination.ProfitCentreID,
                    TransactionInputTypeID = objTransactionEntryDefination.TransactionInputTypeID,
                    TransactionTotal = objTransactionEntryDefination.TransactionTotal,
                    DescriptionHead = objTransactionEntryDefination.DescriptionHead,
                    TransactionDate = objTransactionEntryDefination.TransactionEntryDate,
                    ReceiptDetailid=objTransactionEntryDefination.ReceiptDetailid
                };
                objTransactionHeadModel.FinancialYearID = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString())).FinancialYearID;
                EntityMasterModel objEntityMasterModel = _objIEntityMasterService.GetEntityByID(objTransactionHeadModel.LegalEntityID);
                if (objEntityMasterModel != null)
                {
                    objTransactionHeadModel.VerticalGroupID = objEntityMasterModel.ParentEntityID;
                }
                if (objTransactionHeadModel.VerticalGroupID != 0)
                {
                    EntityMasterModel objEntityMasterModelVerticalGroupID = _objIEntityMasterService.GetEntityByID(objTransactionHeadModel.VerticalGroupID);
                    if (objEntityMasterModelVerticalGroupID != null)
                    {
                        objTransactionHeadModel.BusinessGroupID = objEntityMasterModelVerticalGroupID.ParentEntityID;
                    }
                }
                if (objTransactionHeadModel.BusinessGroupID != 0)
                {
                    EntityMasterModel objEntityMasterModelBusinessGroupID = _objIEntityMasterService.GetEntityByID(objTransactionHeadModel.BusinessGroupID);
                    if (objEntityMasterModelBusinessGroupID != null)
                    {
                        objTransactionHeadModel.CorporateGroupID = objEntityMasterModelBusinessGroupID.ParentEntityID;
                    }
                }

                _objITransactionHeadService.SaveTransactionHead(objTransactionHeadModel, 1);

                objTransactionEntryDefination.FinancialYearID = objTransactionHeadModel.FinancialYearID;
                objTransactionEntryDefination.VerticalGroupID = objTransactionHeadModel.VerticalGroupID;
                objTransactionEntryDefination.CorporateGroupID = objTransactionHeadModel.CorporateGroupID;
                objTransactionEntryDefination.BusinessGroupID = objTransactionHeadModel.BusinessGroupID;
                objTransactionEntryDefination.TransactionHeadID = objTransactionHeadModel.TransactionHeadID;
                objTransactionEntryDefination.TransactionEntryDate = objTransactionHeadModel.TransactionDate;
                objTransactionEntryDefination.ReceiptDetailid = objTransactionHeadModel.ReceiptDetailid;
                foreach (var item in objTransactionEntryDefination.TransactionEntryDetailDefinationsList)
                {
                    var _objAccount = _objChartofAccountsService.GetChartofAccountsByID(item.AccountID);
                    item.GroupID = _objAccount.GroupID;
                    item.CategoryID = _objAccount.CategoryID;
                    item.AccountTypeID = _objAccount.AccountTypeID;
                    item.ParentCategoryID = _objAccount.ParentCategoryID;
                    item.SubGroupID = _objAccount.SubGroupID;
                    item.MajorGroupID = _objAccount.MajorGroupID;
                }
                _objTransactionEntryService.SaveTransactionEntry(objTransactionEntryDefination, 1);
                               
                return Json(objTransactionHeadModel.TransactionHeadID);
            }
            else
            {
                return Json("Error");
            }

        }




        [HttpPost]
        [IgnoreAntiforgeryToken]
        [AllowAnonymous]
        public JsonResult DeleteTransactionHeadFromAPI([FromBody] TransactionEntryDefinations objTransactionEntryDefination)
        {

            //if (objTransactionEntryDefination.TransactionEntryDetailDefinationsList.Where(x => x.AccountID == 0).Count() == 0)
            //{
            //TransactionHeadModel objTransactionHeadModel = new TransactionHeadModel
            //{
            //    LegalEntityID = objTransactionEntryDefination.LegalEntityID,
            //    TransactionDate = objTransactionEntryDefination.TransactionEntryDate,
            //    ReceiptDetailid = objTransactionEntryDefination.ReceiptDetailid
            //};

            //objTransactionEntryDefination.TransactionEntryDate = objTransactionHeadModel.TransactionDate;
            //objTransactionEntryDefination.ReceiptDetailid = objTransactionHeadModel.ReceiptDetailid;
            //foreach (var item inTransactionEntryDefinations objTransactionEntryDefination)
            //    {
            //        var _objAccount = _objChartofAccountsService.GetChartofAccountsByID(item.AccountID);
            //        item.GroupID = _objAccount.GroupID;
            //        item.CategoryID = _objAccount.CategoryID;
            //        item.AccountTypeID = _objAccount.AccountTypeID;
            //        item.ParentCategoryID = _objAccount.ParentCategoryID;
            //        item.SubGroupID = _objAccount.SubGroupID;
            //        item.MajorGroupID = _objAccount.MajorGroupID;
            //    }
            string Respone = "";

            TransactionEntryModel objTransactionEntryModel = new TransactionEntryModel
            {

                Receiptdetailid = objTransactionEntryDefination.ReceiptDetailid,
                LegalEntityID=objTransactionEntryDefination.LegalEntityID,
            };
            objTransactionEntryDefination.ReceiptDetailid = objTransactionEntryModel.Receiptdetailid;
            objTransactionEntryDefination.LegalEntityID = objTransactionEntryModel.LegalEntityID;

            _objTransactionEntryService.SaveTransactionEntryDeleteReceipt(objTransactionEntryDefination, 1);
           



            return Json(objTransactionEntryModel.TransactionHeadID);
            //}
            //else
            //{
            //    return Json("Error");
            //}

        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        [AllowAnonymous]
        public JsonResult GetAccountID([FromBody] ValidatingAccount data)
        {
            return Json(_objChartofAccountsService.GetAccountIDByAccountName(data.AccountName, data.LegalEntityID));
        }

        public IActionResult AddTransactionEntry(int ID)
        {
            InitpageID(ID);
            Initpageinfo(ID);
            return PartialView("AddTransactionEntry");
        }
        public IActionResult Transaction(string currentFilter, string searchString, int? page, int ID, int LegalEntityID, int hidPastdatestatus)
        {
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            if (ID != 0)
            {
                ViewBag.encPageID = ID;
                InitpageID(ID);
                Initpageinfo(ID);
            }
            ViewBag.Passdate_status = hidPastdatestatus;

            return PartialView("Transaction");
        }
        public IActionResult TransactionSuccess(string currentFilter, string searchString, int? page,int ID,int LegalEntityID,int TransactionInputID,int TransactionHeadID,string TransactionRefNo)
        {
            TransactionEntryViewModelList   _objTransactionheadviewmodel = new TransactionEntryViewModelList ();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
             LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            TransactionHeadID = int.Parse(_Authodication.Split("-")[6]);
            string _UserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _intUserID = Convert.ToInt32(_UserData.Split("-")[0]);
            ViewBag.encPageID = ID;

            DateTime now = DateTime.Now;
            DateTime StartDate = new DateTime(now.Year, now.Month, 1);
            DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);
            return View("TransactionSuccess",_objTransactionEntryService.GetAllTransactionHead_TransactionID (ID,LegalEntityID, StartDate, EndDate));

            
        }

        public IActionResult TransactionSuccessDateRange( int hidTransactionID, int LegalEntityID, int TransactionInputID, int TransactionHeadID, DateTime StartDate,DateTime EndDate)
        {
            TransactionEntryViewModelList _objTransactionheadviewmodel = new TransactionEntryViewModelList();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            TransactionHeadID = int.Parse(_Authodication.Split("-")[6]);
            string _UserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _intUserID = Convert.ToInt32(_UserData.Split("-")[0]);
            ViewBag.encPageID = hidTransactionID;

            //DateTime now = DateTime.Now;
            //DateTime StartDate = new DateTime(now.Year, now.Month, 1);
            //DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);
            return PartialView("TransactionSuccessDateRange", _objTransactionEntryService.GetAllTransactionHead_TransactionID(hidTransactionID, LegalEntityID, StartDate, EndDate));


        }
        private PaginatedList<TransactionEntryViewModelList> GetPageTransaction(string currentFilter, string searchString, int UserId, int? page)
        {


            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int CompanyID = int.Parse(_Authodication.Split("-")[5]);
            int LocationID =int.Parse(_Authodication.Split("-")[6]);
            int BranchID = Convert.ToInt32(_Authodication.Split("-")[7]);
            int UserID = Convert.ToInt32(_Authodication.Split("-")[8]);
            int RoleID = Convert.ToInt32(_Authodication.Split("-")[9]);

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var _Transaction = _objTransactionEntryService.GetAllCashReceipts(CompanyID, LocationID).OrderByDescending(x => x.TransactionHeadID).ToList();

            if (!String.IsNullOrEmpty(searchString))
            {
                _Transaction = _Transaction.Where(s => s.TransactionShortRefNo.ToLower().Contains(searchString.ToLower())
                                       || s.TransactionEntryDate.ToShortDateString().Contains(searchString.ToString())).ToList(); 
            }
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            return PaginatedList<TransactionEntryViewModelList>.Create(_Transaction, pageNumber, pageSize);
        }

        public IActionResult ViewVoucherDetails(int ID,string AccountName,int LegalEntityID)
        {

            TransactionEntryViewModelList objTransactionHead = new TransactionEntryViewModelList();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            AccountName = _Authodication.Split("-")[6];
            ViewBag.TransactionHeadID = objTransactionHead.TransactionHeadID;
            ViewBag.TransactionInputTypeID = objTransactionHead.TransactionInputTypeID;
            return PartialView("ViewVoucherDetails", _objTransactionEntryService.GetAllTransactionHead_Transactiondetails(ID, LegalEntityID));
        }

        //For Block Entry
        public IActionResult ViewVoucherDetailsUpdate(int ID, string AccountName, int LegalEntityID)
        {
            TransactionEntryViewModelList objTransactionHead = new TransactionEntryViewModelList();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            AccountName = _Authodication.Split("-")[6];
            ViewBag.TransactionHeadID = objTransactionHead.TransactionHeadID;
            ViewBag.TransactionInputTypeID = objTransactionHead.TransactionInputTypeID;
            return PartialView("ViewVoucherDetailsUpdate", _objTransactionEntryService.GetAllTransactionHead_Transactiondetails(ID, LegalEntityID));
        }

        public IActionResult TransactionCancel(int ID)
        {
            InitpageID(ID);

            return View();
        }
        public IActionResult EditTransactionEntry(int ID)
        {

            TransactionEntryModel objTransactionEntryModel = new TransactionEntryModel();
            objTransactionEntryModel = _objTransactionEntryService.GetTransactionEntryByID(ID);
            return PartialView("EditTransactionEntry", objTransactionEntryModel);
        }
        public IActionResult ViewTransactionEntryByTransactionHeadID(int ID)
        {
            TransactionHeadViewModelList objTransactionHeadViewModelList = _objITransactionHeadService.GetAllTransactionHeadViewModelListByTransactionHeadID(ID);
            ViewBag.GetAllTransactionEntryViewModelListByTransactionHeadID = _objTransactionEntryService.GetAllTransactionEntryViewModelListByTransactionHeadID(ID);
            int PageID = objTransactionHeadViewModelList.TransactionInputTypeID;
            //ViewBag.encPageID = PageID;
            //ViewBag.TransactionInputTypeID = PageID;
            //ViewBag.TransactionDate = objTransactionHeadViewModelList.TransactionDate;
            //ViewBag.TransactionRefNo = objTransactionHeadViewModelList.TransactionRefNo;

            InitpageID(PageID);
            //TransactionEntryModel objTransactionEntryModel = new TransactionEntryModel();
            //objTransactionEntryModel = _objTransactionEntryService.GetTransactionEntryByID(ID);

            return PartialView("ViewTransactionEntryByTransactionHeadID", objTransactionHeadViewModelList);
        }
        public IActionResult DeleteTransactionEntrymodel(int ID)
        {
            ViewBag.TransactionEntryID = ID;
            return PartialView("DeleteTransactionEntrymodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteTransactionEntry(int ID)
        {

            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objTransactionEntryService.DeleteTransactionEntry(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertTransactionEntry([FromBody] TransactionEntryDefinations objTransactionEntryDefinations)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objTransactionEntryService.SaveTransactionEntry(objTransactionEntryDefinations, UserID);
            }
            return Json("OK");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult UpdateTransactionIsActionStatus(string IsStatus, string TransactionHeadID_add)
        {
            string Respone = "";
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);

            if (ModelState.IsValid)
            {
                Respone = _objTransactionEntryService.UpdateTransactionEntry(IsStatus, TransactionHeadID_add);
            }
            return Json("OK");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteTransactionHead(int TransactionHeadID,int ID,int LegalEntityID)
        {
            TransactionEntryViewModelList  _objTransactionheadviewmodel = new TransactionEntryViewModelList();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
           // _objTransactionheadviewmodel.GetAllTransactionHead_TransactionID = _objTransactionEntryService.GetAllTransactionHead_TransactionID(ID, LegalEntityID);

            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            //_objTransactionheadviewmodel.DeleteTransactionHead = _objTransactionEntryService.DeleteTransactionHead(TransactionHeadID,ID,LegalEntityID);
            return Json("ok");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult ClosingBalanceAmount(int LegalEntityID, int AccountID)
        {

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = _objTransactionEntryService.GetClosingBalanceChartofAccountsViewModelByStartDate(LegalEntityID, AccountID, Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString()));
            decimal ClosingBalanceAmount = 0;
            if (objSumChartofAccountsViewModelList != null)
            {
                ClosingBalanceAmount = objSumChartofAccountsViewModelList.Debit - objSumChartofAccountsViewModelList.Credit;
            }
            return Json(ClosingBalanceAmount);
        }
        #region closer print

        // #region Printable
        //[HttpPost]
        //[IgnoreAntiforgeryToken]
        //public JsonResult GetTransactionPrint([FromBody] Printable data)
        //{
        //    TransactionEntryModel objEntry = _AccountingDBcontext.AccountingTransactionEntryModel.Where(x => x.TransactionHeadID == data.printID).FirstOrDefault();
        //    //TransactionEntryService objTransaction = _objTransactionEntryService.GetTransactionEntryByID(data.printID);
        //    string _Authentication = User.Claims.Where(x => x.Type == ClaimTypes.Authentication).Select(x => x.Value).FirstOrDefault();
        //    int _companyid = Convert.ToInt32(_Authentication.Split("-")[0]);
        //    int LocationID = Convert.ToInt32(_Authentication.Split("-")[1]);
        //    int BranchID = Convert.ToInt32(_Authentication.Split("-")[2]);

        //    EntityMasterModel  objCompany = _AccountingDBcontext.AccountingEntityMasterModel.Where(x => x.EntityID == _companyid).FirstOrDefault();
        //    LocationModel objLocation = _AccountingDBcontext.AccountingLocationModel.Where(x => x.LocationID == LocationID).FirstOrDefault();

        //    TransactionHeadViewModelList objTransactionHead = (from _TE in _AccountingDBcontext.AccountingTransactionEntryModel
        //                                                       join _TH in _AccountingDBcontext.AccountingTransactionHeadModel on _TE.TransactionHeadID equals _TH.TransactionHeadID into M1
        //                                                       from _Description in M1.DefaultIfEmpty()
        //                                                       where _TE.TransactionHeadID == data.printID
        //                                                       select new TransactionHeadViewModelList
        //                                                       {
        //                                                           DescriptionHead = _Description.DescriptionHead
        //                                                       }).FirstOrDefault();

        //    var objTransactionEntryViewModelList = (from objTransactionEntry in _AccountingDBcontext.AccountingTransactionEntryModel
        //                                            join _TH in _AccountingDBcontext.AccountingTransactionHeadModel on objTransactionEntry.TransactionHeadID equals objTransactionHead.TransactionHeadID into T1
        //                                            from _Transaction in T1.DefaultIfEmpty()
        //                                            where objTransactionEntry.IsActive == true && objTransactionEntry.TransactionHeadID == data.printID
        //                                            select new TransactionEntryViewModelList
        //                                            {
        //                                                TransactionHeadID = objTransactionEntry.TransactionHeadID,
        //                                                TransactionEntryDate = objTransactionEntry.TransactionEntryDate,
        //                                                Debit = objTransactionEntry.Debit,
        //                                                Credit = objTransactionEntry.Credit,
        //                                                AccountID = objTransactionEntry.AccountID,
        //                                                AccountName = _AccountingDBcontext.AccountingChartofAccountsModel.Where(x => x.AccountID == objTransactionEntry.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
        //                                                TransactionShortRefNo = objTransactionEntry.TransactionShortRefNo,
        //                                                Description = objTransactionEntry.Description,
        //                                                LegalEntityID = objTransactionEntry.LegalEntityID,
        //                                                LegalEntityName = _AccountingDBcontext.AccountingEntityMasterModel.Where(x => x.EntityID == objTransactionEntry.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
        //                                                TransactionInputTypeID = objTransactionEntry.TransactionInputTypeID,
        //                                                TransactionInputTypeName = _AccountingDBcontext.AccountingTransactionInputTypeModel.Where(x => x.TransactionInputTypeID == objTransactionEntry.TransactionInputTypeID && x.IsActive == true).FirstOrDefault().TransactionInputTypeName,
        //                                            }).ToList();


        //    string fileName = Path.Combine(_env.WebRootPath, "PrintableDesign", "PrintContents.xml");
        //    string getContents = Common.Validation.ReadPrintContents("ReceiptCreation", fileName);
        //    StringBuilder strBuilderCollect = new StringBuilder();

        //    StringBuilder strBuilderBalance = new StringBuilder();
        //    decimal decBalanceTot = 0;
        //    string strFinalContents = 0;
        //    foreach (TransactionEntryViewModelList objListdef in objTransactionEntryViewModelList)
        //    {
        //        string Transactionref_no = objListdef.TransactionShortRefNo;
        //        decimal Debit = objListdef.Debit;
        //        decimal Credit = objListdef.Credit;
        //        int AccountID = objListdef.AccountID;
        //        string Description = objListdef.Description;
        //        string date = string.Format("{0:dd/MM/yyyy}", objListdef.TransactionEntryDate);
        //        string LegalEntity = objListdef.LegalEntityName;
        //        string AccountName = objListdef.AccountName;
        //        string categoryName = objListdef.TransactionInputTypeName;
        //        string DescriptionHead = objListdef.DescriptionHead;
        //        strFinalContents = Common.Common.ConvertNumbertoWords((long)Convert.ToDecimal(objListdef.Debit));
        //    }



        //    if (decBalanceTot > 0 & Convert.ToString(decBalanceTot) != " ") strBuilderBalance.AppendFormat("<p>Balance = {0}</p>", decBalanceTot);

        //    string strFinalContents = getContents.Replace("$#COMPANYPHONE#$", objCompany.LandLineNo)
        //        .Replace("$#COMPANYNAME#$", objCompany.CompanyName)
        //        .Replace("$#COMPANYADDRESS#$", objCompany.Address)
        //        .Replace("$#RECEIPTNO#$", "" + objReceipt.intReceiptDetailId)
        //        .Replace("$#RECEIPTDATE#$", objReceipt.strReceiptDate)
        //        .Replace("$#LOCATIONCODE#$", objLocation.LocationCode)
        //        .Replace("$#ACCOUNTNO#$", objReceipt.strLoanaccountNo)
        //        .Replace("$#CUSTOMERNAME#$", objReceipt.strCustomerName)
        //        .Replace("$#AMOUNTINWORDS#$", Common.Common.ConvertNumbertoWords((long)Convert.ToDecimal(objReceipt.strTotalAmount)))
        //        .Replace("$#PAYMODE#$", objReceipt.strPaymode)
        //        .Replace("$#DETAILS#$", objReceipt.strReferenceOrChequeNo)
        //        .Replace("$#PAYMENTTYPE#$", objclosure.strClosureType)
        //        .Replace("<b>$#CURRINSTALL#$/$#TOTINSTALL#$</b> installment of the", "")
        //        .Replace("$#TOTINSTALL#$", "" + objcontract.Tendure)
        //        .Replace("$#VEHICLENO#$", objPDD.RegistrationNo)
        //        .Replace("$#REGDATE#$", objPDD.RegistrationDate)
        //        .Replace("$#TOTAL#$", Math.Round(Convert.ToDecimal(objReceipt.strTotalAmount), 2) + "")
        //        .Replace("$#DENOCOLLECT#$", strBuilderCollect.ToString())
        //        .Replace("$#DENOBALANCE#$", strBuilderBalance.ToString());

        //    return Json(strFinalContents);

        //}
        //#endregion


        //[HttpPost]
        //[IgnoreAntiforgeryToken]
        //public JsonResult GetTransactionforPrint([FromBody] Printable data)
        //{
        //        // SaveClosureRequest objcloser = _objClosureRequestService.GetAllClosureReques

        //    TransactionHeadModel ObjTransactionHeadModel = _accountingDbContext.AccountingTransactionHeadModel.Where(x => x.TransactionHeadID == data.printID).FirstOrDefault();
        //    ChartofAccountsModel  objaccount = _accountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == data.Account_id).FirstOrDefault();
        //    //TransactionHeadModel  objTransactionHeadID = _objITransactionHeadService.GetTransactionHeadByID(data.printID);
        //    string _Authentication = User.Claims.Where(x => x.Type == ClaimTypes.Authentication).Select(x => x.Value).FirstOrDefault();
        //    int _companyid = Convert.ToInt32(_Authentication.Split("-")[0]);
        //    int LocationID = Convert.ToInt32(_Authentication.Split("-")[1]);
        //    int BranchID = Convert.ToInt32(_Authentication.Split("-")[2]);
        //    EntityMasterModel  objCompany = _accountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == _companyid).FirstOrDefault();
        //    LocationModel objLocation = _accountingDbContext.AccountingLocationModel .Where(x => x.LocationID == LocationID).FirstOrDefault();
        //    TransactionHeadModel ObjTransactionHeadModel_Voucher = _accountingDbContext.AccountingTransactionHeadModel.Where(x => x.TransactionRefNo == ObjTransactionHeadModel.TransactionRefNo).FirstOrDefault();


        //    string fileName = Path.Combine(_env.WebRootPath, "PrintableDesign", "PrintContents.xml");
        //    string getContents = Common.Validation.ReadPrintContents("ReceiptCreation", fileName);
        //    StringBuilder strBuilderCollect = new StringBuilder();

        //    StringBuilder strBuilderBalance = new StringBuilder();
        //    decimal decBalanceTot = 0;


        //    //if (ObjTransactionHeadModel_Voucher. != 0)
        //    //{
        //    //    objReceipt.strPaymode = _objLMSDbcontext.LMSConfig_UDOptions.Where(x => x.UDID == objReceipt.intPaymode).Select(x => x.UDDescription).FirstOrDefault();
        //    //}
        //    //else
        //    //{
        //    //    objReceipt.strPaymode = "";
        //    //}

        //    if (decBalanceTot > 0 & Convert.ToString(decBalanceTot) != " ") strBuilderBalance.AppendFormat("<p>Balance = {0}</p>", decBalanceTot);

        //    string strFinalContents = getContents.Replace("$#COMPANYNAME#$", objCompany.Name)
        //        .Replace("$#VoucherNo#$", "" + ObjTransactionHeadModel_Voucher.SlNo)
        //        .Replace("$#Date#$",Convert.ToString (ObjTransactionHeadModel_Voucher.TransactionDate))
        //        .Replace("$#DescriptionHead#$", ObjTransactionHeadModel.DescriptionHead)
        //        .Replace("$#ACCOUNTNAME#$", objaccount.AccountName)
        //        .Replace("$#AMOUNTINWORDS#$", Common.Common.ConvertNumbertoWords((long)Convert.ToDecimal(ObjTransactionHeadModel_Voucher.TransactionTotal)))
        //        .Replace("$#Amount#$", Math.Round(Convert.ToDecimal(ObjTransactionHeadModel.TransactionTotal), 2) + "")
        //        .Replace("$#DENOCOLLECT#$", strBuilderCollect.ToString())
        //        .Replace("$#DENOBALANCE#$", strBuilderBalance.ToString());

        //    return Json(strFinalContents);
        //}

        #endregion

        //public ActionResult Index()
        //{
        //    IEnumerable<> SaveContracts = null;
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            //client.BaseAddress = new Uri("http://202.65.131.134/ChartofAccounts/BindGroupByParentCategoryID?TypeID=1&ParentID=0");
        //            client.BaseAddress = new Uri("http://202.65.131.134/ChartofAccounts/BindGroupByParentCategoryID?TypeID=2&ParentID=2");
        //            //HTTP GET
        //            var responseTask = client.GetAsync("student");
        //            responseTask.Wait();

        //            var result = responseTask.Result;
        //            if (result.IsSuccessStatusCode)
        //            {
        //                var readTask = result.Content.ReadAsAsync<IList<TransactionHeadLMSViewModelCode>>();
        //                readTask.Wait();

        //                SaveContracts = readTask.Result;
        //            }
        //            else //web api sent error response 
        //            {
        //                //log response status here..

        //                SaveContracts = Enumerable.Empty<TransactionHeadLMSViewModelCode>();

        //                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
        //            }
        //        }
        //    }

        //    return View();
        //}


        //[HttpPost]
        //[IgnoreAntiforgeryToken]
        public IActionResult  GroupComparisonReport()
        {
            TransactionEntryViewModelList _objTransactionheadviewmodel = new TransactionEntryViewModelList();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int  LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            int  BusinessAreaID = int.Parse(_Authodication.Split("-")[6]);
            ViewBag.LegalEntityID = LegalEntityID;
            ViewBag.BusinessAreaID = BusinessAreaID;
            FinancialYearModel objfinancialYearModel = _objIFinancialYearService.GetFinancialYearByDate(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            if (objfinancialYearModel != null)
            {
                ViewBag.FinancialYearID = objfinancialYearModel.FinancialYearID;
                ViewBag.StartDate = objfinancialYearModel.StartDate.ToString("dd-MM-yyyy");
                ViewBag.EndDate = objfinancialYearModel.EndDate.ToString("dd-MM-yyyy");
                ViewBag.CurrentDate = DateTime.Now.ToString("dd-MM-yyyy");

            }

            return View("GroupComparisonReport");
        }
        [HttpGet]
        [IgnoreAntiforgeryToken]

        public IActionResult   GroupComparisonReportRange(int BusinessAreaID, string StartDate, string EndDate )
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();


            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            //int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            //int LocationID = int.Parse(strUserData.Split("-")[7]);
            //int UnitID = int.Parse(strUserData.Split("-")[8]);
            //int ProjectID = int.Parse(strUserData.Split("-")[9]);
            //int PageID = 1;
            ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetGroupComparisonReportRangeforProfitandLoss(BusinessAreaID,  StartDate, EndDate);
            return PartialView("GroupComparisonReportRange",_objTransactionEntryService.GetGroupComparisonReportRangeforProfitandLoss(BusinessAreaID, StartDate, EndDate));
        }

        [HttpGet]
        [IgnoreAntiforgeryToken]
        public JsonResult GroupComparisonReportRangeJson(int BusinessAreaID, string StartDate, string EndDate, string StartDate1, string EndDate1)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            ProfitandlossAmount JsonResult = new ProfitandlossAmount();
                       
            //int LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            //int ProfitCentreID = int.Parse(strUserData.Split("-")[6]);
            //int LocationID = int.Parse(strUserData.Split("-")[7]);
            //int UnitID = int.Parse(strUserData.Split("-")[8]);
            //int ProjectID = int.Parse(strUserData.Split("-")[9]);
            //int PageID = 1;
            JsonResult = _objTransactionEntryService.GetGroupComparisonReportRangeforProfitandLosswithjson(BusinessAreaID, StartDate, EndDate,StartDate1,EndDate1 );
            return Json(JsonResult);
        }

        #region Cancel Voucher
        public IActionResult CancelledVouchers()
        {
            ViewBag.TransactionInputType = _objTransactionEntryService.GetAccounting_TransactionInputType();
            return View("CancelledVouchers");
        }

     

        public IActionResult CashReceiptTransaction()
        {
            
            ViewBag.TransactionInputType = _objTransactionEntryService.GetAccounting_TransactionInputTypecash();
            return View("CashReceiptTransaction");
        }


        [HttpGet]
        [IgnoreAntiforgeryToken]
        public IActionResult CancelledVoucherDateRange(int Transaction_TypeId, DateTime StartDate, DateTime EndDate)
        {
            List<TransactionEntryModel> Model = new List<TransactionEntryModel>();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            //ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetGroupComparisonReportRangeforProfitandLoss(BusinessAreaID, StartDate, EndDate);
            //ViewBag.ListOfTrnEntry = _objTransactionEntryService.GetAllTransaction_CancelledVouchers(Transaction_TypeId, StartDate, EndDate);
            ViewBag.GetTrnModel = _objTransactionEntryService.GetAllTransaction_CancelledVouchers(Transaction_TypeId, StartDate, EndDate, LegalEntityID);
            return PartialView("CancelledVoucherDateRange", _objTransactionEntryService.GetAllTransaction_CancelledVouchers(Transaction_TypeId, StartDate, EndDate, LegalEntityID));
        }



        [HttpGet]
        [IgnoreAntiforgeryToken]
        public IActionResult CashReceiptTransactionDateRange(int Transaction_TypeId, DateTime StartDate, DateTime EndDate)
        {
            List<TransactionEntryModel> Model = new List<TransactionEntryModel>();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            //ViewBag.GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate = _objTransactionEntryService.GetGroupComparisonReportRangeforProfitandLoss(BusinessAreaID, StartDate, EndDate);
            //ViewBag.ListOfTrnEntry = _objTransactionEntryService.GetAllTransaction_CancelledVouchers(Transaction_TypeId, StartDate, EndDate);
            ViewBag.GetTrnModel = _objTransactionEntryService.GetAllTransaction_TransactionInputTypecash(Transaction_TypeId, StartDate, EndDate, LegalEntityID);
            return PartialView("CashReceiptTransactionDateRange", _objTransactionEntryService.GetAllTransaction_TransactionInputTypecash(Transaction_TypeId, StartDate, EndDate, LegalEntityID));
        }

        [HttpGet]
        [IgnoreAntiforgeryToken]
        public PartialViewResult CancelledViewVoucherDetails(int ID)
        {
            TransactionEntryViewModelList objTransactionHead = new TransactionEntryViewModelList();
            string _Authodication = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int LegalEntityID = int.Parse(_Authodication.Split("-")[5]);
            //AccountName = _Authodication.Split("-")[6];
            ViewBag.TransactionHeadID = objTransactionHead.TransactionHeadID;
            ViewBag.TransactionInputTypeID = objTransactionHead.TransactionInputTypeID;
            return PartialView("CancelledViewVoucherDetails", _objTransactionEntryService.GetAllTransactionHead_Transactiondetails(ID, LegalEntityID));
        }

        #endregion

    }
}