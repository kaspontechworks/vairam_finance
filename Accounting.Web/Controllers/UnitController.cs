﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class UnitController : Controller
    {
        private readonly IUnitService _objUnitService;
        public UnitController(IUnitService objUnitService)
        {
            _objUnitService = objUnitService;
        }
        public IActionResult Index()
        {

            UnitViewModel _objUnitViewModel = new UnitViewModel();
            _objUnitViewModel.GetAllUnit = _objUnitService.GetAllUnit();
            return View(_objUnitViewModel);
        }
        
        public IActionResult AddUnit()
        {
           
            return PartialView("AddUnit");
        }
        public IActionResult EditUnit(int ID)
        {
           
            UnitModel objUnitModel = new UnitModel();
            objUnitModel = _objUnitService.GetUnitByID(ID);
            return PartialView("EditUnit", objUnitModel);
        }
        public IActionResult DeleteUnitmodel(int ID)
        {
            ViewBag.UnitID = ID;
            return PartialView("DeleteUnitmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteUnit(int ID)
        {

            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objUnitService.DeleteUnit(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertUnit([FromBody] UnitModel objUnitModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objUnitService.SaveUnit(objUnitModel, UserID);
            }
            return Json("OK");
        }
    }
}