﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectService _objProjectService;
        public ProjectController(IProjectService objProjectService)
        {
            _objProjectService = objProjectService;
        }
        public IActionResult Index()
        {

            ProjectViewModel _objProjectViewModel = new ProjectViewModel();
            _objProjectViewModel.GetAllProject = _objProjectService.GetAllProject();
            return View(_objProjectViewModel);
        }
        
        public IActionResult AddProject()
        {
            
            return PartialView("AddProject");
        }
        public IActionResult EditProject(int ID)
        {
           
            ProjectModel objProjectModel = new ProjectModel();
            objProjectModel = _objProjectService.GetProjectByID(ID);
            return PartialView("EditProject", objProjectModel);
        }
        public IActionResult DeleteProjectmodel(int ID)
        {
            ViewBag.ProjectID = ID;
            return PartialView("DeleteProjectmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteProject(int ID)
        {

            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objProjectService.DeleteProject(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertProject([FromBody] ProjectModel objProjectModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objProjectService.SaveProject(objProjectModel, UserID);
            }
            return Json("OK");
        }
    }
}