﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class LedgerGroupMasterController : Controller
    {
        private readonly IChartofAccountsService _objChartofAccountsService;
        private readonly ILedgerGroupMasterService _objLedgerGroupMasterService;
        public LedgerGroupMasterController( ILedgerGroupMasterService objLedgerGroupMasterService, IChartofAccountsService objChartofAccountsService)
        {
            _objLedgerGroupMasterService = objLedgerGroupMasterService;
            _objChartofAccountsService = objChartofAccountsService;
        }
        public IActionResult Index()
        {

            //LedgerGroupMasterViewModel _objLedgerGroupMasterViewModel = new LedgerGroupMasterViewModel();
            //_objLedgerGroupMasterViewModel.GetAllLedgerGroupMaster = _objLedgerGroupMasterService.GetAllLedgerGroupMaster();
            //return View(_objLedgerGroupMasterViewModel);
            ViewBag.GetAllLedgerGroupMasterViewModelList = _objLedgerGroupMasterService.GetAllLedgerGroupMasterViewModelList();
            return View();
        }
        public IActionResult ShowLedgerGroupMasterTableView(int ID)
        {
            return PartialView("ShowLedgerGroupMasterTableView", _objLedgerGroupMasterService.GetAllLedgerGroupMasterByParentCategoryID(ID));
        }
        public IActionResult AddLedgerGroupMaster()
        {
            ViewBag.GetAllGroupTypeModel = _objLedgerGroupMasterService.GetAllGroupTypeModel();
           
            return PartialView("AddLedgerGroupMaster");
        }
        public IActionResult EditLedgerGroupMaster(int ID)
        {
            ViewBag.GetAllGroupTypeModel = _objLedgerGroupMasterService.GetAllGroupTypeModel();
            LedgerGroupMasterModel objLedgerGroupMasterModel = _objLedgerGroupMasterService.GetLedgerGroupMasterByID(ID);
            ViewBag.GetAllLedgerGroupMasterByGroupType = _objLedgerGroupMasterService.GetAllLedgerGroupMasterByGroupType(objLedgerGroupMasterModel.GroupTypeID-1);
            return PartialView("EditLedgerGroupMaster", objLedgerGroupMasterModel);
        }
        public IActionResult DeleteLedgerGroupMastermodel(int ID)
        {
            ViewBag.LedgerGroupMasterID = ID;
            if (_objChartofAccountsService.GetAllChartofAccountsByGroupID(ID).Count > 0)
            {
                return PartialView("noDeleteLedgerGroupMastermodel");
            }
            else
            {
                return PartialView("DeleteLedgerGroupMastermodel");
            }
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteLedgerGroupMaster(int ID)
        {
          
            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
               
                CategoryID = _objLedgerGroupMasterService.DeleteLedgerGroupMaster(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertLedgerGroupMaster([FromBody] LedgerGroupMasterModel objLedgerGroupMasterModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objLedgerGroupMasterService.SaveLedgerGroupMaster(objLedgerGroupMasterModel, UserID);
            }
            return Json("OK");
        }
        [HttpGet]
        public JsonResult BindLedgerGroupMaster(int ID)
        {
            return Json(_objLedgerGroupMasterService.GetAllLedgerGroupMasterByGroupType(ID));
        }
    }
}