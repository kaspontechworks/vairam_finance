﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Common;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserManagementService _objUserManagementService;
        private readonly IEntityMasterService _objIEntityMasterService;
        private readonly ILegalEntityProjectMappingService _objLegalEntityProjectMappingService;
        private readonly ILegalEntityProfitCentreMappingService _objLegalEntityProfitCentreMappingService;
        private readonly ILegalEntityLocationMappingService _objLegalEntityLocationMappingService;
        private readonly ILegalEntityUnitMappingService _objLegalEntityUnitMappingService;
        public UserController(IUserManagementService objUserManagementService, IEntityMasterService objIEntityMasterService, ILegalEntityProjectMappingService objLegalEntityProjectMappingService, ILegalEntityProfitCentreMappingService objLegalEntityProfitCentreMappingService, ILegalEntityLocationMappingService objLegalEntityLocationMappingService, ILegalEntityUnitMappingService objLegalEntityUnitMappingService)
        {
            _objUserManagementService = objUserManagementService;
            _objIEntityMasterService = objIEntityMasterService;
            _objLegalEntityProjectMappingService = objLegalEntityProjectMappingService;
            _objLegalEntityProfitCentreMappingService = objLegalEntityProfitCentreMappingService;
            _objLegalEntityLocationMappingService = objLegalEntityLocationMappingService;
            _objLegalEntityUnitMappingService = objLegalEntityUnitMappingService;
        }
        public IActionResult Index()
        {

            UserManagementViewModel _objUserViewModel = new UserManagementViewModel();
            _objUserViewModel.AllUsers = _objUserManagementService.GetAllUsers();
            return View(_objUserViewModel);
        }
       
        public IActionResult AddUser()
        {
           
            ViewBag.AllRoles = _objUserManagementService.GetAllRoles();
            ViewBag.AllCorporateGroup = _objIEntityMasterService.GetEntityByLevelID(2);
            ViewBag.AllBusinessGroup = _objIEntityMasterService.GetEntityByLevelID(3);
            ViewBag.AllVerticalGroup = _objIEntityMasterService.GetEntityByLevelID(4);
            ViewBag.AllLegalEntity = _objIEntityMasterService.GetEntityByLevelID(5);
            return PartialView("AddUser");
        }
        public IActionResult EditUser(int ID)
        {
            UserModel objUserModel = new UserModel();
            objUserModel = _objUserManagementService.GetUserById(ID);
            ViewBag.AllCorporateGroup = _objIEntityMasterService.GetEntityByLevelID(2);
            if (objUserModel.CorporateGroupID > 0)
            {
                ViewBag.AllBusinessGroup = _objIEntityMasterService.GetEntityByParentEntityID(objUserModel.CorporateGroupID);
            }
            else
            {
                ViewBag.AllBusinessGroup = _objIEntityMasterService.GetEntityByLevelID(3);
            }
            if (objUserModel.BusinessGroupID > 0)
            {
                ViewBag.AllVerticalGroup = _objIEntityMasterService.GetEntityByParentEntityID(objUserModel.BusinessGroupID);
            }
            else
            {
                ViewBag.AllVerticalGroup = _objIEntityMasterService.GetEntityByLevelID(4);
            }
            if (objUserModel.VerticalGroupID > 0)
            {
                ViewBag.AllLegalEntity = _objIEntityMasterService.GetEntityByParentEntityID(objUserModel.VerticalGroupID);
            }
            else
            {
                ViewBag.AllLegalEntity = _objIEntityMasterService.GetEntityByLevelID(5);
            }
            objUserModel.Password = Cryptography.Decrypt(objUserModel.Password, "Accounting");
            ViewBag.AllRoles = _objUserManagementService.GetAllRoles();
            ViewBag.AllProfitCentre = _objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMappingViewModelList(objUserModel.LegalEntityID);
            ViewBag.AllLocation = _objLegalEntityLocationMappingService.GetAllLegalEntityLocationMappingViewModelListByLegalEntityIDProfitCentreID(objUserModel.LegalEntityID, objUserModel.ProfitCentreID);
            ViewBag.AllUnit = _objLegalEntityUnitMappingService.GetAllLegalEntityUnitMappingViewModelListByLegalEntityIDProfitCentreIDLocationID(objUserModel.LegalEntityID, objUserModel.ProfitCentreID, objUserModel.LocationID);
            ViewBag.AllProject = _objLegalEntityProjectMappingService.GetAllLegalEntityProjectMappingViewModelListLegalEntityIDProfitCentreIDLocationIDUnitID(objUserModel.LegalEntityID, objUserModel.ProfitCentreID, objUserModel.LocationID,objUserModel.UnitID);
            return PartialView("EditUser", objUserModel);
        }
        public IActionResult DeleteUsermodel(int ID)
        {
            ViewBag.UserID = ID;
            return PartialView("DeleteUsermodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteUser(int ID)
        {

            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objUserManagementService.DeleteUser(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertUser([FromBody] UserModel objUserModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                if (objUserModel.Password != "")
                {
                    objUserModel.Password = Cryptography.Encrypt(objUserModel.Password, "Accounting");
                }
                _objUserManagementService.SaveUser(objUserModel, UserID);
            }
            return Json("OK");
        }
        [HttpGet]
        public JsonResult BindEntityByParentEntityID(int ParentEntityID)
        {
            return Json(_objIEntityMasterService.GetEntityByParentEntityID(ParentEntityID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityProfitCentreMappingByLegalEntityID(int EntityID)
        {
            return Json(_objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMappingViewModelList(EntityID));
        }

        
        [HttpGet]
        public JsonResult BindLegalEntityLocationMappingByLegalEntityIDProfitCentreID(int EntityID, int ProfitCentreID)
        {
            return Json(_objLegalEntityLocationMappingService.GetAllLegalEntityLocationMappingViewModelListByLegalEntityIDProfitCentreID(EntityID, ProfitCentreID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityUnitMappingByLegalEntityIDProfitCentreIDLocationID(int EntityID, int ProfitCentreID, int LocationID)
        {
            return Json(_objLegalEntityUnitMappingService.GetAllLegalEntityUnitMappingViewModelListByLegalEntityIDProfitCentreIDLocationID(EntityID, ProfitCentreID, LocationID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityProjectMappingByLegalEntityIDProfitCentreIDLocationIDUnitID(int EntityID, int ProfitCentreID, int LocationID, int UnitID)
        {
            return Json(_objLegalEntityProjectMappingService.GetAllLegalEntityProjectMappingViewModelListLegalEntityIDProfitCentreIDLocationIDUnitID(EntityID, ProfitCentreID, LocationID, UnitID));
        }
    }
}