﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class LocationController : Controller
    {
       
        private readonly ILocationService _objLocationService;
        public LocationController(ILocationService objLocationService)
        {
            _objLocationService = objLocationService;
        }
        public IActionResult Index()
        {

            LocationViewModel _objLocationViewModel = new LocationViewModel();
            _objLocationViewModel.GetAllLocation = _objLocationService.GetAllLocation();
            return View(_objLocationViewModel);
        }
        
        public IActionResult AddLocation()
        {
            
            return PartialView("AddLocation");
        }
        public IActionResult EditLocation(int ID)
        {
            
            LocationModel objLocationModel = new LocationModel();
            objLocationModel = _objLocationService.GetLocationByID(ID);
            return PartialView("EditLocation", objLocationModel);
        }
        public IActionResult DeleteLocationmodel(int ID)
        {
            ViewBag.LocationID = ID;
            return PartialView("DeleteLocationmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteLocation(int ID)
        {

            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objLocationService.DeleteLocation(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertLocation([FromBody] LocationModel objLocationModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objLocationService.SaveLocation(objLocationModel, UserID);
            }
            return Json("OK");
        }
    }
}