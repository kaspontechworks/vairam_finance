﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class ProductManagementController : Controller
    {
        private readonly IProductManagementService _objProductManagementService;

        public ProductManagementController(IProductManagementService objProductManagementService) { _objProductManagementService = objProductManagementService; }

        public IActionResult Index()
        {
            ProductManagementViewModel _objProductManagementViewModel = new ProductManagementViewModel();
            _objProductManagementViewModel.GetAllProduct = _objProductManagementService.GetAllProduct();
            return View(_objProductManagementViewModel);
        }
        public IActionResult Index3()
        {
            ProductManagementViewModel _objProductManagementViewModel = new ProductManagementViewModel();
            _objProductManagementViewModel.GetAllProduct = _objProductManagementService.GetAllProduct();
            return View(_objProductManagementViewModel);
        }
        public IActionResult AddProduct()
        {
            ViewBag.AllCase = _objProductManagementService.GetAllCase();
            return PartialView("AddProduct");
        }
        public IActionResult EditProduct(int ID)
        {
            ViewBag.AllCase = _objProductManagementService.GetAllCase();
            ProductManagementViewModel _objCaseManagementViewModel = new ProductManagementViewModel();
            _objCaseManagementViewModel.GetProductID = _objProductManagementService.GetProductByID(ID);
            _objCaseManagementViewModel.GetAllProTatDegByProID = _objProductManagementService.GetAllProTatDegByProID(ID);
            return PartialView("EditProduct", _objCaseManagementViewModel);
        }

        [HttpPost]    
        [IgnoreAntiforgeryToken]
        public JsonResult SaveProducts([FromBody]ProductManagemntWithTatDefination data)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            _objProductManagementService.SaveProduct(data, _UserID);
            return Json("Ok");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteProduct(int ID)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objProductManagementService.DeleteProduct(ID, _UserID);
            }
            return Json("OK");
        }

        public IActionResult DeleteProductmodel(int ID)
        {
            ViewBag.ProductUniqueID = ID;
            return PartialView("DeleteProduct");
        }
    }
}