﻿using Accounting.Web.Common;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Accounting.Web.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    { 
        private readonly IUserManagementService _objUserService;
        private readonly IEntityMasterService _objIEntityMasterService;
        private readonly ILegalEntityProjectMappingService _objLegalEntityProjectMappingService;
        private readonly ILegalEntityProfitCentreMappingService _objLegalEntityProfitCentreMappingService;
        private readonly ILegalEntityLocationMappingService _objLegalEntityLocationMappingService;
        private readonly ILegalEntityUnitMappingService _objLegalEntityUnitMappingService;
        public AccountController(IUserManagementService objUserService, IEntityMasterService objIEntityMasterService, ILegalEntityProjectMappingService objLegalEntityProjectMappingService, ILegalEntityProfitCentreMappingService objLegalEntityProfitCentreMappingService, ILegalEntityLocationMappingService objLegalEntityLocationMappingService, ILegalEntityUnitMappingService objLegalEntityUnitMappingService) {
            _objUserService = objUserService;
            _objIEntityMasterService = objIEntityMasterService;
            _objLegalEntityProjectMappingService = objLegalEntityProjectMappingService;
            _objLegalEntityProfitCentreMappingService = objLegalEntityProfitCentreMappingService;
            _objLegalEntityLocationMappingService = objLegalEntityLocationMappingService;
            _objLegalEntityUnitMappingService = objLegalEntityUnitMappingService;
        }

        //public IActionResult Login() => View();
        public IActionResult Login()
        {

            ViewBag.AllLegalEntity = _objIEntityMasterService.GetEntityByLevelID(5);
            return View();
        }
        public IActionResult LoginHome()
        {
            return View();
        }
        public IActionResult ErrorForbidden() => View();

        [HttpPost]
        public async Task<ActionResult> SignIn(LoginViewModel objLoginViewModel, string returnUrl = null)
        {
            string Str_UsrName = string.Empty;
            if (!ModelState.IsValid) { return View("Login"); }
            UserModel _objUserModel = _objUserService.GetUserByLoginName(objLoginViewModel.UserName);

            //string dfsd = Cryptography.Decrypt("", "Accounting");
            if (_objUserModel.Password != Cryptography.Encrypt(objLoginViewModel.Password, "Accounting")) { return RedirectToAction(nameof(Login)); }
            if (!_objUserModel.IsAccess.HasValue) { return RedirectToAction(nameof(Login)); }

            //HttpContext.Session.SetString("Uname1", "mySessionValue");

            EntityMasterModel _ObjProfitModel = _objUserService.GetBranchNameById(objLoginViewModel.LegalEntityID);
            HttpContext.Session.SetString("UsrName", _objUserModel.UserName);
            HttpContext.Session.SetString("BranchName", _ObjProfitModel.Name);
            RoleModel _objRoleModel = _objUserService.GetRoleById(_objUserModel.RoleID);
            //int CorporateGroupID = _objUserModel.CorporateGroupID;
            //int BusinessGroupID = _objUserModel.BusinessGroupID;
            //int VerticalGroupID = _objUserModel.VerticalGroupID;
            //int LegalEntityID = _objUserModel.LegalEntityID;
            //int ProfitCentreID = _objUserModel.ProfitCentreID;
            //int LocationID = _objUserModel.LocationID;
            //int UnitID = _objUserModel.UnitID;
            //int ProjectID = _objUserModel.ProjectID;

            int CorporateGroupID = 0;
            int BusinessGroupID = 0;
            int VerticalGroupID = 0;
            int LegalEntityID = 0;
            int ProfitCentreID = 0;
            int LocationID = 0;
            int UnitID = 0;
            int ProjectID = 0;

            LegalEntityID = objLoginViewModel.LegalEntityID;
            ProfitCentreID = objLoginViewModel.ProfitCentreID;
            LocationID = objLoginViewModel.LocationID;
            EntityMasterModel objEntityMasterModel = _objIEntityMasterService.GetEntityByID(objLoginViewModel.LegalEntityID);
            if (objEntityMasterModel != null)
            {
                VerticalGroupID = objEntityMasterModel.ParentEntityID;
            }
            if (VerticalGroupID != 0)
            {
                EntityMasterModel objEntityMasterModelVerticalGroupID = _objIEntityMasterService.GetEntityByID(VerticalGroupID);

                if (objEntityMasterModelVerticalGroupID != null)
                {
                    BusinessGroupID = objEntityMasterModelVerticalGroupID.ParentEntityID;
                }
            }
            if (BusinessGroupID != 0)
            {
                EntityMasterModel objEntityMasterModelBusinessGroupID = _objIEntityMasterService.GetEntityByID(BusinessGroupID);
                if (objEntityMasterModelBusinessGroupID != null)
                {
                    CorporateGroupID = objEntityMasterModelBusinessGroupID.ParentEntityID;
                }
            }


            string AccountAccess = "false";
            if (ProfitCentreID != 0 && LocationID == 0 && UnitID == 0 && ProjectID == 0 && LegalEntityID != 0)
            {
                AccountAccess = "true";
            }

            var _objIdentity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, objLoginViewModel.UserName),
                                                            new Claim(ClaimTypes.GroupSid, string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}", _objUserModel.UserID, _objRoleModel.RoleID,CorporateGroupID,BusinessGroupID,VerticalGroupID,LegalEntityID,ProfitCentreID,LocationID,UnitID,ProjectID)),
                                                         new Claim(ClaimTypes.UserData, string.Format("{0}-{1}-{2}-{3}", _objUserModel.UserID, _objRoleModel.RoleID,AccountAccess, objLoginViewModel.UserName)),
                                                         new Claim(ClaimTypes.Role, _objRoleModel.RoleName)}, CookieAuthenticationDefaults.AuthenticationScheme);
            var _objPrincipal = new ClaimsPrincipal(_objIdentity);
            AuthenticationProperties _objAuthProp = new AuthenticationProperties();
            _objAuthProp.ExpiresUtc = DateTime.UtcNow.AddMinutes(30.0);
            _objAuthProp.AllowRefresh = true;
            _objAuthProp.IsPersistent = true;
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, _objPrincipal, _objAuthProp);
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            else
                return Redirect("../Home/Index");
        }
        [HttpPost]
        public async Task<ActionResult> SignIn1(LoginViewModel objLoginViewModel, string returnUrl = null)
        {
            if (!ModelState.IsValid) { return View("Login"); }
            UserModel _objUserModel = _objUserService.GetUserByLoginName(objLoginViewModel.UserName);
            //string dfsd = Cryptography.Decrypt("", "Accounting");
            if (_objUserModel.Password != Cryptography.Encrypt(objLoginViewModel.Password, "Accounting")) { return RedirectToAction(nameof(Login)); }

            if (!_objUserModel.IsAccess.HasValue) { return RedirectToAction(nameof(Login)); }
            
            RoleModel _objRoleModel = _objUserService.GetRoleById(_objUserModel.RoleID);
            int CorporateGroupID = _objUserModel.CorporateGroupID;
            int BusinessGroupID = _objUserModel.BusinessGroupID;
            int VerticalGroupID = _objUserModel.VerticalGroupID;
            int LegalEntityID = _objUserModel.LegalEntityID;
            int ProfitCentreID = _objUserModel.ProfitCentreID;
            int LocationID = _objUserModel.LocationID;
            int UnitID = _objUserModel.UnitID;
            int ProjectID = _objUserModel.ProjectID;
            string AccountAccess = "false";
            if (ProfitCentreID == 0 && LocationID == 0 && UnitID == 0 && ProjectID == 0 && LegalEntityID != 0)
            {
                AccountAccess = "true";
            }

            var _objIdentity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, objLoginViewModel.UserName),
                                                            new Claim(ClaimTypes.GroupSid, string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}-{9}", _objUserModel.UserID, _objRoleModel.RoleID,CorporateGroupID,BusinessGroupID,VerticalGroupID,LegalEntityID,ProfitCentreID,LocationID,UnitID,ProjectID)),
                                                         new Claim(ClaimTypes.UserData, string.Format("{0}-{1}-{2}", _objUserModel.UserID, _objRoleModel.RoleID,AccountAccess)),
                                                         new Claim(ClaimTypes.Role, _objRoleModel.RoleName)}, CookieAuthenticationDefaults.AuthenticationScheme);
            var _objPrincipal = new ClaimsPrincipal(_objIdentity);
            AuthenticationProperties _objAuthProp = new AuthenticationProperties();
            _objAuthProp.ExpiresUtc = DateTime.UtcNow.AddMinutes(30.0);
            _objAuthProp.AllowRefresh = true;
            _objAuthProp.IsPersistent = true;
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, _objPrincipal, _objAuthProp);
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            else
                return Redirect("../Home/Index");
        }
        public async Task<ActionResult> Signout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction(nameof(Login));
        }

        [HttpGet]
        public JsonResult BindLegalEntityProfitCentreMappingByLegalEntityID(int EntityID)
        {
            return Json(_objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMappingViewModelList(EntityID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityLocationMappingByLegalEntityIDProfitCentreID(int EntityID, int ProfitCentreID)
        {
            return Json(_objLegalEntityLocationMappingService.GetAllLegalEntityLocationMappingViewModelListByLegalEntityIDProfitCentreID(EntityID, ProfitCentreID));
        }
    }
}
