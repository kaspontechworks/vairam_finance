﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class ProfitCentreController : Controller
    {
        private readonly IProfitCentreService _objProfitCentreService;
        private readonly IUserManagementService _objUsermanagementservice;
        private readonly IUDoptionsServices _objUDoptionsServices;

        public ProfitCentreController(CDbcontext context, IProfitCentreService objProfitCentreService, IUserManagementService objUsermanagementservice, IUDoptionsServices objUDoptionsServices)
        {
            _objProfitCentreService = objProfitCentreService;
            _objUsermanagementservice = objUsermanagementservice;
            _objUDoptionsServices = objUDoptionsServices;

        }
        public IActionResult Index()
        {
            ProfitCentreViewModel _objProfitCentreViewModel = new ProfitCentreViewModel();
            _objProfitCentreViewModel.GetAllProfitCentre = _objProfitCentreService.GetAllProfitCentre();
            return View(_objProfitCentreViewModel);
        }
        
        public IActionResult AddProfitCentre()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            string Username = strUserData.Split("-")[3];
            ViewBag.Username = _objUsermanagementservice.GetUserByLogin(Username);
            ViewBag.PastdateStatus = _objUDoptionsServices.GetAllUDOOptionsByUDCategoryName("EnabledDisabled");
            return PartialView("AddProfitCentre");
        }
        public IActionResult EditProfitCentre(int ID)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            string Username = strUserData.Split("-")[3];
            ViewBag.Username = _objUsermanagementservice.GetUserByLogin(Username);
            ProfitCentreModel objProfitCentreModel = new ProfitCentreModel();
            objProfitCentreModel = _objProfitCentreService.GetProfitCentreByID(ID);
            ViewBag.PastdateStatus = _objUDoptionsServices.GetAllUDOOptionsByUDCategoryName("EnabledDisabled");
            return PartialView("EditProfitCentre", objProfitCentreModel);
        }
        public IActionResult DeleteProfitCentremodel(int ID)
        {
            ViewBag.ProfitCentreID = ID;
            return PartialView("DeleteProfitCentremodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteProfitCentre(int ID)
        {
          
            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objProfitCentreService.DeleteProfitCentre(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertProfitCentre([FromBody] ProfitCentreModel objProfitCentreModel)
        {

            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objProfitCentreService.SaveProfitCentre(objProfitCentreModel, UserID);
            }
            return Json("OK");
        }
    }
}