﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class LegalEntityProfitCentreMappingController : Controller
    {
        private readonly IEntityMasterService _objIEntityMasterService;
        private readonly IProfitCentreService _objIProfitCentreService;
        private readonly ILegalEntityProfitCentreMappingService _objLegalEntityProfitCentreMappingService;
        public LegalEntityProfitCentreMappingController(IEntityMasterService objIEntityMasterService, ILegalEntityProfitCentreMappingService objLegalEntityProfitCentreMappingService, IProfitCentreService objIProfitCentreService)
        {
            _objIEntityMasterService = objIEntityMasterService;
            _objLegalEntityProfitCentreMappingService = objLegalEntityProfitCentreMappingService;
            _objIProfitCentreService = objIProfitCentreService;
        }
        public IActionResult Index()
        {
            
            ViewBag.AllLegalEntityProfitCentreMappingViewModelList = _objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMappingViewModelList(0);
            LegalEntityProfitCentreMappingViewModel _objLegalEntityProfitCentreMappingViewModel = new LegalEntityProfitCentreMappingViewModel();
            _objLegalEntityProfitCentreMappingViewModel.GetAllLegalEntityProfitCentreMapping = _objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMapping();
            return View(_objLegalEntityProfitCentreMappingViewModel);
         }
       
        public IActionResult AddLegalEntityProfitCentreMapping()
        {
            ViewBag.AllLegalEntity = _objIEntityMasterService.GetEntityByLevelID(5);
            ViewBag.AllProfitCentre = _objIProfitCentreService.GetAllProfitCentre();
            
            return PartialView("AddLegalEntityProfitCentreMapping");
        }
       
        public IActionResult DeleteLegalEntityProfitCentreMappingmodel(int ID)
        {
            ViewBag.LegalEntityProfitCentreMappingID = ID;
            return PartialView("DeleteLegalEntityProfitCentreMappingmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteLegalEntityProfitCentreMapping(int ID)
        {
          
            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objLegalEntityProfitCentreMappingService.DeleteLegalEntityProfitCentreMapping(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertLegalEntityProfitCentreMapping([FromBody] LegalEntityProfitCentreDefinations LegalEntityProfitCentreDefinations)//([FromBody] LegalEntityProfitCentreMappingModel objLegalEntityProfitCentreMappingModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objLegalEntityProfitCentreMappingService.SaveLegalEntityProfitCentreMapping(LegalEntityProfitCentreDefinations, UserID);
            }
            return Json("OK");
        }
        [HttpGet]
        public JsonResult BindLegalEntityProfitCentreMappingByLegalEntityID(int ID)
        {
            return Json(_objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMappingByLegalEntityID(ID));
        }
    }
}