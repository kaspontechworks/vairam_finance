﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class LegalEntityProjectMappingController : Controller
    {
        private readonly IEntityMasterService _objIEntityMasterService;
        private readonly IProjectService _objIProjectService;
        private readonly ILegalEntityProjectMappingService _objLegalEntityProjectMappingService;
        private readonly ILegalEntityProfitCentreMappingService _objLegalEntityProfitCentreMappingService;
        private readonly ILegalEntityLocationMappingService _objLegalEntityLocationMappingService;
        private readonly ILegalEntityUnitMappingService _objLegalEntityUnitMappingService; 
        public LegalEntityProjectMappingController(IEntityMasterService objIEntityMasterService, ILegalEntityProjectMappingService objLegalEntityProjectMappingService, IProjectService objIProjectService, ILegalEntityProfitCentreMappingService objLegalEntityProfitCentreMappingService, ILegalEntityLocationMappingService objLegalEntityLocationMappingService, ILegalEntityUnitMappingService objLegalEntityUnitMappingService)
        {
            _objIEntityMasterService = objIEntityMasterService;
            _objLegalEntityProjectMappingService = objLegalEntityProjectMappingService;
            _objIProjectService = objIProjectService;
            _objLegalEntityProfitCentreMappingService = objLegalEntityProfitCentreMappingService;
            _objLegalEntityLocationMappingService = objLegalEntityLocationMappingService;
            _objLegalEntityUnitMappingService = objLegalEntityUnitMappingService;
        }
        public IActionResult Index()
        {
           
            ViewBag.AllLegalEntityProjectMappingViewModelList = _objLegalEntityProjectMappingService.GetAllLegalEntityProjectMappingViewModelList(0);
            LegalEntityProjectMappingViewModel _objLegalEntityProjectMappingViewModel = new LegalEntityProjectMappingViewModel();
            _objLegalEntityProjectMappingViewModel.GetAllLegalEntityProjectMapping = _objLegalEntityProjectMappingService.GetAllLegalEntityProjectMapping();
            return View(_objLegalEntityProjectMappingViewModel);
        }
       
        public IActionResult AddLegalEntityProjectMapping()
        {
            ViewBag.AllLegalEntity = _objIEntityMasterService.GetEntityByLevelID(5);
            ViewBag.AllProject = _objIProjectService.GetAllProject();
           
            return PartialView("AddLegalEntityProjectMapping");
        }
       
        public IActionResult DeleteLegalEntityProjectMappingmodel(int ID)
        {
            ViewBag.LegalEntityProjectMappingID = ID;
            return PartialView("DeleteLegalEntityProjectMappingmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteLegalEntityProjectMapping(int ID)
        {

            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objLegalEntityProjectMappingService.DeleteLegalEntityProjectMapping(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertLegalEntityProjectMapping([FromBody] LegalEntityProjectDefinations LegalEntityProjectDefinations)//([FromBody] LegalEntityProjectMappingModel objLegalEntityProjectMappingModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objLegalEntityProjectMappingService.SaveLegalEntityProjectMapping(LegalEntityProjectDefinations, UserID);
            }
            return Json("OK");
        }

        [HttpGet]
        public JsonResult BindLegalEntityProfitCentreMappingByLegalEntityID(int EntityID)
        {
            return Json(_objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMappingViewModelList(EntityID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityLocationMappingByLegalEntityIDProfitCentreID(int EntityID, int ProfitCentreID)
        {
            return Json(_objLegalEntityLocationMappingService.GetAllLegalEntityLocationMappingViewModelListByLegalEntityIDProfitCentreID(EntityID, ProfitCentreID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityUnitMappingByLegalEntityIDProfitCentreIDLocationID(int EntityID, int ProfitCentreID, int LocationID)
        {
            return Json(_objLegalEntityUnitMappingService.GetAllLegalEntityUnitMappingViewModelListByLegalEntityIDProfitCentreIDLocationID(EntityID, ProfitCentreID, LocationID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityProjectMappingByLegalEntityID(int EntityID)
        {
            return Json(_objLegalEntityProjectMappingService.GetAllLegalEntityProjectMappingByLegalEntityID(EntityID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityProjectMappingByLegalEntityIDProfitCentreIDLocationIDUnitID(int EntityID, int ProfitCentreID, int LocationID,int UnitID)
        {
            return Json(_objLegalEntityProjectMappingService.GetAllLegalEntityProjectMappingByLegalEntityIDProfitCentreIDLocationIDUnitID(EntityID, ProfitCentreID, LocationID, UnitID));
        }

    }
}