﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class AnalyticsController : Controller
    {
      
        private readonly IAnalyticsService _objAnalyticsService;
        public AnalyticsController(IAnalyticsService objAnalyticsService)
        {
           
            _objAnalyticsService = objAnalyticsService;
        }
        public IActionResult Index()
        {
           
            AnalyticsViewModel _objAnalyticsViewModel = new AnalyticsViewModel();
            _objAnalyticsViewModel.GetAllAnalytics = _objAnalyticsService.GetAllAnalytics();
            return View(_objAnalyticsViewModel);
        }
       
        public IActionResult AddAnalytics()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int _LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            ViewBag.LegalEntity = _LegalEntityID;
           
            return PartialView("AddAnalytics");
        }
        public IActionResult EditAnalytics(int ID)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int _LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            ViewBag.LegalEntity = _LegalEntityID;
            AnalyticsModel objAnalyticsModel = new AnalyticsModel();
            objAnalyticsModel = _objAnalyticsService.GetAnalyticsByID(ID);
            return PartialView("EditAnalytics", objAnalyticsModel);
        }
        public IActionResult DeleteAnalyticsmodel(int ID)
        {
            ViewBag.AnalyticsID = ID;
            return PartialView("DeleteAnalyticsmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteAnalytics(int ID)
        {
          
            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objAnalyticsService.DeleteAnalytics(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertAnalytics([FromBody] AnalyticsModel objAnalyticsModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objAnalyticsService.SaveAnalytics(objAnalyticsModel, UserID);
            }
            return Json("OK");
        }
    }
}