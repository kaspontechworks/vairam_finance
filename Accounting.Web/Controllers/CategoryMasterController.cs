﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class CategoryMasterController : Controller
    {
        private readonly IMajorCategoryService _objIMajorCategoryService;
        private readonly ICategoryMasterService _objCategoryMasterService;
        public CategoryMasterController(IMajorCategoryService objIMajorCategoryService, ICategoryMasterService objCategoryMasterService)
        {
            _objIMajorCategoryService = objIMajorCategoryService;
            _objCategoryMasterService = objCategoryMasterService;
        }
        public IActionResult Index()
        {
            ViewBag.GetAllCategoryMaster = _objCategoryMasterService.GetAllCategoryMasterViewModelList();
            //CategoryMasterViewModel _objCategoryMasterViewModel = new CategoryMasterViewModel();
            //_objCategoryMasterViewModel.GetAllCategoryMaster = _objCategoryMasterService.GetAllCategoryMaster();
            //return View(_objCategoryMasterViewModel);
            return View();
        }
        public IActionResult ShowCategoryMasterTableView(int ID)
        {
            return PartialView("ShowCategoryMasterTableView", _objCategoryMasterService.GetAllCategoryMasterByParentCategoryID(ID));
        }
        public IActionResult AddCategoryMaster()
        {
            ViewBag.AllMajorCategory = _objIMajorCategoryService.GetAllMajorCategory();
           
            return PartialView("AddCategoryMaster");
        }
        public IActionResult EditCategoryMaster(int ID)
        {
            ViewBag.AllMajorCategory = _objIMajorCategoryService.GetAllMajorCategory();
            CategoryMasterModel objCategoryMasterModel = new CategoryMasterModel();
            objCategoryMasterModel = _objCategoryMasterService.GetCategoryMasterByID(ID);
            return PartialView("EditCategoryMaster", objCategoryMasterModel);
        }
        public IActionResult DeleteCategoryMastermodel(int ID)
        {
            ViewBag.CategoryMasterID = ID;
            return PartialView("DeleteCategoryMastermodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteCategoryMaster(int ID)
        {
          
            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objCategoryMasterService.DeleteCategoryMaster(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertCategoryMaster([FromBody] CategoryMasterModel objCategoryMasterModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objCategoryMasterService.SaveCategoryMaster(objCategoryMasterModel, UserID);
            }
            return Json("OK");
        }
    }
}