﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class FinancialYearController : Controller
    {
        private readonly IFinancialYearService _objFinancialYearService;
        public FinancialYearController(IFinancialYearService objFinancialYearService)
        {
            _objFinancialYearService = objFinancialYearService;
        }
        public IActionResult Index()
        {

            FinancialYearViewModel _objFinancialYearViewModel = new FinancialYearViewModel();
            _objFinancialYearViewModel.GetAllFinancialYear = _objFinancialYearService.GetAllFinancialYear();
            return View(_objFinancialYearViewModel);
        }
      
        public IActionResult AddFinancialYear()
        {
            
            return PartialView("AddFinancialYear");
        }
        public IActionResult EditFinancialYear(int ID)
        {

            FinancialYearModel objFinancialYearModel = new FinancialYearModel();
            objFinancialYearModel = _objFinancialYearService.GetFinancialYearByID(ID);
            ViewBag.StartDate = objFinancialYearModel.StartDate.ToString("dd-MM-yyyy");
            ViewBag.EndDate = objFinancialYearModel.EndDate.ToString("dd-MM-yyyy");
            ViewBag.CreatedOn = objFinancialYearModel.CreatedOn.ToString("dd-MM-yyyy");
            ViewBag.IsActive = objFinancialYearModel.IsActive;
            return PartialView("EditFinancialYear", objFinancialYearModel);
        }
        public IActionResult DeleteFinancialYearmodel(int ID)
        {
            ViewBag.FinancialYearID = ID;
            return PartialView("DeleteFinancialYearmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteFinancialYear(int ID)
        {

            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objFinancialYearService.DeleteFinancialYear(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertFinancialYear([FromBody] FinancialYearModel objFinancialYearModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objFinancialYearService.SaveFinancialYear(objFinancialYearModel, UserID);
            }
            return Json("OK");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult GetFinancialYearTodate(DateTime Date)
        {
            DateTime dt = Date.AddYears(1).AddDays(-1);
            string dtt = dt.ToString("dd-MM-yyyy");
            return Json(dtt);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult GetFinancialYearFromdate(DateTime Date)
        {
            DateTime dt = Date.AddYears(-1).AddDays(1);
            string dtt = dt.ToString("dd-MM-yyyy");
            return Json(dtt);
        }
    }
}