﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class LegalEntityLocationMappingController : Controller
    {
        private readonly IEntityMasterService _objIEntityMasterService;
        private readonly ILocationService _objILocationService;
        private readonly ILegalEntityLocationMappingService _objLegalEntityLocationMappingService;
        private readonly ILegalEntityProfitCentreMappingService _objLegalEntityProfitCentreMappingService;
        
        public LegalEntityLocationMappingController(IEntityMasterService objIEntityMasterService, ILegalEntityLocationMappingService objLegalEntityLocationMappingService, ILocationService objILocationService, ILegalEntityProfitCentreMappingService objLegalEntityProfitCentreMappingService)
        {
            _objIEntityMasterService = objIEntityMasterService;
            _objLegalEntityLocationMappingService = objLegalEntityLocationMappingService;
            _objILocationService = objILocationService;
            _objLegalEntityProfitCentreMappingService = objLegalEntityProfitCentreMappingService;
        }
        public IActionResult Index()
        {
            
            ViewBag.AllLegalEntityLocationMappingViewModelList = _objLegalEntityLocationMappingService.GetAllLegalEntityLocationMappingViewModelList(0);
            LegalEntityLocationMappingViewModel _objLegalEntityLocationMappingViewModel = new LegalEntityLocationMappingViewModel();
            _objLegalEntityLocationMappingViewModel.GetAllLegalEntityLocationMapping = _objLegalEntityLocationMappingService.GetAllLegalEntityLocationMapping();
            return View(_objLegalEntityLocationMappingViewModel);
         }
       
        public IActionResult AddLegalEntityLocationMapping()
        {
            ViewBag.AllLegalEntity = _objIEntityMasterService.GetEntityByLevelID(5);
            ViewBag.AllLocation = _objILocationService.GetAllLocation();
            
            return PartialView("AddLegalEntityLocationMapping");
        }
        
        public IActionResult DeleteLegalEntityLocationMappingmodel(int ID)
        {
            ViewBag.LegalEntityLocationMappingID = ID;
            return PartialView("DeleteLegalEntityLocationMappingmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteLegalEntityLocationMapping(int ID)
        {
          
            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objLegalEntityLocationMappingService.DeleteLegalEntityLocationMapping(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertLegalEntityLocationMapping([FromBody] LegalEntityLocationDefinations LegalEntityLocationDefinations)//([FromBody] LegalEntityLocationMappingModel objLegalEntityLocationMappingModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objLegalEntityLocationMappingService.SaveLegalEntityLocationMapping(LegalEntityLocationDefinations, UserID);
            }
            return Json("OK");
        }
      
        [HttpGet]
        public JsonResult BindLegalEntityProfitCentreMappingByLegalEntityID(int EntityID)
        {
            return Json(_objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMappingViewModelList(EntityID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityLocationMappingByLegalEntityID(int EntityID)
        {
            return Json(_objLegalEntityLocationMappingService.GetAllLegalEntityLocationMappingByLegalEntityID(EntityID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityLocationMappingByLegalEntityIDProfitCentreID(int EntityID,int ProfitCentreID)
        {
            return Json(_objLegalEntityLocationMappingService.GetAllLegalEntityLocationMappingByLegalEntityIDProfitCentreID(EntityID, ProfitCentreID));
        }
        
    }
}