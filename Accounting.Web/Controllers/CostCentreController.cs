﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class CostCentreController : Controller
    {
        private readonly ICostCentreService _objCostCentreService;
        public CostCentreController( ICostCentreService objCostCentreService)
        {
            _objCostCentreService = objCostCentreService;
        }
        public IActionResult Index()
        {
           
            CostCentreViewModel _objCostCentreViewModel = new CostCentreViewModel();
            _objCostCentreViewModel.GetAllCostCentre = _objCostCentreService.GetAllCostCentre();
            return View(_objCostCentreViewModel);
        }
        
        public IActionResult AddCostCentre()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int _LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            ViewBag.LegalEntity = _LegalEntityID;
            return PartialView("AddCostCentre");
        }
        public IActionResult EditCostCentre(int ID)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int _LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            ViewBag.LegalEntity = _LegalEntityID;
            CostCentreModel objCostCentreModel = new CostCentreModel();
            objCostCentreModel = _objCostCentreService.GetCostCentreByID(ID);
            return PartialView("EditCostCentre", objCostCentreModel);
        }
        public IActionResult DeleteCostCentremodel(int ID)
        {
            ViewBag.CostCentreID = ID;
            return PartialView("DeleteCostCentremodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteCostCentre(int ID)
        {
          
            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objCostCentreService.DeleteCostCentre(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertCostCentre([FromBody] CostCentreModel objCostCentreModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objCostCentreService.SaveCostCentre(objCostCentreModel, UserID);
            }
            return Json("OK");
        }
    }
}