﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class LegalEntityUnitMappingController : Controller
    {
        private readonly IEntityMasterService _objIEntityMasterService;
        private readonly IUnitService _objIUnitService;
        private readonly ILegalEntityUnitMappingService _objLegalEntityUnitMappingService;
        private readonly ILegalEntityProfitCentreMappingService _objLegalEntityProfitCentreMappingService;
        private readonly ILegalEntityLocationMappingService _objLegalEntityLocationMappingService;
        
        public LegalEntityUnitMappingController(IEntityMasterService objIEntityMasterService, ILegalEntityUnitMappingService objLegalEntityUnitMappingService, IUnitService objIUnitService, ILegalEntityProfitCentreMappingService objLegalEntityProfitCentreMappingService, ILegalEntityLocationMappingService objLegalEntityLocationMappingService)
        {
            _objIEntityMasterService = objIEntityMasterService;
            _objLegalEntityUnitMappingService = objLegalEntityUnitMappingService;
            _objIUnitService = objIUnitService;
            _objLegalEntityProfitCentreMappingService = objLegalEntityProfitCentreMappingService;
            _objLegalEntityLocationMappingService = objLegalEntityLocationMappingService;
        }
        public IActionResult Index()
        {
            
            ViewBag.AllLegalEntityUnitMappingViewModelList = _objLegalEntityUnitMappingService.GetAllLegalEntityUnitMappingViewModelList(0);
            LegalEntityUnitMappingViewModel _objLegalEntityUnitMappingViewModel = new LegalEntityUnitMappingViewModel();
            _objLegalEntityUnitMappingViewModel.GetAllLegalEntityUnitMapping = _objLegalEntityUnitMappingService.GetAllLegalEntityUnitMapping();
            return View(_objLegalEntityUnitMappingViewModel);
        }
       
        public IActionResult AddLegalEntityUnitMapping()
        {
            ViewBag.AllLegalEntity = _objIEntityMasterService.GetEntityByLevelID(5);
            ViewBag.AllUnit = _objIUnitService.GetAllUnit();
            
            return PartialView("AddLegalEntityUnitMapping");
        }
        
        public IActionResult DeleteLegalEntityUnitMappingmodel(int ID)
        {
            ViewBag.LegalEntityUnitMappingID = ID;
            return PartialView("DeleteLegalEntityUnitMappingmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteLegalEntityUnitMapping(int ID)
        {

            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objLegalEntityUnitMappingService.DeleteLegalEntityUnitMapping(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertLegalEntityUnitMapping([FromBody] LegalEntityUnitDefinations LegalEntityUnitDefinations)//([FromBody] LegalEntityUnitMappingModel objLegalEntityUnitMappingModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objLegalEntityUnitMappingService.SaveLegalEntityUnitMapping(LegalEntityUnitDefinations, UserID);
            }
            return Json("OK");
        }

        [HttpGet]
        public JsonResult BindLegalEntityProfitCentreMappingByLegalEntityID(int EntityID)
        {
            return Json(_objLegalEntityProfitCentreMappingService.GetAllLegalEntityProfitCentreMappingViewModelList(EntityID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityLocationMappingByLegalEntityIDProfitCentreID(int EntityID, int ProfitCentreID)
        {
            return Json(_objLegalEntityLocationMappingService.GetAllLegalEntityLocationMappingViewModelListByLegalEntityIDProfitCentreID(EntityID, ProfitCentreID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityUnitMappingByLegalEntityID(int EntityID)
        {
            return Json(_objLegalEntityUnitMappingService.GetAllLegalEntityUnitMappingByLegalEntityID(EntityID));
        }
        [HttpGet]
        public JsonResult BindLegalEntityUnitMappingByLegalEntityIDProfitCentreIDLocationID(int EntityID, int ProfitCentreID,int LocationID)
        {
            return Json(_objLegalEntityUnitMappingService.GetAllLegalEntityUnitMappingByLegalEntityIDProfitCentreIDLocationID(EntityID, ProfitCentreID, LocationID));
        }

    }
}