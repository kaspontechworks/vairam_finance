﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    [AllowAnonymous]
    public class ChartofAccountsController : Controller
    {
        private readonly IChartofAccountsService _objChartofAccountsService;
        private readonly IMajorCategoryService _objMajorCategoryService;
        private readonly ICategoryMasterService _objCategoryMasterService;
        private readonly ILedgerGroupMasterService _objLedgerGroupMasterService;
        
        public ChartofAccountsController(IChartofAccountsService objChartofAccountsService, ICategoryMasterService objCategoryMasterService, ILedgerGroupMasterService objLedgerGroupMasterService, IMajorCategoryService objMajorCategoryService)
        {
            _objChartofAccountsService = objChartofAccountsService;
            _objCategoryMasterService = objCategoryMasterService;
            _objLedgerGroupMasterService = objLedgerGroupMasterService;
            _objMajorCategoryService = objMajorCategoryService;
        }
        public IActionResult Index()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int _LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            //ChartofAccountsViewModel _objChartofAccountsViewModel = new ChartofAccountsViewModel();
            //_objChartofAccountsViewModel.GetAllChartofAccounts = _objChartofAccountsService.GetAllChartofAccountsByLegalEntity(_LegalEntityID);
            //return View(_objChartofAccountsViewModel);
            ViewBag.GetAllChartofAccounts = _objChartofAccountsService.GetAllCategoryMasterViewModelList(_LegalEntityID);
            return View();
        }
       
        public IActionResult AddChartofAccounts()
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int _LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            ViewBag.LegalEntity = _LegalEntityID;
            ViewBag.GetAllAccountType = _objChartofAccountsService.GetAllAccountType();
            ViewBag.GetAllMajorCategory = _objMajorCategoryService.GetAllMajorCategory();
            ViewBag.GetAllMajorGroupID = _objLedgerGroupMasterService.GetAllLedgerGroupMasterByGroupType(1);
           
            return PartialView("AddChartofAccounts");
        }
        public IActionResult EditChartofAccounts(int ID)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.GroupSid).Select(x => x.Value).FirstOrDefault();
            int _LegalEntityID = int.Parse(strUserData.Split("-")[5]);
            ViewBag.LegalEntity = _LegalEntityID;
            ViewBag.GetAllAccountType = _objChartofAccountsService.GetAllAccountType();
            ViewBag.GetAllMajorCategory = _objMajorCategoryService.GetAllMajorCategory();
            ViewBag.GetAllMajorGroupID = _objLedgerGroupMasterService.GetAllLedgerGroupMasterByGroupType(1);
           
            ChartofAccountsModel objChartofAccountsModel = new ChartofAccountsModel();
            objChartofAccountsModel = _objChartofAccountsService.GetChartofAccountsByID(ID);
            ViewBag.GetAllSubCategory = _objCategoryMasterService.GetAllCategoryMasterByParentCategoryID(objChartofAccountsModel.ParentCategoryID);
            ViewBag.GetAllGroupID = _objLedgerGroupMasterService.GetAllLedgerGroupMasterByGroupTypeParentCategoryID(2, objChartofAccountsModel.MajorGroupID);
            ViewBag.GetAllSubGroupID = _objLedgerGroupMasterService.GetAllLedgerGroupMasterByGroupTypeParentCategoryID(3, objChartofAccountsModel.GroupID);
            return PartialView("EditChartofAccounts", objChartofAccountsModel);
        }
        public IActionResult DeleteChartofAccountsmodel(int ID)
        {
            ViewBag.ChartofAccountsID = ID;
            return PartialView("DeleteChartofAccountsmodel");
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteChartofAccounts(int ID)
        {
          
            int CategoryID = 0;
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                CategoryID = _objChartofAccountsService.DeleteChartofAccounts(ID, _UserID);
            }
            return Json(CategoryID);
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertChartofAccounts([FromBody] ChartofAccountsModel objChartofAccountsModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objChartofAccountsService.SaveChartofAccounts(objChartofAccountsModel, UserID);
            }
            return Json("OK");
        }
        [HttpGet]
        public JsonResult BindCategoryByParentCategoryID(int ID)
        {
            return Json(_objCategoryMasterService.GetAllCategoryMasterByParentCategoryID(ID));
        }
        [HttpGet]
        [AllowAnonymous]
        public JsonResult BindGroupByParentCategoryID(int TypeID,int ParentID)
        {
            return Json(_objLedgerGroupMasterService.GetAllLedgerGroupMasterByGroupTypeParentCategoryID(TypeID, ParentID));
        }

        //public ActionResult GetMembers()
        //{
        //    IEnumerable<ChartofAccountsModel> members = null;

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("http://202.65.131.134/ChartofAccounts/BindGroupByParentCategoryID?TypeID=1&ParentID=0");

        //        //Called Member default GET All records  
        //        //GetAsync to send a GET request   
        //        // PutAsync to send a PUT request  
        //        var responseTask = client.GetAsync("member");
        //        responseTask.Wait();

        //        //To store result of web api response.   
        //        var result = responseTask.Result;

        //        //If success received   
        //        if (result.IsSuccessStatusCode)
        //        {
        //            var readTask = result.Content.ReadAsAsync<IList<ChartofAccountsModel>>();
        //            readTask.Wait();

        //            members = readTask.Result;
        //        }
        //        else
        //        {
        //            //Error response received   
        //            members = Enumerable.Empty<ChartofAccountsModel>();
        //            ModelState.AddModelError(string.Empty, "Server error try after some time.");
        //        }
        //    }
        //    return View(members);
        //}
    }
}