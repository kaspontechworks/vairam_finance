﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Services.Srvobj;
using Microsoft.AspNetCore.Mvc;

namespace Accounting.Web.Controllers
{
    public class EntityMasterController : Controller
    {
        private readonly IEntityMasterService _objentityMasterService;
        private readonly IEntityLevelService _objEntityLevelService;
        public EntityMasterController(IEntityMasterService objentityMasterService, IEntityLevelService objEntityLevelService)
        {
            _objentityMasterService = objentityMasterService;
            _objEntityLevelService = objEntityLevelService;
        }

        public IActionResult Index()
        {
            ViewBag.GetAllEntity = _objentityMasterService.GetAllEntityMasterViewModelList();
            return View(); //View(_objentityMasterService.GetAllEntity());
        }
      

        public IActionResult AddEntityMaster()
        {
            ViewBag.AllEntityLevel = _objEntityLevelService.GetAllEntityLevel();
            return PartialView("AddEntityMaster");
        }
        public IActionResult EditEntityMaster(int id)
        {
            EntityMasterModel objEntityMasterModel = _objentityMasterService.GetEntityByID(id);
            ViewBag.AllEntityLevel = _objEntityLevelService.GetAllEntityLevel();
            ViewBag.GetEntityByLevelID = _objentityMasterService.GetEntityByLevelID(objEntityMasterModel.EntityLevel-1);
            return PartialView("EditEntityMaster", objEntityMasterModel);
        }
        [HttpGet]
        public JsonResult BindEntityByLevelID(int ID)
        {
            return Json(_objentityMasterService.GetEntityByLevelID(ID));
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult InsertEntity([FromBody] EntityMasterModel objEntityMasterModel)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objentityMasterService.SaveEntity(objEntityMasterModel, UserID);
            }
            return Json("OK");
        }
        

        public IActionResult DeleteEntityMastermodel(int ID)
        {
            ViewBag.EntityID = ID;
            return PartialView("DeleteEntityMastermodel");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public JsonResult DeleteEntityMaster(int ID)
        {
            string strUserData = User.Claims.Where(x => x.Type == ClaimTypes.UserData).Select(x => x.Value).FirstOrDefault();
            int _UserID = int.Parse(strUserData.Split("-")[0]);
            if (ModelState.IsValid)
            {
                _objentityMasterService.DeleteEntityModel(ID, _UserID);
            }
            return Json("OK");
        }
    }
}   