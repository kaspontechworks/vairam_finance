﻿using Accounting.Web.Services.Srvobj;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services
{
    public static class ServiesCollection
    {
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {
            services.AddTransient<IUserManagementService, UserManagementService>();
            services.AddTransient<IEntityMasterService, EntityMasterService>();
            services.AddTransient<IEntityLevelService, EntityLevelService>();
            services.AddTransient<ICategoryMasterService, CategoryMasterService>();
            services.AddTransient<IMajorCategoryService, MajorCategoryService>();
            services.AddTransient<ILedgerGroupMasterService, LedgerGroupMasterService>();
            services.AddTransient<IChartofAccountsService, ChartofAccountsService>();
            services.AddTransient<ICostCentreService, CostCentreService>();
            services.AddTransient<IAnalyticsService, AnalyticsService>();
            services.AddTransient<IProfitCentreService, ProfitCentreService>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<IUnitService, UnitService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<ILegalEntityProfitCentreMappingService, LegalEntityProfitCentreMappingService>();
            services.AddTransient<ILegalEntityLocationMappingService, LegalEntityLocationMappingService>();
            services.AddTransient<ILegalEntityUnitMappingService, LegalEntityUnitMappingService>();
            services.AddTransient<ILegalEntityProjectMappingService, LegalEntityProjectMappingService>();
            services.AddTransient<ITransactionHeadService, TransactionHeadService>();
            services.AddTransient<ITransactionEntryService, TransactionEntryService>();
            services.AddTransient<IFinancialYearService, FinancialYearService>();

            services.AddTransient<ICaseManagementService, CaseManagementService>();
            services.AddTransient<IProductManagementService, ProductManagementService>();
           
            return services;
        }
    }
}
