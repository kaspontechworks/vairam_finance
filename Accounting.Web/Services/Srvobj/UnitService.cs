﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IUnitService
    {
        List<UnitModel> GetAllUnit();
       // List<UnitModel> GetAllUnitByLegalEntityID(int LegalEntityID);
        void SaveUnit(UnitModel objUnitModel, int UserID);
        UnitModel GetUnitByID(int UnitID);
        int DeleteUnit(int UnitID, int UserID);
    }
    public class UnitService : IUnitService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public UnitService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteUnit(int UnitID, int UserID)
        {
            UnitModel objUnitModel = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == UnitID).FirstOrDefault();
            objUnitModel.IsActive = false;
            _objAccountingDbContext.AccountingUnitModel.Update(objUnitModel);
            _objAccountingDbContext.SaveChanges();
            return objUnitModel.UnitID;
        }

        //public List<UnitModel> GetAllUnitByLegalEntityID(int LegalEntityID)
        //{
        //    return _objAccountingDbContext.AccountingUnitModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        //}
        public List<UnitModel> GetAllUnit()
        {
            return _objAccountingDbContext.AccountingUnitModel.Where(x => x.IsActive == true).ToList();
        }
        public UnitModel GetUnitByID(int UnitID)
        {
            return _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == UnitID && x.IsActive == true).FirstOrDefault();
        }

        public void SaveUnit(UnitModel objUnitModel, int UserID)
        {
            if (objUnitModel.UnitID == 0)
            {
                objUnitModel.IsActive = true;
                objUnitModel.CreatedBy = UserID;
                objUnitModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingUnitModel.Add(objUnitModel);
            }
            else
            {
               // objUnitModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objUnitModel.CreatedOn));
                objUnitModel.LastModifiedBy = UserID;
                objUnitModel.LastModifiedon = DateTime.Now;
                _objAccountingDbContext.AccountingUnitModel.Update(objUnitModel);
            }
            _objAccountingDbContext.SaveChanges();
        }
    }
}
