﻿using Accounting.Web.Common;
using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IUserManagementService
    {
        UserModel GetUserByLoginName(string LoginName);
        List<UserModel> GetUserByLogin(string LoginName);
        RoleModel GetRoleById(int intRoleId);

        List<UserModel> GetAllUsers();
        List<RoleModel> GetAllRoles();
        UserModel GetUserById(int intUserId);

        int SaveUser(UserModel objUserModel,int UserID);
        void SaveRole(RoleModel objRoleModel, int UserID);
        int DeleteUser(int UserID, int ModifiedUserID);
        void UpdateAccess(int userid, bool access);
        EntityMasterModel GetBranchNameById(int EntityId);
    }

    public class UserManagementService : IUserManagementService
    {
        private readonly CDbcontext _objAccountingDbContext;

        public UserManagementService(CDbcontext objCDbContext) { _objAccountingDbContext = objCDbContext; }

        public UserModel GetUserByLoginName(string strLoginName)
        {
            return _objAccountingDbContext.AccountingUsers.Where(x => x.UserName == strLoginName).FirstOrDefault();
        }
        public List<UserModel> GetUserByLogin(string strLoginName)
        {
            return (from objLocation in _objAccountingDbContext.AccountingUsers
                    where objLocation.IsActive == true && objLocation.UserName ==strLoginName
                    select new UserModel
                    {
                        UserName = objLocation.UserName
                    }).ToList();
        }

        public UserModel GetUserById(int intUserId) { return _objAccountingDbContext.AccountingUsers.Where(x => x.UserID == intUserId).FirstOrDefault(); }

        public List<UserModel> GetAllUsers()
        {
            return _objAccountingDbContext.AccountingUsers.Where(x => x.IsActive == true).ToList();
        }

        public int SaveUser(UserModel objUserModel,int UserID)
        {
            int UserIDentity = 0;
            if (_objAccountingDbContext.AccountingUsers.Where(x => x.UserID != objUserModel.UserID && x.UserName == objUserModel.UserName && x.IsActive == true).ToList().Count == 0)
            {
                if (objUserModel.UserID == 0)
                {
                    objUserModel.CreatedBy = UserID;
                    objUserModel.CreatedOn = DateTime.Now;
                    objUserModel.IsActive = true;
                    objUserModel.IsAccess = true;
                    //objUserModel.Password = Cryptography.Encrypt(objUserModel.Password, "Accounting");
                    objUserModel.LastModifiedBy = UserID;
                    _objAccountingDbContext.AccountingUsers.Add(objUserModel);
                }
                else
                {
                    objUserModel.LastModifiedOn = DateTime.Now;
                    objUserModel.LastModifiedBy = UserID;
                    _objAccountingDbContext.AccountingUsers.Update(objUserModel);
                   
                }
                _objAccountingDbContext.SaveChanges();
                UserIDentity = objUserModel.UserID;
            }
            return UserIDentity;
        }
        public int DeleteUser(int UserID, int ModifiedUserID)
        {
            UserModel objUserModel = _objAccountingDbContext.AccountingUsers.Where(x => x.UserID == UserID).FirstOrDefault();
            objUserModel.IsActive = false;
            objUserModel.LastModifiedBy = ModifiedUserID;
            objUserModel.LastModifiedOn = DateTime.Now;
            _objAccountingDbContext.AccountingUsers.Update(objUserModel);
            _objAccountingDbContext.SaveChanges();
            return objUserModel.UserID;
        }

        public void UpdateAccess(int userid, bool access)
        {
            var objuser = _objAccountingDbContext.AccountingUsers.Where(x => x.UserID == userid).FirstOrDefault();
            if (objuser != null)
            {
                objuser.IsAccess = access;
                objuser.LastModifiedOn = DateTime.Now;
                objuser.LastModifiedBy = 0;
                _objAccountingDbContext.AccountingUsers.Update(objuser);
                _objAccountingDbContext.SaveChanges();
            }

        }

        public void SaveRole(RoleModel objRoleModel, int UserID)
        {
            if (objRoleModel.RoleID == 0)
            {
                objRoleModel.IsActive = 1;
                objRoleModel.CreatedOn = DateTime.Now;
                objRoleModel.LastModifiedBy = UserID;
                _objAccountingDbContext.AccountingRoles.Add(objRoleModel);
            }
            else
            {
                objRoleModel.LastModifiedOn = DateTime.Now;
                objRoleModel.LastModifiedBy = UserID;
                _objAccountingDbContext.AccountingRoles.Update(objRoleModel);
            }
            _objAccountingDbContext.SaveChanges();
        }

        public RoleModel GetRoleById(int intRoleId) { return _objAccountingDbContext.AccountingRoles.Where(x => x.RoleID == intRoleId).FirstOrDefault(); }

        public List<RoleModel> GetAllRoles()
        {
            return _objAccountingDbContext.AccountingRoles.Where(x => x.IsActive == 1).ToList();
        }
        public EntityMasterModel GetBranchNameById(int EntityId)
        {
            return _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == EntityId).FirstOrDefault();
        }
    }

}
