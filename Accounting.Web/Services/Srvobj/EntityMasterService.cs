﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IEntityMasterService
    {
        List<EntityMasterModel> GetAllEntity();
        EntityMasterModel GetEntityByID(int LevelID);
        EntityMasterModel GetEntityByEntityCode(string EntityCode);
        List<EntityMasterModel> GetEntityByLevelID(int LevelID);
        List<EntityMasterModel> GetCompany(int LegalEntityID);
        List<EntityMasterModel> GetEntityByParentEntityID(int ParentEntityID);
        void DeleteEntityModel(int ServiceID, int _UserID);
        int SaveEntity(EntityMasterModel objEntityModel,int UserID);

        List<EntityMasterViewModelList> GetAllEntityMasterViewModelList();
    }

    public class EntityMasterService : IEntityMasterService
    {
        private readonly CDbcontext _objaccountingEntityMasterDbContext;

        public EntityMasterService(CDbcontext objaccountingDbContext)
        {
            _objaccountingEntityMasterDbContext = objaccountingDbContext;
        }

        public void DeleteEntityModel(int EntityID, int _UserID)
        {
            EntityMasterModel objentitymodelModel = _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == EntityID).FirstOrDefault();
            objentitymodelModel.IsActive = false;
            _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Update(objentitymodelModel);
            _objaccountingEntityMasterDbContext.SaveChanges();
        }
        public List<EntityMasterModel> GetCompany(int LegalEntityID)
        {
            var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objaccountingEntityMasterDbContext.AccountingTransactionEntryModel
                                                    where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.LegalEntityID == LegalEntityID
                                                    select new EntityMasterModel
                                                    {
                                                        EntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                        Name = _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,
                                                    }).ToList();

            return objTransactionEntryViewModelList;
        }

        public List<EntityMasterModel> GetAllEntity()
        {
            return _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.IsActive == true ).ToList();
        }
        public EntityMasterModel GetEntityByID(int ID)
        {
            return _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.IsActive == true && x.EntityID == ID).FirstOrDefault();
            //return _objCertaDbContext.Certaclient.Where(x => x.IsActive == true && x.CompanyID == CompanyID).ToList();
        }
        public EntityMasterModel GetEntityByEntityCode(string EntityCode)
        {
            return _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.IsActive == true && x.LegalEntityslcode == EntityCode).FirstOrDefault();
            //return _objCertaDbContext.Certaclient.Where(x => x.IsActive == true && x.CompanyID == CompanyID).ToList();
        }
        
        public List<EntityMasterModel> GetEntityByLevelID(int ID)
        {
            return _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.IsActive == true && x.EntityLevel == ID).ToList();
            //return _objCertaDbContext.Certaclient.Where(x => x.IsActive == true && x.CompanyID == CompanyID).ToList();
        }
        public List<EntityMasterModel> GetEntityByParentEntityID(int ParentEntityID)
        {
            return _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.IsActive == true && x.ParentEntityID == ParentEntityID).ToList();
            //return _objCertaDbContext.Certaclient.Where(x => x.IsActive == true && x.CompanyID == CompanyID).ToList();
        }
        public List<EntityMasterViewModelList> GetAllEntityMasterViewModelList()
        {
            var objEntityMasterViewModelList = (from objEntityMasterViewModel in _objaccountingEntityMasterDbContext.AccountingEntityMasterModel
                                                    //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityProjectMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                    //join objAccountingProjectModel in _objAccountingDbContext.AccountingProjectModel on objAccountingLegalEntityProjectMappingModel.ProjectID equals objAccountingProjectModel.ProjectID
                                                where objEntityMasterViewModel.IsActive == true
                                                select new EntityMasterViewModelList
                                                {
                                                    EntityID = objEntityMasterViewModel.EntityID,
                                                    Name = objEntityMasterViewModel.Name,
                                                    EntityLevel = objEntityMasterViewModel.EntityLevel,
                                                    EntityLevelName = _objaccountingEntityMasterDbContext.AccountingEntityLevel.Where(x => x.EntityLevelID == objEntityMasterViewModel.EntityLevel && x.IsActive == true).FirstOrDefault().EntityLevelName,
                                                    ParentEntityID = objEntityMasterViewModel.ParentEntityID,
                                                    ParentEntityName = _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objEntityMasterViewModel.ParentEntityID && x.IsActive == true).FirstOrDefault().Name,
                                                    Address = objEntityMasterViewModel.Address,
                                                    TANNO = objEntityMasterViewModel.TANNO,
                                                    CINNO = objEntityMasterViewModel.CINNO,
                                                    PANNO = objEntityMasterViewModel.PANNO,
                                                    GSTNO = objEntityMasterViewModel.GSTNO,
                                                    LegalEntityslcode = objEntityMasterViewModel.LegalEntityslcode,
                                                }).OrderBy(x => x.EntityLevel).ThenBy(x => x.ParentEntityName).ThenBy(x => x.Name).ToList();
            return objEntityMasterViewModelList;
        }

        public int SaveEntity(EntityMasterModel objEntityModel, int UserID)
        {
            int AccountID = 0;
            if (_objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Where(x => x.EntityID != objEntityModel.EntityID && x.Name == objEntityModel.Name && x.IsActive == true).ToList().Count == 0)
            {
                if (objEntityModel.EntityID == 0)
                {
                    objEntityModel.IsActive = true;
                    objEntityModel.CreatedBy = UserID;
                    objEntityModel.CreatedOn = DateTime.Now;
                    _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Add(objEntityModel);
                }
                else
                {
                    // objChartofAccountsModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objChartofAccountsModel.CreatedOn));
                    objEntityModel.LastModifiedBy = UserID;
                    objEntityModel.LastModifiedOn = DateTime.Now;
                    _objaccountingEntityMasterDbContext.AccountingEntityMasterModel.Update(objEntityModel);
                }
                _objaccountingEntityMasterDbContext.SaveChanges();
                AccountID = objEntityModel.EntityID;
            }
            return AccountID;
        }
        //public EntityMasterModel GetEntityById(int intuserid)
        //{
        //    return _objaccountingEntityMasterDbContext.EntityMaster.Where(x => x.EntityID == intUserId).FirstOrDefault();
        //}

        //public void SaveEntity(EntityMasterModel objEntityModel)
        //{
        //    if (objEntityModel.EntityID == 0)
        //    {
        //        objEntityModel.CreatedOn = DateTime.Now;
        //        objEntityModel.IsActive = true;
        //        objEntityModel.LastModifiedBy = 0;
        //        _objaccountingEntityMasterDbContext.EntityMaster.Add(objEntityModel);
        //    }
        //    else
        //    {
        //        objEntityModel.LastModifiedOn = DateTime.Now;
        //        objEntityModel.LastModifiedBy = 0;
        //        _objaccountingEntityMasterDbContext.EntityMaster.Update(objEntityModel);
        //    }
        //    _objaccountingEntityMasterDbContext.SaveChanges();
        //}
    }
}
