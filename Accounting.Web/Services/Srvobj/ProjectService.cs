﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IProjectService
    {
        List<ProjectModel> GetAllProject();
       // List<ProjectModel> GetAllProjectByLegalEntityID(int LegalEntityID);
        void SaveProject(ProjectModel objProjectModel, int UserID);
        ProjectModel GetProjectByID(int ProjectID);
        int DeleteProject(int ProjectID, int UserID);
    }
    public class ProjectService : IProjectService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public ProjectService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteProject(int ProjectID, int UserID)
        {
            ProjectModel objProjectModel = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == ProjectID).FirstOrDefault();
            objProjectModel.IsActive = false;
            _objAccountingDbContext.AccountingProjectModel.Update(objProjectModel);
            _objAccountingDbContext.SaveChanges();
            return objProjectModel.ProjectID;
        }

        //public List<ProjectModel> GetAllProjectByLegalEntityID(int LegalEntityID)
        //{
        //    return _objAccountingDbContext.AccountingProjectModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        //}
        public List<ProjectModel> GetAllProject()
        {
            return _objAccountingDbContext.AccountingProjectModel.Where(x => x.IsActive == true).ToList();
        }
        public ProjectModel GetProjectByID(int ProjectID)
        {
            return _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == ProjectID && x.IsActive == true).FirstOrDefault();
        }

        public void SaveProject(ProjectModel objProjectModel, int UserID)
        {
            if (objProjectModel.ProjectID == 0)
            {
                objProjectModel.IsActive = true;
                objProjectModel.CreatedBy = UserID;
                objProjectModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingProjectModel.Add(objProjectModel);
            }
            else
            {
               // objProjectModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objProjectModel.CreatedOn));
                objProjectModel.LastModifiedBy = UserID;
                objProjectModel.LastModifiedon = DateTime.Now;
                _objAccountingDbContext.AccountingProjectModel.Update(objProjectModel);
            }
            _objAccountingDbContext.SaveChanges();
        }
    }
}
