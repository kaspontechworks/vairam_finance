﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
        public interface IUDoptionsServices
        {
            List<UDoptionsModel> GetAllUDOOptionsByUDCategoryName(string UDCategoryName);

        }
        public class UDoptionsServices : IUDoptionsServices
    {
            private readonly CDbcontext _objAccountingDbContext;
            public UDoptionsServices(CDbcontext objcertaDbContext) { _objAccountingDbContext = objcertaDbContext; }

            public List<UDoptionsModel> GetAllUDOOptionsByUDCategoryName(string UDCategoryName)
            {
                UDcategoryMaster _objCategoryModel = _objAccountingDbContext.AccountingUDCategoryMasterModel.Where(x => x.IsActive == true && x.UDCategoryName == UDCategoryName).FirstOrDefault();
                return _objAccountingDbContext.AccountingUDOptionsMasterModel.Where(x => x.IsActive == true && x.UDCategoryID == _objCategoryModel.UDCategoryID).ToList();
            }

        }
}

