﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ILegalEntityProjectMappingService
    {
        List<LegalEntityProjectMappingModel> GetAllLegalEntityProjectMapping();
        List<LegalEntityProjectMappingModel> GetAllLegalEntityProjectMappingByLegalEntityID(int LegalEntityID);
        List<LegalEntityProjectMappingModel> GetAllLegalEntityProjectMappingByLegalEntityIDProfitCentreIDLocationIDUnitID(int LegalEntityID, int ProfitCentreID, int LocationID, int UnitID);

        //void SaveLegalEntityProjectMapping(LegalEntityProjectMappingModel objLegalEntityProjectMappingModel, int UserID);
        void SaveLegalEntityProjectMapping(LegalEntityProjectDefinations LegalEntityProjectDefinations, int UserID);
        LegalEntityProjectMappingModel GetLegalEntityProjectMappingByID(int LegalEntityID);
        
        int DeleteLegalEntityProjectMapping(int CategoryID, int UserID);

        List<LegalEntityProjectMappingViewModelList> GetAllLegalEntityProjectMappingViewModelList(int LegalEntityID);
        List<LegalEntityProjectMappingViewModelList> GetAllLegalEntityProjectMappingViewModelListLegalEntityIDProfitCentreIDLocationIDUnitID(int LegalEntityID, int ProfitCentreID, int LocationID, int UnitID);
    }
    public class LegalEntityProjectMappingService : ILegalEntityProjectMappingService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public LegalEntityProjectMappingService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteLegalEntityProjectMapping(int LegalEntityID, int UserID)
        {
            LegalEntityProjectMappingModel objLegalEntityProjectMappingModel = _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Where(x => x.LegalEntityID == LegalEntityID).FirstOrDefault();
            objLegalEntityProjectMappingModel.IsActive = false;
            _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Update(objLegalEntityProjectMappingModel);
            _objAccountingDbContext.SaveChanges();
            return objLegalEntityProjectMappingModel.LegalEntityID;
        }

        public List<LegalEntityProjectMappingModel> GetAllLegalEntityProjectMappingByLegalEntityID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        }
        public List<LegalEntityProjectMappingModel> GetAllLegalEntityProjectMappingByLegalEntityIDProfitCentreIDLocationIDUnitID(int LegalEntityID, int ProfitCentreID, int LocationID, int UnitID)
        {
            return _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID && x.ProfitCentreID == ProfitCentreID && x.LocationID == LocationID && x.UnitID == UnitID).ToList();
        }
        public List<LegalEntityProjectMappingModel> GetAllLegalEntityProjectMapping()
        {
            return _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Where(x => x.IsActive == true).ToList();
        }
        public LegalEntityProjectMappingModel GetLegalEntityProjectMappingByID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Where(x => x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
        }
        public void SaveLegalEntityProjectMapping(LegalEntityProjectDefinations LegalEntityProjectDefinations, int UserID)
        {
            int LegalEntityid = LegalEntityProjectDefinations.LegalEntityID;
            int ProfitCentreiD = LegalEntityProjectDefinations.ProfitCentreID;
            int LocationiD = LegalEntityProjectDefinations.LocationID;
            int UnitiD = LegalEntityProjectDefinations.UnitID;
            if (LegalEntityid != 0)
            {
                List<LegalEntityProjectMappingModel> _Listdef = _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Where(x => x.LegalEntityID == LegalEntityid && x.ProfitCentreID == ProfitCentreiD && x.LocationID == LocationiD && x.UnitID == UnitiD).ToList();
                _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.RemoveRange(_Listdef);
                _objAccountingDbContext.SaveChanges();

                _Listdef = (from t in LegalEntityProjectDefinations.ProjectList
                            select new LegalEntityProjectMappingModel
                            {
                                ProjectID = t.ProjectID,
                                CreatedBy = UserID,
                                CreatedOn = DateTime.Now,
                                IsActive = true,
                                UnitID = UnitiD,
                                LocationID = LocationiD,
                                ProfitCentreID = ProfitCentreiD,
                                LegalEntityID = LegalEntityid
                            }).ToList();
                _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.AddRange(_Listdef);
                _objAccountingDbContext.SaveChanges();

            }

        }
        public List<LegalEntityProjectMappingViewModelList> GetAllLegalEntityProjectMappingViewModelList(int LegalEntityID)
        {
            if (LegalEntityID != 0)
            {
                var objLegalEntityProjectMappingViewModelList = (from objAccountingLegalEntityProjectMappingModel in _objAccountingDbContext.AccountingLegalEntityProjectMappingModel
                                                  //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityProjectMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                  //join objAccountingProjectModel in _objAccountingDbContext.AccountingProjectModel on objAccountingLegalEntityProjectMappingModel.ProjectID equals objAccountingProjectModel.ProjectID
                                                  where objAccountingLegalEntityProjectMappingModel.LegalEntityID == LegalEntityID
                                                  select new LegalEntityProjectMappingViewModelList
                                                  {
                                                      LegalEntityID = objAccountingLegalEntityProjectMappingModel.LegalEntityID,
                                                      LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityProjectMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                      ProfitCentreID = objAccountingLegalEntityProjectMappingModel.ProfitCentreID,
                                                      ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityProjectMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                      LocationID = objAccountingLegalEntityProjectMappingModel.LocationID,
                                                      LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityProjectMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                      UnitID = objAccountingLegalEntityProjectMappingModel.UnitID,
                                                      UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingLegalEntityProjectMappingModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                      ProjectID = objAccountingLegalEntityProjectMappingModel.ProjectID,
                                                      ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingLegalEntityProjectMappingModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                  }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ThenBy(x => x.UnitName).ThenBy(x => x.ProjectName).ToList();
                return objLegalEntityProjectMappingViewModelList;
            }
            else
            {
                var objLegalEntityProjectMappingViewModelList = (from objAccountingLegalEntityProjectMappingModel in _objAccountingDbContext.AccountingLegalEntityProjectMappingModel
                                                                     //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityProjectMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                                     // join objAccountingProjectModel in _objAccountingDbContext.AccountingProjectModel on objAccountingLegalEntityProjectMappingModel.ProjectID equals objAccountingProjectModel.ProjectID
                                                                     //where objAccountingLegalEntityProjectMappingModel.LegalEntityID == LegalEntityID
                                                                 select new LegalEntityProjectMappingViewModelList
                                                                 {
                                                                     LegalEntityID = objAccountingLegalEntityProjectMappingModel.LegalEntityID,
                                                                     LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityProjectMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                                     ProfitCentreID = objAccountingLegalEntityProjectMappingModel.ProfitCentreID,
                                                                     ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityProjectMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                                     LocationID = objAccountingLegalEntityProjectMappingModel.LocationID,
                                                                     LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityProjectMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                                     UnitID = objAccountingLegalEntityProjectMappingModel.UnitID,
                                                                     UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingLegalEntityProjectMappingModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                                     ProjectID = objAccountingLegalEntityProjectMappingModel.ProjectID,
                                                                     ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingLegalEntityProjectMappingModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                                 }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ThenBy(x => x.UnitName).ThenBy(x => x.ProjectName).ToList();

                return objLegalEntityProjectMappingViewModelList;
            }
        }
        public List<LegalEntityProjectMappingViewModelList> GetAllLegalEntityProjectMappingViewModelListLegalEntityIDProfitCentreIDLocationIDUnitID(int LegalEntityID, int ProfitCentreID, int LocationID,int UnitID)
        {

            var objLegalEntityProjectMappingViewModelList = (from objAccountingLegalEntityProjectMappingModel in _objAccountingDbContext.AccountingLegalEntityProjectMappingModel
                                                                 //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityProjectMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                                 //join objAccountingProjectModel in _objAccountingDbContext.AccountingProjectModel on objAccountingLegalEntityProjectMappingModel.ProjectID equals objAccountingProjectModel.ProjectID
                                                             where objAccountingLegalEntityProjectMappingModel.LegalEntityID == LegalEntityID && objAccountingLegalEntityProjectMappingModel.ProfitCentreID == ProfitCentreID && objAccountingLegalEntityProjectMappingModel.LocationID == LocationID && objAccountingLegalEntityProjectMappingModel.UnitID == UnitID
                                                             select new LegalEntityProjectMappingViewModelList
                                                             {
                                                                 LegalEntityID = objAccountingLegalEntityProjectMappingModel.LegalEntityID,
                                                                 LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityProjectMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                                 ProfitCentreID = objAccountingLegalEntityProjectMappingModel.ProfitCentreID,
                                                                 ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityProjectMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                                 LocationID = objAccountingLegalEntityProjectMappingModel.LocationID,
                                                                 LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityProjectMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                                 UnitID = objAccountingLegalEntityProjectMappingModel.UnitID,
                                                                 UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingLegalEntityProjectMappingModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                                 ProjectID = objAccountingLegalEntityProjectMappingModel.ProjectID,
                                                                 ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingLegalEntityProjectMappingModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                             }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ThenBy(x => x.UnitName).ThenBy(x => x.ProjectName).ToList();
            return objLegalEntityProjectMappingViewModelList;

        }
            public void SaveLegalEntityProjectMapping(LegalEntityProjectMappingModel objLegalEntityProjectMappingModel, int UserID)
        {
            //if (objLegalEntityProjectMappingModel.CategoryID == 0)
            //{
            //    objLegalEntityProjectMappingModel.IsActive = true;
            //    objLegalEntityProjectMappingModel.CreatedBy = UserID;
            //    objLegalEntityProjectMappingModel.CreatedOn = DateTime.Now;
            //    _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Add(objLegalEntityProjectMappingModel);
            //}
            //else
            //{
            //   // objLegalEntityProjectMappingModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objLegalEntityProjectMappingModel.CreatedOn));
            //    objLegalEntityProjectMappingModel.LastModifiedBy = UserID;
            //    objLegalEntityProjectMappingModel.LastModifiedon = DateTime.Now;
            //    _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Update(objLegalEntityProjectMappingModel);
            //}
            _objAccountingDbContext.SaveChanges();
        }
    }
}
