﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ITransactionEntryService
    {
        List<TransactionEntryModel> GetAllTransactionEntry();
        // List<TransactionEntryModel> GetAllTransactionEntryByLegalEntityID(int LegalEntityID);
        // int SaveTransactionEntry(TransactionEntryModel objTransactionEntryModel, int UserID);
        List<TransactionEntryModel> DeleteTransactionHead(int TransactionHeadID, int Id, int LegalEntityID);
        int SaveTransactionEntry(TransactionEntryDefinations objTransactionEntryModel, int UserID);
        int SaveTransactionEntryDeleteReceipt(TransactionEntryDefinations objTransactionEntryModel, int UserID);
        string UpdateTransactionEntry(string IsStatus, string HeadId);
        TransactionEntryModel GetTransactionEntryByID(int TransactionEntryID);
        int DeleteTransactionEntry(int TransactionEntryID, int UserID);
        List<TransactionEntryViewModelList> GetAllCashReceipts(int companyID, int LocationID);
        List<TransactionEntryViewModelList> GetAllTransactionEntryViewModelListByTransactionHeadID(int TransactionHeadID);
        List<TransactionEntryViewModelList> GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryViewModelList> GetAllTransactionEntryViewModelListByLegalEntityIDAccountLevelStartDateEndDate(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryViewModelList> GetAllTransactionEntryViewModelListRegisterByLegalEntityIDAccountIDStartDateEndDate(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate);
        SumChartofAccountsViewModelList GetSumChartofAccountsViewModelList(int LegalEntityID, int AccountID, int FinancialYearID);
        SumChartofAccountsViewModelList GetClosingBalanceChartofAccountsViewModelByStartDate(int LegalEntityID, int AccountID, DateTime StartDate);
        SumChartofAccountsViewModelList GetClosingBalanceTrialViewModelByStartDate(int LegalEntityID, int MajorGroupID, int GroupID, DateTime StartDate);
        MajorGroupViewModelList GetClosingMajorGroupViewModelByStartDate(int LegalEntityID, int MajorGroupID, DateTime StartDate);
        List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityID(int LegalEntityID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityIDMajorGroups(int LegalEntityID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityIDGroups(int LegalEntityID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityIDSubGroups(int LegalEntityID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityIDProfitandLoss(int LegalEntityID,int MajorGroupID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryTrialList> GetBalanceSheetAmount(int LegalEntityID,int MajorGroupID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryTrialList> GetGroupComparisonReportRangeforProfitandLoss(int LegalEntityID, string StartDate, string EndDate);
        ProfitandlossAmount GetGroupComparisonReportRangeforProfitandLosswithjson(int BusinessAreaID, string StartDate, string EndDate, string StartDate1, string EndDate1);
        List<TransactionEntryViewModelList> GetAllTransactionHead_Transactiondetails(int ID,int LegalEntityID);        
        List<TransactionEntryViewModelList> GetAllTransactionHead_TransactionID(int ID, int LegalEntityID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryViewModelList> GetReferenceNo(string TransactionShortRefNo);
        List<TransactionEntryViewModelList> GetAllTransactionEntryTrialListMonthlyProfitandLoss(int LegalEntityID, int MajorGroupId, int GroupID, string StartDate, string EndDate);
        List<TransactionEntryViewModelList> GetCompanydetails_ID(int LegalEntityID, int LocationID);
        List<TransactionEntryViewModelList> Getaccountdetails(int LegalEntityID, int GroupID, string StartDate, string EndDate);
        List<TransactionEntryViewModelList> GetAccountProfitandLoss(int LegalEntityID, int GroupID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryViewModelList> GetAccount_balancesheet(int LegalEntityID, int GroupID, DateTime StartDate, DateTime EndDate);
        List<TransactionEntryModel> GetAllTransaction_CancelledVouchers(int Trn_Type, DateTime StartDate, DateTime EndDate, int LegalEntityId);
        List<TransactionEntryModel> GetAllTransaction_TransactionSuccessCash(int Trn_Type, DateTime StartDate, DateTime EndDate, int LegalEntityId);
        List<TransactionEntryModel> GetAllTransaction_TransactionInputTypecash(int Trn_Type, DateTime StartDate, DateTime EndDate, int LegalEntityId);
        List<TransactionInputType> GetAccounting_TransactionInputType();
        List<TransactioninputTypeCash> GetAccounting_TransactionInputTypecashreceipt();
        List<CashTransactionInputType> GetAccounting_TransactionInputTypecash();
    }
    public class TransactionEntryService : ITransactionEntryService
    {

        private readonly CDbcontext _objAccountingDbContext;
        public TransactionEntryService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteTransactionEntry(int TransactionEntryID, int UserID)
        {
            TransactionEntryModel objTransactionEntryModel = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.TransactionEntryID == TransactionEntryID).FirstOrDefault();
            objTransactionEntryModel.IsActive = false;
            _objAccountingDbContext.AccountingTransactionEntryModel.Update(objTransactionEntryModel);
            _objAccountingDbContext.SaveChanges();
            return objTransactionEntryModel.TransactionEntryID;
        }

        public List<TransactionEntryModel> GetAllTransaction_CancelledVouchers(int Trn_Type, DateTime StartDate, DateTime EndDate, int LegalEntityId)
        {
            //List<TransactionEntryModel> objTransactionEntryModel = 
            // return _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.TransactionInputTypeID == Trn_Type && x.IsStatus == 1 && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate).ToList();
            return _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.IsStatus == 2 && x.TransactionInputTypeID == Trn_Type && x.LegalEntityID == LegalEntityId && Convert.ToDateTime((x.LastModifiedon)) >= StartDate && Convert.ToDateTime((x.LastModifiedon)) <= EndDate).GroupBy(G => G.TransactionHeadID).Select(S => S.First()).ToList();
        }

        public List<TransactionEntryModel> GetAllTransaction_TransactionSuccessCash(int Trn_Type, DateTime StartDate, DateTime EndDate, int LegalEntityId)
        {
            //List<TransactionEntryModel> objTransactionEntryModel = 
            // return _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.TransactionInputTypeID == Trn_Type && x.IsStatus == 1 && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate).ToList();
            return _objAccountingDbContext.AccountingTransactionEntryModel2.Where(x => x.IsActive==true && x.IsStatus != 2 && x.TransactionInputTypeID == Trn_Type && x.LegalEntityID == LegalEntityId && Convert.ToDateTime((x.TransactionEntryDate)) >= StartDate && Convert.ToDateTime((x.TransactionEntryDate)) <= EndDate).GroupBy(G => G.TransactionHeadID).Select(S => S.First()).ToList();
        }

        public List<TransactionEntryModel> GetAllTransaction_TransactionInputTypecash(int Trn_Type, DateTime StartDate, DateTime EndDate, int LegalEntityId)
        {
            //List<TransactionEntryModel> objTransactionEntryModel = 
            // return _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.TransactionInputTypeID == Trn_Type && x.IsStatus == 1 && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate).ToList();
            return _objAccountingDbContext.AccountingTransactionEntryModel1.Where(x => x.IsStatus == 2 && x.TransactionInputTypeID == Trn_Type && x.LegalEntityID == LegalEntityId && Convert.ToDateTime((x.TransactionEntryDate)) >= StartDate && Convert.ToDateTime((x.TransactionEntryDate)) <= EndDate).GroupBy(G => G.TransactionHeadID).Select(S => S.First()).ToList();
        }

        public List<TransactionInputType> GetAccounting_TransactionInputType()
        {
            return _objAccountingDbContext.AccountingTransactionInputTypeModel.Where(x => x.IsActive == true && x.LastModifiedBy == 1).ToList();
        }

        public List<TransactioninputTypeCash> GetAccounting_TransactionInputTypecashreceipt()
        {
            return _objAccountingDbContext.AccountingTransactionInputTypeModel2.Where(x => x.IsActive == true && x.LastModifiedBy == 1).ToList();
        }

        public List<CashTransactionInputType> GetAccounting_TransactionInputTypecash()
        {
            return _objAccountingDbContext.AccountingTransactionInputTypeModel1.Where(x => x.IsActive == true && x.LastModifiedBy == 1 ).ToList();
        }


        //public List<TransactionEntryModel> GetAllTransactionEntryByLegalEntityID(int LegalEntityID)
        //{
        //    return _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        //}
        public List<TransactionEntryModel> GetAllTransactionEntry()
        {
            return _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.IsActive == true).ToList();
        }
        public List<TransactionEntryModel> DeleteTransactionHead(int TransactionHeadID, int Id, int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.TransactionHeadID == TransactionHeadID && x.TransactionInputTypeID == Id && x.LegalEntityID == LegalEntityID && x.IsActive == true).ToList();
            //objTransactionHeadModel.IsActive = false;
            //_objAccountingDbContext.AccountingTransactionHeadModel.Update(objTransactionHeadModel);
            //_objAccountingDbContext.SaveChanges();
            //return objTransactionHeadModel.TransactionHeadID;
        }
        [HttpGet]
        [IgnoreAntiforgeryToken]
        [AllowAnonymous]

        public List<TransactionEntryViewModelList> Getaccountdetails(int LegalEntityID, int GroupID, string StartDate, string EndDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };
            TransactionEntryViewModelList objTransactionAccount_Details = (from _TE in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                                           join _TH in _objAccountingDbContext.AccountingChartofAccountsModel on _TE.LedgerGroupID equals _TH.GroupID into M1
                                                                           from _Account in M1.DefaultIfEmpty()
                                                                           where _TE.LedgerGroupID == GroupID && Majorgroupid.Contains(_TE.ParentLedgerGroupID)
                                                                           select new TransactionEntryViewModelList
                                                                           {
                                                                               AccountID = _Account.AccountID,
                                                                               AccountName = _Account.AccountName
                                                                           }).FirstOrDefault();

            var objTransactionEntryViewModelList1 = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                     join _TH in _objAccountingDbContext.AccountingChartofAccountsModel on objAccountingLedgerGroupMasterModel.LedgerGroupID equals _TH.GroupID into M1
                                                     from _Accountname in M1.DefaultIfEmpty()
                                                     where objAccountingLedgerGroupMasterModel.IsActive == true && objAccountingLedgerGroupMasterModel.LedgerGroupID == GroupID &&
                                                     Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID) && _Accountname.LegalEntityID == LegalEntityID
                                                     select new TransactionEntryViewModelList
                                                     {
                                                         MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                         MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                         GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                         GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                         AccountID = _Accountname.AccountID,
                                                         AccountName = _Accountname.AccountName,
                                                         LegalEntityID = _Accountname.LegalEntityID,
                                                         LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == _Accountname.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                     }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryViewModelList objListdef in objTransactionEntryViewModelList1)
            {

                int majorgroup_arr = objListdef.MajorGroupID;

                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = Getsummonthlyprofitandlossclosing(LegalEntityID, majorgroup_arr, objListdef.AccountID, StartDate);
                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetSumMonthlyprofitandlossAll1(LegalEntityID, majorgroup_arr, objListdef.AccountID, StartDate, EndDate);
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                if (objSumChartofAccountsViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumChartofAccountsViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumChartofAccountsViewModelListOpen.Credit;
                }
                if (objSumChartofAccountsViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumChartofAccountsViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumChartofAccountsViewModelListClose.Credit;
                }
                string MajorGroupName = objListdef.MajorGroupName;
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;
                decimal Debit = objListdef.Debit;
                decimal Credit = objListdef.Credit;
            }
            return objTransactionEntryViewModelList1;
        }
        public List<TransactionEntryViewModelList> GetAccountProfitandLoss(int LegalEntityID, int GroupID, DateTime StartDate, DateTime EndDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };
            int[] ParentCategoryID = { 3, 4 };
            TransactionEntryViewModelList objTransactionAccount_Details = (from _TE in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                                           join _TH in _objAccountingDbContext.AccountingChartofAccountsModel on _TE.LedgerGroupID equals _TH.GroupID into M1
                                                                           from _Account in M1.DefaultIfEmpty()
                                                                           where _TE.LedgerGroupID == GroupID && Majorgroupid.Contains(_TE.ParentLedgerGroupID)
                                                                           select new TransactionEntryViewModelList
                                                                           {
                                                                               AccountID = _Account.AccountID,
                                                                               AccountName = _Account.AccountName
                                                                           }).FirstOrDefault();

            var objTransactionEntryViewModelList1 = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                     join _TH in _objAccountingDbContext.AccountingChartofAccountsModel on objAccountingLedgerGroupMasterModel.LedgerGroupID equals _TH.GroupID into M1
                                                     from _Accountname in M1.DefaultIfEmpty()
                                                     where objAccountingLedgerGroupMasterModel.IsActive == true && objAccountingLedgerGroupMasterModel.LedgerGroupID == GroupID &&
                                                     Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID) && ParentCategoryID.Contains(_Accountname.ParentCategoryID) && _Accountname.LegalEntityID == LegalEntityID
                                                     select new TransactionEntryViewModelList
                                                     {
                                                         MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                         MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                         GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                         GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                         AccountID = _Accountname.AccountID,
                                                         AccountName = _Accountname.AccountName,
                                                         LegalEntityID = _Accountname.LegalEntityID,
                                                         LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == _Accountname.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                     }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryViewModelList objListdef in objTransactionEntryViewModelList1)
            {

                int majorgroup_arr = objListdef.MajorGroupID;

                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = Getsum_monthlyprofitandlossclosing(LegalEntityID, majorgroup_arr, objListdef.AccountID, StartDate);
                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetSum_MonthlyprofitandlossAll1(LegalEntityID, majorgroup_arr, objListdef.AccountID, StartDate, EndDate);
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                decimal openingbalance = objSumChartofAccountsViewModelListOpen.Debit - objSumChartofAccountsViewModelListOpen.Credit;
                decimal closingbalance = objSumChartofAccountsViewModelListClose.Debit - objSumChartofAccountsViewModelListClose.Credit;
                decimal debit = objSumChartofAccountsViewModelListClose.Debit - objSumChartofAccountsViewModelListOpen.Debit;
                decimal credit = objSumChartofAccountsViewModelListClose.Credit - objSumChartofAccountsViewModelListOpen.Credit;

                if (objSumChartofAccountsViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumChartofAccountsViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumChartofAccountsViewModelListOpen.Credit;
                }
                if (objSumChartofAccountsViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumChartofAccountsViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumChartofAccountsViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;
                decimal ClosingBalance = objListdef.Credit;
                decimal ClosingBalance1 = objListdef.Debit;
            }
            return objTransactionEntryViewModelList1;
        }
        public List<TransactionEntryViewModelList> GetAccount_balancesheet(int LegalEntityID, int GroupID, DateTime StartDate, DateTime EndDate)
        {
            int[] Majorgroupid = { 63, 64, 65, 66, 67, 68, 69, 70, 72, 74, 73, 75 };
            TransactionEntryViewModelList objTransactionAccount_Details = (from _TE in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                                           join _TH in _objAccountingDbContext.AccountingChartofAccountsModel on _TE.LedgerGroupID equals _TH.GroupID into M1
                                                                           from _Account in M1.DefaultIfEmpty()
                                                                           where _TE.LedgerGroupID == GroupID && Majorgroupid.Contains(_TE.ParentLedgerGroupID)
                                                                           select new TransactionEntryViewModelList
                                                                           {
                                                                               AccountID = _Account.AccountID,
                                                                               AccountName = _Account.AccountName
                                                                           }).FirstOrDefault();

            var objTransactionEntryViewModelList1 = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                     join _TH in _objAccountingDbContext.AccountingChartofAccountsModel on objAccountingLedgerGroupMasterModel.LedgerGroupID equals _TH.GroupID into M1
                                                     from _Accountname in M1.DefaultIfEmpty()
                                                     where objAccountingLedgerGroupMasterModel.IsActive == true && objAccountingLedgerGroupMasterModel.LedgerGroupID == GroupID &&
                                                     Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID) && _Accountname.LegalEntityID == LegalEntityID
                                                     select new TransactionEntryViewModelList
                                                     {
                                                         MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                         MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                         GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                         GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                         AccountID = _Accountname.AccountID,
                                                         AccountName = _Accountname.AccountName,
                                                         LegalEntityID = _Accountname.LegalEntityID,
                                                         LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == _Accountname.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                     }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryViewModelList objListdef in objTransactionEntryViewModelList1)
            {

                int majorgroup_arr = objListdef.MajorGroupID;

                //SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = GetAccount_BalancesheetOpen(LegalEntityID, majorgroup_arr, objListdef.AccountID, StartDate);
                //SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetAccount_Balancesheetclose(LegalEntityID, majorgroup_arr, objListdef.AccountID, StartDate, EndDate);

                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = GetAccount_BalancesheetOpen(LegalEntityID, majorgroup_arr, objListdef.AccountID, StartDate);
                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetAccount_Balancesheetclose(LegalEntityID, majorgroup_arr, objListdef.AccountID, StartDate, EndDate);
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                decimal openingbalance = objSumChartofAccountsViewModelListOpen.Debit - objSumChartofAccountsViewModelListOpen.Credit;
                decimal closingbalance = objSumChartofAccountsViewModelListClose.Debit - objSumChartofAccountsViewModelListClose.Credit;
                decimal debit = objSumChartofAccountsViewModelListClose.Debit - objSumChartofAccountsViewModelListOpen.Debit;
                decimal credit = objSumChartofAccountsViewModelListClose.Credit - objSumChartofAccountsViewModelListOpen.Credit;
                if (objSumChartofAccountsViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumChartofAccountsViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumChartofAccountsViewModelListOpen.Credit;
                }
                if (objSumChartofAccountsViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumChartofAccountsViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumChartofAccountsViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;
                decimal ClosingBalance = objListdef.Credit;
                decimal ClosingBalance1 = objListdef.Debit;

            }
            return objTransactionEntryViewModelList1;
        }
        public SumChartofAccountsViewModelList GetAccount_BalancesheetOpen(int LegalEntityID, int MajorGroupID, int AccountID, DateTime StartDate)
        {
            int[] Majorgroupid = { 63, 64, 65, 66, 67, 68, 69, 70, 72, 74, 73, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;

            return objSumChartofAccountsViewModelList;
        }
        private SumChartofAccountsViewModelList GetAccount_Balancesheetclose(int LegalEntityID, int majorGroupID, int AccountID, DateTime StartDate, DateTime EndDate)
        {
            int[] Majorgroupid = { 63, 64, 65, 66, 67, 68, 69, 70, 72, 74, 73, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(majorGroupID) && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.MajorGroupID == majorGroupID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;
            return objSumChartofAccountsViewModelList;

        }
        public TransactionEntryModel GetTransactionEntryByID(int TransactionEntryID)
        {
            return _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.TransactionEntryID == TransactionEntryID && x.IsActive == true).FirstOrDefault();
        }

        public List<TransactionEntryViewModelList> GetAllTransactionHead_TransactionID(int ID, int LegalEntityID, DateTime StartDate, DateTime EndDate)
        {
            if(ID != 10)
            {
                var objTransactionEntryViewModelList = (from objTransactionEntry in _objAccountingDbContext.AccountingTransactionEntryModel
                                                        join objLocations in _objAccountingDbContext.AccountingProfitCentreModel on objTransactionEntry.ProfitCentreID
                                                         equals objLocations.ProfitCentreID
                                                        select new
                                                        {
                                                            objTransactionEntry,
                                                            objLocations
                                                        } into M1
                                                        where M1.objTransactionEntry.IsActive == true && M1.objTransactionEntry.IsStatus != 2 && M1.objTransactionEntry.TransactionInputTypeID == ID && M1.objTransactionEntry.LegalEntityID == LegalEntityID && Convert.ToDateTime((M1.objTransactionEntry.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime(M1.objTransactionEntry.TransactionEntryDate.ToShortDateString()) <= EndDate
                                                        group M1 by new { M1.objTransactionEntry.TransactionShortRefNo, M1.objTransactionEntry.TransactionEntryDate, M1.objTransactionEntry.TransactionHeadID, M1.objTransactionEntry.TransactionInputTypeID, M1.objLocations.Pastdate_status }
                                                        into grouping
                                                        select new TransactionEntryViewModelList
                                                        {
                                                            //TransactionHeadID = objTransactionEntry.TransactionHeadID,
                                                            //TransactionEntryDate =Convert.ToDateTime(objTransactionEntry.TransactionEntryDate.ToString ("dd/MM/yyyy")),
                                                            //Debit = objTransactionEntry.Debit,
                                                            //Credit = objTransactionEntry.Credit,
                                                            //AccountID = objTransactionEntry.AccountID,
                                                            //AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objTransactionEntry.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                            //TransactionShortRefNo = objTransactionEntry.TransactionShortRefNo,
                                                            //Description = objTransactionEntry.Description,
                                                            //LegalEntityID = objTransactionEntry.LegalEntityID,
                                                            //LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objTransactionEntry.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                            //TransactionInputTypeID = objTransactionEntry.TransactionInputTypeID,
                                                            //TransactionInputTypeName = _objAccountingDbContext.AccountingTransactionInputTypeModel.Where(x => x.TransactionInputTypeID == objTransactionEntry.TransactionInputTypeID && x.IsActive == true).FirstOrDefault().TransactionInputTypeName,
                                                            TransactionShortRefNo = grouping.Key.TransactionShortRefNo,
                                                            TransactionEntryDate = grouping.Key.TransactionEntryDate,
                                                            TransactionHeadID = grouping.Key.TransactionHeadID,
                                                            TransactionInputTypeID = grouping.Key.TransactionInputTypeID,
                                                            Pastdate_status = grouping.Key.Pastdate_status

                                                        }).OrderBy(x => x.TransactionEntryDate);
                                                     
            var orderby_Transactiondata = objTransactionEntryViewModelList.ToList();
                return orderby_Transactiondata;

            }
            else
            {
                var objTransactionEntryViewModelList = (from objTransactionEntry in _objAccountingDbContext.AccountingTransactionEntryModel
                                                        join objLocations in _objAccountingDbContext.AccountingProfitCentreModel on objTransactionEntry.ProfitCentreID
                                                         equals objLocations.ProfitCentreID
                                                        select new
                                                        {
                                                            objTransactionEntry,
                                                            objLocations
                                                        } into M1
                                                        where M1.objTransactionEntry.IsStatus != 2 && M1.objTransactionEntry.TransactionInputTypeID == ID && M1.objTransactionEntry.LegalEntityID == LegalEntityID && Convert.ToDateTime((M1.objTransactionEntry.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime(M1.objTransactionEntry.TransactionEntryDate.ToShortDateString()) <= EndDate
                                                        group M1 by new { M1.objTransactionEntry.TransactionShortRefNo, M1.objTransactionEntry.TransactionEntryDate, M1.objTransactionEntry.TransactionHeadID, M1.objTransactionEntry.TransactionInputTypeID, M1.objLocations.Pastdate_status }
                                                        into grouping
                                                        select new TransactionEntryViewModelList
                                                        {
                                                            //TransactionHeadID = objTransactionEntry.TransactionHeadID,
                                                            //TransactionEntryDate =Convert.ToDateTime(objTransactionEntry.TransactionEntryDate.ToString ("dd/MM/yyyy")),
                                                            //Debit = objTransactionEntry.Debit,
                                                            //Credit = objTransactionEntry.Credit,
                                                            //AccountID = objTransactionEntry.AccountID,
                                                            //AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objTransactionEntry.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                            //TransactionShortRefNo = objTransactionEntry.TransactionShortRefNo,
                                                            //Description = objTransactionEntry.Description,
                                                            //LegalEntityID = objTransactionEntry.LegalEntityID,
                                                            //LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objTransactionEntry.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                            //TransactionInputTypeID = objTransactionEntry.TransactionInputTypeID,
                                                            //TransactionInputTypeName = _objAccountingDbContext.AccountingTransactionInputTypeModel.Where(x => x.TransactionInputTypeID == objTransactionEntry.TransactionInputTypeID && x.IsActive == true).FirstOrDefault().TransactionInputTypeName,
                                                            TransactionShortRefNo = grouping.Key.TransactionShortRefNo,
                                                            TransactionEntryDate = grouping.Key.TransactionEntryDate,
                                                            TransactionHeadID = grouping.Key.TransactionHeadID,
                                                            TransactionInputTypeID = grouping.Key.TransactionInputTypeID,
                                                            Pastdate_status = grouping.Key.Pastdate_status

                                                        }).OrderByDescending(x => x.TransactionEntryDate);
                var orderby_Transactiondata = objTransactionEntryViewModelList.ToList();
                return orderby_Transactiondata;

            }

        }
        public List<TransactionEntryViewModelList> GetCompanydetails_ID(int LegalEntityID, int LocationID)
        {

            var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                    where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.LegalEntityID == LegalEntityID && objAccountingTransactionEntryModel.LocationID == LocationID
                                                    select new TransactionEntryViewModelList
                                                    {
                                                        LegalEntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                        LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                        LocationID = objAccountingTransactionEntryModel.LocationID,
                                                        LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingTransactionEntryModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                    });
            var orderby_Transactiondata = objTransactionEntryViewModelList.ToList();
            return orderby_Transactiondata;

        }
        public IEnumerable<SelectListItem> Months
        {
            get
            {
                return DateTimeFormatInfo
                       .InvariantInfo
                       .MonthNames
                       .Select((monthName, index) => new SelectListItem
                       {
                           Value = (index + 1).ToString(),
                           Text = monthName
                       });
            }
        }
        public List<TransactionEntryViewModelList> GetAllTransactionHead_Transactiondetails(int ID, int LegalEntityID)
        {

            TransactionHeadViewModelList objTransactionHead = (from _TE in _objAccountingDbContext.AccountingTransactionEntryModel
                                                               join _TH in _objAccountingDbContext.AccountingTransactionHeadModel on _TE.TransactionHeadID equals _TH.TransactionHeadID into M1
                                                               from _Description in M1.DefaultIfEmpty()
                                                               where _TE.TransactionHeadID == ID
                                                               select new TransactionHeadViewModelList
                                                               {
                                                                   DescriptionHead = _Description.DescriptionHead,
                                                                  TransactionInputTypeID = _Description.TransactionInputTypeID,
                                                                   IsActive = _TE.IsActive
                                                               }).FirstOrDefault();
            int TransactionInput = objTransactionHead.TransactionInputTypeID;

            List<TransactionEntryViewModelList> objTransactionEntryViewModelList=new List<TransactionEntryViewModelList>();

            if (TransactionInput != 10 && objTransactionHead.IsActive == true)
            {
                objTransactionEntryViewModelList = (from objTransactionEntry in _objAccountingDbContext.AccountingTransactionEntryModel
                                                    join _TH in _objAccountingDbContext.AccountingTransactionHeadModel on objTransactionEntry.TransactionHeadID equals objTransactionHead.TransactionHeadID into T1
                                                    from _Transaction in T1.DefaultIfEmpty()
                                                    where
                                                    objTransactionEntry.IsActive == true && objTransactionEntry.TransactionHeadID == ID
                                                    select new TransactionEntryViewModelList
                                                    {
                                                        TransactionHeadID = objTransactionEntry.TransactionHeadID,
                                                        TransactionEntryDate = objTransactionEntry.TransactionEntryDate,
                                                        Debit = objTransactionEntry.Debit,
                                                        Credit = objTransactionEntry.Credit,
                                                        AccountID = objTransactionEntry.AccountID,
                                                        AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objTransactionEntry.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                        TransactionShortRefNo = objTransactionEntry.TransactionShortRefNo,
                                                        Description = objTransactionEntry.Description,
                                                        LegalEntityID = objTransactionEntry.LegalEntityID,
                                                        LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objTransactionEntry.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                        TransactionInputTypeID = objTransactionEntry.TransactionInputTypeID,
                                                        TransactionInputTypeName = _objAccountingDbContext.AccountingTransactionInputTypeModel.Where(x => x.TransactionInputTypeID == objTransactionEntry.TransactionInputTypeID && x.IsActive == true).FirstOrDefault().TransactionInputTypeName,
                                                        DescriptionHead = objTransactionHead.DescriptionHead

                                                    }).OrderBy(x => x.TransactionShortRefNo).ToList();
                // Do this if you don't care about selection
                //            List<SelectListItem> selectList = states.Select(state => new SelectListItem {Value = state.Id.ToString(), Text = state.Name}).ToList();

                // var objtransactionReferenceNo=objTransactionEntryViewModelList.Where(x => x.TransactionShortRefNo.Equals(TransactionRefNo)).ToList();
                foreach (TransactionEntryViewModelList objListdef in objTransactionEntryViewModelList)
                {

                    string strFinalContents = "";
                    string Transactionref_no = objListdef.TransactionShortRefNo;
                    decimal Debit = objListdef.Debit;
                    decimal Credit = objListdef.Credit;
                    int AccountID = objListdef.AccountID;
                    string Description = objListdef.Description;
                    string date = objListdef.TransactionEntryDate.ToShortDateString();
                    string LegalEntity = objListdef.LegalEntityName;
                    string AccountName = objListdef.AccountName;
                    string categoryName = objListdef.TransactionInputTypeName;
                    string DescriptionHead = objListdef.DescriptionHead;
                    strFinalContents = Common.Common.ConvertNumbertoWords((long)Convert.ToDecimal(objListdef.Debit));
                }
                //return objTransactionEntryViewModelList;

            }
            if (TransactionInput == 10 || TransactionInput !=10 && objTransactionHead.IsActive == false)
            {

                objTransactionEntryViewModelList = (from objTransactionEntry in _objAccountingDbContext.AccountingTransactionEntryModel
                                                    join _TH in _objAccountingDbContext.AccountingTransactionHeadModel on objTransactionEntry.TransactionHeadID equals objTransactionHead.TransactionHeadID into T1
                                                    from _Transaction in T1.DefaultIfEmpty()
                                                    where objTransactionEntry.TransactionHeadID == ID
                                                    select new TransactionEntryViewModelList
                                                    {
                                                        TransactionHeadID = objTransactionEntry.TransactionHeadID,
                                                        TransactionEntryDate = objTransactionEntry.TransactionEntryDate,
                                                        Debit = objTransactionEntry.Debit,
                                                        Credit = objTransactionEntry.Credit,
                                                        AccountID = objTransactionEntry.AccountID,
                                                        AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objTransactionEntry.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                        TransactionShortRefNo = objTransactionEntry.TransactionShortRefNo,
                                                        Description = objTransactionEntry.Description,
                                                        LegalEntityID = objTransactionEntry.LegalEntityID,
                                                        LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objTransactionEntry.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                        TransactionInputTypeID = objTransactionEntry.TransactionInputTypeID,
                                                        TransactionInputTypeName = _objAccountingDbContext.AccountingTransactionInputTypeModel.Where(x => x.TransactionInputTypeID == objTransactionEntry.TransactionInputTypeID && x.IsActive == true).FirstOrDefault().TransactionInputTypeName,
                                                        DescriptionHead = objTransactionHead.DescriptionHead

                                                    }).OrderBy(x => x.TransactionShortRefNo).ToList();
                // Do this if you don't care about selection
                //            List<SelectListItem> selectList = states.Select(state => new SelectListItem {Value = state.Id.ToString(), Text = state.Name}).ToList();

                // var objtransactionReferenceNo=objTransactionEntryViewModelList.Where(x => x.TransactionShortRefNo.Equals(TransactionRefNo)).ToList();
                foreach (TransactionEntryViewModelList objListdef in objTransactionEntryViewModelList)
                {

                    string strFinalContents = "";
                    string Transactionref_no = objListdef.TransactionShortRefNo;
                    decimal Debit = objListdef.Debit;
                    decimal Credit = objListdef.Credit;
                    int AccountID = objListdef.AccountID;
                    string Description = objListdef.Description;
                    string date = objListdef.TransactionEntryDate.ToShortDateString();
                    string LegalEntity = objListdef.LegalEntityName;
                    string AccountName = objListdef.AccountName;
                    string categoryName = objListdef.TransactionInputTypeName;
                    string DescriptionHead = objListdef.DescriptionHead;
                    strFinalContents = Common.Common.ConvertNumbertoWords((long)Convert.ToDecimal(objListdef.Debit));
                }
                //return objTransactionEntryViewModelList;
            }

            return objTransactionEntryViewModelList;

        }
        public List<TransactionEntryViewModelList> GetReferenceNo(string TransactionShortRefNo)
        {


            var objTransactionEntryViewModelList = (from objTransactionEntry in _objAccountingDbContext.AccountingTransactionEntryModel
                                                    where objTransactionEntry.IsActive == true
                                                    select new TransactionEntryViewModelList
                                                    {
                                                        TransactionShortRefNo = objTransactionEntry.TransactionShortRefNo
                                                    }).ToList();

            return objTransactionEntryViewModelList;

        }
        public List<TransactionEntryViewModelList> GetAllTransactionEntryViewModelListByTransactionHeadID(int TransactionHeadID)
        {
            var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                    where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.TransactionHeadID == TransactionHeadID
                                                    select new TransactionEntryViewModelList
                                                    {
                                                        TransactionEntryID = objAccountingTransactionEntryModel.TransactionEntryID,
                                                        TransactionEntryDate = objAccountingTransactionEntryModel.TransactionEntryDate,
                                                        TransactionHeadID = objAccountingTransactionEntryModel.TransactionHeadID,
                                                        TransactionRefNo = objAccountingTransactionEntryModel.TransactionRefNo,
                                                        TransactionShortRefNo = objAccountingTransactionEntryModel.TransactionShortRefNo,
                                                        FinancialYearID = objAccountingTransactionEntryModel.FinancialYearID,
                                                        TransactionInputTypeID = objAccountingTransactionEntryModel.TransactionInputTypeID,
                                                        CorporateGroupID = objAccountingTransactionEntryModel.CorporateGroupID,
                                                        BusinessGroupID = objAccountingTransactionEntryModel.BusinessGroupID,
                                                        VerticalGroupID = objAccountingTransactionEntryModel.VerticalGroupID,
                                                        LegalEntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                        LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                        ProfitCentreID = objAccountingTransactionEntryModel.ProfitCentreID,
                                                        ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingTransactionEntryModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                        LocationID = objAccountingTransactionEntryModel.LocationID,
                                                        LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingTransactionEntryModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                        UnitID = objAccountingTransactionEntryModel.UnitID,
                                                        UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingTransactionEntryModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                        ProjectID = objAccountingTransactionEntryModel.ProjectID,
                                                        ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingTransactionEntryModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                        RowNo = objAccountingTransactionEntryModel.RowNo,
                                                        RowType = objAccountingTransactionEntryModel.RowType,
                                                        CashorBankAccountNo = objAccountingTransactionEntryModel.CashorBankAccountNo,
                                                        OpenDebit = objAccountingTransactionEntryModel.OpenDebit,
                                                        OpenCredit = objAccountingTransactionEntryModel.OpenCredit,
                                                        Debit = objAccountingTransactionEntryModel.Debit,
                                                        Credit = objAccountingTransactionEntryModel.Credit,
                                                        CloseDebit = objAccountingTransactionEntryModel.CloseDebit,
                                                        CloseCredit = objAccountingTransactionEntryModel.CloseCredit,
                                                        Description = objAccountingTransactionEntryModel.Description,
                                                        AccountID = objAccountingTransactionEntryModel.AccountID,
                                                        AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objAccountingTransactionEntryModel.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                        AccountTypeID = objAccountingTransactionEntryModel.AccountTypeID,
                                                        AccountTypeName = _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.AccountTypeID == objAccountingTransactionEntryModel.AccountTypeID && x.IsActive == true).FirstOrDefault().AccountTypeName,
                                                        CategoryID = objAccountingTransactionEntryModel.CategoryID,
                                                        CategoryName = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == objAccountingTransactionEntryModel.CategoryID && x.IsActive == true).FirstOrDefault().CategoryName,
                                                        ParentCategoryID = objAccountingTransactionEntryModel.ParentCategoryID,
                                                        ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objAccountingTransactionEntryModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                        MajorGroupID = objAccountingTransactionEntryModel.MajorGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingTransactionEntryModel.GroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        SubGroupID = objAccountingTransactionEntryModel.SubGroupID,
                                                        SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                    }).OrderBy(x => x.RowNo).ThenBy(x => x.LocationName).ToList();
            return objTransactionEntryViewModelList;

        }
        public List<TransactionEntryViewModelList> GetAllTransactionEntryViewModelListByLegalEntityIDAccountIDStartDateEndDate(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate)
        {

            var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                    where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.LegalEntityID == LegalEntityID && objAccountingTransactionEntryModel.AccountID == AccountID && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) <= EndDate
                                                    select new TransactionEntryViewModelList
                                                    {
                                                        TransactionEntryID = objAccountingTransactionEntryModel.TransactionEntryID,
                                                        TransactionEntryDate = objAccountingTransactionEntryModel.TransactionEntryDate,
                                                        TransactionHeadID = objAccountingTransactionEntryModel.TransactionHeadID,
                                                        TransactionRefNo = objAccountingTransactionEntryModel.TransactionRefNo,
                                                        TransactionShortRefNo = objAccountingTransactionEntryModel.TransactionShortRefNo,
                                                        FinancialYearID = objAccountingTransactionEntryModel.FinancialYearID,
                                                        TransactionInputTypeID = objAccountingTransactionEntryModel.TransactionInputTypeID,
                                                        CorporateGroupID = objAccountingTransactionEntryModel.CorporateGroupID,
                                                        BusinessGroupID = objAccountingTransactionEntryModel.BusinessGroupID,
                                                        VerticalGroupID = objAccountingTransactionEntryModel.VerticalGroupID,
                                                        LegalEntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                        LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                        ProfitCentreID = objAccountingTransactionEntryModel.ProfitCentreID,
                                                        ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingTransactionEntryModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                        LocationID = objAccountingTransactionEntryModel.LocationID,
                                                        LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingTransactionEntryModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                        UnitID = objAccountingTransactionEntryModel.UnitID,
                                                        UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingTransactionEntryModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                        ProjectID = objAccountingTransactionEntryModel.ProjectID,
                                                        ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingTransactionEntryModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                        RowNo = objAccountingTransactionEntryModel.RowNo,
                                                        RowType = objAccountingTransactionEntryModel.RowType,
                                                        CashorBankAccountNo = objAccountingTransactionEntryModel.CashorBankAccountNo,
                                                        OpenDebit = objAccountingTransactionEntryModel.OpenDebit,
                                                        OpenCredit = objAccountingTransactionEntryModel.OpenCredit,
                                                        Debit = objAccountingTransactionEntryModel.Debit,
                                                        Credit = objAccountingTransactionEntryModel.Credit,
                                                        CloseDebit = objAccountingTransactionEntryModel.CloseDebit,
                                                        CloseCredit = objAccountingTransactionEntryModel.CloseCredit,
                                                        Description = objAccountingTransactionEntryModel.Description,
                                                        AccountID = objAccountingTransactionEntryModel.AccountID,
                                                        AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objAccountingTransactionEntryModel.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                        AccountTypeID = objAccountingTransactionEntryModel.AccountTypeID,
                                                        AccountTypeName = _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.AccountTypeID == objAccountingTransactionEntryModel.AccountTypeID && x.IsActive == true).FirstOrDefault().AccountTypeName,
                                                        CategoryID = objAccountingTransactionEntryModel.CategoryID,
                                                        CategoryName = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == objAccountingTransactionEntryModel.CategoryID && x.IsActive == true).FirstOrDefault().CategoryName,
                                                        ParentCategoryID = objAccountingTransactionEntryModel.ParentCategoryID,
                                                        ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objAccountingTransactionEntryModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                        MajorGroupID = objAccountingTransactionEntryModel.MajorGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingTransactionEntryModel.GroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        SubGroupID = objAccountingTransactionEntryModel.SubGroupID,
                                                        SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                    }).OrderBy(x => x.TransactionEntryDate).ThenBy(x => x.TransactionHeadID).ThenBy(x => x.RowNo).ThenBy(x => x.LocationName).ToList();
            return objTransactionEntryViewModelList;

        }
        public List<TransactionEntryViewModelList> GetAllTransactionEntryViewModelListByLegalEntityIDAccountLevelStartDateEndDate(int LegalEntityID, int AccountLevel, DateTime StartDate, DateTime EndDate)
        {
            if (AccountLevel == 1)
            {
                var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                        where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.LegalEntityID == LegalEntityID && objAccountingTransactionEntryModel.MajorGroupID != 0 && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) <= EndDate
                                                        select new TransactionEntryViewModelList
                                                        {
                                                            TransactionEntryID = objAccountingTransactionEntryModel.TransactionEntryID,
                                                            TransactionEntryDate = objAccountingTransactionEntryModel.TransactionEntryDate,
                                                            TransactionHeadID = objAccountingTransactionEntryModel.TransactionHeadID,
                                                            TransactionRefNo = objAccountingTransactionEntryModel.TransactionRefNo,
                                                            TransactionShortRefNo = objAccountingTransactionEntryModel.TransactionShortRefNo,
                                                            FinancialYearID = objAccountingTransactionEntryModel.FinancialYearID,
                                                            TransactionInputTypeID = objAccountingTransactionEntryModel.TransactionInputTypeID,
                                                            CorporateGroupID = objAccountingTransactionEntryModel.CorporateGroupID,
                                                            BusinessGroupID = objAccountingTransactionEntryModel.BusinessGroupID,
                                                            VerticalGroupID = objAccountingTransactionEntryModel.VerticalGroupID,
                                                            LegalEntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                            LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                            ProfitCentreID = objAccountingTransactionEntryModel.ProfitCentreID,
                                                            ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingTransactionEntryModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                            LocationID = objAccountingTransactionEntryModel.LocationID,
                                                            LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingTransactionEntryModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                            UnitID = objAccountingTransactionEntryModel.UnitID,
                                                            UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingTransactionEntryModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                            ProjectID = objAccountingTransactionEntryModel.ProjectID,
                                                            ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingTransactionEntryModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                            RowNo = objAccountingTransactionEntryModel.RowNo,
                                                            RowType = objAccountingTransactionEntryModel.RowType,
                                                            CashorBankAccountNo = objAccountingTransactionEntryModel.CashorBankAccountNo,
                                                            OpenDebit = objAccountingTransactionEntryModel.OpenDebit,
                                                            OpenCredit = objAccountingTransactionEntryModel.OpenCredit,
                                                            Debit = objAccountingTransactionEntryModel.Debit,
                                                            Credit = objAccountingTransactionEntryModel.Credit,
                                                            CloseDebit = objAccountingTransactionEntryModel.CloseDebit,
                                                            CloseCredit = objAccountingTransactionEntryModel.CloseCredit,
                                                            Description = objAccountingTransactionEntryModel.Description,
                                                            AccountID = objAccountingTransactionEntryModel.AccountID,
                                                            AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objAccountingTransactionEntryModel.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                            AccountTypeID = objAccountingTransactionEntryModel.AccountTypeID,
                                                            AccountTypeName = _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.AccountTypeID == objAccountingTransactionEntryModel.AccountTypeID && x.IsActive == true).FirstOrDefault().AccountTypeName,
                                                            CategoryID = objAccountingTransactionEntryModel.CategoryID,
                                                            CategoryName = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == objAccountingTransactionEntryModel.CategoryID && x.IsActive == true).FirstOrDefault().CategoryName,
                                                            ParentCategoryID = objAccountingTransactionEntryModel.ParentCategoryID,
                                                            ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objAccountingTransactionEntryModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                            MajorGroupID = objAccountingTransactionEntryModel.MajorGroupID,
                                                            MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                            GroupID = objAccountingTransactionEntryModel.GroupID,
                                                            GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                            SubGroupID = objAccountingTransactionEntryModel.SubGroupID,
                                                            SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                        }).OrderBy(x => x.TransactionEntryDate).ThenBy(x => x.TransactionHeadID).ThenBy(x => x.RowNo).ThenBy(x => x.LocationName).ToList();
                return objTransactionEntryViewModelList;
            }
            else if (AccountLevel == 2)
            {
                var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                        where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.LegalEntityID == LegalEntityID && objAccountingTransactionEntryModel.GroupID != 0 && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) <= EndDate
                                                        select new TransactionEntryViewModelList
                                                        {
                                                            TransactionEntryID = objAccountingTransactionEntryModel.TransactionEntryID,
                                                            TransactionEntryDate = objAccountingTransactionEntryModel.TransactionEntryDate,
                                                            TransactionHeadID = objAccountingTransactionEntryModel.TransactionHeadID,
                                                            TransactionRefNo = objAccountingTransactionEntryModel.TransactionRefNo,
                                                            TransactionShortRefNo = objAccountingTransactionEntryModel.TransactionShortRefNo,
                                                            FinancialYearID = objAccountingTransactionEntryModel.FinancialYearID,
                                                            TransactionInputTypeID = objAccountingTransactionEntryModel.TransactionInputTypeID,
                                                            CorporateGroupID = objAccountingTransactionEntryModel.CorporateGroupID,
                                                            BusinessGroupID = objAccountingTransactionEntryModel.BusinessGroupID,
                                                            VerticalGroupID = objAccountingTransactionEntryModel.VerticalGroupID,
                                                            LegalEntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                            LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                            ProfitCentreID = objAccountingTransactionEntryModel.ProfitCentreID,
                                                            ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingTransactionEntryModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                            LocationID = objAccountingTransactionEntryModel.LocationID,
                                                            LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingTransactionEntryModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                            UnitID = objAccountingTransactionEntryModel.UnitID,
                                                            UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingTransactionEntryModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                            ProjectID = objAccountingTransactionEntryModel.ProjectID,
                                                            ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingTransactionEntryModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                            RowNo = objAccountingTransactionEntryModel.RowNo,
                                                            RowType = objAccountingTransactionEntryModel.RowType,
                                                            CashorBankAccountNo = objAccountingTransactionEntryModel.CashorBankAccountNo,
                                                            OpenDebit = objAccountingTransactionEntryModel.OpenDebit,
                                                            OpenCredit = objAccountingTransactionEntryModel.OpenCredit,
                                                            Debit = objAccountingTransactionEntryModel.Debit,
                                                            Credit = objAccountingTransactionEntryModel.Credit,
                                                            CloseDebit = objAccountingTransactionEntryModel.CloseDebit,
                                                            CloseCredit = objAccountingTransactionEntryModel.CloseCredit,
                                                            Description = objAccountingTransactionEntryModel.Description,
                                                            AccountID = objAccountingTransactionEntryModel.AccountID,
                                                            AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objAccountingTransactionEntryModel.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                            AccountTypeID = objAccountingTransactionEntryModel.AccountTypeID,
                                                            AccountTypeName = _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.AccountTypeID == objAccountingTransactionEntryModel.AccountTypeID && x.IsActive == true).FirstOrDefault().AccountTypeName,
                                                            CategoryID = objAccountingTransactionEntryModel.CategoryID,
                                                            CategoryName = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == objAccountingTransactionEntryModel.CategoryID && x.IsActive == true).FirstOrDefault().CategoryName,
                                                            ParentCategoryID = objAccountingTransactionEntryModel.ParentCategoryID,
                                                            ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objAccountingTransactionEntryModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                            MajorGroupID = objAccountingTransactionEntryModel.MajorGroupID,
                                                            MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                            GroupID = objAccountingTransactionEntryModel.GroupID,
                                                            GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                            SubGroupID = objAccountingTransactionEntryModel.SubGroupID,
                                                            SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                        }).OrderBy(x => x.TransactionEntryDate).ThenBy(x => x.TransactionHeadID).ThenBy(x => x.RowNo).ThenBy(x => x.LocationName).ToList();
                return objTransactionEntryViewModelList;
            }
            else if (AccountLevel == 3)
            {
                var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                        where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.LegalEntityID == LegalEntityID && objAccountingTransactionEntryModel.SubGroupID != 0 && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) <= EndDate
                                                        select new TransactionEntryViewModelList
                                                        {
                                                            TransactionEntryID = objAccountingTransactionEntryModel.TransactionEntryID,
                                                            TransactionEntryDate = objAccountingTransactionEntryModel.TransactionEntryDate,
                                                            TransactionHeadID = objAccountingTransactionEntryModel.TransactionHeadID,
                                                            TransactionRefNo = objAccountingTransactionEntryModel.TransactionRefNo,
                                                            TransactionShortRefNo = objAccountingTransactionEntryModel.TransactionShortRefNo,
                                                            FinancialYearID = objAccountingTransactionEntryModel.FinancialYearID,
                                                            TransactionInputTypeID = objAccountingTransactionEntryModel.TransactionInputTypeID,
                                                            CorporateGroupID = objAccountingTransactionEntryModel.CorporateGroupID,
                                                            BusinessGroupID = objAccountingTransactionEntryModel.BusinessGroupID,
                                                            VerticalGroupID = objAccountingTransactionEntryModel.VerticalGroupID,
                                                            LegalEntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                            LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                            ProfitCentreID = objAccountingTransactionEntryModel.ProfitCentreID,
                                                            ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingTransactionEntryModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                            LocationID = objAccountingTransactionEntryModel.LocationID,
                                                            LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingTransactionEntryModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                            UnitID = objAccountingTransactionEntryModel.UnitID,
                                                            UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingTransactionEntryModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                            ProjectID = objAccountingTransactionEntryModel.ProjectID,
                                                            ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingTransactionEntryModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                            RowNo = objAccountingTransactionEntryModel.RowNo,
                                                            RowType = objAccountingTransactionEntryModel.RowType,
                                                            CashorBankAccountNo = objAccountingTransactionEntryModel.CashorBankAccountNo,
                                                            OpenDebit = objAccountingTransactionEntryModel.OpenDebit,
                                                            OpenCredit = objAccountingTransactionEntryModel.OpenCredit,
                                                            Debit = objAccountingTransactionEntryModel.Debit,
                                                            Credit = objAccountingTransactionEntryModel.Credit,
                                                            CloseDebit = objAccountingTransactionEntryModel.CloseDebit,
                                                            CloseCredit = objAccountingTransactionEntryModel.CloseCredit,
                                                            Description = objAccountingTransactionEntryModel.Description,
                                                            AccountID = objAccountingTransactionEntryModel.AccountID,
                                                            AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objAccountingTransactionEntryModel.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                            AccountTypeID = objAccountingTransactionEntryModel.AccountTypeID,
                                                            AccountTypeName = _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.AccountTypeID == objAccountingTransactionEntryModel.AccountTypeID && x.IsActive == true).FirstOrDefault().AccountTypeName,
                                                            CategoryID = objAccountingTransactionEntryModel.CategoryID,
                                                            CategoryName = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == objAccountingTransactionEntryModel.CategoryID && x.IsActive == true).FirstOrDefault().CategoryName,
                                                            ParentCategoryID = objAccountingTransactionEntryModel.ParentCategoryID,
                                                            ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objAccountingTransactionEntryModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                            MajorGroupID = objAccountingTransactionEntryModel.MajorGroupID,
                                                            MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                            GroupID = objAccountingTransactionEntryModel.GroupID,
                                                            GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                            SubGroupID = objAccountingTransactionEntryModel.SubGroupID,
                                                            SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                        }).OrderBy(x => x.TransactionEntryDate).ThenBy(x => x.TransactionHeadID).ThenBy(x => x.RowNo).ThenBy(x => x.LocationName).ToList();
                return objTransactionEntryViewModelList;
            }
            else
            {
                var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                        where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.LegalEntityID == LegalEntityID && objAccountingTransactionEntryModel.AccountID != 0 && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) <= EndDate
                                                        select new TransactionEntryViewModelList
                                                        {
                                                            TransactionEntryID = objAccountingTransactionEntryModel.TransactionEntryID,
                                                            TransactionEntryDate = objAccountingTransactionEntryModel.TransactionEntryDate,
                                                            TransactionHeadID = objAccountingTransactionEntryModel.TransactionHeadID,
                                                            TransactionRefNo = objAccountingTransactionEntryModel.TransactionRefNo,
                                                            TransactionShortRefNo = objAccountingTransactionEntryModel.TransactionShortRefNo,
                                                            FinancialYearID = objAccountingTransactionEntryModel.FinancialYearID,
                                                            TransactionInputTypeID = objAccountingTransactionEntryModel.TransactionInputTypeID,
                                                            CorporateGroupID = objAccountingTransactionEntryModel.CorporateGroupID,
                                                            BusinessGroupID = objAccountingTransactionEntryModel.BusinessGroupID,
                                                            VerticalGroupID = objAccountingTransactionEntryModel.VerticalGroupID,
                                                            LegalEntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                            LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                            ProfitCentreID = objAccountingTransactionEntryModel.ProfitCentreID,
                                                            ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingTransactionEntryModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                            LocationID = objAccountingTransactionEntryModel.LocationID,
                                                            LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingTransactionEntryModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                            UnitID = objAccountingTransactionEntryModel.UnitID,
                                                            UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingTransactionEntryModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                            ProjectID = objAccountingTransactionEntryModel.ProjectID,
                                                            ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingTransactionEntryModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                            RowNo = objAccountingTransactionEntryModel.RowNo,
                                                            RowType = objAccountingTransactionEntryModel.RowType,
                                                            CashorBankAccountNo = objAccountingTransactionEntryModel.CashorBankAccountNo,
                                                            OpenDebit = objAccountingTransactionEntryModel.OpenDebit,
                                                            OpenCredit = objAccountingTransactionEntryModel.OpenCredit,
                                                            Debit = objAccountingTransactionEntryModel.Debit,
                                                            Credit = objAccountingTransactionEntryModel.Credit,
                                                            CloseDebit = objAccountingTransactionEntryModel.CloseDebit,
                                                            CloseCredit = objAccountingTransactionEntryModel.CloseCredit,
                                                            Description = objAccountingTransactionEntryModel.Description,
                                                            AccountID = objAccountingTransactionEntryModel.AccountID,
                                                            AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objAccountingTransactionEntryModel.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                            AccountTypeID = objAccountingTransactionEntryModel.AccountTypeID,
                                                            AccountTypeName = _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.AccountTypeID == objAccountingTransactionEntryModel.AccountTypeID && x.IsActive == true).FirstOrDefault().AccountTypeName,
                                                            CategoryID = objAccountingTransactionEntryModel.CategoryID,
                                                            CategoryName = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == objAccountingTransactionEntryModel.CategoryID && x.IsActive == true).FirstOrDefault().CategoryName,
                                                            ParentCategoryID = objAccountingTransactionEntryModel.ParentCategoryID,
                                                            ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objAccountingTransactionEntryModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                            MajorGroupID = objAccountingTransactionEntryModel.MajorGroupID,
                                                            MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                            GroupID = objAccountingTransactionEntryModel.GroupID,
                                                            GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                            SubGroupID = objAccountingTransactionEntryModel.SubGroupID,
                                                            SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                        }).OrderBy(x => x.TransactionEntryDate).ThenBy(x => x.TransactionHeadID).ThenBy(x => x.RowNo).ThenBy(x => x.LocationName).ToList();
                return objTransactionEntryViewModelList;
            }
        }

        public List<TransactionEntryViewModelList> GetAllTransactionEntryViewModelListRegisterByLegalEntityIDAccountIDStartDateEndDate(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate)
        {

            var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                    where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.LegalEntityID == LegalEntityID && objAccountingTransactionEntryModel.AccountID == AccountID && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) >= StartDate && Convert.ToDateTime((objAccountingTransactionEntryModel.TransactionEntryDate).ToShortDateString()) <= EndDate
                                                    select new TransactionEntryViewModelList
                                                    {
                                                        TransactionEntryID = objAccountingTransactionEntryModel.TransactionEntryID,
                                                        TransactionEntryDate = objAccountingTransactionEntryModel.TransactionEntryDate,
                                                        TransactionHeadID = objAccountingTransactionEntryModel.TransactionHeadID,
                                                        TransactionRefNo = objAccountingTransactionEntryModel.TransactionRefNo,
                                                        TransactionShortRefNo = objAccountingTransactionEntryModel.TransactionShortRefNo,
                                                        FinancialYearID = objAccountingTransactionEntryModel.FinancialYearID,
                                                        TransactionInputTypeID = objAccountingTransactionEntryModel.TransactionInputTypeID,
                                                        CorporateGroupID = objAccountingTransactionEntryModel.CorporateGroupID,
                                                        BusinessGroupID = objAccountingTransactionEntryModel.BusinessGroupID,
                                                        VerticalGroupID = objAccountingTransactionEntryModel.VerticalGroupID,
                                                        LegalEntityID = objAccountingTransactionEntryModel.LegalEntityID,
                                                        LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingTransactionEntryModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                        ProfitCentreID = objAccountingTransactionEntryModel.ProfitCentreID,
                                                        ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingTransactionEntryModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                        LocationID = objAccountingTransactionEntryModel.LocationID,
                                                        LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingTransactionEntryModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                        UnitID = objAccountingTransactionEntryModel.UnitID,
                                                        UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingTransactionEntryModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                        ProjectID = objAccountingTransactionEntryModel.ProjectID,
                                                        ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objAccountingTransactionEntryModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                        RowNo = objAccountingTransactionEntryModel.RowNo,
                                                        RowType = objAccountingTransactionEntryModel.RowType,
                                                        CashorBankAccountNo = objAccountingTransactionEntryModel.CashorBankAccountNo,
                                                        OpenDebit = objAccountingTransactionEntryModel.OpenDebit,
                                                        OpenCredit = objAccountingTransactionEntryModel.OpenCredit,
                                                        Debit = objAccountingTransactionEntryModel.Debit,
                                                        Credit = objAccountingTransactionEntryModel.Credit,
                                                        CloseDebit = objAccountingTransactionEntryModel.CloseDebit,
                                                        CloseCredit = objAccountingTransactionEntryModel.CloseCredit,
                                                        Description = objAccountingTransactionEntryModel.Description,
                                                        AccountID = objAccountingTransactionEntryModel.AccountID,
                                                        AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objAccountingTransactionEntryModel.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                        AccountTypeID = objAccountingTransactionEntryModel.AccountTypeID,
                                                        AccountTypeName = _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.AccountTypeID == objAccountingTransactionEntryModel.AccountTypeID && x.IsActive == true).FirstOrDefault().AccountTypeName,
                                                        CategoryID = objAccountingTransactionEntryModel.CategoryID,
                                                        CategoryName = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == objAccountingTransactionEntryModel.CategoryID && x.IsActive == true).FirstOrDefault().CategoryName,
                                                        ParentCategoryID = objAccountingTransactionEntryModel.ParentCategoryID,
                                                        ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objAccountingTransactionEntryModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                        MajorGroupID = objAccountingTransactionEntryModel.MajorGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingTransactionEntryModel.GroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        SubGroupID = objAccountingTransactionEntryModel.SubGroupID,
                                                        SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingTransactionEntryModel.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                    }).OrderBy(x => x.TransactionEntryDate).ThenBy(x => x.TransactionHeadID).ToList();
            return objTransactionEntryViewModelList;

        }
        public List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityID(int LegalEntityID, DateTime StartDate, DateTime EndDate)
        {
            var objTransactionEntryViewModelList = (from objAccountingChartofAccounts in _objAccountingDbContext.AccountingChartofAccountsModel
                                                    where objAccountingChartofAccounts.IsActive == true && objAccountingChartofAccounts.LegalEntityID == LegalEntityID
                                                    select new TransactionEntryTrialList
                                                    {
                                                        AccountID = objAccountingChartofAccounts.AccountID,
                                                        AccountName = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == objAccountingChartofAccounts.AccountID && x.IsActive == true).FirstOrDefault().AccountName,
                                                        MajorGroupID = objAccountingChartofAccounts.MajorGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingChartofAccounts.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingChartofAccounts.GroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingChartofAccounts.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        SubGroupID = objAccountingChartofAccounts.SubGroupID,
                                                        SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingChartofAccounts.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryTrialList objListdef in objTransactionEntryViewModelList)
            {
                //if (openingbalance == 0 && closingbalance == 0 && debit == 0 && credit == 0)
                //{
                //    objListdef.AccountID  =0;
                //    objListdef.AccountName = null;
                //    objListdef.MajorGroupName = null;
                //    objListdef.GroupName = null;
                //    objListdef.SubGroupName = null;
                //    objListdef.OpenDebit = 0 ;
                //    objListdef.OpenCredit = 0;
                //    objListdef.CloseCredit = 0;
                //    objListdef.CloseDebit = 0;
                //    objListdef.Debit = 0;
                //    objListdef.Credit = 0;
                //}
                //else
                //{ 
                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = GetClosingBalanceChartofAccountsViewModelByStartDate(LegalEntityID, objListdef.AccountID, Convert.ToDateTime(StartDate.ToShortDateString()));
                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetSumChartofAccountsViewModelListAll(LegalEntityID, objListdef.AccountID, Convert.ToDateTime(StartDate.ToShortDateString()), Convert.ToDateTime(EndDate.ToShortDateString()));
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                decimal openingbalance = objSumChartofAccountsViewModelListOpen.Debit - objSumChartofAccountsViewModelListOpen.Credit;
                decimal closingbalance = objSumChartofAccountsViewModelListClose.Debit - objSumChartofAccountsViewModelListClose.Credit;
                decimal debit = objSumChartofAccountsViewModelListClose.Debit - objSumChartofAccountsViewModelListOpen.Debit;
                decimal credit = objSumChartofAccountsViewModelListClose.Credit - objSumChartofAccountsViewModelListOpen.Credit;

                if (objSumChartofAccountsViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumChartofAccountsViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumChartofAccountsViewModelListOpen.Credit;
                }
                if (objSumChartofAccountsViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumChartofAccountsViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumChartofAccountsViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;

            }

            return objTransactionEntryViewModelList;

        }
        public List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityIDProfitandLoss(int LegalEntityID, int MajorGroupID, DateTime StartDate, DateTime EndDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            //string objTransaction = "select a.MajorgroupID,a.AccountID,a.AccountName,b.debit,b.credit from Accounting_ChartofAccounts as a inner join accounting_transactionentry as b on a.MajorgroupID = b.MajorgroupID where a.LegalEntityID =0 and a.MajorGroupID in(72, 74, 73, 75)";
            var objTransactionEntryViewModelList = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID)
                                                    select new TransactionEntryTrialList
                                                    {
                                                        MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryTrialList objListdef in objTransactionEntryViewModelList)
            {
                int majorgroup_arr = objListdef.MajorGroupID;
                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = GetClosingBalanceChartofAccountsViewModelByStartDate1(LegalEntityID, majorgroup_arr, objListdef.GroupID, Convert.ToDateTime(StartDate.ToShortDateString()));
                SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetSumChartofAccountsViewModelListAll1(LegalEntityID, majorgroup_arr, objListdef.GroupID, Convert.ToDateTime(StartDate.ToShortDateString()), Convert.ToDateTime(EndDate.ToShortDateString()));
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                decimal openingbalance = objSumChartofAccountsViewModelListOpen.Debit - objSumChartofAccountsViewModelListOpen.Credit;
                decimal closingbalance = objSumChartofAccountsViewModelListClose.Debit - objSumChartofAccountsViewModelListClose.Credit;
                decimal debit = objSumChartofAccountsViewModelListClose.Debit - objSumChartofAccountsViewModelListOpen.Debit;
                decimal credit = objSumChartofAccountsViewModelListClose.Credit - objSumChartofAccountsViewModelListOpen.Credit;

                if (objSumChartofAccountsViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumChartofAccountsViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumChartofAccountsViewModelListOpen.Credit;
                }
                if (objSumChartofAccountsViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumChartofAccountsViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumChartofAccountsViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;

            }
            return objTransactionEntryViewModelList;
           
        }
        private SumChartofAccountsViewModelList GetSumChartofAccountsViewModelListAll1(int LegalEntityID, int majorGroupID, int GroupID, DateTime StartDate, DateTime EndDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
 x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(majorGroupID) && x.GroupID == GroupID && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.LegalEntityID == LegalEntityID && x.MajorGroupID == majorGroupID && x.GroupID == GroupID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            return objSumChartofAccountsViewModelList;

        }
        private SumChartofAccountsViewModelList GetSumMonthlyprofitandlossAll1(int LegalEntityID, int majorGroupID, int AccountID, string StartDate, string EndDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(majorGroupID) && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.MajorGroupID == majorGroupID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= Convert.ToDateTime(EndDate));
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;
            return objSumChartofAccountsViewModelList;

        }
        private SumChartofAccountsViewModelList GetSum_MonthlyprofitandlossAll1(int LegalEntityID, int majorGroupID, int AccountID, DateTime StartDate, DateTime EndDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(majorGroupID) && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.MajorGroupID == majorGroupID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;
            return objSumChartofAccountsViewModelList;

        }

        public List<TransactionEntryViewModelList> GetAllTransactionEntryTrialListMonthlyProfitandLoss(int LegalEntityID, int MajorGroupId, int GroupID, string StartDate, string EndDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };
            DateTime reference = DateTime.Now;
            Calendar calendar = CultureInfo.CurrentCulture.Calendar;

            //IEnumerable<int> daysInMonth = Enumerable.Range(1, calendar.GetDaysInMonth(reference.yea, reference.Month));

            var objTransactionEntryViewModelList = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID)
                                                    select new TransactionEntryViewModelList
                                                    {
                                                        MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryViewModelList objListdef in objTransactionEntryViewModelList)
            {
                if (objListdef.AccountID == 0)
                {
                    int majorgroup_arr = objListdef.MajorGroupID;

                    GroupViewModelList objSumGroupViewModelListOpen = GetDebitcreditStartdate_profitandloss(LegalEntityID, majorgroup_arr, objListdef.GroupID, StartDate);
                    GroupViewModelList objSumGroupViewModelListClose = GetGroupViewModelListAll_profitandloss(LegalEntityID, majorgroup_arr, objListdef.GroupID, StartDate, EndDate);
                    decimal OpeningDebitBalanceAmount = 0;
                    decimal OpeningCreditBalanceAmount = 0;
                    decimal ClosingDebitBalanceAmount = 0;
                    decimal ClosingCreditBalanceAmount = 0;
                    if (objSumGroupViewModelListOpen != null)
                    {
                        OpeningDebitBalanceAmount = objSumGroupViewModelListOpen.Debit;
                        OpeningCreditBalanceAmount = objSumGroupViewModelListOpen.Credit;
                    }
                    if (objSumGroupViewModelListClose != null)
                    {
                        ClosingDebitBalanceAmount = objSumGroupViewModelListClose.Debit;
                        ClosingCreditBalanceAmount = objSumGroupViewModelListClose.Credit;
                    }
                    string MajorGroupName = objListdef.MajorGroupName;
                    objListdef.OpenDebit = OpeningDebitBalanceAmount;
                    objListdef.OpenCredit = OpeningCreditBalanceAmount;
                    objListdef.CloseDebit = ClosingDebitBalanceAmount;
                    objListdef.CloseCredit = ClosingCreditBalanceAmount;
                    objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                    objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;
                    decimal Debit = objListdef.Debit;
                    decimal Credit = objListdef.Credit;
                }

            }
            return objTransactionEntryViewModelList;

        }

        private GroupViewModelList GetGroupViewModelListAll_profitandloss(int legalEntityID, int MajorgroupID, int groupID, string startDate, string endDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
                x.GroupID == groupID && x.LegalEntityID == legalEntityID && Majorgroupid.Contains(MajorgroupID) && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }

            GroupViewModelList objGroupViewModelList = new GroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            //x.FinancialYearID == FinancialYearID && 
            x.GroupID == groupID && x.LegalEntityID == legalEntityID && x.MajorGroupID == MajorgroupID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= Convert.ToDateTime(endDate));
            objGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objGroupViewModelList.GroupID = groupID;
            return objGroupViewModelList;
        }

        private GroupViewModelList GetDebitcreditStartdate_profitandloss(int LegalEntityID, int MajorGroupID, int GroupID, string StartDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
           x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }
            GroupViewModelList objGroupViewModelList = new GroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < Convert.ToDateTime(StartDate));
            objGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objGroupViewModelList.GroupID = GroupID;
            return objGroupViewModelList;
        }

        public List<TransactionEntryTrialList> GetBalanceSheetAmount(int LegalEntityID, int MajorGroupID, DateTime StartDate, DateTime EndDate)
        {
            int[] Majorgroupid = { 63, 64, 65, 66, 67, 68, 69, 70, 72, 74, 73, 75 };

            //string objTransaction = "select a.MajorgroupID,a.AccountID,a.AccountName,b.debit,b.credit from Accounting_ChartofAccounts as a inner join accounting_transactionentry as b on a.MajorgroupID = b.MajorgroupID where a.LegalEntityID =0 and a.MajorGroupID in(72, 74, 73, 75)";


            var objTransactionEntryViewModelList = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID)
                                                    select new TransactionEntryTrialList
                                                    {
                                                        MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();
            foreach (TransactionEntryTrialList objListdef in objTransactionEntryViewModelList)
            {
                int majorgroup_arr = objListdef.MajorGroupID;
                //SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = GetSumOpeningBalanceSheet(LegalEntityID, majorgroup_arr, objListdef.GroupID,StartDate);
                //SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetSumClosingBalancesheet(LegalEntityID, majorgroup_arr, objListdef.GroupID, StartDate, EndDate);
                GroupViewModelList objSumGroupViewModelListOpen = GetClosingGroupViewModelByStartDate(LegalEntityID, objListdef.GroupID, Convert.ToDateTime(StartDate.ToShortDateString()));
                GroupViewModelList objSumGroupViewModelListClose = GetGroupViewModelListAll(LegalEntityID, objListdef.GroupID, StartDate, EndDate);

                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                decimal openingbalance = objSumGroupViewModelListOpen.Debit - objSumGroupViewModelListOpen.Credit;
                decimal closingbalance = objSumGroupViewModelListClose.Debit - objSumGroupViewModelListClose.Credit;
                decimal debit = objSumGroupViewModelListClose.Debit - objSumGroupViewModelListOpen.Debit;
                decimal credit = objSumGroupViewModelListClose.Credit - objSumGroupViewModelListOpen.Credit;

                if (objSumGroupViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumGroupViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumGroupViewModelListOpen.Credit;
                }
                if (objSumGroupViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumGroupViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumGroupViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;

            }
            return objTransactionEntryViewModelList;
        }
        public List<TransactionEntryTrialList> GetGroupComparisonReportRangeforProfitandLoss(int LegalEntityID, string StartDate, string EndDate)
        {
            int[] Majorgroupid = { 72, 74, 73, 75 };

            //string objTransaction = "select a.MajorgroupID,a.AccountID,a.AccountName,b.debit,b.credit from Accounting_ChartofAccounts as a inner join accounting_transactionentry as b on a.MajorgroupID = b.MajorgroupID where a.LegalEntityID =0 and a.MajorGroupID in(72, 74, 73, 75)";

            decimal AmountDirectIncome = 0;
            decimal AmountDirectExpenses = 0;
            decimal AmountInDirectIncome = 0;
            decimal AmountInDirectExpenses = 0;
            decimal TotalProfitandLoss = 0;
            var objTransactionEntryViewModelList = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID)
                                                    select new TransactionEntryTrialList
                                                    {
                                                        MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();
            foreach (TransactionEntryTrialList objListdef in objTransactionEntryViewModelList)
            {
                int majorgroup_arr = objListdef.MajorGroupID;
                //SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = GetSumOpeningBalanceSheet(LegalEntityID, majorgroup_arr, objListdef.GroupID,StartDate);
                //SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetSumClosingBalancesheet(LegalEntityID, majorgroup_arr, objListdef.GroupID, StartDate, EndDate);
                GroupViewModelList objSumGroupViewModelListOpen = GetGroupComparisonByStartDate(LegalEntityID, majorgroup_arr, objListdef.GroupID, StartDate);
                GroupViewModelList objSumGroupViewModelListClose = GetGroupComparisonListAll(LegalEntityID, majorgroup_arr, objListdef.GroupID, StartDate, EndDate);


                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                decimal openingbalance = objSumGroupViewModelListOpen.Debit - objSumGroupViewModelListOpen.Credit;
                decimal closingbalance = objSumGroupViewModelListClose.Debit - objSumGroupViewModelListClose.Credit;
                decimal debit = objSumGroupViewModelListClose.Debit - objSumGroupViewModelListOpen.Debit;
                decimal credit = objSumGroupViewModelListClose.Credit - objSumGroupViewModelListOpen.Credit;

                if (objSumGroupViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumGroupViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumGroupViewModelListOpen.Credit;
                }
                if (objSumGroupViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumGroupViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumGroupViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;

                if (majorgroup_arr == 72)
                {
                    decimal closingbalanceforDirectincome =  objSumGroupViewModelListClose.Credit - objSumGroupViewModelListClose.Debit;
                     AmountDirectIncome = AmountDirectIncome + closingbalanceforDirectincome;
                }
                if (majorgroup_arr == 73)
                {
                    decimal closingbalanceforInDirectIncome = objSumGroupViewModelListClose.Credit - objSumGroupViewModelListClose.Debit;
                    AmountInDirectIncome = AmountInDirectIncome + closingbalanceforInDirectIncome;
                }
                if (majorgroup_arr == 74)
                {
                    decimal closingbalanceforDirectexpense = objSumGroupViewModelListClose.Debit - objSumGroupViewModelListClose.Credit;
                    AmountDirectExpenses = AmountDirectExpenses + closingbalanceforDirectexpense;
                }

                if (majorgroup_arr == 75)
                {
                    decimal closingbalanceforInDirectexpense = objSumGroupViewModelListClose.Debit - objSumGroupViewModelListClose.Credit;
                    AmountInDirectExpenses = AmountInDirectExpenses + closingbalanceforInDirectexpense;
                }


            }
             TotalProfitandLoss = (AmountDirectIncome - AmountDirectExpenses) + (AmountInDirectIncome - AmountInDirectExpenses );

            return objTransactionEntryViewModelList;
        }
        public ProfitandlossAmount GetGroupComparisonReportRangeforProfitandLosswithjson(int BusinessAreaID, string StartDate, string EndDate, string StartDate1, string EndDate1)
        {
            int[] Majorgroupid = { 72, 74, 73, 75 };
            ProfitandlossAmount ResponeResult = new ProfitandlossAmount();
            //string objTransaction = "select a.MajorgroupID,a.AccountID,a.AccountName,b.debit,b.credit from Accounting_ChartofAccounts as a inner join accounting_transactionentry as b on a.MajorgroupID = b.MajorgroupID where a.LegalEntityID =0 and a.MajorGroupID in(72, 74, 73, 75)";
            // Get Date For Month Only
            var MonthstartdateCol3 = new DateTime(Convert.ToDateTime(EndDate).Year, Convert.ToDateTime(EndDate).Month, 1).ToShortDateString();
            var MonthEnddateCol3 = EndDate;
            var MonthstartdateCol4 = new DateTime(Convert.ToDateTime(EndDate1).Year, Convert.ToDateTime(EndDate1).Month, 1).ToShortDateString();
            var MonthEnddateCol4 = EndDate1;
            //
            decimal AmountDirectIncome = 0;
            decimal AmountDirectExpenses = 0;
            decimal AmountInDirectIncome = 0;
            decimal AmountInDirectExpenses = 0;
            decimal TotalProfitandLoss = 0;
            //---
            decimal AmountDirectIncome1 = 0;
            decimal AmountDirectExpenses1 = 0;
            decimal AmountInDirectIncome1 = 0;
            decimal AmountInDirectExpenses1 = 0;
            decimal TotalProfitandLoss1 = 0;
            //---
            decimal AmountDirectIncome2 = 0;
            decimal AmountDirectExpenses2 = 0;
            decimal AmountInDirectIncome2 = 0;
            decimal AmountInDirectExpenses2 = 0;
            decimal TotalProfitandLoss2 = 0;
            //---
            decimal AmountDirectIncome3 = 0;
            decimal AmountDirectExpenses3 = 0;
            decimal AmountInDirectIncome3 = 0;
            decimal AmountInDirectExpenses3 = 0;
            decimal TotalProfitandLoss3 = 0;
            int majorgroup_arr = 0;
            decimal Debit = 0;
            decimal Credit =0;
            decimal Debit_Currentyear = 0;
            decimal Credit_Currentyear=0;
            decimal Debit_Previousyear = 0;
            decimal Credit_Previousyear = 0;
            decimal Debit_Currentyearwise = 0;
            decimal Credit_Currentyearwise = 0;
            decimal Debit_Previousyearwise = 0;
            decimal Credit_Previousyearwise = 0;
            GroupViewModelList objSumGroupViewModelListOpen = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListClose = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListOpen1 = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListClose1 = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListOpen2 = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListClose2 = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListOpen3 = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListClose3 = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListOpen4 = new GroupViewModelList();
            GroupViewModelList objSumGroupViewModelListClose4 = new GroupViewModelList();

            decimal amount_DirectIncomeDebit = 0;
            decimal amount_DirectIncomecredit = 0;
            decimal amount_DirectExpenseDebit = 0;
            decimal amount_DirectExpensecredit = 0;
            decimal amount_InDirectExpenseDebit = 0;
            decimal amount_InDirectExpensecredit = 0;

            decimal amount_DirectIncomeDebit1 = 0;
            decimal amount_DirectIncomecredit1 = 0;
            decimal amount_DirectExpenseDebit1= 0;
            decimal amount_DirectExpensecredit1 = 0;
            decimal amount_InDirectExpenseDebit1 = 0;
            decimal amount_InDirectExpensecredit1 = 0;

            decimal amount_DirectIncomecredit2 = 0;
            decimal amount_DirectIncomeDebit2 = 0;
            decimal amount_DirectExpenseDebit2= 0;
            decimal amount_DirectExpensecredit2 = 0;
            decimal amount_InDirectExpenseDebit2 = 0;
            decimal amount_InDirectExpensecredit2 = 0;

            decimal amount_DirectIncomecredit3 = 0;
            decimal amount_DirectIncomeDebit3= 0;
            decimal amount_DirectExpenseDebit3= 0;
            decimal amount_DirectExpensecredit3 = 0;
            decimal amount_InDirectExpenseDebit3 = 0;
            decimal amount_InDirectExpensecredit3 = 0;

            var objTransactionEntryViewModelList = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID)
                                                    select new TransactionEntryTrialList
                                                    {
                                                        MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();
            var objTransactionEntryViewModelList1 = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && Majorgroupid.Contains(objAccountingLedgerGroupMasterModel.ParentLedgerGroupID)
                                                    select new TransactionEntryTrialList
                                                    {
                                                        MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryTrialList objListdef in objTransactionEntryViewModelList)
            {
                majorgroup_arr = objListdef.MajorGroupID;
             
                //SumChartofAccountsViewModelList objSumChartofAccountsViewModelListOpen = GetSumOpeningBalanceSheet(LegalEntityID, majorgroup_arr, objListdef.GroupID,StartDate);
                //SumChartofAccountsViewModelList objSumChartofAccountsViewModelListClose = GetSumClosingBalancesheet(LegalEntityID, majorgroup_arr, objListdef.GroupID, StartDate, EndDate);
                 objSumGroupViewModelListOpen = GetGroupComparisonByStartDate(BusinessAreaID, majorgroup_arr, objListdef.GroupID, StartDate);
                 objSumGroupViewModelListClose = GetGroupComparisonListAll(BusinessAreaID, majorgroup_arr, objListdef.GroupID, StartDate, EndDate);
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                decimal openingbalance = objSumGroupViewModelListOpen.Debit - objSumGroupViewModelListOpen.Credit;
                decimal closingbalance = objSumGroupViewModelListClose.Debit - objSumGroupViewModelListClose.Credit;
                decimal debit = objSumGroupViewModelListClose.Debit - objSumGroupViewModelListOpen.Debit;
                decimal credit = objSumGroupViewModelListClose.Credit - objSumGroupViewModelListOpen.Credit;

                if (objSumGroupViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumGroupViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumGroupViewModelListOpen.Credit;
                }
                if (objSumGroupViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumGroupViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumGroupViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit4 = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit4 = objListdef.CloseCredit - objListdef.OpenCredit;
                Debit_Previousyearwise = objListdef.Debit4;
                Credit_Previousyearwise = objListdef.Credit4;

                objSumGroupViewModelListOpen1 = GetGroupComparisonByStartDate(BusinessAreaID , majorgroup_arr, objListdef.GroupID, StartDate1);
                 objSumGroupViewModelListClose1 = GetGroupComparisonListAll(BusinessAreaID , majorgroup_arr, objListdef.GroupID, StartDate1, EndDate1);

                decimal OpeningDebitBalanceAmount1 = 0;
                decimal OpeningCreditBalanceAmount1 = 0;
                decimal ClosingDebitBalanceAmount1 = 0;
                decimal ClosingCreditBalanceAmount1 = 0;
                decimal openingbalance1 = objSumGroupViewModelListOpen1.Debit - objSumGroupViewModelListOpen1.Credit;
                decimal closingbalance1 = objSumGroupViewModelListClose1.Debit - objSumGroupViewModelListClose1.Credit;
                decimal debit1 = objSumGroupViewModelListClose1.Debit - objSumGroupViewModelListOpen1.Debit;
                decimal credit1 = objSumGroupViewModelListClose1.Credit - objSumGroupViewModelListOpen1.Credit;

                if (objSumGroupViewModelListOpen1 != null)
                {
                    OpeningDebitBalanceAmount1 = objSumGroupViewModelListOpen1.Debit;
                    OpeningCreditBalanceAmount1 = objSumGroupViewModelListOpen1.Credit;
                }
                if (objSumGroupViewModelListClose1 != null)
                {
                    ClosingDebitBalanceAmount1 = objSumGroupViewModelListClose1.Debit;
                    ClosingCreditBalanceAmount1 = objSumGroupViewModelListClose1.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount1;
                objListdef.OpenCredit = OpeningCreditBalanceAmount1;
                objListdef.CloseDebit = ClosingDebitBalanceAmount1;
                objListdef.CloseCredit = ClosingCreditBalanceAmount1;
                objListdef.Debit2 = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit2 = objListdef.CloseCredit - objListdef.OpenCredit;
                Debit_Currentyearwise = objListdef.Debit2;
                Credit_Currentyearwise = objListdef.Credit2;

                 objSumGroupViewModelListOpen2 = GetGroupComparisonByStartDate(BusinessAreaID , majorgroup_arr, objListdef.GroupID, MonthstartdateCol3);
                 objSumGroupViewModelListClose2 = GetGroupComparisonListAll(BusinessAreaID , majorgroup_arr, objListdef.GroupID, MonthstartdateCol3, EndDate);

                decimal OpeningDebitBalanceAmount2 = 0;
                decimal OpeningCreditBalanceAmount2 = 0;
                decimal ClosingDebitBalanceAmount2= 0;
                decimal ClosingCreditBalanceAmount2 = 0;
                decimal openingbalance2 = objSumGroupViewModelListOpen2.Debit - objSumGroupViewModelListOpen2.Credit;
                decimal closingbalance2 = objSumGroupViewModelListClose2.Debit - objSumGroupViewModelListClose2.Credit;
                decimal debit2 = objSumGroupViewModelListClose2.Debit - objSumGroupViewModelListOpen2.Debit;
                decimal credit2 = objSumGroupViewModelListClose2.Credit - objSumGroupViewModelListOpen2.Credit;

                if (objSumGroupViewModelListOpen2 != null)
                {
                    OpeningDebitBalanceAmount2 = objSumGroupViewModelListOpen2.Debit;
                    OpeningCreditBalanceAmount2 = objSumGroupViewModelListOpen2.Credit;
                }
                if (objSumGroupViewModelListClose2 != null)
                {
                    ClosingDebitBalanceAmount2 = objSumGroupViewModelListClose2.Debit;
                    ClosingCreditBalanceAmount2 = objSumGroupViewModelListClose2.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount2;
                objListdef.OpenCredit = OpeningCreditBalanceAmount2;
                objListdef.CloseDebit = ClosingDebitBalanceAmount2;
                objListdef.CloseCredit = ClosingCreditBalanceAmount2;
                objListdef.Debit1 = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit1 = objListdef.CloseCredit - objListdef.OpenCredit;
                Debit_Previousyear = objListdef.Debit1;
                Credit_Previousyear = objListdef.Credit1;



                objSumGroupViewModelListOpen3 = GetGroupComparisonByStartDate(BusinessAreaID , majorgroup_arr, objListdef.GroupID, MonthstartdateCol4);
                objSumGroupViewModelListClose3 = GetGroupComparisonListAll(BusinessAreaID , majorgroup_arr, objListdef.GroupID, MonthstartdateCol4, EndDate1);

                decimal OpeningDebitBalanceAmount3 = 0;
                decimal OpeningCreditBalanceAmount3 = 0;
                decimal ClosingDebitBalanceAmount3 = 0;
                decimal ClosingCreditBalanceAmount3 = 0;
                decimal openingbalance3 = objSumGroupViewModelListOpen3.Debit - objSumGroupViewModelListOpen3.Credit;
                decimal closingbalance3 = objSumGroupViewModelListClose3.Debit - objSumGroupViewModelListClose3.Credit;
                decimal debit3 = objSumGroupViewModelListClose3.Debit - objSumGroupViewModelListOpen3.Debit;
                decimal credit3 = objSumGroupViewModelListClose3.Credit - objSumGroupViewModelListOpen3.Credit;

                if (objSumGroupViewModelListOpen3 != null)
                {
                    OpeningDebitBalanceAmount3 = objSumGroupViewModelListOpen3.Debit;
                    OpeningCreditBalanceAmount3 = objSumGroupViewModelListOpen3.Credit;
                }
                if (objSumGroupViewModelListClose3 != null)
                {
                    ClosingDebitBalanceAmount3 = objSumGroupViewModelListClose3.Debit;
                    ClosingCreditBalanceAmount3 = objSumGroupViewModelListClose3.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount3;
                objListdef.OpenCredit = OpeningCreditBalanceAmount3;
                objListdef.CloseDebit = ClosingDebitBalanceAmount3;
                objListdef.CloseCredit = ClosingCreditBalanceAmount3;
                objListdef.Debit5 = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit5 = objListdef.CloseCredit - objListdef.OpenCredit;
                 Debit_Currentyear  = objListdef.Debit5;
                 Credit_Currentyear  = objListdef.Credit5;


            }
            foreach (var item in objTransactionEntryViewModelList)
            {

                majorgroup_arr = item.MajorGroupID;
                if (majorgroup_arr == 72)
                {
                    //current year and Month wise
                    Debit_Currentyear  = item.Debit5 ;
                    Credit_Currentyear  = item.Credit5;
                    amount_DirectIncomeDebit = amount_DirectIncomeDebit + Debit_Currentyear ;
                    amount_DirectIncomecredit = amount_DirectIncomecredit + Credit_Currentyear ;

                    //Previous year and Month Wise
                    Debit_Previousyear = item.Debit1 ;
                    Credit_Previousyear = item.Credit1;
                    amount_DirectIncomeDebit1 = amount_DirectIncomeDebit1 + Debit_Previousyear;
                    amount_DirectIncomecredit1 = amount_DirectIncomecredit1 + Credit_Previousyear;

                    //Current year and year wise
                    Debit_Currentyearwise  = item.Debit2;
                    Credit_Currentyearwise  = item.Credit2;
                    amount_DirectIncomeDebit2 = amount_DirectIncomeDebit2 + Debit_Currentyearwise;
                    amount_DirectIncomecredit2= amount_DirectIncomecredit2 + Credit_Currentyearwise;

                    //Previous year and year wise
                    Debit_Previousyearwise  = item.Debit4;
                   Credit_Previousyearwise  = item.Credit4;
                    amount_DirectIncomeDebit3 = amount_DirectIncomeDebit3 + Debit_Previousyearwise;
                    amount_DirectIncomecredit3= amount_DirectIncomecredit3 + Credit_Previousyearwise;



                }
                //Direct Income
                if (majorgroup_arr == 72)
                {
                    //Check year wise and month wise,Debit>0

                    if (Debit_Currentyear  > 0 && Credit_Currentyear == 0 || Debit_Previousyear > 0 && Credit_Previousyear == 0 || Debit_Currentyearwise > 0 && Credit_Currentyearwise == 0 || Debit_Previousyearwise > 0 && Credit_Previousyearwise == 0)
                    {
                        decimal closingbalanceforDirectincome = Debit_Previousyearwise ;
                        AmountDirectIncome = AmountDirectIncome + closingbalanceforDirectincome;

                        decimal closingbalanceforDirectincome1 = Debit_Currentyearwise;
                        AmountDirectIncome1 = AmountDirectIncome1 + closingbalanceforDirectincome1;

                        decimal closingbalanceforDirectincome2 = Debit_Previousyear;
                        AmountDirectIncome2 = AmountDirectIncome2 + closingbalanceforDirectincome2;

                        //decimal closingbalanceforDirectincome3 = objSumGroupViewModelListClose3.Credit - objSumGroupViewModelListClose3.Debit;
                        //AmountDirectIncome3 = AmountDirectIncome3 + closingbalanceforDirectincome3;
                        decimal closingbalanceforDirectincome3 = Debit_Currentyear;
                        AmountDirectIncome3 = AmountDirectIncome3 + closingbalanceforDirectincome3;

                    }
                    //Check year wise and month wise,Credit > 0

                    if (Debit_Currentyear == 0 && Credit_Currentyear > 0 || Debit_Previousyear == 0 && Credit_Previousyear > 0 || Debit_Currentyearwise == 0 && Credit_Currentyearwise > 0 || Debit_Previousyearwise == 0 && Credit_Previousyearwise > 0)
                    {
                        decimal closingbalanceforDirectincome = Credit_Previousyearwise ;
                        AmountDirectIncome = AmountDirectIncome + closingbalanceforDirectincome;

                        decimal closingbalanceforDirectincome1 = Credit_Currentyearwise;
                        AmountDirectIncome1 = AmountDirectIncome1 + closingbalanceforDirectincome1;

                        decimal closingbalanceforDirectincome2 = Credit_Previousyear;
                        AmountDirectIncome2 = AmountDirectIncome2 + closingbalanceforDirectincome2;

                        //decimal closingbalanceforDirectincome3 = objSumGroupViewModelListClose3.Credit - objSumGroupViewModelListClose3.Debit;
                        //AmountDirectIncome3 = AmountDirectIncome3 + closingbalanceforDirectincome3;
                        decimal closingbalanceforDirectincome3 = Credit_Currentyear;
                        AmountDirectIncome3 = AmountDirectIncome3 + closingbalanceforDirectincome3;

                    }
                    //Check year wise and month wise,Debit and Credit >0 
                    if (Debit_Currentyear > 0 && Credit_Currentyear > 0 || Debit_Previousyear > 0 && Credit_Previousyear > 0 || Debit_Currentyearwise > 0 && Credit_Currentyearwise > 0 || Debit_Previousyearwise > 0 && Credit_Previousyearwise > 0)
                    {
                        decimal closingbalanceforDirectincome = Debit_Previousyearwise - Credit_Previousyearwise;
                        AmountDirectIncome = AmountDirectIncome + closingbalanceforDirectincome;

                        decimal closingbalanceforDirectincome1 = Debit_Currentyearwise - Credit_Currentyearwise;
                        AmountDirectIncome1 = AmountDirectIncome1 + closingbalanceforDirectincome1;

                        decimal closingbalanceforDirectincome2 = Debit_Previousyear - Credit_Previousyear;
                        AmountDirectIncome2 = AmountDirectIncome2 + closingbalanceforDirectincome2;

                        //decimal closingbalanceforDirectincome3 = objSumGroupViewModelListClose3.Credit - objSumGroupViewModelListClose3.Debit;
                        //AmountDirectIncome3 = AmountDirectIncome3 + closingbalanceforDirectincome3;
                        decimal closingbalanceforDirectincome3 = Debit_Currentyear - Credit_Currentyear;
                        AmountDirectIncome3 = AmountDirectIncome3 + closingbalanceforDirectincome3;

                    }
                }
            }
            //Get year wise and month wise ,Debit>0
            if (amount_DirectIncomeDebit > 0 && amount_DirectIncomecredit == 0 || amount_DirectIncomeDebit1 > 0 && amount_DirectIncomecredit1 == 0 ||amount_DirectIncomeDebit2 > 0 && amount_DirectIncomecredit2 == 0 || amount_DirectIncomeDebit3 > 0 && amount_DirectIncomecredit3 == 0)
            {
                AmountDirectIncome = amount_DirectIncomeDebit3;
                AmountDirectIncome1 = amount_DirectIncomeDebit2;
                AmountDirectIncome2 = amount_DirectIncomeDebit1;
                AmountDirectIncome3 = amount_DirectIncomeDebit;
            }
            //Get year wise and month wise,Credit > 0
            if (amount_DirectIncomeDebit == 0 && amount_DirectIncomecredit > 0 || amount_DirectIncomeDebit1 == 0 && amount_DirectIncomecredit1 > 0 || amount_DirectIncomeDebit2 == 0 && amount_DirectIncomecredit2 > 0 || amount_DirectIncomeDebit3 == 0 && amount_DirectIncomecredit3 > 0)
            {
                AmountDirectIncome = amount_DirectIncomecredit3;
                AmountDirectIncome1 = amount_DirectIncomecredit2;
                AmountDirectIncome2 = amount_DirectIncomecredit1;
                AmountDirectIncome3 = amount_DirectIncomecredit;
            }
            //Get year wise and month wise , Debit and credit > 0
            if (amount_DirectIncomeDebit > 0 && amount_DirectIncomecredit > 0 || amount_DirectIncomeDebit1 > 0 && amount_DirectIncomecredit1 > 0 || amount_DirectIncomeDebit2 > 0 && amount_DirectIncomecredit2 > 0 || amount_DirectIncomeDebit3 > 0 && amount_DirectIncomecredit3 > 0)
            {
                AmountDirectIncome = amount_DirectIncomecredit3 - amount_DirectIncomeDebit3 ;
                AmountDirectIncome1 = amount_DirectIncomecredit2 - amount_DirectIncomeDebit2 ;
                AmountDirectIncome2 = amount_DirectIncomecredit1  - amount_DirectIncomeDebit1;
                AmountDirectIncome3 = amount_DirectIncomecredit  - amount_DirectIncomeDebit ;
            }


            foreach (var item in objTransactionEntryViewModelList)
            {
                majorgroup_arr = item.MajorGroupID;
                if (majorgroup_arr == 73)
                {
                    Debit = item.Debit;
                    Credit = item.Credit;

                }

                if (majorgroup_arr == 73)
                {
                    decimal closingbalanceforInDirectIncome = objSumGroupViewModelListClose.Credit - objSumGroupViewModelListClose.Debit;
                    AmountInDirectIncome = AmountInDirectIncome + closingbalanceforInDirectIncome;

                    decimal closingbalanceforInDirectIncome1 = objSumGroupViewModelListClose1.Credit - objSumGroupViewModelListClose1.Debit;
                    AmountInDirectIncome1 = AmountInDirectIncome1 + closingbalanceforInDirectIncome1;

                    decimal closingbalanceforInDirectIncome2 = objSumGroupViewModelListClose2.Credit - objSumGroupViewModelListClose2.Debit;
                    AmountInDirectIncome2 = AmountInDirectIncome2 + closingbalanceforInDirectIncome2;

                    decimal closingbalanceforInDirectIncome3 = objSumGroupViewModelListClose3.Credit - objSumGroupViewModelListClose3.Debit;
                    AmountInDirectIncome3 = AmountInDirectIncome3 + closingbalanceforInDirectIncome3;

                }
            }
            //Direct Expenses
            foreach (var item in objTransactionEntryViewModelList)
            {
                majorgroup_arr = item.MajorGroupID;
                if (majorgroup_arr == 74)
                {
                    //current year and Month wise
                    Debit_Currentyear = item.Debit5;
                    Credit_Currentyear = item.Credit5;
                    amount_DirectExpenseDebit = amount_DirectExpenseDebit   + Debit_Currentyear;
                    amount_DirectExpensecredit  = amount_DirectExpensecredit  + Credit_Currentyear;

                    //Previous year and Month wise
                    Debit_Previousyear = item.Debit1;
                    Credit_Previousyear = item.Credit1;
                    amount_DirectExpenseDebit1 = amount_DirectExpenseDebit1 + Debit_Previousyear;
                    amount_DirectExpensecredit1 = amount_DirectExpensecredit1 + Credit_Previousyear;

                    //Current year and year wise
                    Debit_Currentyearwise  = item.Debit2;
                    Credit_Currentyearwise = item.Credit2;
                    amount_DirectExpenseDebit2   = amount_DirectExpenseDebit2 + Debit_Currentyearwise;
                    amount_DirectExpensecredit2 = amount_DirectExpensecredit2 + Credit_Currentyearwise;

                    //Previous year and year wise
                    Debit_Previousyearwise  = item.Debit4;
                    Credit_Previousyearwise = item.Credit4;
                    amount_DirectExpenseDebit3   = amount_DirectExpenseDebit3 + Debit_Previousyearwise;
                    amount_DirectExpensecredit3 = amount_DirectExpensecredit3 + Credit_Previousyearwise;



                }
                if (majorgroup_arr == 74)
                {
                    //Check year wise and Month wise, Debit > 0
                    if (Debit_Currentyear > 0 && Credit_Currentyear == 0 || Debit_Previousyear > 0 && Credit_Previousyear == 0 || Debit_Currentyearwise > 0 && Credit_Currentyearwise == 0 || Debit_Previousyearwise > 0 && Credit_Previousyearwise == 0)
                    {
                        decimal closingbalanceforDirectexpense = Debit_Previousyearwise;
                        AmountDirectExpenses = AmountDirectExpenses + closingbalanceforDirectexpense;

                        decimal closingbalanceforDirectexpense1 = Debit_Currentyearwise;
                        AmountDirectExpenses1 = AmountDirectExpenses1 + closingbalanceforDirectexpense1;

                        decimal closingbalanceforDirectExpense2 = Debit_Previousyear;
                        AmountDirectExpenses2 = AmountDirectExpenses2 + closingbalanceforDirectExpense2;

                        decimal closingbalanceforDirectExpense3 = Debit_Currentyear;
                        AmountDirectExpenses3 = AmountDirectExpenses3 + closingbalanceforDirectExpense3;
                    }
                    //Check year wise and Month wise ,Credit>0
                    if (Debit_Currentyear == 0 && Credit_Currentyear > 0 || Debit_Previousyear == 0 && Credit_Previousyear > 0 || Debit_Currentyearwise == 0 && Credit_Currentyearwise > 0 || Debit_Previousyearwise == 0 && Credit_Previousyearwise > 0)
                    {
                        decimal closingbalanceforDirectexpense = Credit_Previousyearwise;
                        AmountDirectExpenses = AmountDirectExpenses + closingbalanceforDirectexpense;

                        decimal closingbalanceforDirectexpense1 = Credit_Currentyearwise;
                        AmountDirectExpenses1 = AmountDirectExpenses1 + closingbalanceforDirectexpense1;

                        decimal closingbalanceforDirectExpense2 = Credit_Previousyear;
                        AmountDirectExpenses2 = AmountDirectExpenses2 + closingbalanceforDirectExpense2;

                        decimal closingbalanceforDirectExpense3 = Credit_Currentyear;
                        AmountDirectExpenses3 = AmountDirectExpenses3 + closingbalanceforDirectExpense3;
                    }
                    //Check year wise and Month wise, Debit > 0 and Credit > 0
                    if (Debit_Currentyear > 0 && Credit_Currentyear > 0 || Debit_Previousyear > 0 && Credit_Previousyear > 0 || Debit_Currentyearwise > 0 && Credit_Currentyearwise > 0 || Debit_Previousyearwise > 0 && Credit_Previousyearwise > 0)
                    {
                        decimal closingbalanceforDirectexpense = Debit_Previousyearwise - Credit_Previousyearwise;
                        AmountDirectExpenses = AmountDirectExpenses + closingbalanceforDirectexpense;

                        decimal closingbalanceforDirectexpense1 = Debit_Currentyearwise - Credit_Currentyearwise;
                        AmountDirectExpenses1 = AmountDirectExpenses1 + closingbalanceforDirectexpense1;

                        decimal closingbalanceforDirectExpense2 = Debit_Previousyear - Credit_Previousyear;
                        AmountDirectExpenses2 = AmountDirectExpenses2 + closingbalanceforDirectExpense2;

                        //decimal closingbalanceforDirectExpense3 = objSumGroupViewModelListClose3.Credit - objSumGroupViewModelListClose3.Debit;
                        //AmountDirectExpenses3 = AmountDirectExpenses3 + closingbalanceforDirectExpense3;
                        decimal closingbalanceforDirectExpense3 = Debit_Currentyear - Credit_Currentyear;
                        AmountDirectExpenses3 = AmountDirectExpenses3 + closingbalanceforDirectExpense3;
                    }

                }
            }
            //Get Year and Month wise data,  Debit>0  and Credit>0
            if (amount_DirectExpenseDebit > 0 && amount_DirectExpensecredit == 0 || amount_DirectExpenseDebit1 > 0 && amount_DirectExpensecredit1 == 0 || amount_DirectExpenseDebit2 >0 && amount_DirectExpensecredit2 ==0 || amount_DirectExpenseDebit3 > 0 && amount_DirectExpensecredit3 ==0)
            {
                AmountDirectExpenses = amount_DirectExpenseDebit3;
                AmountDirectExpenses1 = amount_DirectExpenseDebit2;
                AmountDirectExpenses2 = amount_DirectExpenseDebit1;
                AmountDirectExpenses3 = amount_DirectExpenseDebit;
            }
            //Get Year and Month wise data,  Debit>0  

            if (amount_DirectExpenseDebit == 0 && amount_DirectExpensecredit > 0 || amount_DirectExpenseDebit1 == 0 && amount_DirectExpensecredit1 > 0 || amount_DirectExpenseDebit2 == 0 && amount_DirectExpensecredit2 > 0 || amount_DirectExpenseDebit3 == 0 && amount_DirectExpensecredit3 > 0)
            {

                AmountDirectExpenses = amount_DirectExpensecredit3;
                AmountDirectExpenses1 = amount_DirectExpensecredit2;
                AmountDirectExpenses2 = amount_DirectExpensecredit1;
                AmountDirectExpenses3 = amount_DirectExpensecredit;
            }
            //Get Year and Month wise data,  Credit>0

            if (amount_DirectExpenseDebit > 0 && amount_DirectExpensecredit > 0 || amount_DirectExpenseDebit1 > 0 && amount_DirectExpensecredit1 > 0 || amount_DirectExpenseDebit2 > 0 && amount_DirectExpensecredit2 > 0 || amount_DirectExpenseDebit3 > 0 && amount_DirectExpensecredit3 > 0)
            {
                AmountDirectExpenses = amount_DirectExpenseDebit3 - amount_DirectExpensecredit3;
                AmountDirectExpenses1 = amount_DirectExpenseDebit2 - amount_DirectExpensecredit2;
                AmountDirectExpenses2 = amount_DirectExpenseDebit1 - amount_DirectExpensecredit1;
                AmountDirectExpenses3 = amount_DirectExpenseDebit - amount_DirectExpensecredit;
            }

            //Indirect Expenses
            foreach (var item in objTransactionEntryViewModelList)
            {
                majorgroup_arr = item.MajorGroupID;
                if (majorgroup_arr == 75)
                {
                    //Current year and Month wise
                    //current year and Month wise
                    Debit_Currentyear = item.Debit5;
                    Credit_Currentyear = item.Credit5;
                    amount_InDirectExpenseDebit  = amount_InDirectExpenseDebit  + Debit_Currentyear;
                    amount_InDirectExpensecredit  = amount_InDirectExpensecredit  + Credit_Currentyear;

                    //Previous year and Month wise
                    Debit_Previousyear = item.Debit1;
                    Credit_Previousyear = item.Credit1;
                    amount_InDirectExpenseDebit1 = amount_InDirectExpenseDebit1 + Debit_Previousyear;
                    amount_InDirectExpensecredit1 = amount_InDirectExpensecredit1 + Credit_Previousyear;

                    //current year and year wise
                    Debit_Currentyearwise = item.Debit2;
                    Credit_Currentyearwise  = item.Credit2;
                    amount_InDirectExpenseDebit2 = amount_InDirectExpenseDebit2 + Debit_Currentyearwise ;
                    amount_InDirectExpensecredit2 = amount_InDirectExpensecredit2 + Credit_Currentyearwise ;

                    //Previous year and year wise
                    Debit_Previousyearwise = item.Debit4;
                    Credit_Previousyearwise  = item.Credit4;
                    amount_InDirectExpenseDebit3 = amount_InDirectExpenseDebit3 + Debit_Previousyearwise ;
                    amount_InDirectExpensecredit3 = amount_InDirectExpensecredit3 + Credit_Previousyearwise  ;



                }

                if (majorgroup_arr == 75)
                {
                    //Check Month and Year wise, debit>0 and credit>0
                    if (Debit_Currentyear  > 0 && Credit_Currentyear > 0 || Debit_Previousyear > 0 && Credit_Previousyear > 0 || Debit_Currentyearwise >0 && Credit_Currentyearwise > 0 || Debit_Previousyearwise > 0 && Credit_Previousyearwise > 0)
                    {
                        decimal closingbalanceforInDirectexpense = Debit_Previousyearwise - Credit_Previousyearwise ;
                        AmountInDirectExpenses = AmountInDirectExpenses + closingbalanceforInDirectexpense;

                        decimal closingbalanceforInDirectexpense1 = Debit_Currentyearwise - Credit_Currentyearwise;
                        AmountInDirectExpenses1 = AmountInDirectExpenses1 + closingbalanceforInDirectexpense1;

                        decimal closingbalanceforInDirectexpense2 = Debit_Previousyear -Credit_Previousyear ;
                        AmountInDirectExpenses2 = AmountInDirectExpenses2 + closingbalanceforInDirectexpense2;

                        decimal closingbalanceforInDirectexpense3 = Debit_Currentyear - Credit_Currentyear;
                        AmountInDirectExpenses3 = AmountInDirectExpenses3 + closingbalanceforInDirectexpense3;
                    }
                    //Check Month and Year wise, debit>0 
                    if (Debit_Currentyear > 0 && Credit_Currentyear == 0 || Debit_Previousyear > 0 && Credit_Previousyear == 0 || Debit_Currentyearwise > 0 && Credit_Currentyearwise == 0 || Debit_Previousyearwise > 0 && Credit_Previousyearwise == 0)
                    {
                        decimal closingbalanceforInDirectexpense = Debit_Previousyearwise ;
                        AmountInDirectExpenses = AmountInDirectExpenses + closingbalanceforInDirectexpense;

                        decimal closingbalanceforInDirectexpense1 = Debit_Currentyearwise ;
                        AmountInDirectExpenses1 = AmountInDirectExpenses1 + closingbalanceforInDirectexpense1;

                        decimal closingbalanceforInDirectexpense2 = Debit_Previousyear;
                        AmountInDirectExpenses2 = AmountInDirectExpenses2 + closingbalanceforInDirectexpense2;

                        decimal closingbalanceforInDirectexpense3 = Debit_Currentyear;
                        AmountInDirectExpenses3 = AmountInDirectExpenses3 + closingbalanceforInDirectexpense3;
                    }
                    //Check Month and Year wise, credit>0
                    if (Debit_Currentyear == 0 && Credit_Currentyear > 0 || Debit_Previousyear == 0 && Credit_Previousyear > 0 || Debit_Currentyearwise == 0 && Credit_Currentyearwise > 0 || Debit_Previousyearwise == 0 && Credit_Previousyearwise > 0)
                    {
                        decimal closingbalanceforInDirectexpense = Credit_Previousyearwise ;
                        AmountInDirectExpenses = AmountInDirectExpenses + closingbalanceforInDirectexpense;

                        decimal closingbalanceforInDirectexpense1 = Credit_Currentyearwise;
                        AmountInDirectExpenses1 = AmountInDirectExpenses1 + closingbalanceforInDirectexpense1;

                        decimal closingbalanceforInDirectexpense2 = Credit_Previousyear;
                        AmountInDirectExpenses2 = AmountInDirectExpenses2 + closingbalanceforInDirectexpense2;

                        decimal closingbalanceforInDirectexpense3 = Credit_Currentyear;
                        AmountInDirectExpenses3 = AmountInDirectExpenses3 + closingbalanceforInDirectexpense3;
                    }
                }

            }
            //Get Current year and Month wise data, Debit > 0 
            if (amount_InDirectExpenseDebit > 0 && amount_InDirectExpensecredit == 0 || amount_InDirectExpenseDebit1 > 0 && amount_InDirectExpensecredit1 == 0 || amount_InDirectExpenseDebit2 > 0 && amount_InDirectExpensecredit2 == 0 || amount_InDirectExpenseDebit3 > 0 && amount_InDirectExpensecredit3 ==0)
            {
                AmountInDirectExpenses = amount_InDirectExpenseDebit3;
                AmountInDirectExpenses1 = amount_InDirectExpenseDebit2;
                AmountInDirectExpenses2 = amount_InDirectExpenseDebit1;
                AmountInDirectExpenses3 = amount_InDirectExpenseDebit;
            }
            //Get Current year and Month wise data, Credit > 0 
           if (amount_InDirectExpenseDebit == 0 && amount_InDirectExpensecredit > 0 || amount_InDirectExpenseDebit1 == 0 && amount_InDirectExpensecredit1 > 0 || amount_InDirectExpenseDebit2 == 0 && amount_InDirectExpensecredit2 > 0 || amount_InDirectExpenseDebit3 ==0  && amount_InDirectExpensecredit3 > 0)
            {
                AmountInDirectExpenses = amount_InDirectExpensecredit3;
                AmountInDirectExpenses1 = amount_InDirectExpensecredit2;
                AmountInDirectExpenses2 = amount_InDirectExpensecredit1;
                AmountInDirectExpenses3 = amount_InDirectExpensecredit;
            }
            //Get Current year and Month wise data, debit >0 and Credit > 0 
            if (amount_InDirectExpenseDebit > 0 && amount_InDirectExpensecredit > 0 || amount_InDirectExpenseDebit1 > 0 && amount_InDirectExpensecredit1 > 0 || amount_InDirectExpenseDebit2 > 0 && amount_InDirectExpensecredit2 > 0 || amount_InDirectExpenseDebit3 > 0 && amount_InDirectExpensecredit3 > 0)
            {
                AmountInDirectExpenses = amount_InDirectExpenseDebit3 - amount_InDirectExpensecredit3;
                AmountInDirectExpenses1 = amount_InDirectExpenseDebit2 - amount_InDirectExpensecredit2;
                AmountInDirectExpenses2 = amount_InDirectExpenseDebit1 - amount_InDirectExpensecredit1;
                AmountInDirectExpenses3 = amount_InDirectExpenseDebit - amount_InDirectExpensecredit;
            }


            TotalProfitandLoss2 = (AmountDirectIncome2 - AmountDirectExpenses2) + (AmountInDirectIncome2 - AmountInDirectExpenses2);
            ResponeResult.Column3 = TotalProfitandLoss2 / 100000;

            TotalProfitandLoss = (AmountDirectIncome - AmountDirectExpenses) + (AmountInDirectIncome - AmountInDirectExpenses);
                TotalProfitandLoss1 = (AmountDirectIncome1 - AmountDirectExpenses1) + (AmountInDirectIncome1 - AmountInDirectExpenses1);
                TotalProfitandLoss3 = (AmountDirectIncome3 - AmountDirectExpenses3) + (AmountInDirectIncome3 - AmountInDirectExpenses3);
                ResponeResult.Column1 = TotalProfitandLoss / 100000;
                ResponeResult.Column2 = TotalProfitandLoss1 / 100000;
                ResponeResult.Column4 = TotalProfitandLoss3 / 100000;
            
            return ResponeResult;
        }

        private SumChartofAccountsViewModelList GetSumOpeningBalanceSheet(int legalEntityID, int majorgroup_arr, int GroupID, DateTime Startdate)
        {
            int[] Majorgroupid = { 63, 64, 65, 66, 67, 68, 69, 70, 72, 74, 73, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            x.GroupID == GroupID && x.LegalEntityID == legalEntityID && Majorgroupid.Contains(majorgroup_arr) && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.GroupID == GroupID && x.LegalEntityID == legalEntityID && Majorgroupid.Contains(majorgroup_arr) && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= Startdate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.GroupID = GroupID;

            return objSumChartofAccountsViewModelList;

        }

        private SumChartofAccountsViewModelList GetSumClosingBalancesheet(int LegalEntityID, int majorGroupID, int GroupID, DateTime StartDate, DateTime EndDate)
        {
            int[] Majorgroupid = { 63, 64, 65, 66, 67, 68, 69, 70, 72, 74, 73, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(majorGroupID) && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.MajorGroupID == majorGroupID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.GroupID = GroupID;
            return objSumChartofAccountsViewModelList;

        }


        public List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityIDMajorGroups(int LegalEntityID, DateTime StartDate, DateTime EndDate)
        {
            var objTransactionEntryViewModelList = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && objAccountingLedgerGroupMasterModel.GroupTypeID == 1
                                                    select new TransactionEntryTrialList
                                                    {
                                                        MajorGroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryTrialList objListdef in objTransactionEntryViewModelList)
            {

                MajorGroupViewModelList objMajorGroupViewModelListOpen = GetClosingMajorGroupViewModelByStartDate(LegalEntityID, objListdef.MajorGroupID, Convert.ToDateTime(StartDate.ToShortDateString()));
                MajorGroupViewModelList objMajorGroupViewModelListClose = GetMajorGroupViewModelListAll(LegalEntityID, objListdef.MajorGroupID, StartDate, EndDate);
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                if (objMajorGroupViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objMajorGroupViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objMajorGroupViewModelListOpen.Credit;
                }
                if (objMajorGroupViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objMajorGroupViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objMajorGroupViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;
            }
            return objTransactionEntryViewModelList;

        }
        public List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityIDGroups(int LegalEntityID, DateTime StartDate, DateTime EndDate)
        {
            var objTransactionEntryViewModelList = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && objAccountingLedgerGroupMasterModel.GroupTypeID == 2
                                                    select new TransactionEntryTrialList
                                                    {
                                                        MajorGroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                        MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryTrialList objListdef in objTransactionEntryViewModelList)
            {

                GroupViewModelList objSumGroupViewModelListOpen = GetClosingGroupViewModelByStartDate(LegalEntityID, objListdef.GroupID, Convert.ToDateTime(StartDate.ToShortDateString()));
                GroupViewModelList objSumGroupViewModelListClose = GetGroupViewModelListAll(LegalEntityID, objListdef.GroupID, StartDate, EndDate);
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                if (objSumGroupViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumGroupViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumGroupViewModelListOpen.Credit;
                }
                if (objSumGroupViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumGroupViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumGroupViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;
            }
            return objTransactionEntryViewModelList;

        }
        public List<TransactionEntryTrialList> GetAllTransactionEntryTrialListByLegalEntityIDSubGroups(int LegalEntityID, DateTime StartDate, DateTime EndDate)
        {
            var objTransactionEntryViewModelList = (from objAccountingLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                    where objAccountingLedgerGroupMasterModel.IsActive == true && objAccountingLedgerGroupMasterModel.GroupTypeID == 3
                                                    select new TransactionEntryTrialList
                                                    {
                                                        MajorGroupID = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().ParentLedgerGroupID,
                                                        //MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        GroupID = objAccountingLedgerGroupMasterModel.ParentLedgerGroupID,
                                                        GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                        SubGroupID = objAccountingLedgerGroupMasterModel.LedgerGroupID,
                                                        SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objAccountingLedgerGroupMasterModel.LedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName
                                                    }).OrderBy(x => x.MajorGroupName).ThenBy(x => x.GroupName).ThenBy(x => x.SubGroupName).ThenBy(x => x.AccountName).ToList();

            foreach (TransactionEntryTrialList objListdef in objTransactionEntryViewModelList)
            {

                objListdef.MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objListdef.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName;
                SubGroupViewModelList objSumSubGroupViewModelListOpen = GetClosingSubGroupViewModelByStartDate(LegalEntityID, objListdef.SubGroupID, Convert.ToDateTime(StartDate.ToShortDateString()));
                SubGroupViewModelList objSumSubGroupViewModelListClose = GetSubGroupViewModelListAll(LegalEntityID, objListdef.SubGroupID, StartDate, EndDate);
                decimal OpeningDebitBalanceAmount = 0;
                decimal OpeningCreditBalanceAmount = 0;
                decimal ClosingDebitBalanceAmount = 0;
                decimal ClosingCreditBalanceAmount = 0;
                if (objSumSubGroupViewModelListOpen != null)
                {
                    OpeningDebitBalanceAmount = objSumSubGroupViewModelListOpen.Debit;
                    OpeningCreditBalanceAmount = objSumSubGroupViewModelListOpen.Credit;
                }
                if (objSumSubGroupViewModelListClose != null)
                {
                    ClosingDebitBalanceAmount = objSumSubGroupViewModelListClose.Debit;
                    ClosingCreditBalanceAmount = objSumSubGroupViewModelListClose.Credit;
                }
                objListdef.OpenDebit = OpeningDebitBalanceAmount;
                objListdef.OpenCredit = OpeningCreditBalanceAmount;
                objListdef.CloseDebit = ClosingDebitBalanceAmount;
                objListdef.CloseCredit = ClosingCreditBalanceAmount;
                objListdef.Debit = objListdef.CloseDebit - objListdef.OpenDebit;
                objListdef.Credit = objListdef.CloseCredit - objListdef.OpenCredit;
            }
            return objTransactionEntryViewModelList;

        }
        //public int SaveTransactionEntry(TransactionEntryModel objTransactionEntryModel, int UserID)
        //{
        //    int TransactionEntryID = 0;
        //    if (objTransactionEntryModel.TransactionEntryID == 0)
        //    {
        //        objTransactionEntryModel.IsActive = true;
        //        objTransactionEntryModel.CreatedBy = UserID;
        //        objTransactionEntryModel.CreatedOn = DateTime.Now;
        //        _objAccountingDbContext.AccountingTransactionEntryModel.Add(objTransactionEntryModel);
        //    }
        //    else
        //    {
        //       // objTransactionEntryModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objTransactionEntryModel.CreatedOn));
        //        objTransactionEntryModel.LastModifiedBy = UserID;
        //        objTransactionEntryModel.LastModifiedon = DateTime.Now;
        //        _objAccountingDbContext.AccountingTransactionEntryModel.Update(objTransactionEntryModel);
        //    }
        //    _objAccountingDbContext.SaveChanges();
        //    TransactionEntryID = objTransactionEntryModel.TransactionEntryID;
        //    return TransactionEntryID;
        //}

        public SumChartofAccountsViewModelList GetSumChartofAccountsViewModelList(int LegalEntityID, int AccountID, int FinancialYearID)
        {
            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x => x.FinancialYearID == FinancialYearID && x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.FinancialYearID == FinancialYearID && x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;
            return objSumChartofAccountsViewModelList;
        }
        public SumChartofAccountsViewModelList GetSumChartofAccountsViewModelListAll(int LegalEntityID, int AccountID, DateTime StartDate, DateTime EndDate)
        {
            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;
            return objSumChartofAccountsViewModelList;
        }
        public SumChartofAccountsViewModelList GetClosingBalanceChartofAccountsViewModelByStartDate(int LegalEntityID, int AccountID, DateTime StartDate)
        {
            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;
            return objSumChartofAccountsViewModelList;
        }
        public SumChartofAccountsViewModelList GetClosingBalanceTrialViewModelByStartDate(int LegalEntityID, int GroupID, int AccountID, DateTime StartDate)
        {
            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x => x.GroupID == GroupID &&
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.GroupID == GroupID &&
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.GroupID = GroupID;
            return objSumChartofAccountsViewModelList;
        }
        public SumChartofAccountsViewModelList Getsummonthlyprofitandlossclosing(int LegalEntityID, int MajorGroupID, int AccountID, string StartDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < Convert.ToDateTime(StartDate));
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;

            return objSumChartofAccountsViewModelList;
        }
        public SumChartofAccountsViewModelList Getsum_monthlyprofitandlossclosing(int LegalEntityID, int MajorGroupID, int AccountID, DateTime StartDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.AccountID == AccountID && x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSumChartofAccountsViewModelList.AccountID = AccountID;

            return objSumChartofAccountsViewModelList;
        }

        public SumChartofAccountsViewModelList GetClosingBalanceChartofAccountsViewModelByStartDate1(int LegalEntityID, int MajorGroupID, int Groupid, DateTime StartDate)
        {
            int[] Majorgroupid = { 72, 73, 74, 75 };

            OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
           x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.GroupID == Groupid && x.IsActive == true).FirstOrDefault();
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (objOpeningBalanceModel != null)
            {
                openDebit = objOpeningBalanceModel.Debit;
                openCredit = objOpeningBalanceModel.Credit;
            }

            SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
             x.LegalEntityID == LegalEntityID && Majorgroupid.Contains(MajorGroupID) && x.GroupID == Groupid && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objSumChartofAccountsViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSumChartofAccountsViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            return objSumChartofAccountsViewModelList;
        }

        public MajorGroupViewModelList GetClosingMajorGroupViewModelByStartDate(int LegalEntityID, int MajorGroupID, DateTime StartDate)
        {
            //OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.MajorGroupID == MajorGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            //decimal openDebit = 0;
            //decimal openCredit = 0;
            //if (objOpeningBalanceModel != null)
            //{
            //    openDebit = objOpeningBalanceModel.Debit;
            //    openCredit = objOpeningBalanceModel.Credit;
            //}
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
          //x.FinancialYearID == FinancialYearID &&
          x.MajorGroupID == MajorGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }
            MajorGroupViewModelList objMajorGroupViewModelList = new MajorGroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.MajorGroupID == MajorGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objMajorGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objMajorGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objMajorGroupViewModelList.MajorGroupID = MajorGroupID;
            return objMajorGroupViewModelList;
        }
        public MajorGroupViewModelList GetMajorGroupViewModelListAll(int LegalEntityID, int MajorGroupID, DateTime StartDate, DateTime EndDate)
        {
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.FinancialYearID == FinancialYearID &&
            x.MajorGroupID == MajorGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }

            MajorGroupViewModelList objMajorGroupViewModelList = new MajorGroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            //x.FinancialYearID == FinancialYearID && 
            x.MajorGroupID == MajorGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objMajorGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objMajorGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objMajorGroupViewModelList.MajorGroupID = MajorGroupID;
            return objMajorGroupViewModelList;
        }
        public SumChartofAccountsViewModelList GetClosingTrialBalanceAccountByStartDate(int LegalEntityID, int MajorGroupID, int GroupID, DateTime StartDate)
        {
            //OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            //decimal openDebit = 0;
            //decimal openCredit = 0;
            //if (objOpeningBalanceModel != null)
            //{
            //    openDebit = objOpeningBalanceModel.Debit;
            //    openCredit = objOpeningBalanceModel.Credit;
            //}
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x => x.MajorGroupID == MajorGroupID &&
           //x.FinancialYearID == FinancialYearID &&
           x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }
            SumChartofAccountsViewModelList objGroupViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.MajorGroupID == MajorGroupID &&
            x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objGroupViewModelList.GroupID = GroupID;
            return objGroupViewModelList;
        }
        public SumChartofAccountsViewModelList GetTrialBalanceAccountlListAll(int LegalEntityID, int MajorgroupID, int GroupID, DateTime StartDate, DateTime EndDate)
        {
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x => x.MajorGroupID == MajorgroupID &&
            //x.FinancialYearID == FinancialYearID &&
            x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }

            SumChartofAccountsViewModelList objGroupViewModelList = new SumChartofAccountsViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x => x.MajorGroupID == MajorgroupID &&
            //x.FinancialYearID == FinancialYearID && 
            x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objGroupViewModelList.GroupID = GroupID;
            return objGroupViewModelList;
        }

        public GroupViewModelList GetClosingGroupViewModelByStartDate(int LegalEntityID, int GroupID, DateTime StartDate)
        {
            //OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            //decimal openDebit = 0;
            //decimal openCredit = 0;
            //if (objOpeningBalanceModel != null)
            //{
            //    openDebit = objOpeningBalanceModel.Debit;
            //    openCredit = objOpeningBalanceModel.Credit;
            //}
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
           //x.FinancialYearID == FinancialYearID &&
           x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }
            GroupViewModelList objGroupViewModelList = new GroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objGroupViewModelList.GroupID = GroupID;
            return objGroupViewModelList;
        }
        public GroupViewModelList GetGroupComparisonByStartDate(int BusinessAreaID, int MajorGroupId, int GroupID, string StartDate)
        {
            //OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            //decimal openDebit = 0;
            //decimal openCredit = 0;
            //if (objOpeningBalanceModel != null)
            //{
            //    openDebit = objOpeningBalanceModel.Debit;
            //    openCredit = objOpeningBalanceModel.Credit;
            //}

            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
           //x.FinancialYearID == FinancialYearID &&
           x.GroupID == GroupID && x.ProfitCentreID  == BusinessAreaID && x.MajorGroupID == MajorGroupId && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }
            GroupViewModelList objGroupViewModelList = new GroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.GroupID == GroupID && x.ProfitCentreID  == BusinessAreaID && x.MajorGroupID == MajorGroupId && x.IsActive == true && Convert.ToDateTime(x.TransactionEntryDate.ToShortDateString()) < Convert.ToDateTime(StartDate));
            objGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objGroupViewModelList.GroupID = GroupID;
            return objGroupViewModelList;
        }
        public GroupViewModelList GetGroupViewModelListAll(int LegalEntityID, int GroupID, DateTime StartDate, DateTime EndDate)
        {
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.FinancialYearID == FinancialYearID &&
            x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }

            GroupViewModelList objGroupViewModelList = new GroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            //x.FinancialYearID == FinancialYearID && 
            x.GroupID == GroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objGroupViewModelList.GroupID = GroupID;
            return objGroupViewModelList;
        }
        public GroupViewModelList GetGroupComparisonListAll(int BusinessAreaID, int MajorGroupId, int GroupID, string StartDate, string EndDate)
        {
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.FinancialYearID == FinancialYearID &&
            x.GroupID == GroupID && x.ProfitCentreID   == BusinessAreaID  && x.MajorGroupID ==MajorGroupId && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }

            GroupViewModelList objGroupViewModelList = new GroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            //x.FinancialYearID == FinancialYearID && 
            x.GroupID == GroupID && x.ProfitCentreID  == BusinessAreaID && x.MajorGroupID ==MajorGroupId && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= Convert.ToDateTime(EndDate));
            objGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objGroupViewModelList.GroupID = GroupID;
            return objGroupViewModelList;
        }
        public SubGroupViewModelList GetClosingSubGroupViewModelByStartDate(int LegalEntityID, int SubGroupID, DateTime StartDate)
        {
            //OpeningBalanceModel objOpeningBalanceModel = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.SubGroupID == SubGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
            //decimal openDebit = 0;
            //decimal openCredit = 0;
            //if (objOpeningBalanceModel != null)
            //{
            //    openDebit = objOpeningBalanceModel.Debit;
            //    openCredit = objOpeningBalanceModel.Credit;
            //}
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.FinancialYearID == FinancialYearID &&
            x.SubGroupID == SubGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }
            SubGroupViewModelList objSubGroupViewModelList = new SubGroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            x.SubGroupID == SubGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) < StartDate);
            objSubGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSubGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSubGroupViewModelList.SubGroupID = SubGroupID;
            return objSubGroupViewModelList;
        }
        public SubGroupViewModelList GetSubGroupViewModelListAll(int LegalEntityID, int SubGroupID, DateTime StartDate, DateTime EndDate)
        {
            var AccountingOpeningBalancelist = _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x =>
            //x.FinancialYearID == FinancialYearID &&
            x.SubGroupID == SubGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true);
            decimal openDebit = 0;
            decimal openCredit = 0;
            if (AccountingOpeningBalancelist != null)
            {
                openDebit = AccountingOpeningBalancelist.Sum(x => x.Debit);
                openCredit = AccountingOpeningBalancelist.Sum(x => x.Credit);
            }

            SubGroupViewModelList objSubGroupViewModelList = new SubGroupViewModelList();
            var list = _objAccountingDbContext.AccountingTransactionEntryModel.Where(x =>
            //x.FinancialYearID == FinancialYearID && 
            x.SubGroupID == SubGroupID && x.LegalEntityID == LegalEntityID && x.IsActive == true && Convert.ToDateTime((x.TransactionEntryDate).ToShortDateString()) <= EndDate);
            objSubGroupViewModelList.Debit = list.Sum(x => x.Debit) + openDebit;
            objSubGroupViewModelList.Credit = list.Sum(x => x.Credit) + openCredit;
            objSubGroupViewModelList.SubGroupID = SubGroupID;
            return objSubGroupViewModelList;
        }
        public int SaveTransactionEntry(TransactionEntryDefinations TransactionEntryDefinations, int UserID)
        {
            int LegalEntityid = TransactionEntryDefinations.LegalEntityID;
            //int receiptDetailid = TransactionEntryDefinations.ReceiptDetailid;

            if (LegalEntityid != 0)
            {
                //List<LegalEntityProjectMappingModel> _Listdef = _objAccountingDbContext.AccountingLegalEntityProjectMappingModel.Where(x => x.LegalEntityID == LegalEntityid).ToList();
                //_objAccountingDbContext.AccountingLegalEntityProjectMappingModel.RemoveRange(_Listdef);
                //_objAccountingDbContext.SaveChanges();

                //TransactionHeadModel objTransactionHeadModel = new TransactionHeadModel();
                //objTransactionHeadModel.FinancialYearID = TransactionEntryDefinations.FinancialYearID;
                //objTransactionHeadModel.TransactionDate = DateTime.Now;
                //objTransactionHeadModel.CreatedBy = UserID;
                //objTransactionHeadModel.CreatedOn = DateTime.Now;
                //objTransactionHeadModel.IsActive = true;
                //_objAccountingDbContext.AccountingTransactionHeadModel.Add(objTransactionHeadModel);
                //_objAccountingDbContext.SaveChanges();
                //int TransactionHeadiD = objTransactionHeadModel.TransactionHeadID;

                TransactionHeadModel objTransactionHeadModel = _objAccountingDbContext.AccountingTransactionHeadModel.Where(x => x.TransactionHeadID == TransactionEntryDefinations.TransactionHeadID && x.IsActive == true).FirstOrDefault();
                if (objTransactionHeadModel != null)
                {
                    objTransactionHeadModel.TransactionTotal = TransactionEntryDefinations.TransactionTotal;
                    objTransactionHeadModel.DescriptionHead = TransactionEntryDefinations.DescriptionHead;
                    objTransactionHeadModel.TransactionDate = TransactionEntryDefinations.TransactionEntryDate;
                    objTransactionHeadModel.ReceiptDetailid = TransactionEntryDefinations.ReceiptDetailid;
                    objTransactionHeadModel.TransactionStatus = 1;
                    _objAccountingDbContext.AccountingTransactionHeadModel.Update(objTransactionHeadModel);
                    _objAccountingDbContext.SaveChanges();

                    List<TransactionEntryModel> _Listdef;
                    if (TransactionEntryDefinations.TransactionInputTypeID != 10)
                    {

                        _Listdef = (from t in TransactionEntryDefinations.TransactionEntryDetailDefinationsList
                                    select new TransactionEntryModel
                                    {
                                        TransactionEntryDate = Convert.ToDateTime(TransactionEntryDefinations.TransactionEntryDate.ToShortDateString()),// DateTime.Now,
                                        FinancialYearID = TransactionEntryDefinations.FinancialYearID,
                                        TransactionHeadID = objTransactionHeadModel.TransactionHeadID,
                                        TransactionRefNo = objTransactionHeadModel.TransactionRefNo,
                                        TransactionShortRefNo = objTransactionHeadModel.TransactionShortRefNo,
                                        TransactionInputTypeID = TransactionEntryDefinations.TransactionInputTypeID,
                                        CorporateGroupID = TransactionEntryDefinations.CorporateGroupID,
                                        BusinessGroupID = TransactionEntryDefinations.BusinessGroupID,
                                        VerticalGroupID = TransactionEntryDefinations.VerticalGroupID,
                                        LegalEntityID = TransactionEntryDefinations.LegalEntityID,
                                        Receiptdetailid=TransactionEntryDefinations.ReceiptDetailid,
                                        ProfitCentreID = TransactionEntryDefinations.ProfitCentreID,
                                        LocationID = TransactionEntryDefinations.LocationID,
                                        UnitID = TransactionEntryDefinations.UnitID,
                                        ProjectID = TransactionEntryDefinations.ProjectID,
                                        Debit = t.Debit,
                                        Credit = t.Credit,
                                        Description = t.Description,
                                        AccountID = t.AccountID,
                                        AccountTypeID = t.AccountTypeID,
                                        ParentCategoryID = t.ParentCategoryID,
                                        CategoryID = t.CategoryID,
                                        MajorGroupID = t.MajorGroupID,
                                        GroupID = t.GroupID,
                                        SubGroupID = t.SubGroupID,
                                        RowNo = t.RowNo,
                                        RowType = t.RowType,
                                        CashorBankAccountNo = t.CashorBankAccountNo,
                                        CreatedBy = UserID,
                                        CreatedOn = DateTime.Now,
                                        IsActive = true
                                      
                                        
                                    }).ToList();
                        //foreach (TransactionEntryModel objListdef in _Listdef)
                        //{
                        //    SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = GetSumChartofAccountsViewModelList(objListdef.LegalEntityID, objListdef.AccountID, objListdef.FinancialYearID);
                        //    decimal sumDebit = objSumChartofAccountsViewModelList.Debit;
                        //    decimal sumCredit = objSumChartofAccountsViewModelList.Credit;
                        //    objListdef.OpenCredit = sumCredit;
                        //    objListdef.OpenDebit = sumDebit;
                        //    objListdef.CloseCredit = sumCredit + objListdef.Credit;
                        //    objListdef.CloseDebit = sumDebit + objListdef.Debit;
                        //}
                        _objAccountingDbContext.AccountingTransactionEntryModel.AddRange(_Listdef);
                        _objAccountingDbContext.SaveChanges();
                    }

                    else if (TransactionEntryDefinations.TransactionInputTypeID == 10)
                    {
                        List<TransactionEntryModel> _Listdef1;

                        _Listdef1 = (from t in TransactionEntryDefinations.TransactionEntryDetailDefinationsList
                                     select new TransactionEntryModel
                                     {
                                         TransactionEntryDate = Convert.ToDateTime(TransactionEntryDefinations.TransactionEntryDate.ToShortDateString()),// DateTime.Now,
                                         FinancialYearID = TransactionEntryDefinations.FinancialYearID,
                                         TransactionHeadID = objTransactionHeadModel.TransactionHeadID,
                                         TransactionRefNo = objTransactionHeadModel.TransactionRefNo,
                                         TransactionShortRefNo = objTransactionHeadModel.TransactionShortRefNo,
                                         TransactionInputTypeID = TransactionEntryDefinations.TransactionInputTypeID,
                                         CorporateGroupID = TransactionEntryDefinations.CorporateGroupID,
                                         BusinessGroupID = TransactionEntryDefinations.BusinessGroupID,
                                         VerticalGroupID = TransactionEntryDefinations.VerticalGroupID,
                                         LegalEntityID = TransactionEntryDefinations.LegalEntityID,
                                         Receiptdetailid=TransactionEntryDefinations.ReceiptDetailid,
                                         ProfitCentreID = TransactionEntryDefinations.ProfitCentreID,
                                         LocationID = TransactionEntryDefinations.LocationID,
                                         UnitID = TransactionEntryDefinations.UnitID,
                                         ProjectID = TransactionEntryDefinations.ProjectID,
                                         Debit = t.Debit,
                                         Credit = t.Credit,
                                         Description = t.Description,
                                         AccountID = t.AccountID,
                                         AccountTypeID = t.AccountTypeID,
                                         ParentCategoryID = t.ParentCategoryID,
                                         CategoryID = t.CategoryID,
                                         MajorGroupID = t.MajorGroupID,
                                         GroupID = t.GroupID,
                                         SubGroupID = t.SubGroupID,
                                         RowNo = t.RowNo,
                                         RowType = t.RowType,
                                         CashorBankAccountNo = t.CashorBankAccountNo,
                                         CreatedBy = UserID,
                                         CreatedOn = DateTime.Now,
                                         IsActive = true
                                     }).ToList();
                        //foreach (TransactionEntryModel objListdef in _Listdef)
                        //{
                        //    SumChartofAccountsViewModelList objSumChartofAccountsViewModelList = GetSumChartofAccountsViewModelList(objListdef.LegalEntityID, objListdef.AccountID, objListdef.FinancialYearID);
                        //    decimal sumDebit = objSumChartofAccountsViewModelList.Debit;
                        //    decimal sumCredit = objSumChartofAccountsViewModelList.Credit;
                        //    objListdef.OpenCredit = sumCredit;
                        //    objListdef.OpenDebit = sumDebit;
                        //    objListdef.CloseCredit = sumCredit + objListdef.Credit;
                        //    objListdef.CloseDebit = sumDebit + objListdef.Debit;
                        //}
                        _objAccountingDbContext.AccountingTransactionEntryModel.AddRange(_Listdef1);
                        _objAccountingDbContext.SaveChanges();
                    }

                }
            }
            return LegalEntityid;
        }




        public int SaveTransactionEntryDeleteReceipt(TransactionEntryDefinations TransactionEntryDefinations, int UserID)
        {
            int LegalEntityid = TransactionEntryDefinations.LegalEntityID;
            int Receiptdetailid = TransactionEntryDefinations.ReceiptDetailid;


            // string strQry = "update Accounting_TransactionEntry set isac={0},LastModifiedOn=convert(varchar(10), GETDATE(),120 ),IsActive= 0 where TransactionHeadID ={1} ";
            string strQry = "update Accounting_TransactionEntry set IsStatus=0,LastModifiedOn=convert(varchar(10), GETDATE(),120 ),IsActive=0 where ReceiptDetailid ={0} ";
            try
            {
                using (var cmd = _objAccountingDbContext.Database.GetDbConnection().CreateCommand())
                {
                    cmd.CommandText = string.Format(strQry,Receiptdetailid);
                    _objAccountingDbContext.Database.OpenConnection();
                    cmd.ExecuteNonQuery();
                    _objAccountingDbContext.Database.CloseConnection();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //if (LegalEntityid != 0)
            //{

            //        List<TransactionEntryModel> _Listdef;


            //            _Listdef = (from t in TransactionEntryDefinations.TransactionEntryDetailDefinationsList
            //                        select new TransactionEntryModel
            //                        {
            //                           // TransactionEntryDate = Convert.ToDateTime(TransactionEntryDefinations.TransactionEntryDate.ToShortDateString()),// DateTime.Now,
            //                            LegalEntityID = TransactionEntryDefinations.LegalEntityID,
            //                            Receiptdetailid = TransactionEntryDefinations.ReceiptDetailid,
            //                            LastModifiedBy=100,
            //                            LastModifiedon= DateTime.Now,
            //                            IsActive = false
            //                        }).ToList();

            //            _objAccountingDbContext.AccountingTransactionEntryModel.AddRange(_Listdef);
            //            _objAccountingDbContext.SaveChanges();

            //}
            return LegalEntityid;
        }


        //For Trn_Entry Update
        public string UpdateTransactionEntry(string IsStatus, string HeadId)
        {

            // string strQry = "update Accounting_TransactionEntry set IsStatus={0} where TransactionHeadID ={1} ";
            string strQry = "update Accounting_TransactionEntry set IsStatus={0},LastModifiedOn=convert(varchar(10), GETDATE(),120 ),IsActive= 0 where TransactionHeadID ={1} ";
            try
            {
                using (var cmd = _objAccountingDbContext.Database.GetDbConnection().CreateCommand())
                {
                    cmd.CommandText = string.Format(strQry, IsStatus, HeadId);
                    _objAccountingDbContext.Database.OpenConnection();
                    cmd.ExecuteNonQuery();
                    _objAccountingDbContext.Database.CloseConnection();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "Success";
        }

        public List<TransactionEntryViewModelList> GetAllCashReceipts(int companyID, int LocationID)
        {
            return (from objTransaction_Entry in _objAccountingDbContext.AccountingTransactionEntryModel
                    where objTransaction_Entry.IsActive == true && objTransaction_Entry.LegalEntityID == companyID && objTransaction_Entry.LocationID == LocationID
                    select new TransactionEntryViewModelList
                    {
                        TransactionShortRefNo = objTransaction_Entry.TransactionShortRefNo,
                        TransactionEntryDate = DateTime.Parse(objTransaction_Entry.TransactionEntryDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture))
                    }).OrderBy(x => x.TransactionEntryDate).ToList();
        }

        


    }
}