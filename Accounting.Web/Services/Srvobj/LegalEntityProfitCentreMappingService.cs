﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ILegalEntityProfitCentreMappingService
    {
        List<LegalEntityProfitCentreMappingModel> GetAllLegalEntityProfitCentreMapping();
        List<LegalEntityProfitCentreMappingModel> GetAllLegalEntityProfitCentreMappingByLegalEntityID(int LegalEntityID);
        //void SaveLegalEntityProfitCentreMapping(LegalEntityProfitCentreMappingModel objLegalEntityProfitCentreMappingModel, int UserID);
        void SaveLegalEntityProfitCentreMapping(LegalEntityProfitCentreDefinations LegalEntityProfitCentreDefinations, int UserID);
        LegalEntityProfitCentreMappingModel GetLegalEntityProfitCentreMappingByID(int CategoryID);
        int DeleteLegalEntityProfitCentreMapping(int CategoryID, int UserID);

        List<LegalEntityProfitCentreMappingViewModelList> GetAllLegalEntityProfitCentreMappingViewModelList(int LegalEntityID);
    }
    public class LegalEntityProfitCentreMappingService : ILegalEntityProfitCentreMappingService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public LegalEntityProfitCentreMappingService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteLegalEntityProfitCentreMapping(int LegalEntityID, int UserID)
        {
            LegalEntityProfitCentreMappingModel objLegalEntityProfitCentreMappingModel = _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.Where(x => x.LegalEntityID == LegalEntityID).FirstOrDefault();
            objLegalEntityProfitCentreMappingModel.IsActive = false;
            _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.Update(objLegalEntityProfitCentreMappingModel);
            _objAccountingDbContext.SaveChanges();
            return objLegalEntityProfitCentreMappingModel.LegalEntityID;
        }

        public List<LegalEntityProfitCentreMappingModel> GetAllLegalEntityProfitCentreMappingByLegalEntityID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        }
        public List<LegalEntityProfitCentreMappingModel> GetAllLegalEntityProfitCentreMapping()
        {
            return _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.Where(x => x.IsActive == true).ToList();
        }
        public LegalEntityProfitCentreMappingModel GetLegalEntityProfitCentreMappingByID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.Where(x => x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
        }
        public void SaveLegalEntityProfitCentreMapping(LegalEntityProfitCentreDefinations LegalEntityProfitCentreDefinations, int UserID)
        {
            int LegalEntityid = LegalEntityProfitCentreDefinations.LegalEntityID;

            if(LegalEntityid!=0)
            {
                List<LegalEntityProfitCentreMappingModel> _Listdef = _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.Where(x => x.LegalEntityID == LegalEntityid).ToList();
                _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.RemoveRange(_Listdef);
                _objAccountingDbContext.SaveChanges();

                _Listdef = (from t in LegalEntityProfitCentreDefinations.ProfitCentreList
                                 select new LegalEntityProfitCentreMappingModel
                                 {
                                     ProfitCentreID = t.ProfitCentreID,
                                     CreatedBy = UserID,
                                     CreatedOn = DateTime.Now,
                                     IsActive = true,
                                     LegalEntityID = LegalEntityid
                                 }).ToList();
                _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.AddRange(_Listdef);
                _objAccountingDbContext.SaveChanges();

            }

        }
        public List<LegalEntityProfitCentreMappingViewModelList> GetAllLegalEntityProfitCentreMappingViewModelList(int LegalEntityID)
        {
            if (LegalEntityID != 0)
            {
                var objLegalEntityProfitCentreMappingViewModelList = (from objAccountingLegalEntityProfitCentreMappingModel in _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel
                                                  join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityProfitCentreMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                  join objAccountingProfitCentreModel in _objAccountingDbContext.AccountingProfitCentreModel on objAccountingLegalEntityProfitCentreMappingModel.ProfitCentreID equals objAccountingProfitCentreModel.ProfitCentreID
                                                  where objAccountingLegalEntityProfitCentreMappingModel.LegalEntityID == LegalEntityID
                                                  select new LegalEntityProfitCentreMappingViewModelList
                                                  {
                                                      LegalEntityID = objAccountingEntityMasterModel.EntityID,
                                                      LegalEntityName = objAccountingEntityMasterModel.Name,
                                                      ProfitCentreID = objAccountingProfitCentreModel.ProfitCentreID,
                                                      ProfitCentreName = objAccountingProfitCentreModel.ProfitCentreName
                                                  }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ToList();
                return objLegalEntityProfitCentreMappingViewModelList;
            }
            else
            {
                var objLegalEntityProfitCentreMappingViewModelList = (from objAccountingLegalEntityProfitCentreMappingModel in _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel
                                                  join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityProfitCentreMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                  join objAccountingProfitCentreModel in _objAccountingDbContext.AccountingProfitCentreModel on objAccountingLegalEntityProfitCentreMappingModel.ProfitCentreID equals objAccountingProfitCentreModel.ProfitCentreID
                                                  //where objAccountingLegalEntityProfitCentreMappingModel.LegalEntityID == LegalEntityID
                                                  select new LegalEntityProfitCentreMappingViewModelList
                                                  {
                                                      LegalEntityID = objAccountingEntityMasterModel.EntityID,
                                                      LegalEntityName = objAccountingEntityMasterModel.Name,
                                                      ProfitCentreID = objAccountingProfitCentreModel.ProfitCentreID,
                                                      ProfitCentreName = objAccountingProfitCentreModel.ProfitCentreName
                                                  }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ToList();
                
                return objLegalEntityProfitCentreMappingViewModelList;
            }
        }
        public void SaveLegalEntityProfitCentreMapping(LegalEntityProfitCentreMappingModel objLegalEntityProfitCentreMappingModel, int UserID)
        {
            //if (objLegalEntityProfitCentreMappingModel.CategoryID == 0)
            //{
            //    objLegalEntityProfitCentreMappingModel.IsActive = true;
            //    objLegalEntityProfitCentreMappingModel.CreatedBy = UserID;
            //    objLegalEntityProfitCentreMappingModel.CreatedOn = DateTime.Now;
            //    _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.Add(objLegalEntityProfitCentreMappingModel);
            //}
            //else
            //{
            //   // objLegalEntityProfitCentreMappingModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objLegalEntityProfitCentreMappingModel.CreatedOn));
            //    objLegalEntityProfitCentreMappingModel.LastModifiedBy = UserID;
            //    objLegalEntityProfitCentreMappingModel.LastModifiedon = DateTime.Now;
            //    _objAccountingDbContext.AccountingLegalEntityProfitCentreMappingModel.Update(objLegalEntityProfitCentreMappingModel);
            //}
            _objAccountingDbContext.SaveChanges();
        }
    }
}
