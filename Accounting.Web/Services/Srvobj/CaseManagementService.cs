﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ICaseManagementService
    {
        List<CaseManagementModel> GetAllCase();
        CaseManagementModel GetCaseByID(int ID);
        void SaveCase(CaseManagementModel objCaseManagementModel,int _UserID);

        void Deletecase(int ID, int _UserID);

    }
    public class CaseManagementService : ICaseManagementService
    {
        private readonly CDbcontext _objcertaDbContext;
        public CaseManagementService(CDbcontext objcertaDbContext) { _objcertaDbContext = objcertaDbContext; }

        public List<CaseManagementModel> GetAllCase()
        {
            return _objcertaDbContext.AccountingCaseManagement.Where(x => x.IsActive == true).ToList();
        }
        public CaseManagementModel GetCaseByID(int ID)
        {
            return _objcertaDbContext.AccountingCaseManagement.Where(x => x.IsActive == true && x.CaseID == ID).FirstOrDefault();
        }
        public void SaveCase(CaseManagementModel objCaseManagementModel, int _UserID)
        {

            if (objCaseManagementModel.CaseID == 0)
            {
                objCaseManagementModel.CreatedOn = DateTime.Now;
                objCaseManagementModel.IsActive = true;
                objCaseManagementModel.CreatedBy = _UserID;
                objCaseManagementModel.LastModifiedBy = 0;
                _objcertaDbContext.AccountingCaseManagement.Add(objCaseManagementModel);
            }
            else
            {
                objCaseManagementModel.LastModifiedOn = DateTime.Now;
                objCaseManagementModel.LastModifiedBy = _UserID;
                _objcertaDbContext.AccountingCaseManagement.Update(objCaseManagementModel);
            }
            _objcertaDbContext.SaveChanges();
        }

        public void Deletecase(int ID, int _UserID)
        {
            CaseManagementModel objCaseManagementModel = _objcertaDbContext.AccountingCaseManagement.Where(x => x.CaseID == ID).FirstOrDefault();
            objCaseManagementModel.IsActive = false;
            _objcertaDbContext.AccountingCaseManagement.Update(objCaseManagementModel);
            _objcertaDbContext.SaveChanges();
        }
    }
}
