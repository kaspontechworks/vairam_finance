﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ILegalEntityUnitMappingService
    {
        List<LegalEntityUnitMappingModel> GetAllLegalEntityUnitMapping();
        List<LegalEntityUnitMappingModel> GetAllLegalEntityUnitMappingByLegalEntityID(int LegalEntityID);
        List<LegalEntityUnitMappingModel> GetAllLegalEntityUnitMappingByLegalEntityIDProfitCentreIDLocationID(int LegalEntityID,int ProfitCentreID,int LocationID);
        
        //void SaveLegalEntityUnitMapping(LegalEntityUnitMappingModel objLegalEntityUnitMappingModel, int UserID);
        void SaveLegalEntityUnitMapping(LegalEntityUnitDefinations LegalEntityUnitDefinations, int UserID);
        LegalEntityUnitMappingModel GetLegalEntityUnitMappingByID(int CategoryID);
        int DeleteLegalEntityUnitMapping(int CategoryID, int UserID);

        List<LegalEntityUnitMappingViewModelList> GetAllLegalEntityUnitMappingViewModelList(int LegalEntityID);
        List<LegalEntityUnitMappingViewModelList> GetAllLegalEntityUnitMappingViewModelListByLegalEntityIDProfitCentreIDLocationID(int LegalEntityID, int ProfitCentreID, int LocationID);
    }
    public class LegalEntityUnitMappingService : ILegalEntityUnitMappingService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public LegalEntityUnitMappingService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteLegalEntityUnitMapping(int LegalEntityID, int UserID)
        {
            LegalEntityUnitMappingModel objLegalEntityUnitMappingModel = _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Where(x => x.LegalEntityID == LegalEntityID).FirstOrDefault();
            objLegalEntityUnitMappingModel.IsActive = false;
            _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Update(objLegalEntityUnitMappingModel);
            _objAccountingDbContext.SaveChanges();
            return objLegalEntityUnitMappingModel.LegalEntityID;
        }

        public List<LegalEntityUnitMappingModel> GetAllLegalEntityUnitMappingByLegalEntityID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        }

        public List<LegalEntityUnitMappingModel> GetAllLegalEntityUnitMappingByLegalEntityIDProfitCentreIDLocationID(int LegalEntityID, int ProfitCentreID, int LocationID)
        {
            return _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID && x.ProfitCentreID == ProfitCentreID && x.LocationID == LocationID).ToList();
        }
        public List<LegalEntityUnitMappingModel> GetAllLegalEntityUnitMapping()
        {
            return _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Where(x => x.IsActive == true).ToList();
        }
        public LegalEntityUnitMappingModel GetLegalEntityUnitMappingByID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Where(x => x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
        }
        public void SaveLegalEntityUnitMapping(LegalEntityUnitDefinations LegalEntityUnitDefinations, int UserID)
        {
            int LegalEntityid = LegalEntityUnitDefinations.LegalEntityID;
            int ProfitCentreID = LegalEntityUnitDefinations.ProfitCentreID;
            int LocationID = LegalEntityUnitDefinations.LocationID;
            if (LegalEntityid!=0)
            {
                List<LegalEntityUnitMappingModel> _Listdef = _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Where(x => x.LegalEntityID == LegalEntityid && x.ProfitCentreID == ProfitCentreID && x.LocationID == LocationID).ToList();
                _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.RemoveRange(_Listdef);
                _objAccountingDbContext.SaveChanges();

                _Listdef = (from t in LegalEntityUnitDefinations.UnitList
                                 select new LegalEntityUnitMappingModel
                                 {
                                     UnitID = t.UnitID,
                                     CreatedBy = UserID,
                                     CreatedOn = DateTime.Now,
                                     IsActive = true,
                                     LocationID = LocationID,
                                     ProfitCentreID = ProfitCentreID,
                                     LegalEntityID = LegalEntityid
                                 }).ToList();
                _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.AddRange(_Listdef);
                _objAccountingDbContext.SaveChanges();

            }

        }
        public List<LegalEntityUnitMappingViewModelList> GetAllLegalEntityUnitMappingViewModelList(int LegalEntityID)
        {
            if (LegalEntityID != 0)
            {
                var objLegalEntityUnitMappingViewModelList = (from objAccountingLegalEntityUnitMappingModel in _objAccountingDbContext.AccountingLegalEntityUnitMappingModel
                                                  //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityUnitMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                  //join objAccountingUnitModel in _objAccountingDbContext.AccountingUnitModel on objAccountingLegalEntityUnitMappingModel.UnitID equals objAccountingUnitModel.UnitID
                                                  where objAccountingLegalEntityUnitMappingModel.LegalEntityID == LegalEntityID
                                                  select new LegalEntityUnitMappingViewModelList
                                                  {
                                                      LegalEntityID = objAccountingLegalEntityUnitMappingModel.LegalEntityID,
                                                      LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityUnitMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                      ProfitCentreID = objAccountingLegalEntityUnitMappingModel.ProfitCentreID,
                                                      ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityUnitMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                      LocationID = objAccountingLegalEntityUnitMappingModel.LocationID,
                                                      LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityUnitMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                      UnitID = objAccountingLegalEntityUnitMappingModel.UnitID,
                                                      UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingLegalEntityUnitMappingModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName
                                                  }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ThenBy(x => x.UnitName).ToList();
                return objLegalEntityUnitMappingViewModelList;
            }
            else
            {
                var objLegalEntityUnitMappingViewModelList = (from objAccountingLegalEntityUnitMappingModel in _objAccountingDbContext.AccountingLegalEntityUnitMappingModel
                                                  //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityUnitMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                  //join objAccountingUnitModel in _objAccountingDbContext.AccountingUnitModel on objAccountingLegalEntityUnitMappingModel.UnitID equals objAccountingUnitModel.UnitID
                                                  //where objAccountingLegalEntityUnitMappingModel.LegalEntityID == LegalEntityID
                                                  select new LegalEntityUnitMappingViewModelList
                                                  {
                                                      LegalEntityID = objAccountingLegalEntityUnitMappingModel.LegalEntityID,
                                                      LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityUnitMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                      ProfitCentreID = objAccountingLegalEntityUnitMappingModel.ProfitCentreID,
                                                      ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityUnitMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                      LocationID = objAccountingLegalEntityUnitMappingModel.LocationID,
                                                      LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityUnitMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                      UnitID = objAccountingLegalEntityUnitMappingModel.UnitID,
                                                      UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingLegalEntityUnitMappingModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName
                                                  }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ThenBy(x => x.UnitName).ToList();

                return objLegalEntityUnitMappingViewModelList;
            }
        }
        public List<LegalEntityUnitMappingViewModelList> GetAllLegalEntityUnitMappingViewModelListByLegalEntityIDProfitCentreIDLocationID(int LegalEntityID,int ProfitCentreID,int LocationID)
        {

            var objLegalEntityUnitMappingViewModelList = (from objAccountingLegalEntityUnitMappingModel in _objAccountingDbContext.AccountingLegalEntityUnitMappingModel
                                                              //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityUnitMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                              //join objAccountingUnitModel in _objAccountingDbContext.AccountingUnitModel on objAccountingLegalEntityUnitMappingModel.UnitID equals objAccountingUnitModel.UnitID
                                                          where objAccountingLegalEntityUnitMappingModel.LegalEntityID == LegalEntityID && objAccountingLegalEntityUnitMappingModel.ProfitCentreID == ProfitCentreID && objAccountingLegalEntityUnitMappingModel.LocationID == LocationID
                                                          select new LegalEntityUnitMappingViewModelList
                                                          {
                                                              LegalEntityID = objAccountingLegalEntityUnitMappingModel.LegalEntityID,
                                                              LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityUnitMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                              ProfitCentreID = objAccountingLegalEntityUnitMappingModel.ProfitCentreID,
                                                              ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityUnitMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                              LocationID = objAccountingLegalEntityUnitMappingModel.LocationID,
                                                              LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityUnitMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                              UnitID = objAccountingLegalEntityUnitMappingModel.UnitID,
                                                              UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objAccountingLegalEntityUnitMappingModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName
                                                          }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ThenBy(x => x.UnitName).ToList();
            return objLegalEntityUnitMappingViewModelList;

        }
            public void SaveLegalEntityUnitMapping(LegalEntityUnitMappingModel objLegalEntityUnitMappingModel, int UserID)
        {
            //if (objLegalEntityUnitMappingModel.CategoryID == 0)
            //{
            //    objLegalEntityUnitMappingModel.IsActive = true;
            //    objLegalEntityUnitMappingModel.CreatedBy = UserID;
            //    objLegalEntityUnitMappingModel.CreatedOn = DateTime.Now;
            //    _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Add(objLegalEntityUnitMappingModel);
            //}
            //else
            //{
            //   // objLegalEntityUnitMappingModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objLegalEntityUnitMappingModel.CreatedOn));
            //    objLegalEntityUnitMappingModel.LastModifiedBy = UserID;
            //    objLegalEntityUnitMappingModel.LastModifiedon = DateTime.Now;
            //    _objAccountingDbContext.AccountingLegalEntityUnitMappingModel.Update(objLegalEntityUnitMappingModel);
            //}
            _objAccountingDbContext.SaveChanges();
        }
    }
}
