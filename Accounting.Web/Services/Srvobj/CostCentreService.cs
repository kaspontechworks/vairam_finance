﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ICostCentreService
    {
        List<CostCentreModel> GetAllCostCentre();
        List<CostCentreModel> GetAllCostCentreByLegalEntityID(int LegalEntityID);
        void SaveCostCentre(CostCentreModel objCostCentreModel, int UserID);
        CostCentreModel GetCostCentreByID(int CostCentreID);
        int DeleteCostCentre(int CostCentreID, int UserID);
    }
    public class CostCentreService : ICostCentreService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public CostCentreService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteCostCentre(int CostCentreID, int UserID)
        {
            CostCentreModel objCostCentreModel = _objAccountingDbContext.AccountingCostCentreModel.Where(x => x.CostCentreID == CostCentreID).FirstOrDefault();
            objCostCentreModel.IsActive = false;
            _objAccountingDbContext.AccountingCostCentreModel.Update(objCostCentreModel);
            _objAccountingDbContext.SaveChanges();
            return objCostCentreModel.CostCentreID;
        }

        public List<CostCentreModel> GetAllCostCentreByLegalEntityID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingCostCentreModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        }
        public List<CostCentreModel> GetAllCostCentre()
        {
            return _objAccountingDbContext.AccountingCostCentreModel.Where(x => x.IsActive == true).ToList();
        }
        public CostCentreModel GetCostCentreByID(int CostCentreID)
        {
            return _objAccountingDbContext.AccountingCostCentreModel.Where(x => x.CostCentreID == CostCentreID && x.IsActive == true).FirstOrDefault();
        }

        public void SaveCostCentre(CostCentreModel objCostCentreModel, int UserID)
        {
            if (objCostCentreModel.CostCentreID == 0)
            {
                objCostCentreModel.IsActive = true;
                objCostCentreModel.CreatedBy = UserID;
                objCostCentreModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingCostCentreModel.Add(objCostCentreModel);
            }
            else
            {
               // objCostCentreModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objCostCentreModel.CreatedOn));
                objCostCentreModel.LastModifiedBy = UserID;
                objCostCentreModel.LastModifiedon = DateTime.Now;
                _objAccountingDbContext.AccountingCostCentreModel.Update(objCostCentreModel);
            }
            _objAccountingDbContext.SaveChanges();
        }
    }
}
