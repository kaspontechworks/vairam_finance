﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IFinancialYearService
    {
        List<FinancialYearModel> GetAllFinancialYear();
       // List<FinancialYearModel> GetAllFinancialYearByLegalEntityID(int LegalEntityID);
        void SaveFinancialYear(FinancialYearModel objFinancialYearModel, int UserID);
        FinancialYearModel GetFinancialYearByID(int FinancialYearID);
        FinancialYearModel GetFinancialYearByDate(DateTime Date);
        int DeleteFinancialYear(int FinancialYearID, int UserID);
    }
    public class FinancialYearService : IFinancialYearService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public FinancialYearService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteFinancialYear(int FinancialYearID, int UserID)
        {
            FinancialYearModel objFinancialYearModel = _objAccountingDbContext.AccountingFinancialYearModel.Where(x => x.FinancialYearID == FinancialYearID).FirstOrDefault();
            objFinancialYearModel.IsActive = false;
            _objAccountingDbContext.AccountingFinancialYearModel.Update(objFinancialYearModel);
            _objAccountingDbContext.SaveChanges();
            return objFinancialYearModel.FinancialYearID;
        }

        //public List<FinancialYearModel> GetAllFinancialYearByLegalEntityID(int LegalEntityID)
        //{
        //    return _objAccountingDbContext.AccountingFinancialYearModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        //}
        public List<FinancialYearModel> GetAllFinancialYear()
        {
            return _objAccountingDbContext.AccountingFinancialYearModel.Where(x => x.IsActive == true).ToList();
        
        }
        public FinancialYearModel GetFinancialYearByID(int FinancialYearID)
        {
            return _objAccountingDbContext.AccountingFinancialYearModel.Where(x => x.FinancialYearID == FinancialYearID && x.IsActive == true).FirstOrDefault();
        }
        public FinancialYearModel GetFinancialYearByDate(DateTime Date)
        {
            return _objAccountingDbContext.AccountingFinancialYearModel.Where(x => x.StartDate<= Date && x.EndDate >= Date && x.IsActive == true).FirstOrDefault();
        }

        public void SaveFinancialYear(FinancialYearModel objFinancialYearModel, int UserID)
        {
            if (objFinancialYearModel.FinancialYearID == 0)
            {
                objFinancialYearModel.IsActive = true;
                objFinancialYearModel.CreatedBy = UserID;
                objFinancialYearModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingFinancialYearModel.Add(objFinancialYearModel);
            }
            else
            {
                // objFinancialYearModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objFinancialYearModel.CreatedOn));
                objFinancialYearModel.LastModifiedBy = UserID;
                objFinancialYearModel.LastModifiedon = DateTime.Now;
                UpdateFinancialYrEntries(objFinancialYearModel);
               objFinancialYearModel.IsActive = true;
                _objAccountingDbContext.AccountingFinancialYearModel.Update(objFinancialYearModel);

            }
            _objAccountingDbContext.SaveChanges();
        }

        public void UpdateFinancialYrEntries(FinancialYearModel ObjModel)
        {
            int IsActiveNot = ObjModel.IsActive == false ? 0 : 1;
            string strQry = "";
            strQry = "update Accounting_TransactionEntry set IsActive={0} where TransactionEntryDate between " + "'" + "{1} " + "'" + "and" + "'" + "{2} " + "'" + "and TransactionInputTypeID=10";
            try
            {
                using (var cmd = _objAccountingDbContext.Database.GetDbConnection().CreateCommand())
                {
                    cmd.CommandText = string.Format(strQry, IsActiveNot, ObjModel.StartDate.ToString("yyyy-MM-dd"), ObjModel.EndDate.ToString("yyyy-MM-dd"));
                    _objAccountingDbContext.Database.OpenConnection();
                    cmd.ExecuteNonQuery();
                    _objAccountingDbContext.Database.CloseConnection();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //
        }
    }
}
