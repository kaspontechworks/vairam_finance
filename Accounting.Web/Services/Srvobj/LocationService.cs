﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ILocationService
    {
        List<LocationModel> GetAllLocation();
       // List<LocationModel> GetAllLocationByLegalEntityID(int LegalEntityID);
        void SaveLocation(LocationModel objLocationModel, int UserID);
        LocationModel GetLocationByID(int LocationID);
        int DeleteLocation(int LocationID, int UserID);
    }
    public class LocationService : ILocationService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public LocationService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteLocation(int LocationID, int UserID)
        {
            LocationModel objLocationModel = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == LocationID).FirstOrDefault();
            objLocationModel.IsActive = false;
            _objAccountingDbContext.AccountingLocationModel.Update(objLocationModel);
            _objAccountingDbContext.SaveChanges();
            return objLocationModel.LocationID;
        }

        //public List<LocationModel> GetAllLocationByLegalEntityID(int LegalEntityID)
        //{
        //    return _objAccountingDbContext.AccountingLocationModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        //}
        public List<LocationModel> GetAllLocation()
        {
            return _objAccountingDbContext.AccountingLocationModel.Where(x => x.IsActive == true).ToList();
        }
        public LocationModel GetLocationByID(int LocationID)
        {
            return _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == LocationID && x.IsActive == true).FirstOrDefault();
        }

        public void SaveLocation(LocationModel objLocationModel, int UserID)
        {
            if (objLocationModel.LocationID == 0)
            {
                objLocationModel.IsActive = true;
                objLocationModel.CreatedBy = UserID;
                objLocationModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingLocationModel.Add(objLocationModel);
            }
            else
            {
               // objLocationModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objLocationModel.CreatedOn));
                objLocationModel.LastModifiedBy = UserID;
                objLocationModel.LastModifiedon = DateTime.Now;
                _objAccountingDbContext.AccountingLocationModel.Update(objLocationModel);
            }
            _objAccountingDbContext.SaveChanges();
        }
    }
}
