﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ITransactionHeadService
    {
        List<TransactionHeadModel> GetAllTransactionHead();
       // List<TransactionHeadModel> GetAllTransactionHeadByLegalEntityID(int LegalEntityID);
        int SaveTransactionHead(TransactionHeadModel objTransactionHeadModel, int UserID);
        // int SaveTransactionHeadToLms(TransactionHeadModel objTransactionHeadModel, int UserID);
        //List<TransactionHeadModel> GetAllTransactionHead_TransactionID(int TransactionInputTypeID,int LegalEntityID);
        TransactionHeadModel GetTransactionHeadByID(int TransactionHeadID);
        List<TransactionHeadViewModelList> GetAllTransactionHeadViewModelList(int TransactionInputTypeID, int LegalEntityID, int ProfitCentreID, int LocationID, int UnitID, int ProjectID);
        TransactionHeadViewModelList GetAllTransactionHeadViewModelListByTransactionHeadID(int TransactionHeadID);
        TransactionHeadViewModelCode GetRefNoTransactionHeadModel(int TransactionInputTypeID, int LegalEntityID, int ProfitCentreID, int LocationID, int UnitID, int ProjectID, int FinancialYearID);

        
    }
    public class TransactionHeadService : ITransactionHeadService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public TransactionHeadService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        //public List<TransactionHeadModel> GetAllTransactionHead_TransactionID(int TransactionInputTypeID, int LegalEntityID)
        //{
        //    return _objAccountingDbContext.AccountingTransactionHeadModel.Where(x => x.TransactionInputTypeID == TransactionInputTypeID && x.LegalEntityID==LegalEntityID && x.IsActive == true).ToList();
        //}
        public List<TransactionHeadModel> GetAllTransactionHead()
        {
            return _objAccountingDbContext.AccountingTransactionHeadModel.Where(x => x.IsActive == true).ToList();
        }
        public TransactionHeadModel GetTransactionHeadByID(int TransactionHeadID)
        {
            return _objAccountingDbContext.AccountingTransactionHeadModel.Where(x => x.TransactionHeadID == TransactionHeadID && x.IsActive == true).FirstOrDefault();
        }
        public int SaveTransactionHead(TransactionHeadModel objTransactionHeadModel, int UserID)
        {
            int TransactionInputTypeID = objTransactionHeadModel.TransactionInputTypeID;
            int LegalEntityid = objTransactionHeadModel.LegalEntityID;
            int ProfitCentreID = objTransactionHeadModel.ProfitCentreID;
            int LocationID = objTransactionHeadModel.LocationID;
            int UnitID = objTransactionHeadModel.UnitID;
            int ProjectID = objTransactionHeadModel.ProjectID;
            int FinancialYearID = objTransactionHeadModel.FinancialYearID;
           // int ReceiptDetailid = objTransactionHeadModel.ReceiptDetailid;

            string TransactionInputTypeslcode = "";
            string LegalEntityslcode = "";
            string ProfitCentreslcode = "";
            string Locationslcode = "";
            string Unitslcode = "";
            string Projectslcode = "";
            string TransactionRefNo = "";
            string TransactionShortRefNo = "";
            int SlNo = 1;
            TransactionHeadViewModelCode objTransactionHeadViewModelCode = GetRefNoTransactionHeadModel(TransactionInputTypeID, LegalEntityid, ProfitCentreID, LocationID, UnitID, ProjectID, FinancialYearID);
            if (objTransactionHeadViewModelCode != null)
            {

                TransactionInputTypeslcode = objTransactionHeadViewModelCode.TransactionInputTypeslcode;
                LegalEntityslcode = objTransactionHeadViewModelCode.LegalEntityslcode;
                ProfitCentreslcode = objTransactionHeadViewModelCode.ProfitCentreslcode;
                Locationslcode = objTransactionHeadViewModelCode.Locationslcode;
                Unitslcode = objTransactionHeadViewModelCode.Unitslcode;
                Projectslcode = objTransactionHeadViewModelCode.Projectslcode;
                TransactionRefNo = objTransactionHeadViewModelCode.TransactionRefNo;
                TransactionShortRefNo = objTransactionHeadViewModelCode.TransactionShortRefNo;
                SlNo = objTransactionHeadViewModelCode.SlNo;
            }
            int TransactionHeadID = 0;
            if (LegalEntityid != 0)
            {
                    objTransactionHeadModel.TransactionInputTypeslcode = TransactionInputTypeslcode;
                    objTransactionHeadModel.LegalEntityslcode = LegalEntityslcode;
                    objTransactionHeadModel.ProfitCentreslcode = ProfitCentreslcode;
                    objTransactionHeadModel.Locationslcode = Locationslcode;
                    objTransactionHeadModel.Unitslcode = Unitslcode;
                    objTransactionHeadModel.Projectslcode = Projectslcode;
                    objTransactionHeadModel.TransactionRefNo = TransactionRefNo;
                objTransactionHeadModel.TransactionShortRefNo = TransactionShortRefNo;
                objTransactionHeadModel.SlNo = SlNo;
                objTransactionHeadModel.CreatedBy = UserID;
                objTransactionHeadModel.CreatedOn = DateTime.Now;
                objTransactionHeadModel.IsActive = true;
                objTransactionHeadModel.TransactionStatus = 0;
               // objTransactionHeadModel.ReceiptDetailid = ReceiptDetailid;
                _objAccountingDbContext.AccountingTransactionHeadModel.Add(objTransactionHeadModel);
                _objAccountingDbContext.SaveChanges();
                TransactionHeadID = objTransactionHeadModel.TransactionHeadID;
            }

            return TransactionHeadID;
        }
        public TransactionHeadViewModelCode GetRefNoTransactionHeadModel( int TransactionInputTypeID, int LegalEntityID, int ProfitCentreID, int LocationID, int UnitID, int ProjectID, int FinancialYearID)
        {
            string refNo = "";
            string ShortRefNo = "";
            string TransactionInputTypeslcode = "";
            if (TransactionInputTypeID == 1)
            {
                TransactionInputTypeslcode = "CRCT";//"CSHR"; //"Cash receipts";
            }
            else if (TransactionInputTypeID == 2)
            {
                TransactionInputTypeslcode = "CPAY";//"CSHP";//"Cash payments";
            }
            else if (TransactionInputTypeID == 3)
            {
                TransactionInputTypeslcode = "BRCT";//"BNKR";// "Bank receipts";
            }
            else if (TransactionInputTypeID == 4)
            {
                TransactionInputTypeslcode = "BPAY";//"BNKP";//"Bank Payments";
            }
            else if (TransactionInputTypeID == 5)
            {
                TransactionInputTypeslcode = "JV";//"JORN"; //"Journal entries";
            }
            else if (TransactionInputTypeID == 10)
            {
                TransactionInputTypeslcode = "CE";//"JORN"; //"Closing entries";
            }

            //if (refNo != "")
            //{
            //    refNo = refNo + "/" + TransactionInputTypeslcode;
            //}
            //else
            //{
                refNo = TransactionInputTypeslcode;
            ShortRefNo = TransactionInputTypeslcode;
            //}

            string LegalEntityslcode = "";
           
            if (LegalEntityID != 0)
            {
                EntityMasterModel objEntityMasterModel = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == LegalEntityID).FirstOrDefault();
                if (objEntityMasterModel != null)
                {
                    LegalEntityslcode = objEntityMasterModel.LegalEntityslcode;
                    if (refNo != "")
                    {
                        refNo = refNo +"/"+ LegalEntityslcode;
                    }
                    else
                    {
                        refNo = LegalEntityslcode;
                    }
                    if (ShortRefNo != "")
                    {
                        ShortRefNo = ShortRefNo + "/" + LegalEntityslcode;
                    }
                    else
                    {
                        ShortRefNo = LegalEntityslcode;
                    }
                    
                }
            }

            string ProfitCentreslcode = "";
            if (ProfitCentreID != 0)
            {
                ProfitCentreModel objProfitCentreModel = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == ProfitCentreID).FirstOrDefault();
                if (objProfitCentreModel != null)
                {
                    ProfitCentreslcode = objProfitCentreModel.ProfitCentreslcode;
                    if (refNo != "")
                    {
                        refNo = refNo + "/" + ProfitCentreslcode;
                    }
                    else
                    {
                        refNo = ProfitCentreslcode;
                    }
                }
            }
            string Locationslcode = "";
            if (LocationID != 0)
            {
                LocationModel objLocationModel = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == LocationID).FirstOrDefault();
                if (objLocationModel != null)
                {
                    Locationslcode = objLocationModel.Locationslcode;
                    if (refNo != "")
                    {
                        refNo = refNo + "/" + Locationslcode;
                    }
                    else
                    {
                        refNo = Locationslcode;
                    }
                }
            }
            string Unitslcode = "";
            if (UnitID != 0)
            {
                UnitModel objUnitModel = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == UnitID).FirstOrDefault();
                if (objUnitModel != null)
                {
                    Unitslcode = objUnitModel.Unitslcode;
                    if (refNo != "")
                    {
                        refNo = refNo + "/" + Unitslcode;
                    }
                    else
                    {
                        refNo = Unitslcode;
                    }
                }
            }
            string Projectslcode = "";
            if (ProjectID != 0)
            {
                ProjectModel objProjectModel = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == ProjectID).FirstOrDefault();
                if (objProjectModel != null)
                {
                    Projectslcode = objProjectModel.Projectslcode;
                    if (refNo != "")
                    {
                        refNo = refNo + "/" + Projectslcode;
                    }
                    else
                    {
                        refNo = Projectslcode;
                    }
                }
            }

            TransactionHeadViewModelCode objTransactionHeadViewModelCode = new TransactionHeadViewModelCode();
            TransactionHeadModel objTransactionHeadModel = _objAccountingDbContext.AccountingTransactionHeadModel.Where(x => x.IsActive == true && x.TransactionInputTypeID == TransactionInputTypeID  && x.LegalEntityID == LegalEntityID  && x.ProfitCentreID == ProfitCentreID && x.LocationID == LocationID && x.UnitID == UnitID && x.ProjectID == ProjectID && x.LegalEntityslcode== LegalEntityslcode && x.ProfitCentreslcode == ProfitCentreslcode && x.Locationslcode == Locationslcode && x.Unitslcode == Unitslcode && x.Projectslcode == Projectslcode).OrderByDescending(x => x.SlNo).FirstOrDefault();
            int SlNo = 1;
            if (objTransactionHeadModel != null)
            {
                if (objTransactionHeadModel.SlNo > 0)
                {
                    SlNo = objTransactionHeadModel.SlNo + 1;
                }
            }

            if (refNo != "")
            {
                refNo = refNo + "/" + SlNo.ToString();
            }
            else
            {
                refNo = SlNo.ToString();
            }
            if (ShortRefNo != "")
            {
                ShortRefNo = ShortRefNo + "/../" + SlNo.ToString();
            }
            else
            {
                ShortRefNo = SlNo.ToString();
            }
            string financialcode = "";
            if (FinancialYearID != 0)
            {
                FinancialYearModel objFinancialYearModel = _objAccountingDbContext.AccountingFinancialYearModel.Where(x => x.FinancialYearID == FinancialYearID).FirstOrDefault();
                if (objFinancialYearModel != null)
                {
                    financialcode = objFinancialYearModel.StartDate.Year.ToString().Substring(2, 2) + "-" + objFinancialYearModel.EndDate.Year.ToString().Substring(2, 2);
                    if (refNo != "")
                    {
                        refNo = refNo + "/" + financialcode;
                    }
                    else
                    {
                        refNo = financialcode;
                    }
                    if (ShortRefNo != "")
                    {
                        ShortRefNo = ShortRefNo + "/" + financialcode;
                    }
                    else
                    {
                        ShortRefNo = financialcode;
                    }
                }
            }

            objTransactionHeadViewModelCode.TransactionInputTypeslcode = TransactionInputTypeslcode;
            objTransactionHeadViewModelCode.LegalEntityslcode = LegalEntityslcode;
            objTransactionHeadViewModelCode.ProfitCentreslcode = ProfitCentreslcode;
            objTransactionHeadViewModelCode.Locationslcode = Locationslcode;
            objTransactionHeadViewModelCode.Unitslcode = Unitslcode;
            objTransactionHeadViewModelCode.Projectslcode = Projectslcode;
            objTransactionHeadViewModelCode.TransactionRefNo = refNo;
            objTransactionHeadViewModelCode.TransactionShortRefNo = ShortRefNo;
          
            
            objTransactionHeadViewModelCode.SlNo = SlNo;
            
            return objTransactionHeadViewModelCode;
        }
        public List<TransactionHeadViewModelList> GetAllTransactionHeadViewModelList(int TransactionInputTypeID,int LegalEntityID,int ProfitCentreID,int LocationID,int UnitID,int ProjectID)
        {

            var objGetAllTransactionHeadViewModelList = (from objTransactionHeadModel in _objAccountingDbContext.AccountingTransactionHeadModel
                                                         //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objTransactionHeadModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                         //join objAccountingProfitCentreModel in _objAccountingDbContext.AccountingProfitCentreModel on objTransactionHeadModel.ProfitCentreID equals objAccountingProfitCentreModel.ProfitCentreID
                                                         //join objAccountingLocationModel in _objAccountingDbContext.AccountingLocationModel on objTransactionHeadModel.LocationID equals objAccountingLocationModel.LocationID
                                                         //join objAccountingUnitModel in _objAccountingDbContext.AccountingUnitModel on objTransactionHeadModel.UnitID equals objAccountingUnitModel.UnitID
                                                         //join objAccountingProjectModel in _objAccountingDbContext.AccountingProjectModel on objTransactionHeadModel.ProjectID equals objAccountingProjectModel.ProjectID
                                                         where objTransactionHeadModel.IsActive == true && objTransactionHeadModel.TransactionStatus == 1 && objTransactionHeadModel.TransactionInputTypeID == TransactionInputTypeID && objTransactionHeadModel.LegalEntityID == LegalEntityID && objTransactionHeadModel.ProfitCentreID == ProfitCentreID && objTransactionHeadModel.LocationID == LocationID && objTransactionHeadModel.UnitID == UnitID && objTransactionHeadModel.ProjectID == ProjectID 
                                                         select new TransactionHeadViewModelList
                                                         {
                                                             TransactionHeadID = objTransactionHeadModel.TransactionHeadID,
                                                             TransactionDate = objTransactionHeadModel.TransactionDate,
                                                             TransactionRefNo = objTransactionHeadModel.TransactionRefNo,
                                                             TransactionShortRefNo = objTransactionHeadModel.TransactionShortRefNo,
                                                             FinancialYearID = objTransactionHeadModel.FinancialYearID,
                                                             TransactionInputTypeID = objTransactionHeadModel.TransactionInputTypeID,
                                                             CorporateGroupID = objTransactionHeadModel.CorporateGroupID,
                                                             BusinessGroupID = objTransactionHeadModel.BusinessGroupID,
                                                             VerticalGroupID = objTransactionHeadModel.VerticalGroupID,
                                                             LegalEntityID = objTransactionHeadModel.LegalEntityID,
                                                             LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objTransactionHeadModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                             ProfitCentreID = objTransactionHeadModel.ProfitCentreID,
                                                             ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objTransactionHeadModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                             LocationID = objTransactionHeadModel.LocationID,
                                                             LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objTransactionHeadModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                             UnitID = objTransactionHeadModel.UnitID,
                                                             UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objTransactionHeadModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                             ProjectID = objTransactionHeadModel.ProjectID,
                                                             ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objTransactionHeadModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                             TransactionTotal = objTransactionHeadModel.TransactionTotal,
                                                             DescriptionHead = objTransactionHeadModel.DescriptionHead
                                                         }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.LocationName).ToList();
                return objGetAllTransactionHeadViewModelList;
           
        }
        public TransactionHeadViewModelList GetAllTransactionHeadViewModelListByTransactionHeadID(int TransactionHeadID)
        {

            var objGetAllTransactionHeadViewModelList = (from objTransactionHeadModel in _objAccountingDbContext.AccountingTransactionHeadModel
                                                             //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objTransactionHeadModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                             //join objAccountingProfitCentreModel in _objAccountingDbContext.AccountingProfitCentreModel on objTransactionHeadModel.ProfitCentreID equals objAccountingProfitCentreModel.ProfitCentreID
                                                             //join objAccountingLocationModel in _objAccountingDbContext.AccountingLocationModel on objTransactionHeadModel.LocationID equals objAccountingLocationModel.LocationID
                                                             //join objAccountingUnitModel in _objAccountingDbContext.AccountingUnitModel on objTransactionHeadModel.UnitID equals objAccountingUnitModel.UnitID
                                                             //join objAccountingProjectModel in _objAccountingDbContext.AccountingProjectModel on objTransactionHeadModel.ProjectID equals objAccountingProjectModel.ProjectID
                                                         where objTransactionHeadModel.IsActive == true && objTransactionHeadModel.TransactionStatus == 1 && objTransactionHeadModel.TransactionHeadID == TransactionHeadID
                                                         select new TransactionHeadViewModelList
                                                         {
                                                             TransactionHeadID = objTransactionHeadModel.TransactionHeadID,
                                                             TransactionDate = objTransactionHeadModel.TransactionDate,
                                                             TransactionRefNo = objTransactionHeadModel.TransactionRefNo,
                                                             TransactionShortRefNo = objTransactionHeadModel.TransactionShortRefNo,
                                                             FinancialYearID = objTransactionHeadModel.FinancialYearID,
                                                             TransactionInputTypeID = objTransactionHeadModel.TransactionInputTypeID,
                                                             CorporateGroupID = objTransactionHeadModel.CorporateGroupID,
                                                             BusinessGroupID = objTransactionHeadModel.BusinessGroupID,
                                                             VerticalGroupID = objTransactionHeadModel.VerticalGroupID,
                                                             LegalEntityID = objTransactionHeadModel.LegalEntityID,
                                                             LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objTransactionHeadModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                             ProfitCentreID = objTransactionHeadModel.ProfitCentreID,
                                                             ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objTransactionHeadModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                             LocationID = objTransactionHeadModel.LocationID,
                                                             LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objTransactionHeadModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName,
                                                             UnitID = objTransactionHeadModel.UnitID,
                                                             UnitName = _objAccountingDbContext.AccountingUnitModel.Where(x => x.UnitID == objTransactionHeadModel.UnitID && x.IsActive == true).FirstOrDefault().UnitName,
                                                             ProjectID = objTransactionHeadModel.ProjectID,
                                                             ProjectName = _objAccountingDbContext.AccountingProjectModel.Where(x => x.ProjectID == objTransactionHeadModel.ProjectID && x.IsActive == true).FirstOrDefault().ProjectName,
                                                             TransactionTotal = objTransactionHeadModel.TransactionTotal,
                                                             DescriptionHead = objTransactionHeadModel.DescriptionHead
                                                         }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.LocationName).FirstOrDefault();
            return objGetAllTransactionHeadViewModelList;

        }

        //public int SaveTransactionHeadToLms(TransactionHeadModel objTransactionHeadModel, int UserID)
        //{
        //    return view();
        //}
        //public int SaveTransactionHead(TransactionHeadModel objTransactionHeadModel, int UserID)
        //{
        //    int TransactionHeadID = 0;
        //    if (objTransactionHeadModel.TransactionHeadID == 0)
        //    {
        //        objTransactionHeadModel.IsActive = true;
        //        objTransactionHeadModel.CreatedBy = UserID;
        //        objTransactionHeadModel.CreatedOn = DateTime.Now;
        //        _objAccountingDbContext.AccountingTransactionHeadModel.Add(objTransactionHeadModel);
        //    }
        //    else
        //    {
        //       // objTransactionHeadModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objTransactionHeadModel.CreatedOn));
        //        objTransactionHeadModel.LastModifiedBy = UserID;
        //        objTransactionHeadModel.LastModifiedon = DateTime.Now;
        //        _objAccountingDbContext.AccountingTransactionHeadModel.Update(objTransactionHeadModel);
        //    }
        //    _objAccountingDbContext.SaveChanges();
        //    TransactionHeadID = objTransactionHeadModel.TransactionHeadID;
        //    return TransactionHeadID;
        //}
    }
}
