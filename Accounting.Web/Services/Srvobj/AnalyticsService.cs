﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IAnalyticsService
    {
        List<AnalyticsModel> GetAllAnalytics();
        List<AnalyticsModel> GetAllAnalyticsByLegalEntityID(int LegalEntityID);
        void SaveAnalytics(AnalyticsModel objAnalyticsModel, int UserID);
        AnalyticsModel GetAnalyticsByID(int AnalyticsID);
        int DeleteAnalytics(int AnalyticsID, int UserID);
    }
    public class AnalyticsService : IAnalyticsService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public AnalyticsService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteAnalytics(int AnalyticsID, int UserID)
        {
            AnalyticsModel objAnalyticsModel = _objAccountingDbContext.AccountingAnalyticsModel.Where(x => x.AnalyticsID == AnalyticsID).FirstOrDefault();
            objAnalyticsModel.IsActive = false;
            _objAccountingDbContext.AccountingAnalyticsModel.Update(objAnalyticsModel);
            _objAccountingDbContext.SaveChanges();
            return objAnalyticsModel.AnalyticsID;
        }

        public List<AnalyticsModel> GetAllAnalyticsByLegalEntityID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingAnalyticsModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        }
        public List<AnalyticsModel> GetAllAnalytics()
        {
            return _objAccountingDbContext.AccountingAnalyticsModel.Where(x => x.IsActive == true).ToList();
        }
        public AnalyticsModel GetAnalyticsByID(int AnalyticsID)
        {
            return _objAccountingDbContext.AccountingAnalyticsModel.Where(x => x.AnalyticsID == AnalyticsID && x.IsActive == true).FirstOrDefault();
        }

        public void SaveAnalytics(AnalyticsModel objAnalyticsModel, int UserID)
        {
            if (objAnalyticsModel.AnalyticsID == 0)
            {
                objAnalyticsModel.IsActive = true;
                objAnalyticsModel.CreatedBy = UserID;
                objAnalyticsModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingAnalyticsModel.Add(objAnalyticsModel);
            }
            else
            {
               // objAnalyticsModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objAnalyticsModel.CreatedOn));
                objAnalyticsModel.LastModifiedBy = UserID;
                objAnalyticsModel.LastModifiedon = DateTime.Now;
                _objAccountingDbContext.AccountingAnalyticsModel.Update(objAnalyticsModel);
            }
            _objAccountingDbContext.SaveChanges();
        }
    }
}
