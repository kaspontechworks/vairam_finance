﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IProductManagementService
    {
        List<ProductManagementModel> GetAllProduct();
        List<CaseManagementModel> GetAllCase();
        List<ProductManagement_TatDefination> GetAllProTatDegByProID( int _intID);
        
        ProductManagementModel GetProductByID(int _intID);
        void SaveProduct(ProductManagemntWithTatDefination objData, int intUserID);

        void DeleteProduct(int _intID, int _UserID);

    }
    public class ProductManagementService : IProductManagementService
    {
        private readonly CDbcontext _objcertaDbContext;
        public ProductManagementService(CDbcontext objcertaDbContext) { _objcertaDbContext = objcertaDbContext; }

        public List<ProductManagementModel> GetAllProduct()
        {
            return _objcertaDbContext.AccountingProductManagement.Where(x => x.IsActive == true).ToList();
        }
        public List<CaseManagementModel> GetAllCase()
        {
            return _objcertaDbContext.AccountingCaseManagement.Where(x => x.IsActive == true).ToList();
        }
        public List<ProductManagement_TatDefination> GetAllProTatDegByProID(int _intID)
        {
            return _objcertaDbContext.AccountingProductMagtTatDef.Where(x => x.ProductID == _intID && x.IsActive == true).ToList();
        }
        public ProductManagementModel GetProductByID(int _intID)
        {
            return _objcertaDbContext.AccountingProductManagement.Where(x => x.IsActive == true && x.ProductID == _intID).FirstOrDefault();
        }
        public void SaveProduct(ProductManagemntWithTatDefination objData, int intUserID)
        {
            ProductManagementModel _objProductModel = _objcertaDbContext.AccountingProductManagement.Where(x => x.ProductID == Convert.ToInt32(objData.ProductID)).FirstOrDefault();
            if (_objProductModel == null)
            {
                _objProductModel = new ProductManagementModel
                {
                    ProductName = objData.ProductName,
                    ProductUniqueID = objData.ProductUniqueID,
                    CreatedOn = DateTime.Now,
                    CreatedBy = intUserID,
                    IsActive = true,
                    ProductID = 0
                };
                _objcertaDbContext.AccountingProductManagement.Add(_objProductModel);
            }
            else
            {
                _objProductModel.LastModifiedBy = intUserID;
                _objProductModel.LastModifiedOn = DateTime.Now;
                _objProductModel.ProductName = objData.ProductName;
                _objProductModel.ProductUniqueID = objData.ProductUniqueID;
                _objcertaDbContext.AccountingProductManagement.Update(_objProductModel);
            }
            _objcertaDbContext.SaveChanges();

            int _intProductId = _objcertaDbContext.AccountingProductManagement.Where(x => x.ProductName == objData.ProductName
                && x.ProductUniqueID == objData.ProductUniqueID).Select(x => x.ProductID).FirstOrDefault();

            List<ProductManagement_TatDefination> _ltProdTatDef = _objcertaDbContext.AccountingProductMagtTatDef.Where(x => x.ProductID == _intProductId).ToList();
            _objcertaDbContext.AccountingProductMagtTatDef.RemoveRange(_ltProdTatDef);
            _objcertaDbContext.SaveChanges();

            _ltProdTatDef = (from t in objData.objtatDefinations
                             select new ProductManagement_TatDefination
                             {
                                 CaseManagmentID = Convert.ToInt32(t.CaseID),
                                 CreatedBy = intUserID,
                                 CreatedOn = DateTime.Now,
                                 IsActive = true,
                                 Duration = Convert.ToInt32(t.Duration),
                                 IsDaysHours = Convert.ToInt32(t.isDaysorhrs) == 1 ? true : false,
                                 ProductID = _intProductId,
                                 TATDefinationID = 0
                             }).ToList();
            _objcertaDbContext.AccountingProductMagtTatDef.AddRange(_ltProdTatDef);
            _objcertaDbContext.SaveChanges();
        }

        public void DeleteProduct(int _intID, int _UserID)
        {
            ProductManagementModel objData = _objcertaDbContext.AccountingProductManagement.Where(x => x.ProductID == _intID).FirstOrDefault();
            objData.IsActive = false;
            _objcertaDbContext.AccountingProductManagement.Update(objData);
            _objcertaDbContext.SaveChanges();
        }
    }
}
