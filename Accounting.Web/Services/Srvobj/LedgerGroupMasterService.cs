﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ILedgerGroupMasterService
    {
        List<LedgerGroupMasterModel> GetAllLedgerGroupMaster();
        List<GroupTypeModel> GetAllGroupTypeModel();
        List<LedgerGroupMasterModel> GetAllLedgerGroupMasterByParentCategoryID(int ParentLedgerGroupID);
        List<LedgerGroupMasterModel> GetAllLedgerGroupMasterByGroupType(int GroupTypeID);
        List<LedgerGroupMasterModel> GetAllLedgerGroupMasterByGroupTypeParentCategoryID(int GroupTypeID, int ParentLedgerGroupID);
        void SaveLedgerGroupMaster(LedgerGroupMasterModel objLedgerGroupMasterModel, int UserID);
        LedgerGroupMasterModel GetLedgerGroupMasterByID(int LedgerGroupID);
        int DeleteLedgerGroupMaster(int LedgerGroupID, int UserID);
        List<LedgerGroupMasterViewModelList> GetAllLedgerGroupMasterViewModelList();
    }
    public class LedgerGroupMasterService : ILedgerGroupMasterService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public LedgerGroupMasterService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteLedgerGroupMaster(int LedgerGroupID, int UserID)
        {
            LedgerGroupMasterModel objLedgerGroupMasterModel = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == LedgerGroupID).FirstOrDefault();
            objLedgerGroupMasterModel.IsActive = false;
            _objAccountingDbContext.AccountingLedgerGroupMasterModel.Update(objLedgerGroupMasterModel);
            _objAccountingDbContext.SaveChanges();
            return objLedgerGroupMasterModel.LedgerGroupID;
        }

        public List<LedgerGroupMasterModel> GetAllLedgerGroupMasterByParentCategoryID(int ParentLedgerGroupID)
        {
            return _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.IsActive == true && x.ParentLedgerGroupID == ParentLedgerGroupID).ToList();
        }
        public List<LedgerGroupMasterModel> GetAllLedgerGroupMasterByGroupType(int GroupTypeID)
        {
            return _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.IsActive == true && x.GroupTypeID == GroupTypeID).ToList();
        }
        public List<LedgerGroupMasterModel> GetAllLedgerGroupMasterByGroupTypeParentCategoryID(int GroupTypeID,int ParentLedgerGroupID)
        {
            return _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.IsActive == true && x.GroupTypeID == GroupTypeID && x.ParentLedgerGroupID == ParentLedgerGroupID).ToList();
        }
        
        public List<LedgerGroupMasterModel> GetAllLedgerGroupMaster()
        {
            return _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.IsActive == true).ToList();
        }
        public List<GroupTypeModel> GetAllGroupTypeModel()
        {
            return _objAccountingDbContext.AccountingGroupTypeModel.Where(x => x.IsActive == true).ToList();
        }
        public LedgerGroupMasterModel GetLedgerGroupMasterByID(int LedgerGroupID)
        {
            return _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == LedgerGroupID && x.IsActive == true).FirstOrDefault();
        }
        public List<LedgerGroupMasterViewModelList> GetAllLedgerGroupMasterViewModelList()
        {
            var objLedgerGroupMasterViewModelList = (from objLedgerGroupMasterModel in _objAccountingDbContext.AccountingLedgerGroupMasterModel
                                                  where objLedgerGroupMasterModel.IsActive == true
                                                  select new LedgerGroupMasterViewModelList
                                                  {

                                                      LedgerGroupID = objLedgerGroupMasterModel.LedgerGroupID,
                                                      LedgerGroupName = objLedgerGroupMasterModel.LedgerGroupName,
                                                      ParentLedgerGroupID = objLedgerGroupMasterModel.ParentLedgerGroupID,
                                                      ParentLedgerGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objLedgerGroupMasterModel.ParentLedgerGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                      GroupTypeID = objLedgerGroupMasterModel.GroupTypeID,
                                                      GroupTypeName = _objAccountingDbContext.AccountingGroupTypeModel.Where(x => x.GroupTypeID == objLedgerGroupMasterModel.GroupTypeID && x.IsActive == true).FirstOrDefault().GroupTypeName,
                                                  }).OrderBy(x => x.GroupTypeName).ThenBy(x => x.LedgerGroupName).ThenBy(x => x.ParentLedgerGroupName).ToList();
            return objLedgerGroupMasterViewModelList;
        }
        public void SaveLedgerGroupMaster(LedgerGroupMasterModel objLedgerGroupMasterModel, int UserID)
        {
            if (objLedgerGroupMasterModel.LedgerGroupID == 0)
            {
                objLedgerGroupMasterModel.IsActive = true;
                objLedgerGroupMasterModel.CreatedBy = UserID;
                objLedgerGroupMasterModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingLedgerGroupMasterModel.Add(objLedgerGroupMasterModel);
            }
            else
            {
               // objLedgerGroupMasterModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objLedgerGroupMasterModel.CreatedOn));
                objLedgerGroupMasterModel.LastModifiedBy = UserID;
                objLedgerGroupMasterModel.LastModifiedon = DateTime.Now;
                _objAccountingDbContext.AccountingLedgerGroupMasterModel.Update(objLedgerGroupMasterModel);
            }
            _objAccountingDbContext.SaveChanges();
        }
    }
}
