﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ICategoryMasterService
    {
        List<CategoryMasterModel> GetAllCategoryMaster();
        List<CategoryMasterModel> GetAllCategoryMasterByParentCategoryID(int ParentCategoryID);
        void SaveCategoryMaster(CategoryMasterModel objCategoryMasterModel, int UserID);
        CategoryMasterModel GetCategoryMasterByID(int CategoryID);
        int DeleteCategoryMaster(int CategoryID, int UserID);

        List<CategoryMasterViewModelList> GetAllCategoryMasterViewModelList();
    }
    public class CategoryMasterService : ICategoryMasterService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public CategoryMasterService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteCategoryMaster(int CategoryID, int UserID)
        {
            CategoryMasterModel objCategoryMasterModel = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == CategoryID).FirstOrDefault();
            objCategoryMasterModel.IsActive = false;
            _objAccountingDbContext.AccountingCategoryMasterModel.Update(objCategoryMasterModel);
            _objAccountingDbContext.SaveChanges();
            return objCategoryMasterModel.CategoryID;
        }

        public List<CategoryMasterModel> GetAllCategoryMasterByParentCategoryID(int ParentCategoryID)
        {
            return _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.IsActive == true && x.ParentCategoryID == ParentCategoryID).ToList();
        }
        public List<CategoryMasterModel> GetAllCategoryMaster()
        {
            return _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.IsActive == true).ToList();
        }
        public List<CategoryMasterViewModelList> GetAllCategoryMasterViewModelList()
        {
            var objCategoryMasterViewModelList = (from objCategoryMasterModel in _objAccountingDbContext.AccountingCategoryMasterModel
                                                  where objCategoryMasterModel.IsActive == true
                                                  select new CategoryMasterViewModelList
                                                  {
                                                      CategoryID = objCategoryMasterModel.CategoryID,
                                                      CategoryName = objCategoryMasterModel.CategoryName,
                                                      ParentCategoryID = objCategoryMasterModel.ParentCategoryID,
                                                      ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objCategoryMasterModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                  }).OrderBy(x => x.ParentCategoryName).ThenBy(x => x.CategoryName).ToList();
            return objCategoryMasterViewModelList;
           
        }
        public CategoryMasterModel GetCategoryMasterByID(int CategoryID)
        {
            return _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == CategoryID && x.IsActive == true).FirstOrDefault();
        }

        public void SaveCategoryMaster(CategoryMasterModel objCategoryMasterModel, int UserID)
        {
            if (objCategoryMasterModel.CategoryID == 0)
            {
                objCategoryMasterModel.IsActive = true;
                objCategoryMasterModel.CreatedBy = UserID;
                objCategoryMasterModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingCategoryMasterModel.Add(objCategoryMasterModel);
            }
            else
            {
               // objCategoryMasterModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objCategoryMasterModel.CreatedOn));
                objCategoryMasterModel.LastModifiedBy = UserID;
                objCategoryMasterModel.LastModifiedon = DateTime.Now;
                _objAccountingDbContext.AccountingCategoryMasterModel.Update(objCategoryMasterModel);
            }
            _objAccountingDbContext.SaveChanges();
        }
    }
}
