﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface ILegalEntityLocationMappingService
    {
        List<LegalEntityLocationMappingModel> GetAllLegalEntityLocationMapping();
        List<LegalEntityLocationMappingModel> GetAllLegalEntityLocationMappingByLegalEntityID(int LegalEntityID);
        List<LegalEntityLocationMappingModel> GetAllLegalEntityLocationMappingByLegalEntityIDProfitCentreID(int EntityID, int ProfitCentreID);
        //void SaveLegalEntityLocationMapping(LegalEntityLocationMappingModel objLegalEntityLocationMappingModel, int UserID);
        void SaveLegalEntityLocationMapping(LegalEntityLocationDefinations LegalEntityLocationDefinations, int UserID);
        LegalEntityLocationMappingModel GetLegalEntityLocationMappingByID(int CategoryID);
        int DeleteLegalEntityLocationMapping(int CategoryID, int UserID);

        List<LegalEntityLocationMappingViewModelList> GetAllLegalEntityLocationMappingViewModelList(int LegalEntityID);
        List<LegalEntityLocationMappingViewModelList> GetAllLegalEntityLocationMappingViewModelListByLegalEntityIDProfitCentreID(int LegalEntityID, int ProfitCentreID);
    }
    public class LegalEntityLocationMappingService : ILegalEntityLocationMappingService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public LegalEntityLocationMappingService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteLegalEntityLocationMapping(int LegalEntityID, int UserID)
        {
            LegalEntityLocationMappingModel objLegalEntityLocationMappingModel = _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Where(x => x.LegalEntityID == LegalEntityID).FirstOrDefault();
            objLegalEntityLocationMappingModel.IsActive = false;
            _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Update(objLegalEntityLocationMappingModel);
            _objAccountingDbContext.SaveChanges();
            return objLegalEntityLocationMappingModel.LegalEntityID;
        }

        public List<LegalEntityLocationMappingModel> GetAllLegalEntityLocationMappingByLegalEntityID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        }
        public List<LegalEntityLocationMappingModel> GetAllLegalEntityLocationMappingByLegalEntityIDProfitCentreID(int LegalEntityID, int ProfitCentreID)
        {
            return _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID && x.ProfitCentreID == ProfitCentreID).ToList();
        }
        public List<LegalEntityLocationMappingModel> GetAllLegalEntityLocationMapping()
        {
            return _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Where(x => x.IsActive == true).ToList();
        }
        public LegalEntityLocationMappingModel GetLegalEntityLocationMappingByID(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Where(x => x.LegalEntityID == LegalEntityID && x.IsActive == true).FirstOrDefault();
        }
        public void SaveLegalEntityLocationMapping(LegalEntityLocationDefinations LegalEntityLocationDefinations, int UserID)
        {
            int LegalEntityid = LegalEntityLocationDefinations.LegalEntityID;
            int ProfitCentreID = LegalEntityLocationDefinations.ProfitCentreID;
            if (LegalEntityid!=0)
            {

                List<LegalEntityLocationMappingModel> _Listdef = _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Where(x => x.LegalEntityID == LegalEntityid && x.ProfitCentreID == ProfitCentreID).ToList();
                _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.RemoveRange(_Listdef);
                _objAccountingDbContext.SaveChanges();

                _Listdef = (from t in LegalEntityLocationDefinations.LocationList
                                 select new LegalEntityLocationMappingModel
                                 {
                                     LocationID = t.LocationID,
                                     CreatedBy = UserID,
                                     CreatedOn = DateTime.Now,
                                     IsActive = true,
                                     ProfitCentreID = ProfitCentreID,
                                     LegalEntityID = LegalEntityid
                                 }).ToList();
                _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.AddRange(_Listdef);
                _objAccountingDbContext.SaveChanges();

            }

        }

        public List<LegalEntityLocationMappingViewModelList> GetAllLegalEntityLocationMappingViewModelList(int LegalEntityID)
        {
            if (LegalEntityID != 0)
            {
                var objLegalEntityLocationMappingViewModelList = (from objAccountingLegalEntityLocationMappingModel in _objAccountingDbContext.AccountingLegalEntityLocationMappingModel
                                                 // join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityLocationMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                 // join objAccountingLocationModel in _objAccountingDbContext.AccountingLocationModel on objAccountingLegalEntityLocationMappingModel.LocationID equals objAccountingLocationModel.LocationID
                                                  where objAccountingLegalEntityLocationMappingModel.LegalEntityID == LegalEntityID
                                                  select new LegalEntityLocationMappingViewModelList
                                                  {
                                                      LegalEntityID = objAccountingLegalEntityLocationMappingModel.LegalEntityID,
                                                      LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityLocationMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                      ProfitCentreID= objAccountingLegalEntityLocationMappingModel.ProfitCentreID,
                                                      ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityLocationMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                      LocationID = objAccountingLegalEntityLocationMappingModel.LocationID,
                                                      LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityLocationMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName
                                                  }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ToList();
                return objLegalEntityLocationMappingViewModelList;
            }
            else
            {
                var objLegalEntityLocationMappingViewModelList = (from objAccountingLegalEntityLocationMappingModel in _objAccountingDbContext.AccountingLegalEntityLocationMappingModel
                                                  //join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityLocationMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                  //join objAccountingLocationModel in _objAccountingDbContext.AccountingLocationModel on objAccountingLegalEntityLocationMappingModel.LocationID equals objAccountingLocationModel.LocationID
                                                  //where objAccountingLegalEntityLocationMappingModel.LegalEntityID == LegalEntityID
                                                  select new LegalEntityLocationMappingViewModelList
                                                  {
                                                      LegalEntityID = objAccountingLegalEntityLocationMappingModel.LegalEntityID,
                                                      LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityLocationMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                      ProfitCentreID = objAccountingLegalEntityLocationMappingModel.ProfitCentreID,
                                                      ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityLocationMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                      LocationID = objAccountingLegalEntityLocationMappingModel.LocationID,
                                                      LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityLocationMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName
                                                  }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ToList();
                
                return objLegalEntityLocationMappingViewModelList;
            }
        }
        public List<LegalEntityLocationMappingViewModelList> GetAllLegalEntityLocationMappingViewModelListByLegalEntityIDProfitCentreID(int LegalEntityID,int ProfitCentreID)
        {
           
                var objLegalEntityLocationMappingViewModelList = (from objAccountingLegalEntityLocationMappingModel in _objAccountingDbContext.AccountingLegalEntityLocationMappingModel
                                                                      // join objAccountingEntityMasterModel in _objAccountingDbContext.AccountingEntityMasterModel on objAccountingLegalEntityLocationMappingModel.LegalEntityID equals objAccountingEntityMasterModel.EntityID
                                                                      // join objAccountingLocationModel in _objAccountingDbContext.AccountingLocationModel on objAccountingLegalEntityLocationMappingModel.LocationID equals objAccountingLocationModel.LocationID
                                                                  where objAccountingLegalEntityLocationMappingModel.LegalEntityID == LegalEntityID && objAccountingLegalEntityLocationMappingModel.ProfitCentreID == ProfitCentreID
                                                                  select new LegalEntityLocationMappingViewModelList
                                                                  {
                                                                      LegalEntityID = objAccountingLegalEntityLocationMappingModel.LegalEntityID,
                                                                      LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objAccountingLegalEntityLocationMappingModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,//objAccountingEntityMasterModel.Name,
                                                                      ProfitCentreID = objAccountingLegalEntityLocationMappingModel.ProfitCentreID,
                                                                      ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingLegalEntityLocationMappingModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                                      LocationID = objAccountingLegalEntityLocationMappingModel.LocationID,
                                                                      LocationName = _objAccountingDbContext.AccountingLocationModel.Where(x => x.LocationID == objAccountingLegalEntityLocationMappingModel.LocationID && x.IsActive == true).FirstOrDefault().LocationName
                                                                  }).OrderBy(x => x.LegalEntityName).ThenBy(x => x.ProfitCentreName).ThenBy(x => x.LocationName).ToList();
                return objLegalEntityLocationMappingViewModelList;
           
            
        }
        public void SaveLegalEntityLocationMapping(LegalEntityLocationMappingModel objLegalEntityLocationMappingModel, int UserID)
        {
            //if (objLegalEntityLocationMappingModel.CategoryID == 0)
            //{
            //    objLegalEntityLocationMappingModel.IsActive = true;
            //    objLegalEntityLocationMappingModel.CreatedBy = UserID;
            //    objLegalEntityLocationMappingModel.CreatedOn = DateTime.Now;
            //    _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Add(objLegalEntityLocationMappingModel);
            //}
            //else
            //{
            //   // objLegalEntityLocationMappingModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objLegalEntityLocationMappingModel.CreatedOn));
            //    objLegalEntityLocationMappingModel.LastModifiedBy = UserID;
            //    objLegalEntityLocationMappingModel.LastModifiedon = DateTime.Now;
            //    _objAccountingDbContext.AccountingLegalEntityLocationMappingModel.Update(objLegalEntityLocationMappingModel);
            //}
            _objAccountingDbContext.SaveChanges();
        }
    }
}
