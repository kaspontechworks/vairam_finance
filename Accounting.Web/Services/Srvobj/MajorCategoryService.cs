﻿using Accounting.Web.Common;
using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IMajorCategoryService
    {

        List<MajorCategoryModel> GetAllMajorCategory();
        MajorCategoryModel GetMajorCategoryById(int MajorCategoryID);

    }

    public class MajorCategoryService : IMajorCategoryService
    {
        private readonly CDbcontext _objAccountingDbContext;

        public MajorCategoryService(CDbcontext objCDbContext) { _objAccountingDbContext = objCDbContext; }

       

        public MajorCategoryModel GetMajorCategoryById(int MajorCategoryID) { return _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == MajorCategoryID).FirstOrDefault(); }

        public List<MajorCategoryModel> GetAllMajorCategory()
        {
            return _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.IsActive == true).ToList();
        }

    }

}
