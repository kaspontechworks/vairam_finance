﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IEntityLevelService
    {
        List<EntityLevelModel> GetAllEntityLevel();
        EntityLevelModel GetEntityLevelByID(int ID);
        //void SaveCase(EntityLevelModel objEntityLevelModel,int _UserID);

        //void Deletecase(int ID, int _UserID);

    }
    public class EntityLevelService : IEntityLevelService
    {
        private readonly CDbcontext _objcertaDbContext;
        public EntityLevelService(CDbcontext objcertaDbContext) { _objcertaDbContext = objcertaDbContext; }

        public List<EntityLevelModel> GetAllEntityLevel()
        {
            return _objcertaDbContext.AccountingEntityLevel.Where(x => x.IsActive == true).ToList();
        }
        public EntityLevelModel GetEntityLevelByID(int ID)
        {
            return _objcertaDbContext.AccountingEntityLevel.Where(x => x.IsActive == true && x.EntityLevelID == ID).FirstOrDefault();
        }
        //public void SaveCase(EntityLevelModel objEntityLevelModel, int _UserID)
        //{

        //    if (objEntityLevelModel.CaseID == 0)
        //    {
        //        objEntityLevelModel.CreatedOn = DateTime.Now;
        //        objEntityLevelModel.IsActive = true;
        //        objEntityLevelModel.CreatedBy = _UserID;
        //        objEntityLevelModel.LastModifiedBy = 0;
        //        _objcertaDbContext.AccountingEntityLevel.Add(objEntityLevelModel);
        //    }
        //    else
        //    {
        //        objEntityLevelModel.LastModifiedOn = DateTime.Now;
        //        objEntityLevelModel.LastModifiedBy = _UserID;
        //        _objcertaDbContext.AccountingEntityLevel.Update(objEntityLevelModel);
        //    }
        //    _objcertaDbContext.SaveChanges();
        //}

        //public void Deletecase(int ID, int _UserID)
        //{
        //    EntityLevelModel objEntityLevelModel = _objcertaDbContext.AccountingEntityLevel.Where(x => x.CaseID == ID).FirstOrDefault();
        //    objEntityLevelModel.IsActive = false;
        //    _objcertaDbContext.AccountingEntityLevel.Update(objEntityLevelModel);
        //    _objcertaDbContext.SaveChanges();
        //}
    }
}
