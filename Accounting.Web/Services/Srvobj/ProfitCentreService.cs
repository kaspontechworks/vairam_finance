﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IProfitCentreService
    {
        List<ProfitCentreModel> GetAllProfitCentre();
        List<ProfitCentreModel> GetProfitCentreName(int ProfitCentreID);
        void SaveProfitCentre(ProfitCentreModel objProfitCentreModel, int UserID);
        ProfitCentreModel GetProfitCentreByID(int ProfitCentreID);
        ProfitCentreModel GetProfitCentreByProfitCentreslcode(string ProfitCentreslcode);
        int DeleteProfitCentre(int ProfitCentreID, int UserID);
    }
    public class ProfitCentreService : IProfitCentreService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public ProfitCentreService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteProfitCentre(int ProfitCentreID, int UserID)
        {
            ProfitCentreModel objProfitCentreModel = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == ProfitCentreID).FirstOrDefault();
            objProfitCentreModel.IsActive = false;
            _objAccountingDbContext.AccountingProfitCentreModel.Update(objProfitCentreModel);
            _objAccountingDbContext.SaveChanges();
            return objProfitCentreModel.ProfitCentreID;
        }
        public List<ProfitCentreModel> GetProfitCentreName(int ProfitCentreID)
        {
            var objTransactionEntryViewModelList = (from objAccountingTransactionEntryModel in _objAccountingDbContext.AccountingTransactionEntryModel
                                                    where objAccountingTransactionEntryModel.IsActive == true && objAccountingTransactionEntryModel.ProfitCentreID == ProfitCentreID 
                                                    select new ProfitCentreModel
                                                    {
                                                        ProfitCentreID = objAccountingTransactionEntryModel.ProfitCentreID,
                                                        ProfitCentreName = _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == objAccountingTransactionEntryModel.ProfitCentreID && x.IsActive == true).FirstOrDefault().ProfitCentreName,
                                                    }).ToList();

            return objTransactionEntryViewModelList;
        }
        //public List<ProfitCentreModel> GetAllProfitCentreByLegalEntityID(int LegalEntityID)
        //{
        //    return _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).ToList();
        //}
        public List<ProfitCentreModel> GetAllProfitCentre()
        {
            return _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.IsActive == true).ToList();
        }
        public ProfitCentreModel GetProfitCentreByID(int ProfitCentreID)
        {
            return _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreID == ProfitCentreID && x.IsActive == true).FirstOrDefault();
        }
        public ProfitCentreModel GetProfitCentreByProfitCentreslcode(string ProfitCentreslcode)
        {
            return _objAccountingDbContext.AccountingProfitCentreModel.Where(x => x.ProfitCentreslcode == ProfitCentreslcode && x.IsActive == true).FirstOrDefault();
        }
        
        public void SaveProfitCentre(ProfitCentreModel objProfitCentreModel, int UserID)
        {
            if (objProfitCentreModel.ProfitCentreID == 0)
            {
                objProfitCentreModel.IsActive = true;
                objProfitCentreModel.CreatedBy = UserID;
                objProfitCentreModel.CreatedOn = DateTime.Now;
                _objAccountingDbContext.AccountingProfitCentreModel.Add(objProfitCentreModel);
            }
            else
            {
               // objProfitCentreModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objProfitCentreModel.CreatedOn));
                objProfitCentreModel.LastModifiedBy = UserID;
                objProfitCentreModel.LastModifiedon = DateTime.Now;
                _objAccountingDbContext.AccountingProfitCentreModel.Update(objProfitCentreModel);
                //if (objProfitCentreModel.Pastdatestatus == 10)
                //{
                //    string strqry = "update HPMS_Config_Locations set Pastdate_status = 1 where LocationID=" + data.LocationID;
                //}
                //else
                //{
                //    string strqry = "update HPMS_Config_Locations set Pastdate_status = 0 where LocationID=" + data.LocationID;
                //}

            }
            _objAccountingDbContext.SaveChanges();
        }
    }
}
