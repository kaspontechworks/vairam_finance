﻿using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using Accounting.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IChartofAccountsService
    {
        List<ChartofAccountsModel> GetAllChartofAccounts();
        List<ChartofAccountsModel> GetAllAccounts();
        List<AccountTypeModel> GetAllAccountType();
        ChartofAccountsModel GetChartofAccountsByID(int AccountID);
        List<ChartofAccountsModel> GetAllChartofAccountsByLegalEntity(int LegalEntityID);
        List<ChartofAccountsModel> GetAllCashChartofAccountsByLegalEntity(int LegalEntityID);
        List<ChartofAccountsModel> GetAllBankChartofAccountsByLegalEntity(int LegalEntityID);
        int SaveChartofAccounts(ChartofAccountsModel objChartofAccountsModel, int UserID);
        int DeleteChartofAccounts(int AccountID, int UserID);
        int GetAccountIDByAccountName(string AccountName, int LegalEntityID);
        List<ChartofAccountsViewModelList> GetAllCategoryMasterViewModelList(int LegalEntityID);
        List<ChartofAccountsModel> GetAllChartofAccountsByGroupID(int GroupID);
    }
    public class ChartofAccountsService : IChartofAccountsService
    {
        private readonly CDbcontext _objAccountingDbContext;
        public ChartofAccountsService(CDbcontext objAccountingDbContext) { _objAccountingDbContext = objAccountingDbContext; }

        public int DeleteChartofAccounts(int AccountID, int UserID)
        {
            ChartofAccountsModel objChartofAccountsModel = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == AccountID).FirstOrDefault();
            objChartofAccountsModel.IsActive = false;
            _objAccountingDbContext.AccountingChartofAccountsModel.Update(objChartofAccountsModel);
            _objAccountingDbContext.SaveChanges();
            return objChartofAccountsModel.AccountID;
        }

        //public List<ChartofAccountsModel> GetAllChartofAccountsByParentCategoryID(int ParentAccountID)
        //{
        //    return _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.IsActive == true && x.ParentAccountID == ParentAccountID).ToList();
        //}
        //public List<ChartofAccountsModel> GetAllChartofAccountsByGroupType(int GroupTypeID)
        //{
        //    return _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.IsActive == true && x.GroupTypeID == GroupTypeID).ToList();
        //}
        public List<ChartofAccountsModel> GetAllAccounts()
        {
            return (from objAccounts in _objAccountingDbContext.AccountingChartofAccountsModel
                    where objAccounts.IsActive == true
                    select new ChartofAccountsModel
                    {
                        AccountID = objAccounts.AccountID,
                        AccountName = objAccounts.AccountName
                    }).ToList();
        }
        public List<ChartofAccountsModel> GetAllChartofAccounts()
        {
            return _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.IsActive == true).OrderBy(x => x.AccountName).ToList();
        }
        public List<ChartofAccountsModel> GetAllChartofAccountsByLegalEntity(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.IsActive == true && x.LegalEntityID == LegalEntityID).OrderBy(x => x.AccountName).ToList();
        }
        public List<ChartofAccountsModel> GetAllChartofAccountsByGroupID(int GroupID)
        {
            return _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.IsActive == true && (x.MajorGroupID == GroupID || x.GroupID == GroupID || x.SubGroupID == GroupID)).OrderBy(x => x.AccountName).ToList();
        }
        public List<ChartofAccountsModel> GetAllCashChartofAccountsByLegalEntity(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.IsActive == true && x.AccountTypeID == 2 && x.LegalEntityID == LegalEntityID).OrderBy(x => x.AccountName).ToList();
        }
        public List<ChartofAccountsModel> GetAllBankChartofAccountsByLegalEntity(int LegalEntityID)
        {
            return _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.IsActive == true && x.AccountTypeID == 1 && x.LegalEntityID == LegalEntityID).OrderBy(x => x.AccountName).ToList();
        }
        public List<AccountTypeModel> GetAllAccountType()
        {
            return _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.IsActive == true).ToList();
        }
        public ChartofAccountsModel GetChartofAccountsByID(int AccountID)
        {
            return _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID == AccountID && x.IsActive == true).FirstOrDefault();
        }
        public List<ChartofAccountsViewModelList> GetAllCategoryMasterViewModelList(int LegalEntityID)
        {
            var objChartofAccountsViewModelList = (from objChartofAccountsViewModel in _objAccountingDbContext.AccountingChartofAccountsModel
                                                   where objChartofAccountsViewModel.IsActive == true && objChartofAccountsViewModel.LegalEntityID == LegalEntityID
                                                   select new ChartofAccountsViewModelList
                                                   {
                                                       AccountID = objChartofAccountsViewModel.AccountID,
                                                       AccountName = objChartofAccountsViewModel.AccountName,
                                                       AccountCode = objChartofAccountsViewModel.AccountCode,
                                                       AccountTypeID = objChartofAccountsViewModel.AccountTypeID,
                                                       AccountTypeName = _objAccountingDbContext.AccountingAccountTypeModel.Where(x => x.AccountTypeID == objChartofAccountsViewModel.AccountTypeID && x.IsActive == true).FirstOrDefault().AccountTypeName,
                                                       LegalEntityID=objChartofAccountsViewModel.LegalEntityID,
                                                       LegalEntityName = _objAccountingDbContext.AccountingEntityMasterModel.Where(x => x.EntityID == objChartofAccountsViewModel.LegalEntityID && x.IsActive == true).FirstOrDefault().Name,
                                                       CategoryID = objChartofAccountsViewModel.CategoryID,
                                                       CategoryName = _objAccountingDbContext.AccountingCategoryMasterModel.Where(x => x.CategoryID == objChartofAccountsViewModel.CategoryID && x.IsActive == true).FirstOrDefault().CategoryName,
                                                       ParentCategoryID = objChartofAccountsViewModel.ParentCategoryID,
                                                       ParentCategoryName = _objAccountingDbContext.AccountingMajorCategoryModel.Where(x => x.MajorCategoryID == objChartofAccountsViewModel.ParentCategoryID && x.IsActive == true).FirstOrDefault().MajorCategoryName,
                                                       MajorGroupID = objChartofAccountsViewModel.MajorGroupID,
                                                       MajorGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objChartofAccountsViewModel.MajorGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                       GroupID = objChartofAccountsViewModel.GroupID,
                                                       GroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objChartofAccountsViewModel.GroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                       SubGroupID = objChartofAccountsViewModel.SubGroupID,
                                                       SubGroupName = _objAccountingDbContext.AccountingLedgerGroupMasterModel.Where(x => x.LedgerGroupID == objChartofAccountsViewModel.SubGroupID && x.IsActive == true).FirstOrDefault().LedgerGroupName,
                                                       IsCostcenter = objChartofAccountsViewModel.IsCostcenter,
                                                       IsBudgetary = objChartofAccountsViewModel.IsBudgetary,
                                                       IsAnalytics = objChartofAccountsViewModel.IsAnalytics,
                                                       IsStatutory = objChartofAccountsViewModel.IsStatutory,
                                                       IsParty = objChartofAccountsViewModel.IsParty,
                                                       Description = objChartofAccountsViewModel.Description,
                                                   }).OrderBy(x => x.ParentCategoryName).ThenBy(x => x.CategoryName).ToList();
            return objChartofAccountsViewModelList;
        }
        public int SaveChartofAccounts(ChartofAccountsModel objChartofAccountsModel, int UserID)
        {
            int AccountID = 0;
            if (_objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.AccountID != objChartofAccountsModel.AccountID && x.LegalEntityID == objChartofAccountsModel.LegalEntityID && x.AccountName == objChartofAccountsModel.AccountName && x.IsActive == true).ToList().Count == 0)
            {
                if (objChartofAccountsModel.AccountID == 0)
                {
                    objChartofAccountsModel.IsActive = true;
                    objChartofAccountsModel.CreatedBy = UserID;
                    objChartofAccountsModel.CreatedOn = DateTime.Now;
                    _objAccountingDbContext.AccountingChartofAccountsModel.Add(objChartofAccountsModel);
                }
                else
                {
                    // objChartofAccountsModel.CreatedOn = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", objChartofAccountsModel.CreatedOn));
                    objChartofAccountsModel.LastModifiedBy = UserID;
                    objChartofAccountsModel.LastModifiedon = DateTime.Now;
                    _objAccountingDbContext.AccountingChartofAccountsModel.Update(objChartofAccountsModel);
                }
                _objAccountingDbContext.SaveChanges();
                AccountID = objChartofAccountsModel.AccountID;
            }
            return AccountID;
        }
        public int GetAccountIDByAccountName(string AccountName, int LegalEntityID)
        {
            int AccountID = 0;
            AccountID = _objAccountingDbContext.AccountingChartofAccountsModel.Where(x => x.LegalEntityID == LegalEntityID && x.AccountName == AccountName && x.IsActive == true).Select(x=>x.AccountID).FirstOrDefault();
            return AccountID;
        }
    }
}
