﻿using Accounting.Web.Common;
using Accounting.Web.Models;
using Accounting.Web.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.Web.Services.Srvobj
{
    public interface IOpeningBalanceService
    {

        List<OpeningBalanceModel> GetAllOpeningBalance();
        OpeningBalanceModel GetOpeningBalanceById(int OpeningBalanceID);
        OpeningBalanceModel GetOpeningBalanceByAccountId(int AccountId, int LegalEntityID);
    }

    public class OpeningBalanceService : IOpeningBalanceService
    {
        private readonly CDbcontext _objAccountingDbContext;

        public OpeningBalanceService(CDbcontext objCDbContext) { _objAccountingDbContext = objCDbContext; }


        public OpeningBalanceModel GetOpeningBalanceById(int OpeningBalanceID) { return _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x => x.OpeningBalanceID == OpeningBalanceID).FirstOrDefault(); }
        public OpeningBalanceModel GetOpeningBalanceByAccountId(int AccountId,int LegalEntityID) { return _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x => x.AccountID == AccountId && x.LegalEntityID==LegalEntityID).FirstOrDefault(); }
        public List<OpeningBalanceModel> GetAllOpeningBalance()
        {
            return _objAccountingDbContext.AccountingOpeningBalanceModel.Where(x => x.IsActive == true).ToList();
        }
    }

}
